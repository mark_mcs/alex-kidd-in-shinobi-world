import std.algorithm.searching;
import std.conv;
import std.stdio;
import std.range;

T[] asSlice(T)(auto ref T buf)
{
    return (&buf)[0..1];    
}


int main(string[] args)
{
    auto file = new File(args[1], "rb");
    auto outfile = new File(args[2], "wb");
    auto streamCount = to!uint(args[3]);
    uint offset = 0;
    
    if (args.length > 4)
    {
        string offsetString = args[4];
        if (offsetString.startsWith("0x"))
        {
            offsetString = offsetString[2..$];
            offset = parse!uint(offsetString, 16);
        }
        else
        {
            offset = parse!uint(offsetString);
        }
    }
    
    if (offset > 0)
    {
        file.seek(offset);
    }
    
    auto buf = new ubyte[0x8000];
    
    ubyte flags;
    ubyte value;
    
    int outpointer;
    int limit;
    
    for (int i=0; i<streamCount; ++i) 
    {
        int savedPointer = outpointer;
        
        for (;;)
        {
            file.rawRead(asSlice(flags));
            if (flags == 0) 
            {
                break;
            }
            
            bool fill = (flags & 0x80) == 0;
            int count = flags & 0x7f;
            
            if (fill) 
            {
                file.rawRead(asSlice(value));
                for(int j=0; j<count; ++j) 
                {
                    buf[outpointer] = value;
                    limit = outpointer > limit ? outpointer : limit;
                    outpointer += streamCount;
                }
            }
            else
            {
                for (int j=0; j<count; ++j)
                {
                    file.rawRead(asSlice(value));
                    buf[outpointer] = value;
                    limit = outpointer > limit ? outpointer : limit;
                    outpointer += streamCount;
                }
            }
        }    
        outpointer = savedPointer + 1;
    }
    

    writefln("End of stream at 0x%08X", file.tell());
    outfile.rawWrite(buf[0..limit+1]);
    file.close();
    outfile.close();
    return 0;
}
