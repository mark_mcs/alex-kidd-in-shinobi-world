.def MAX_ENERGY     6

.def Object_State               0
.def Object_Flags01             1

.def Object_MaxAnimFrame        4
.def Object_AnimFrame           5
.def Object_FrameCountReset     6
.def Object_FrameCountDown      7

.def Object_FacingRight         9


; Bitfield at IX+34
.def OBJECT_F34_DISABLE_ANIM    1


.def Alex_State_Idle            1
.def Alex_State_Walk            2
.def Alex_State_Stopping        3
.def Alex_State_Jump            4
.def Alex_State_Sword           5

.def Alex_State_Duck            7

.def Alex_State_Fall            $0A

.def Alex_State_Hurt            $0C

.def Alex_State_Dead            $19

