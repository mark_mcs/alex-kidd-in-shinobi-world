.def Mode_LoadIntroSequence         $00
.def Mode_IntroSequence             $01

.def Mode_EnterPauseScreen          $03
.def Mode_InPauseScreen             $04
.def Mode_ExitPauseScreen           $05
.def Mode_Level                     $06

.def Mode_LoadTitleCard             $09
.def Mode_TitleCard                 $0A

.def Mode_LoadBossRoom              $0C

.def Mode_LoadGameOver              $11
.def Mode_GameOver                  $12
