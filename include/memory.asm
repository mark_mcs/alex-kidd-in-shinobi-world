
.enum $C001 export
SystemRegion db
.ende

; Copies of the VDP registers
.enum $C010 export
VDPRegister0            db
VDPRegister1            db
VDPRegister2            db
VDPRegister3            db
VDPRegister4            db
VDPRegister5            db
VDPRegister6            db
VDPRegister7            db
VDPRegister8            db
VDPRegister9            db
.ende

.enum $C01B export
InputFlags              db
InputFlagsChanged       db
InputFlags2             db
InputFlagsChanged2      db
.ende

.enum $C01F export
_RAM_C01F_ db
PaletteBuffer   dsb 32              ; $C020 working copy of the sprite palette
.ende

.enum $C02C export
_RAM_C02C_ db
_RAM_C02D_ db
_RAM_C02E_ db
_RAM_C02F_ db
_RAM_C030_ db
_RAM_C031_ dw
.ende

.enum $C03F export
_RAM_C03F_      db
GameMode        db          ; $C040
_RAM_C041_      db
PauseFlag       db          ; $C042
_RAM_C043_      dw
.ende

.enum $C047 export
WorkingSATPtr           dw          ; pointer to the current working SAT
WorkingSAT_HPosPtr      dw
SavedGameMode           db          ; $C04B
_RAM_C04C_              db
StageNumber             db          ; $C04D
_RAM_C04E_              db
_RAM_C04F_              db
_RAM_C050_              db
_RAM_C051_              db
_RAM_C052_              db
_RAM_C053_              db
.ende

.enum $C055 export
_RAM_C055_ db
_RAM_C056_ db
.ende

.enum $C05C export
_RAM_C05C_ db
_RAM_C05D_ dw
.ende

.enum $C061 export
_RAM_C061_ db
_RAM_C062_ db
_RAM_C063_ db
_RAM_C064_ db
_RAM_C065_ db
_RAM_C066_ db
_RAM_C067_ db
_RAM_C068_ db
.ende

.enum $C06A export
_RAM_C06A_ db
_RAM_C06B_ db
.ende

.enum $C071 export
_RAM_C071_ db           ; flag set when mappings need updating due to Y scroll
_RAM_C072_ dw           ; offset into screen map buffer
.ende

.enum $C076 export
_RAM_C076_ db
_RAM_C077_ db
_RAM_C078_ dw
_RAM_C07A_ dw
_RAM_C07C_ db
.ende

.enum $C080 export
_RAM_C080_ db
_RAM_C081_ db
_RAM_C082_ db
_RAM_C083_ dw
_RAM_C085_ db
_RAM_C086_ dw
_RAM_C088_ db
_RAM_C089_ db
_RAM_C08A_ dw
.ende

.enum $C08E export
_RAM_C08E_ db
_RAM_C08F_ db
_RAM_C090_ dw
_RAM_C092_ db
_RAM_C093_ db
_RAM_C094_ db
_RAM_C095_ db
_RAM_C096_ dw
_RAM_C098_ db
_RAM_C099_ db
_RAM_C09A_ db
_RAM_C09B_ db
_RAM_C09C_ db
_RAM_C09D_ db
_RAM_C09E_ db
_RAM_C09F_ db
AccelerationValue               dw      ; $C0A0
_RAM_C0A2_ db
_RAM_C0A3_ db
_RAM_C0A4_ db
_RAM_C0A5_ dw
_RAM_C0A7_ db
_RAM_C0A8_ dw
_RAM_C0AA_ db
.ende

.enum $C0AD export
_RAM_C0AD_ db
_RAM_C0AE_ db
_RAM_C0AF_ db
_RAM_C0B0_ db
_RAM_C0B1_ db
_RAM_C0B2_ db
_RAM_C0B3_ db
_RAM_C0B4_ db
_RAM_C0B5_ db
_RAM_C0B6_ db           ; number of continues?
_RAM_C0B7_ dw
_RAM_C0B9_ dw
_RAM_C0BB_ dw
_RAM_C0BD_ db
_RAM_C0BE_ db
_RAM_C0BF_ db
_RAM_C0C0_ db
_RAM_C0C1_ db
_RAM_C0C2_ db
.ende

.enum $C0C4 export
_RAM_C0C4_ db
_RAM_C0C5_ dw
_RAM_C0C7_ db
_RAM_C0C8_ dw
_RAM_C0CA_ dw
_RAM_C0CC_ dw
_RAM_C0CE_ db
CurrentScore            db  ; $C0CF - current score (BCD), stored in a "little
                            ;         endian" format (low BCD bytes first).
_RAM_C0D0_ db
_RAM_C0D1_ db
_RAM_C0D2_ db               ; TODO: hi score might actually be here
HiScore                 db  ; $C0D3 - hi-score. same format as above.
_RAM_C0D4_ db
_RAM_C0D5_ db
.ende

.enum $C0D7 export
_RAM_C0D7_              db
_RAM_C0D8_              db
FontNumberMappings      dw  ; $C0D9 - pointer to char code mappings for the
                            ;         current number font (e.g. score)
FontNumberMappingsSize  dw  ; $C0DB - size of each mapping element.
                            ;         hi = row count, lo = bytes per row
_RAM_C0DD_              db
_RAM_C0DE_              db
_RAM_C0DF_              db
_RAM_C0E0_              dw
.ende

.enum $C0E2 export
_RAM_C0E2_ dw
_RAM_C0E4_ dw
_RAM_C0E6_ dw
_RAM_C0E8_ dw
_RAM_C0EA_ dw
_RAM_C0EC_ dw
_RAM_C0EE_ dw
_RAM_C0F0_ dw
_RAM_C0F2_ dw
_RAM_C0F4_ dw
_RAM_C0F6_ dw
_RAM_C0F8_ dw
_RAM_C0FA_ dw
_RAM_C0FC_ dw
_RAM_C0FE_ db
_RAM_C0FF_ db
.ende

.enum $C200 export
Alex_State              db
_RAM_C201_              db
_RAM_C202_              dw
Alex_MaxAnimFrame       db
_RAM_C205_              db
_RAM_C206_              db
Alex_FrameCountDown     db
_RAM_C208_              db
_RAM_C209_              db
_RAM_C20A_              db
_RAM_C20B_              db
_RAM_C20C_              db
_RAM_C20D_              db
Alex_SpeedY             db      ; $C20E
_RAM_C20F_              db
Alex_SpeedX             db      ; $C210
_RAM_C211_              db
_RAM_C212_              db
_RAM_C213_              db
_RAM_C214_              db
_RAM_C215_              db
_RAM_C216_              db
_RAM_C217_              db
_RAM_C218_              db
_RAM_C219_              db
_RAM_C21A_              db
_RAM_C21B_              db
_RAM_C21C_              db
.ende

.enum $C220 export
_RAM_C220_ db
_RAM_C221_ db
_RAM_C222_ db
_RAM_C223_ db
_RAM_C224_ db
.ende

.enum $C226 export
_RAM_C226_ db
_RAM_C227_ db
.ende

.enum $C229 export
_RAM_C229_ db
.ende

.enum $C22C export
Alex_PowerupState   db
_RAM_C22D_          db
Alex_Health         db                  ; $C22E
Alex_LifeCounter    db                  ; $C22F
_RAM_C230_          db
_RAM_C231_          db
.ende

.enum $C240 export
_RAM_C240_ dw
.ende

.enum $C24A export
_RAM_C24A_ dw
_RAM_C24C_ dw
.ende

.enum $C260 export
_RAM_C260_ dw
.ende

.enum $C2E0 export
_RAM_C2E0_ db
.ende

.enum $C2EA export
_RAM_C2EA_ dw
.ende

.enum $C2EC export
_RAM_C2EC_ dw
.ende

.enum $C300 export
_RAM_C300_ db
_RAM_C301_ db
.ende

.enum $C304 export
_RAM_C304_ db
_RAM_C305_ db
.ende

.enum $C307 export
_RAM_C307_ db
.ende

.enum $C309 export
_RAM_C309_ db
_RAM_C30A_ db
_RAM_C30B_ db
_RAM_C30C_ db
_RAM_C30D_ db
_RAM_C30E_ dw
_RAM_C310_ dw
.ende

.enum $C313 export
_RAM_C313_ db
_RAM_C314_ dw
_RAM_C316_ db
_RAM_C317_ db
_RAM_C318_ db
.ende

.enum $C31C export
_RAM_C31C_ db
_RAM_C31D_ db
_RAM_C31E_ db
_RAM_C31F_ db
_RAM_C320_ dw
.ende

.enum $C325 export
_RAM_C325_ db
.ende

.enum $C327 export
_RAM_C327_ db
.ende

.enum $C32B export
_RAM_C32B_ db
.ende

.enum $C32D export
_RAM_C32D_ db
_RAM_C32E_ dw
_RAM_C330_ dw
.ende

.enum $C33C export
_RAM_C33C_ db
_RAM_C33D_ db
.ende

.enum $C33F export
_RAM_C33F_ db
_RAM_C340_ db
.ende

.enum $C34B export
_RAM_C34B_ db
.ende

.enum $C34D export
_RAM_C34D_ db
.ende

.enum $C353 export
_RAM_C353_ db
.ende

.enum $C360 export
_RAM_C360_ db
.ende

.enum $C380 export
_RAM_C380_ db
.ende

.enum $C3A0 export
_RAM_C3A0_ db
.ende

.enum $C3C0 export
_RAM_C3C0_ dw
.ende

.enum $C3CA export
_RAM_C3CA_ dw
.ende

.enum $C3D3 export
_RAM_C3D3_ db
.ende

.enum $C3E0 export
_RAM_C3E0_ db
.ende

.enum $C400 export
_RAM_C400_ db
.ende

.enum $C480 export
_RAM_C480_ db
.ende

.enum $C4A0 export
_RAM_C4A0_ db
.ende

.enum $C500 export
_RAM_C500_ dw
.ende

.enum $C511 export
_RAM_C511_ dw
.ende

.enum $CA00 export
_RAM_CA00_ db
.ende

.enum $CAC0 export
_RAM_CAC0_ dw
.ende

.enum $CB56 export
_RAM_CB56_ db
.ende

.enum $D000 export
_RAM_D000_ db
.ende

.enum $D500 export
WorkingSAT dsb 64
.ende

.enum $D580 export
WorkingSAT_HPos dsb 128
.ende

.enum $D600 export
ScreenMapBuffer dsb 700
.ende

.enum $D631 export
_RAM_D631_ db
.ende

.enum $DD00 export
_RAM_DD00_ db
.ende

.enum $DD02 export
_RAM_DD02_ db
_RAM_DD03_ db
_RAM_DD04_ db
.ende

.enum $DD07 export
_RAM_DD07_ db
_RAM_DD08_ db
_RAM_DD09_ dw
_RAM_DD0B_ db
_RAM_DD0C_ db
_RAM_DD0D_ db
_RAM_DD0E_ dw
_RAM_DD10_ db
.ende

.enum $DD1D export
_RAM_DD1D_ db
_RAM_DD1E_ dw
_RAM_DD20_ dw
.ende

.enum $DD2D export
_RAM_DD2D_ db
_RAM_DD2E_ db
_RAM_DD2F_ db
_RAM_DD30_ dw
_RAM_DD32_ db
_RAM_DD33_ db
_RAM_DD34_ dw
_RAM_DD36_ db
_RAM_DD37_ db
.ende

.enum $DD40 export
_RAM_DD40_ dw
.ende

.enum $DD4A export
_RAM_DD4A_ dw
_RAM_DD4C_ db
_RAM_DD4D_ db
_RAM_DD4E_ db
_RAM_DD4F_ db
_RAM_DD50_ dw
.ende

.enum $DD6E export
_RAM_DD6E_ db
.ende

.enum $DD92 export
_RAM_DD92_ dw
_RAM_DD94_ db
.ende

.enum $DDA0 export
_RAM_DDA0_ db
.ende

.enum $DDF0 export
_RAM_DDF0_ dw
.ende

.enum $DDF4 export
_RAM_DDF4_ dw
.ende

.enum $DDF8 export
_RAM_DDF8_ db
_RAM_DDF9_ dw
_RAM_DDFB_ dw
.ende

.enum $DDFE export
_RAM_DDFE_ db
_RAM_DDFF_ db
.ende

.enum $DE01 export
_RAM_DE01_          db
_RAM_DE02_          db
_RAM_DE03_          db
Sound_MusicTrigger  db
_RAM_DE05_          db
_RAM_DE06_          db
_RAM_DE07_          db
_RAM_DE08_          db
.ende

.enum $DE0A export
_RAM_DE0A_ db
_RAM_DE0B_ db
.ende

.enum $DE0D export
_RAM_DE0D_ db
.ende

.enum $DE18 export
_RAM_DE18_ db
.ende

.enum $DE1C export
_RAM_DE1C_ db
_RAM_DE1D_ db
_RAM_DE1E_ db
_RAM_DE1F_ db
_RAM_DE20_ db
.ende

.enum $DE28 export
_RAM_DE28_ db
.ende

.enum $DE2A export
_RAM_DE2A_ db
.ende

.enum $DE40 export
_RAM_DE40_ db
.ende

.enum $DE60 export
_RAM_DE60_ db
.ende

.enum $DE68 export
_RAM_DE68_ db
.ende

.enum $DE80 export
_RAM_DE80_ db
.ende

.enum $DEA0 export
_RAM_DEA0_ db
.ende

.enum $DEBA export
_RAM_DEBA_ db
.ende

.enum $DEC0 export
_RAM_DEC0_ db
.ende

.enum $DEDA export
_RAM_DEDA_ db
.ende

.enum $DEE0 export
_RAM_DEE0_ db
.ende

.enum $DEFA export
_RAM_DEFA_ db
.ende

.enum $FA01 export
_RAM_FA01_ db
.ende

.enum $FFFC export
_RAM_FFFC_ db
.ende

.enum $FFFF export
_RAM_FFFF_ db
.ende
