.MEMORYMAP
SLOTSIZE $7FF0
SLOT 0 $0000
SLOTSIZE $10
SLOT 1 $7FF0
SLOTSIZE $4000
SLOT 2 $8000
DEFAULTSLOT 2
.ENDME
.ROMBANKMAP
BANKSTOTAL 16
BANKSIZE $7FF0
BANKS 1
BANKSIZE $10
BANKS 1
BANKSIZE $4000
BANKS 14
.ENDRO

.emptyfill $FF

.include "include/sms.asm"
.include "include/memory.asm"
.include "include/game_modes.asm"
.include "include/defines.asm"
.include "include/music_values.asm"


.BANK 0 SLOT 0
.ORG $0000

_LABEL_0_:
    di
    im      1
    jp      main


; ==============================================================================
;  call_jump_table(uint8_t index, uint16_t *table)
; ------------------------------------------------------------------------------
;  Jumps to the routine at table[index]
; ------------------------------------------------------------------------------
;  In:
;    A      - Index into the table
;    HL     - table of function pointers
; ------------------------------------------------------------------------------
.org $0008
call_jump_table:                                                    ; $0008
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    jp      (hl)



_LABEL_13_:
    jp      _LABEL_1028_


; ==============================================================================
;  uint16_t* get_array_index(uint8_t index, uint16_t *array)
; ------------------------------------------------------------------------------
;  Calculates a pointer to element at array[index], where array is an array of
;  16-bit words.
; ------------------------------------------------------------------------------
;  In:
;    A      - Index into the array
;    HL     - Array of 16-bit values
;  Out:
;     HL    - Pointer to array[index]
;  Clobbers:
;     A
; ------------------------------------------------------------------------------
.org $0018
get_array_index:                                                    ; $0018
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ret



_LABEL_23_:
    jp      _LABEL_1460_


; ==============================================================================
;  void vdp_write_control(uint16_t control_word)
; ------------------------------------------------------------------------------
;  Writes a 16bit value to the VDP control port
; ------------------------------------------------------------------------------
;  In:
;    DE     - The value to write to the control port
;  Clobbers:
;     A
; ------------------------------------------------------------------------------
.org $0028
vdp_write_control:
    di
    ld      a, e
    out     (Ports_VDP_Control), a
    ld      a, d
    out     (Ports_VDP_Control), a
    ei
    ret


_LABEL_31_:
    jp      _LABEL_7205_


; ==============================================================================
;  void vdp_interrupt(void)
; ------------------------------------------------------------------------------
;  VDP interrupt handler
; ------------------------------------------------------------------------------
; ------------------------------------------------------------------------------
.org $0038
vdp_interrupt:
    push    af
    in      a, (Ports_VDP_Control)
    or      a
    jp      p, vdp_line_interrupt
    ; else we're dealing with a V-Blank interrupt
    ld      a, (_RAM_C041_)
    or      a
    jr      z, vblank_handler
    jp      _LABEL_15F_


; ==============================================================================
;  void wait_for_c041(void)
; ------------------------------------------------------------------------------
;  Enbles interrupts and waits for $C041 to be set to 0.
; ------------------------------------------------------------------------------
; ------------------------------------------------------------------------------
wait_for_c041:                                                      ; $0048
    ld      (_RAM_C041_), a
    ei
-:
    ld      a, (_RAM_C041_)
    or      a
    jr      nz, -
    ret


; ==============================================================================
;  void nmi(void)
; ------------------------------------------------------------------------------
;  NMI handler
; ------------------------------------------------------------------------------
; ------------------------------------------------------------------------------
.org $0066
nmi:
    push    af
    ld      a, (GameMode)
    cp      Mode_ExitPauseScreen
    jr      z, ++
    cp      Mode_EnterPauseScreen
    jr      z, +
-:
    pop     af
    retn

+:  ; enter the pause state
    ld      (SavedGameMode), a
    ld      a, Mode_InPauseScreen
    ld      (GameMode), a
    jr      -

++: ; exit the pause state
    ld      a, (SavedGameMode)
    ld      (GameMode), a
    ; clear the pause flag
    xor     a
    ld      (PauseFlag), a
    jr      -


; ==============================================================================
;  void vblank_handler(void)
; ------------------------------------------------------------------------------
;  V-Blank interrupt handler.
; ------------------------------------------------------------------------------
; ------------------------------------------------------------------------------
vblank_handler:                                                     ; $008B
    ; save all regs & the current bank in frame 2
    push    bc
    push    de
    push    hl
    ex      af, af'
    exx
    push    af
    push    bc
    push    de
    push    hl
    push    ix
    push    iy
    ld      a, (Frame2_Select)
    push    af
    
    ld      a, $02
    ld      (Frame2_Select), a
    call    _LABEL_83F1_
    
    ; restore bank in frame 2 and all regs
    pop     af
    ld      (Frame2_Select), a
    pop     iy
    pop     ix
    pop     hl
    pop     de
    pop     bc
    pop     af
    exx
    ex      af, af'
    pop     hl
    pop     de
    pop     bc
    pop     af
    ei
    ret

; ==============================================================================
;  void main(void)
; ------------------------------------------------------------------------------
;  Main entry point
; ------------------------------------------------------------------------------
; ------------------------------------------------------------------------------
main:                                                               ; $00B8
    ld      sp, $DFF0
    ld      hl, Frame_Control
    ; disable ROM write protect?
    ld      (hl), $80
    ; Frame0 = 0
    inc     hl
    ld      (hl), $00
    ; Frame1 = 1
    inc     hl
    ld      (hl), $01
    ; Frame2 = 2
    inc     hl
    ld      (hl), $02
    
    call    detect_region
    ; call this if japanese
    or      a
    call    z, _LABEL_71E_
    
    di
    ld      hl, $0000
    ld      e, l
    ld      d, l
    jr      +

reset:
    di
    ld      hl, (_RAM_C0D2_)
    ld      de, (_RAM_C0D4_)
+:
    ld      (Sound_MusicTrigger), hl
    ld      (_RAM_DE06_), de
    ld      sp, $DFF0
    
    ; clear work RAM
    ld      hl, VDPRegister0
    ld      de, VDPRegister1
    ld      bc, $1DEF
    ld      (hl), $00
    ldir
    
    ld      hl, (Sound_MusicTrigger)
    ld      de, (_RAM_DE06_)
    ld      (_RAM_C0D2_), hl
    ld      (_RAM_C0D4_), de
    
    call    _LABEL_4A6_
    
    ; probably the sound driver. check later
    ld      a, :_LABEL_870B_
    ld      (Frame2_Select), a
    call    _LABEL_870B_
    
    call    vdp_init_registers
    call    _LABEL_72C_
-:
    ld      a, (GameMode)
    ld      hl, game_mode_handlers
    rst     $08    ; call_jump_table
    jp      -

; 5th entry of Jump Table from 135 (indexed by unknown)
_LABEL_120_:
    ld      a, $05
    ld      (GameMode), a
    ld      a, $01
    ld      (PauseFlag), a
    ld      a, $02
    ld      (Frame2_Select), a
    ; turn off sound
    call    _LABEL_871E_
; 6th entry of Jump Table from 135 (indexed by unknown)
_LABEL_132_:
    jp      _LABEL_72C_

; Jump Table from 135 to 15E (21 entries, indexed by unknown)
game_mode_handlers:                                                 ; $0135
.dw load_intro_sequence             ; $00
.dw _LABEL_944_                     ; $01 - intro sequence
.dw load_powerup_and_hud_gfx        ; $02
.dw mode_handler_enter_pause_menu   ; $03 - enter pause screen
.dw _LABEL_120_                     ; $04 - in pause screen
.dw _LABEL_132_                     ; $05 - exit pause screen
.dw _LABEL_C71_                     ; $06 - in level
.dw _LABEL_CD2_                     ; $07
.dw _LABEL_D8D_                     ; $08
.dw mode_handler_load_title_card    ; $09
.dw _LABEL_162D_                    ; $0A - level intro screen
.dw _LABEL_14D1_                    ; $0B
.dw mode_handler_load_boss_room     ; $0C
.dw _LABEL_2F3F_                    ; $0D
.dw _LABEL_15F_                     ; $0E
.dw _LABEL_6F96_                    ; $0F
.dw _LABEL_6FD7_                    ; $10 - map screen
.dw mode_handler_load_game_over     ; $11
.dw _LABEL_715F_                    ; $12
.dw _LABEL_7315_                    ; $13
.dw _LABEL_7388_                    ; $14

; 15th entry of Jump Table from 135 (indexed by unknown)
_LABEL_15F_:
    ld      hl, $0190
    rst     $08    ; call_jump_table
    ld      a, (PauseFlag)
    or      a
    jr      nz, ++
    ld      a, $02
    ld      (Frame2_Select), a
    call    _LABEL_83F1_
    ld      a, (_RAM_C0FE_)
    or      a
    jr      nz, ++
    ld      a, (_RAM_C0FF_)
    inc     a
    cp      $06
    jr      nz, +
    xor     a
    ld      (_RAM_C0FF_), a
    call    _LABEL_83F1_
    jr      ++

+:
    ld      (_RAM_C0FF_), a
++:
    xor     a
    ld      (_RAM_C041_), a
    pop     af
    ei
    ret

; Jump Table from 192 to 19F (7 entries, indexed by unknown)
_DATA_192_:
.dw _LABEL_1A0_ _LABEL_1B6_ _LABEL_1BF_ _LABEL_1E4_ _LABEL_1F4_ _LABEL_203_ _LABEL_234_

; 1st entry of Jump Table from 192 (indexed by unknown)
_LABEL_1A0_:
    ld      b, $FF
-:
    djnz    -
    ld      b, $40
-:
    djnz    -
    ld      a, $20
    ld      (_RAM_C01F_), a
    call    _LABEL_24D_
    call    vdp_update_sprites
    jp      read_controllers

; 2nd entry of Jump Table from 192 (indexed by unknown)
_LABEL_1B6_:
    call    _LABEL_6177_
    call    ++
    jp      vdp_update_sprites

; 3rd entry of Jump Table from 192 (indexed by unknown)
_LABEL_1BF_:
    ld      a, (_RAM_DD0B_)
    or      a
    jr      nz, +
    call    _LABEL_1906_
    call    _LABEL_2CF_
+:
    call    _LABEL_24D_
    call    ++
    call    _LABEL_4AFC_
    call    vdp_update_sprites
    jp      read_controllers

++:
    ld      a, (_RAM_C076_)
    or      a
    jp      z, _LABEL_AF3_
    jp      _LABEL_C41_

; 4th entry of Jump Table from 192 (indexed by unknown)
_LABEL_1E4_:
    call    vdp_update_sprites
    call    _LABEL_6177_
    ld      a, (_RAM_C076_)
    or      a
    jp      z, _LABEL_AF3_
    jp      _LABEL_C41_

; 5th entry of Jump Table from 192 (indexed by unknown)
_LABEL_1F4_:
    call    _LABEL_701A_
    call    _LABEL_1714_
    call    _LABEL_71DB_
    call    vdp_update_sprites
    jp      read_controllers

; 6th entry of Jump Table from 192 (indexed by unknown)
_LABEL_203_:
    ld      a, $C0
    ld      (_RAM_C01F_), a
    call    read_controllers
    call    _LABEL_24D_
    ld      a, $08
    ld      (Frame2_Select), a
    call    _LABEL_7559_
    ld      hl, (_RAM_DD02_)
    ld      a, l
    or      h
    jr      z, +
    ld      a, $06
    ld      (Frame2_Select), a
    ld      de, $7A00
    ld      bc, $0100
    call    copy_to_vram
    ld      hl, $0000
    ld      (_RAM_DD02_), hl
+:
    jp      vdp_update_sprites

; 7th entry of Jump Table from 192 (indexed by unknown)
_LABEL_234_:
    ld      b, $FF
-:
    djnz    -
    ld      b, $60
-:
    djnz    -
    ld      a, $20
    ld      (_RAM_C01F_), a
    call    _LABEL_24D_
    call    _LABEL_751C_
    call    vdp_update_sprites
    jp      read_controllers

_LABEL_24D_:
    ld      hl, _RAM_C01F_
    ld      a, (hl)
    or      a
    ret     z
    
    ld      b, $FF
-:  djnz    -

    ld      a, (_RAM_C0FE_)
    or      a
    ld      b, $60
    jr      nz, _LABEL_24D__1_
    ld      b, $A0
_LABEL_24D__1_:
    djnz    _LABEL_24D__1_
-:
    dec     a
    jr      nz, -
    ld      (hl), a
    inc     hl
    ; set the CRAM address
    out     (Ports_VDP_Control), a
    ld      a, VDPCTL_CRAMWriteAddress
    out     (Ports_VDP_Control), a
    
    ; fill 64 bytes of palette data
    ld      c, Ports_VDP_Data
    ld      b, 64
-:
    outi
    djnz    -
    ret


; ==============================================================================
;  void vdp_update_sprites(void)
; ------------------------------------------------------------------------------
;  Updates the VDP's sprite attribue table with the copy stored in work RAM.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
vdp_update_sprites:                                                 ; $0277
    xor     a
    out     (Ports_VDP_Control), a
    ld      a, VDPCTL_WriteAddress | $3F
    out     (Ports_VDP_Control), a
    ld      c, Ports_VDP_Data
    ld      hl, WorkingSAT
    ld      b, $3F
-:  outi
    nop
    jr      nz, -
    
    ; copy the h-pos & char codes
    inc     a
    out     (Ports_VDP_Control), a
    dec     a
    out     (Ports_VDP_Control), a
    ld      hl, WorkingSAT_HPos
    ld      b, $7F
-:  outi
    nop
    jr      nz, -
    ret


; ==============================================================================
;  void vdp_line_interrupt(void)
; ------------------------------------------------------------------------------
;  VDP line interrupt handler.
; ------------------------------------------------------------------------------
; ------------------------------------------------------------------------------
vdp_line_interrupt:
    ld      a, (_RAM_C04C_)
    or      a
    jr      nz, +
    inc     a
    ld      (_RAM_C04C_), a
    ld      a, (VDPRegister8)
    out     ($BF), a
    ld      a, $88
    out     ($BF), a
    ld      a, $FF
    out     ($BF), a
    ld      a, $8A
    out     ($BF), a
+:
    pop     af
    ei
    ret

; Data from 2B9 to 2CE (22 bytes)
.db $3E $4F $32 $1A $C0 $3A $10 $C0 $F6 $10 $32 $10 $C0 $CD $2C $07
.db $5F $16 $80 $C3 $28 $00

_LABEL_2CF_:
    ld      ix, _RAM_DDA0_
    ld      b, $08
-:
    push    bc
    ld      a, (ix+0)
    or      a
    jr      z, ++
    bit     0, a
    jr      nz, +
    rrca
    ld      hl, $02FC
    rst     $18    ; get_array_index
    ld      e, (ix+1)
    ld      d, (ix+2)
    ld      bc, $0204
    call    draw_screen_map_element
+:
    dec     (ix+0)
++:
    inc     ix
    inc     ix
    inc     ix
    pop     bc
    djnz    -
    ret

; Data from 2FE to 327 (42 bytes)
.db $08 $03 $10 $03 $18 $03 $20 $03 $20 $03 $B8 $01 $B9 $01 $BA $01
.db $BB $01 $B4 $01 $B5 $01 $B6 $01 $B7 $01 $B0 $01 $B1 $01 $B2 $01
.db $B3 $01 $AC $01 $AD $01 $AE $01 $AF $01


; ==============================================================================
;  void decode_tiles_to_vram(uint8_t *source, uint16_t vram_address)
; ------------------------------------------------------------------------------
;  Decodes tile data directly into VRAM. The bitplanes are grouped together
;  (i.e. biplane 0 from all tiles stored together, followed by bitplane 1, etc)
;  and RLE compressed.
; ------------------------------------------------------------------------------
;  In:
;    HL     - Source address in work RAM
;    DE     - Destination address in VRAM
; ------------------------------------------------------------------------------
decode_tiles_to_vram:                                               ; $0328
    ld      b, $04      ; number of bitplanes to copy
    ex      af, af'
    ld      a, b        ; the stride between copying bytes - always 4
    ex      af, af'
---:
    push    bc
    push    de
--:
    ; read the byte count
    ld      a, (hl)
    ; 0 means end of this sequence of bitplanes
    inc     hl
    or      a
    jr      z, ++
    
    ; store the copy flag in C, counter in B
    ld      c, a
    and     $7F
    ld      b, a
    
-:  ; set the VRAM address and write a byte
    rst     $28    ; vdp_write_control
    ld      a, (hl)
    out     (Ports_VDP_Data), a
    
    ; if the copy flag was set don't bother moving the source pointer (i.e. copy
    ; the same value again).
    bit     7, c
    jr      z, +
    inc     hl
    
+:  ex      af, af'
    inc     de
    cp      $01
    jr      z, +
    inc     de
    cp      $02
    jr      z, +
    inc     de
    inc     de
+:
    ex      af, af'
    djnz    -
    
    bit     7, c
    jr      nz, --
    inc     hl
    jp      --

++:
    pop     de
    inc     de
    pop     bc
    ; copy the next sequence of bitplanes
    djnz    ---
    ret


; ==============================================================================
;  void copy_to_vram(uint8_t *source, uint16_t vram_address, uint16_t count)
; ------------------------------------------------------------------------------
;  Copies a block of data from work RAM to VRAM.
; ------------------------------------------------------------------------------
;  In:
;    BC     - count
;    HL     - source address
;    DE     - VRAM destination address
;  Out:
;    None
;  Clobbers:
;    A, B
; ------------------------------------------------------------------------------
copy_to_vram:                                                       ; $035F
    rst     $28    ; vdp_write_control
    ld      a, c
    or      a
    jr      z, +
    inc     b
+:
    ld      a, b
    ld      b, c
    ld      c, Ports_VDP_Data
-:
    outi
    nop
    nop
    jr      nz, -
    dec     a
    jr      nz, -
    ret


; ==============================================================================
;  void fill_vram(uint16_t value, uint16_t vram_address, uint16_t count)
; ------------------------------------------------------------------------------
;  Fills a block of VRAM with the specified 16-bit value.
; ------------------------------------------------------------------------------
;  In:
;    BC     - count
;    HL     - Value to write
;    DE     - VRAM destination address
;  Out:
;    None
;  Clobbers:
;    A
; ------------------------------------------------------------------------------
fill_vram:                                                          ; $0373
    rst     $28    ; vdp_write_control
    ld      a, c
    or      a
    jr      z, _fill_vram_1
    inc     b
_fill_vram_1:
    ld      a, l
    out     (Ports_VDP_Data), a
    push    af
    pop     af
    ld      a, h
    out     (Ports_VDP_Data), a
    dec     c
    jr      nz, _fill_vram_1
    djnz    _fill_vram_1
    ret
 

; Data from 387 to 3A0 (26 bytes)
.db $C5 $EF $41 $0E $BE $ED $A3 $3A $45 $C0 $00 $ED $79 $00 $20 $F5
.db $EB $01 $40 $00 $09 $EB $C1 $10 $E7 $C9


; ==============================================================================
;  void draw_screen_map_element(uint8_t *source, uint16_t vramAddress,
;                               uint8_t rowCount, uint8_t columnCount)
; ------------------------------------------------------------------------------
;  Copies a rectangle of screen map data to the X/Y coordinate indicated by the
;  VRAM destination address.
; ------------------------------------------------------------------------------
;  In:
;    B      - row count
;    C      - number of bytes per row
;    HL     - source
;    DE     - VRAM dest
; ------------------------------------------------------------------------------
draw_screen_map_element:                                            ; $03A1
    push    bc
    rst     $28    ; vdp_write_control
    ld      b, c
    ld      c, Ports_VDP_Data

-:  ; copy a row of mapping data
    outi
    nop
    jp      nz, -

    ; move to the next row in the screen map
    ld      a, $40
    add     a, e
    ld      e, a
    jr      nc, +
    inc     d
+:
    pop     bc
    djnz    draw_screen_map_element
    ret


; Data from 3B7 to 3E7 (49 bytes)
.db $32 $46 $C0 $EF $7E $D9 $0E $BE $06 $04 $67 $3A $46 $C0 $1F $54
.db $38 $02 $16 $00 $ED $51 $10 $F6 $D9 $23 $0B $78 $B1 $C2 $BB $03
.db $C9 $EF $0E $BE $ED $A3 $3A $45 $C0 $00 $ED $79 $00 $C2 $DB $03
.db $C9


; ==============================================================================
;  void vdp_init_registers(void)
; ------------------------------------------------------------------------------
;  Initialises the VDP registers to the hard-coded defaults. Also sets colour 0
;  of the background palette to black.
; ------------------------------------------------------------------------------
;  Clobbers:
;    A, BC, DE, HL
; ------------------------------------------------------------------------------
vdp_init_registers:                                                 ; $03E8
    ; copy the 11 default register values to the VDP
    ld      hl, DATA_InitialVdpRegisters
    ld      de, VDPRegister0
    ld      bc, $0B00 | VDPCTL_WriteRegister
-:
    ld      a, (hl)
    ld      (de), a
    out     (Ports_VDP_Control), a
    ld      a, c
    out     (Ports_VDP_Control), a
    inc     hl
    inc     de
    inc     c
    djnz    -

    ; set the first background colour to black
    ld      de, $C010
    xor     a
    ; fall through

vdp_write_byte_at_address:                                          ; 40401
    push    af
    rst     $28    ; vdp_write_control
    pop     af
    out     (Ports_VDP_Data), a
    ret


DATA_InitialVdpRegisters:                                           ; $0407
.db $36 $A2 $FF $FF $FF $FF $FB $00 $00 $00 $FF


; ==============================================================================
;  void read_controllers(void)
; ------------------------------------------------------------------------------
;  Reads the button states from the two controller ports and stores the result
;  in the input global variables.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
read_controllers:                                                   ; $0412
    ; read the controller buttons and mask out anything from port 2
    in      a, (Ports_IO1)
    ld      hl, InputFlags
    or      $C0
    cpl
    ld      b, a
    ; work out which buttons changed on this frame
    xor     (hl)
    ld      (hl), b
    inc     hl
    and     b
    ; store the changed buttons
    ld      (hl), a
    inc     hl
    ; get the buttons states for controller 2
    in      a, (Ports_IO1)
    and     $C0
    ld      b, a
    in      a, (Ports_IO2)
    and     $3F
    add     a, b
    rlca
    rlca
    cpl
    ; store the button states
    ld      b, a
    xor     (hl)
    ld      (hl), b
    inc     hl
    and     b
    ld      (hl), a
    ; check to see if the reset button is pressed
    in      a, (Ports_IO2)
    and     $10
    ret     nz
    jp      reset


; Data from 43C to 482 (71 bytes)
.db $3A $00 $C0 $2F $E6 $E8 $FE $20 $CA $56 $04 $FE $40 $CA $56 $04
.db $FE $80 $CA $56 $04 $3E $A8 $32 $00 $C0 $3A $00 $C0 $F6 $04 $D3
.db $3E $01 $00 $07 $78 $E6 $01 $D3 $F2 $5F $DB $F2 $E6 $07 $BB $20
.db $01 $0C $10 $F0 $79 $FE $07 $28 $01 $AF $E6 $01 $D3 $F2 $32 $00
.db $DE $3A $00 $C0 $D3 $3E $C9


; ==============================================================================
;  bool detect_region(void)
; ------------------------------------------------------------------------------
;  Determines whether the system is Japanese or Export and sets the global
;  SystemRegion variable.
; ------------------------------------------------------------------------------
;  In:
;    None
;  Out:
;    SystemRegion   - set to 0 for Japan, $FF for Export
;    A              - copy of SystemRegion
;  Clobbers:
;    HL
; ------------------------------------------------------------------------------
detect_region:                                                  ; $0483
    ; region detection
    ld      hl, SystemRegion
    ld      a, %11110101
    out     (Ports_IO_Control), a
    in      a, (Ports_IO2)
    and     %11000000
    cp      $C0
    jr      nz, +
    
    ld      a, %01010101
    out     (Ports_IO_Control), a
    in      a, (Ports_IO2)
    and     %11000000
    or      a
    jr      nz, +
    
    ; System is Export
    ld      a, %11111111
    out     (Ports_IO_Control), a
    ld      (hl), a
    ret

+:  ; System is Japanese
    xor     a
    ld      (hl), a
    ret


_LABEL_4A6_:
    ld      hl, $0000
    ; wait for a v-blank
-:  in      a, (Ports_VDP_Control)
    or      a
    jp      p, -
    ; ...and another
-:  in      a, (Ports_VDP_Control)
    or      a
    jp      p, -
    ; ... and another
-:  inc     hl
    in      a, (Ports_VDP_Control)
    or      a
    jp      p, -
    
    ; does HL get set in the VBlank routine?
    xor     a
    ld      de, $0800
    sbc     hl, de
    rla
    ld      (_RAM_C0FE_), a
    ret


; ==============================================================================
;  void alex_add_life(void)
; ------------------------------------------------------------------------------
;  Adds 1 to the life counter (BCD) and caps at 99.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
alex_add_life:                                                      ; $04C7
_LABEL_4C7_:
    ld      hl, Alex_LifeCounter
    ld      a, (hl)
    add     a, $01
    daa
    jr      nc, +
    ld      a, $99
+:
    ld      (hl), a
    ret

_LABEL_4D4_:
    ld      hl, Alex_LifeCounter
    ld      a, (hl)
    sub     $01
    ret     c
    daa
    ld      (hl), a
    xor     a
    ret

; Data from 4DF to 4F4 (22 bytes)
.db $08 $3A $DD $C0 $B7 $C0 $08 $11 $CF $C0 $CD $00 $05 $21 $CE $C0
.db $DC $1A $05 $C3 $3D $05

_LABEL_4F5_:
    xor     a
    ld      hl, _RAM_C0D8_
    ld      (hl), a
    dec     hl
    ld      (hl), a
    dec     hl
    ld      (hl), a
    ex      de, hl
    ld      a, b
    ld      c, a
    ld      hl, _DATA_5AD_
    ld      b, $00
    add     hl, bc
    add     hl, bc
    add     hl, bc
    ld      a, (de)
    add     a, (hl)
    daa
    ld      (de), a
    inc     de
    inc     hl
    ld      a, (de)
    adc     a, (hl)
    daa
    ld      (de), a
    inc     de
    inc     hl
    ld      a, (de)
    adc     a, (hl)
    daa
    ld      (de), a
    ret

-:
    bit     0, (hl)
    ret     nz
    set     0, (hl)
    ld      c, $99
    inc     hl
    inc     hl
    ld      (hl), c
    inc     hl
    ld      (hl), c
    ret

_LABEL_527_:
    ld      de, _RAM_C0D0_
    ld      hl, _RAM_C0D7_
    ld      a, (de)
    add     a, (hl)
    daa
    ld      (de), a
    inc     de
    inc     hl
    ld      a, (de)
    adc     a, (hl)
    daa
    ld      (de), a
    ld      hl, _RAM_C0CE_
    call    c, -
    ld      hl, _RAM_C0D1_
    ld      de, _RAM_C0D5_
    ld      a, (de)
    sub     (hl)
    dec     hl
    dec     de
    jr      c, ++
    jr      z, +
    ret     nc
+:
    ld      a, (de)
    sub     (hl)
    ret     nc
++:
    ld      a, (hl)
    ld      (de), a
    inc     hl
    inc     de
    ld      a, (hl)
    ld      (de), a
    ret

; ==============================================================================
;  void font_draw_packed_bcd(uint8_t *bcdData, uint16_t vramAddressCommand)
; ------------------------------------------------------------------------------
;  draws 2.5 bytes of packed BCD data
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
font_draw_packed_bcd:                                               ; $0556
    push    de
    call    +
    pop     de
    inc     hl
    dec     de
    dec     de
    dec     de
    dec     de
    push    de
    call    +
    pop     de
    inc     hl
    dec     de
    dec     de
    dec     de
    dec     de
    ; FIXME: not needed
    jr      font_draw_bcd


; ==============================================================================
;  void font_draw_bcd(uint8_t *bcdData, uint16_t vramAddressCommand)
; ------------------------------------------------------------------------------
;  Draws a single BCD digit from the low nybble of the byte pointed to by
;  bcdData.
; ------------------------------------------------------------------------------
;  In:
;    HL     - pointer to the BCD value
;    DE     - VDP command word to set the VRAM address
; ------------------------------------------------------------------------------
font_draw_bcd:                                                      ; $056C
    ld      bc, $0201
    jr      ++


_draw_bcd_values
+:
    ld      bc, $0200
++:
    xor     a
-:
    push    bc
    ; get the next decimal digit into A
    rld
    push    af
    push    hl
    push    de
    dec     c
    jr      nz, +
    or      a
    jr      nz, +
    dec     b
    jr      z, +
    ld      a, $0A
+:
    ; get a pointer to the char code data for the current digit
    ld      hl, (FontNumberMappings)
    rst     $18    ; get_array_index
    ; get the size of an element
    ld      bc, (FontNumberMappingsSize)
    call    draw_screen_map_element
    pop     de

    ; work out by how much to increase the VRAM pointer
    ld      a, (FontNumberMappingsSize)
    ld      h, $02
    sub     h
    jr      z, ++
    sub     h
    jr      z, +
    inc     de
    inc     de
    inc     de
    inc     de
+:
    inc     de
    inc     de
++:
    inc     de
    inc     de

    ; loop around and get the next digit
    pop     hl
    pop     af
    pop     bc
    djnz    -
    rld
    ret

; Data from 5AD to 5CD (33 bytes)
_DATA_5AD_:
.db $00 $00 $00 $00 $01 $00 $00 $03 $00 $00 $05 $00 $00 $20 $00 $00
.db $40 $00 $00 $60 $00 $00 $80 $00 $00 $00 $01 $00 $20 $01 $00 $00
.db $02

; ==============================================================================
;  void decompress_interleaved_rle(uint8_t *compressed, uint8_t *dest, 
;                                  uint8_t count)
; ------------------------------------------------------------------------------
;  Decompresses an interleaved RLE stream into memory. The streams are stored
;  in a de-interleaved format for compression. This routine decompresses and
;  interleaves directly into RAM.
; ------------------------------------------------------------------------------
;  In:
;    HL     - source
;    DE     - destination
;    C      - number of interleaved streams
; ------------------------------------------------------------------------------
decompress_interleaved_rle:                                         ; $05CE
    ld      b, c
---:
    push    de
    push    bc

--: ; read the flags byte
    ld      a, (hl)
    ; jump if we've reached the end of the stream
    or      a
    jr      z, +++
    ; jump if "run length" bit is set
    jp      p, ++

    ; store the count in B
    and     $7F
    ld      b, a

-:  ; increase source pointer & copy a byte
    inc     hl
    ld      a, (hl)
    ld      (de), a

    ; increase the destination pointer by "stride"
    ld      a, c
    add     a, e
    ld      e, a
    jr      nc, +
    inc     d

+:  djnz    -
    ; end of this compressed run - read the next flag byte
    inc     hl
    jr      --

++: ; RLE compression has occurred - decompress here
    ld      b, a
    inc     hl

-:  ; copy the same byte "count" times
    ld      a, (hl)
    ld      (de), a
    ; increment destination pointer by "stride"
    ld      a, c
    add     a, e
    ld      e, a
    jr      nc, +
    inc     d

+:  djnz    -
    inc     hl
    jr      --

+++:
    pop     bc
    pop     de
    inc     de
    inc     hl
    djnz    ---
    ret


_LABEL_5FF_:
    push    hl
    ld      hl, (_RAM_C043_)
    ld      a, h
    rrca
    rrca
    xor     h
    rrca
    xor     l
    rrca
    rrca
    rrca
    rrca
    xor     l
    rrca
    adc     hl, hl
    jr      nz, +
    ld      hl, $733C
+:
    ld      a, r
    xor     l
    ld      (_RAM_C043_), hl
    pop     hl
    ret


; ==============================================================================
;  void reset_screen(void)
; ------------------------------------------------------------------------------
;  Resets the screen by clearing the first tile, resetting the screen map to
;  that tile and disabling all sprites.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
reset_screen:                                                       ; $061E
    call    _LABEL_66F_
    ; clear the first background tile
    ld      hl, $0000
    ld      de, (VDPCTL_WriteAddress << 8) | $2000
    ld      bc, $0020
    call    fill_vram
    ; reset the screen map to use the first background tile
    ld      hl, $0100
    ld      de, (VDPCTL_WriteAddress << 8) | VDP_ScreenMap
    ld      bc, $0300
    call    fill_vram
    ; disable all sprites
    ld      de, (VDPCTL_WriteAddress << 8) | VDP_SATAddress
    ld      a, $D0
    ld      (WorkingSAT), a
    call    vdp_write_byte_at_address
    ; reset the background X scroll
    xor     a
    ld      (VDPRegister8), a
    ld      de, (VDPCTL_WriteRegister | 8) << 8
    jp      vdp_write_control


; ==============================================================================
;  void enable_display(void)
; ------------------------------------------------------------------------------
;  Enables V-sync interrupts and turns the display on.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
enable_display:                                                     ; $064E
    ; enable vsync interrupts...
    ld      a, (VDPRegister1)
    ld      e, a
    ld      d, VDPCTL_WriteRegister | 1
    set     VDP_FrameInterrupts, e
    rst     $28    ; vdp_write_control
    push    af
    pop     af

    ; ...and wait for a frame
    in      a, (Ports_VDP_Control)
    call    _LABEL_72C_

    ; ...enable the display
    set     VDP_DisplayVisible, e
    rst     $28    ; vdp_write_control
    ld      a, e
    ld      (VDPRegister1), a

    ld      a, (VDPRegister0)
    ld      e, a
    ld      d, VDPCTL_WriteRegister | 0
    rst     $28    ; vdp_write_control
    jp      _LABEL_72C_



_LABEL_66F_:
    call    +
    call    _LABEL_897_
    ; turn off the screen
    ld      a, (VDPRegister1)
    and     VDP_DisplayVisibleBit ~ $FF
    ld      (VDPRegister1), a
    ld      e, a
    ld      d, VDPCTL_WriteRegister | 1
    jp      vdp_write_control


_LABEL_683_:
    ld      a, $03
--:
    call    ++
    ld      b, $08
-:
    call    _LABEL_72C_
    djnz    -
    dec     a
    jp      p, --
    ret

+:
    xor     a
--:
    call    _LABEL_6F2_
    ld      b, $08
-:
    call    _LABEL_72C_
    djnz    -
    inc     a
    cp      $04
    jr      nz, --
    ret

_LABEL_6A5_:
    xor     a
--:
    call    +++
    ld      b, $05
-:
    call    _LABEL_72C_
    djnz    -
    inc     a
    cp      $04
    jr      nz, --
    ret

++:
    ld      hl, _RAM_C500_
    ld      de, PaletteBuffer
    ld      bc, $0020
    ldir
    push    af
    ld      hl, $C020
    ld      b, $20
    ld      c, a
    jr      _LABEL_6F9_

; Data from 6CA to 6DD (20 bytes)
.db $21 $00 $C5 $11 $20 $C0 $01 $11 $00 $ED $B0 $F5 $21 $20 $C0 $06
.db $11 $4F $18 $1B

+++:
    ld      hl, _RAM_C511_
    ld      de, _RAM_C031_
    ld      bc, $000F
    ldir
    push    af
    ld      hl, $C031
    ld      b, $0F
    ld      c, a
    jr      _LABEL_6F9_

_LABEL_6F2_:
    push    af
    ld      hl, $C020
    ld      b, $20
    ld      c, a
_LABEL_6F9_:
    rld
    and     $03
    sub     c
    jr      nc, +
    xor     a
+:
    rld
    ld      e, a
    rrca
    rrca
    and     $03
    sub     c
    jr      nc, +
    xor     a
+:
    rlca
    rlca
    ld      d, a
    ld      a, e
    and     $03
    sub     c
    jr      nc, +
    xor     a
+:
    or      d
    rld
    inc     hl
    djnz    _LABEL_6F9_
    pop     af
    ret

_LABEL_71E_:
    ld      b, $14
_LABEL_720_:
    ld      de, $FFFF
--:
    ld      hl, $39DE
-:
    add     hl, de
    jr      c, -
    djnz    --
    ret

_LABEL_72C_:
    push    af
    push    bc
    push    de
    push    hl
    ex      af, af'
    exx
    push    af
    push    bc
    push    de
    push    hl
    push    ix
    push    iy

    ld      a, (Frame2_Select)
    push    af
    ld      a, $01
    call    wait_for_c041
    pop     af
    ld      (Frame2_Select), a

    pop     iy
    pop     ix
    pop     hl
    pop     de
    pop     bc
    pop     af
    exx
    ex      af, af'
    pop     hl
    pop     de
    pop     bc
    pop     af
    ret

; Data from 756 to 768 (19 bytes)
.db $C5 $06 $08 $1A $0F $CB $11 $10 $FB $71 $23 $13 $C1 $0B $78 $B1
.db $20 $EE $C9


; ==============================================================================
;  void vdp_mirror_region(uint16_t vram_src, uint16_t vram_dest,
;                         uint16_t byte_count)
; ------------------------------------------------------------------------------
;  Copies a block of tile data from vram_src, mirror is horizontally and copies
;  the result to vram_dest.
; ------------------------------------------------------------------------------
;  In:
;    BC     - number of bytes to copy
;    DE     - VRAM source address
;    HL     - VRAM destination address
;  Out:
;    None.
;  Clobbers:
;    A
; ------------------------------------------------------------------------------
vdp_mirror_region:                                                  ; $0769
    res     6, d
-:  ; read a byte of data from VRAM src
    push    bc
    rst     $28    ; vdp_write_control
    push    af
    pop     af
    in      a, (Ports_VDP_Data)
    ; flip it horizontally
    rrca
    rl      b
    rrca
    rl      b
    rrca
    rl      b
    rrca
    rl      b
    rrca
    rl      b
    rrca
    rl      b
    rrca
    rl      b
    rrca
    rl      b
    ; write the flipped value to the VRAM destination address
    ex      de, hl
    rst     $28    ; vdp_write_control
    ex      de, hl
    ld      a, b
    push    af
    pop     af
    out     (Ports_VDP_Data), a
    pop     bc
    inc     hl
    inc     de
    ; loop until done
    dec     bc
    ld      a, b
    or      c
    jr      nz, -
    ret


; ==============================================================================
;  int16_t negate_de(int16_t value)
; ------------------------------------------------------------------------------
;  Negates the DE register pair.
; ------------------------------------------------------------------------------
;  In:
;    DE     - value
;  Out:
;    DE     - negated value
;  Clobbers:
;    A
; ------------------------------------------------------------------------------
negate_de:                                                          ; $079A
    xor     a
    sub     e
    ld      e, a
    sbc     a, a
    sub     d
    ld      d, a
    ret


; ==============================================================================
;  int16_t negate_hl(int16_t value)
; ------------------------------------------------------------------------------
;  Negates the HL register
; ------------------------------------------------------------------------------
;  In:
;    HL     - value
;  Out:
;    HL     - negated value
;  Clobbers:
;    A
; ------------------------------------------------------------------------------
negate_hl:                                                          ; $07A1
    xor     a
    sub     l
    ld      l, a
    sbc     a, a
    sub     h
    ld      h, a
    ret


_LABEL_7A8_:
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    ld      d, (hl)
    ld      e, a
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ret

_LABEL_7B9_:
    ld      a, (_RAM_C066_)
    ld      b, a
    ld      hl, (_RAM_C064_)
    add     hl, de
    ld      a, (_RAM_C06A_)
    or      a
    jr      z, _LABEL_814_
    xor     b
    jr      nz, +
    bit     7, d
    jr      z, _LABEL_814_
+:
    ld      a, h
    cp      $C0
    jr      c, ++
    bit     7, d
    jr      nz, +
    add     a, $40
    inc     b
    ld      h, a
    ld      a, (_RAM_C06A_)
    cp      b
    jr      nz, ++
    jr      c, ++
    dec     b
    inc     hl
    ex      de, hl
    or      a
    sbc     hl, de
    ex      de, hl
    ld      hl, $BFFF
    jr      ++

+:
    sub     $40
    dec     b
    ld      h, a
    ld      a, (_RAM_C06B_)
    dec     a
    cp      b
    jr      nz, ++
    ld      a, h
    add     a, $40
    ld      h, a
    inc     b
    or      a
    sbc     hl, de
    ex      de, hl
    ld      hl, $0000
    xor     a
    sub     e
    ld      e, a
    sbc     a, a
    sub     d
    ld      d, a
++:
    ld      (_RAM_C064_), hl
    ld      a, b
    ld      (_RAM_C066_), a
    ret

_LABEL_814_:
    ld      de, $0000
    ret

_LABEL_818_:
    ld      a, (_RAM_C07C_)
    ld      c, a
    or      a
    jr      z, ++
    ld      a, (_RAM_C066_)
    or      a
    ld      hl, $CA00
    jr      z, +
    ld      b, a
    ld      de, $00C0
-:
    add     hl, de
    djnz    -
+:
    ld      a, c
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, $0C
    sub     c
    ld      c, a
    ld      b, $00
    ld      de, _RAM_DD10_
    ldir
    ld      a, $C0
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      c, $0C
    ldir
    ret

++:
    ld      a, (_RAM_C076_)
    or      a
_LABEL_850_:
    ld      a, (_RAM_C051_)
    jr      z, +
    ld      a, (_RAM_C065_)
    cp      $BF
    ld      a, (_RAM_C066_)
    jr      nz, +
    inc     a
+:
    ld      b, a
    ld      hl, $CA00
    or      a
    ret     z
    ld      de, $00C0
-:
    add     hl, de
    djnz    -
    ret


; ==============================================================================
;  void load_stage_palettes(void)
; ------------------------------------------------------------------------------
;  Loads the palette for the stage/sub-stage into work RAM ready for copying
;  to the VDP.
; ------------------------------------------------------------------------------
;  In:
;    (StageNumber)      - 
;    (SubStageNumber)   -
; ------------------------------------------------------------------------------
load_stage_palettes:                                                ; $086D
    ; get a pointer to the palette data for the current stage
    ld      a, :DATA_StagePalettePointers
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, DATA_StagePalettePointers
    rst     $18    ; get_array_index
    ld      a, (_RAM_DD07_)
    dec     a
    rst     $18    ; get_array_index
    ld      de, PaletteBuffer
    ld      bc, 32
    ldir
    ret


_LABEL_887_:
    ld      hl, _RAM_DD50_
    ld      bc, $003B
    jr      _LABEL_8AD_

_LABEL_88F_:
    push    ix
    pop     hl
_LABEL_892_:
    ld      bc, $001F
    jr      _LABEL_8AD_

_LABEL_897_:
    ld      hl, _RAM_C240_
    ld      bc, $04BF
    jr      _LABEL_8AD_

_LABEL_89F_:
    ld      hl, _RAM_C300_
    ld      bc, $01E0
    jr      _LABEL_8AD_

_LABEL_8A7_:
    ld      hl, _RAM_DD40_
    ld      bc, $000A
_LABEL_8AD_:
    ld      e, l
    ld      d, h
    inc     de
    ld      (hl), $00
    ldir
    ret

; Data from 8B5 to 8C2 (14 bytes)
.db $11 $00 $D6 $0E $02 $CD $CE $05 $CD $D6 $10 $C3 $4E $06

_LABEL_8C3_:
    ld      de, $8800
    rst     $28    ; vdp_write_control
    ld      a, e
    ld      (VDPRegister8), a
    inc     d
    rst     $28    ; vdp_write_control
    ld      a, e
    ld      (VDPRegister9), a
    ret

; ==============================================================================
;  void load_intro_sequence(void)
; ------------------------------------------------------------------------------
;  Loads the graphics, screen map & sprites for the intro sequence
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
; 1st entry of Jump Table from 135 (indexed by unknown)
load_intro_sequence:                                                ; $08D2
    call    _LABEL_871E_
    call    reset_screen
    call    _LABEL_923_

    ld      a, Mode_IntroSequence
    ld      (GameMode), a

    ld      a, :_DATA_20000_
    ld      (Frame2_Select), a
    ld      hl, _DATA_20000_
    ld      de, PaletteBuffer
    ld      bc, 32
    ldir
    ; HL now points to GFX_Intro_Sprites
    ld      de, (VDPCTL_WriteAddress << 8) | $0000
    call    decode_tiles_to_vram
    ; copy the letter tiles
    ld      hl, GFX_Intro_Letters
    ld      de, $6F40
    call    decode_tiles_to_vram
    ; decompress the screen mappings
    ld      hl, SCR_Intro_Screen
    ld      de, ScreenMapBuffer
    ld      c, $02
    call    decompress_interleaved_rle
    ; write the screen to the VDP
    call    copy_screenmap_to_vram

    call    _LABEL_7155_
    ld      ix, _RAM_C300_
    ld      (ix+0), $0A
    call    _LABEL_34FB_

    ld      a, $8E
    ld      (Sound_MusicTrigger), a
    jp      enable_display


_LABEL_923_:
    ld      de, $8016
    rst     $28    ; vdp_write_control
    ld      a, e
    ld      (VDPRegister0), a
    ld      de, $81A0
    rst     $28    ; vdp_write_control
    ld      a, e
    ld      (VDPRegister1), a
    call    _LABEL_8C3_
    ld      hl, _RAM_C04F_
    ld      de, _RAM_C050_
    ld      bc, $002D
    ld      (hl), $00
    ldir
    ret


; 2nd entry of Jump Table from 135 (indexed by unknown)
_LABEL_944_:
    ld      a, (_RAM_DD00_)
    ld      hl, $094D
    jp      call_jump_table

; Jump Table from 94D to 950 (2 entries, indexed by unknown)
_DATA_94D_:
.dw _LABEL_951_ _LABEL_96F_

; 1st entry of Jump Table from 94D (indexed by unknown)
_LABEL_951_:
    ld      a, (_RAM_DD04_)
    or      a
    jr      nz, +
    ld      a, (InputFlagsChanged)
    and     BTN_1 | BTN_2
    jr      nz, _LABEL_96F_
    call    _LABEL_2CBB_
    call    use_default_working_sat
    call    _LABEL_66CB_
    call    disable_sprites_current_working_sat
    ld      a, $06
    jp      wait_for_c041

; 2nd entry of Jump Table from 94D (indexed by unknown)
_LABEL_96F_:
    ld      a, $B5
    ld      (Sound_MusicTrigger), a
    call    reset_screen
    call    vdp_init_registers
    ld      a, $0F
    ld      (GameMode), a
    ld      a, $01
    ld      (_RAM_C0B6_), a
    call    _LABEL_2AFF_
    call    _LABEL_1DED_
    ld      a, $03
    jp      wait_for_c041

+:
    call    vdp_init_registers
    call    reset_screen
    xor     a
    ld      (StageNumber), a
    ld      (_RAM_DD04_), a
    inc     a
    ld      (_RAM_C0DD_), a
    call    _LABEL_2AFF_
    call    _LABEL_1DED_
    ld      a, $FF
    ld      (Alex_Health), a
    call    _LABEL_7006_
    call    load_powerup_and_hud_gfx
    call    _LABEL_8A7_
    call    load_level_tilesets
    call    _LABEL_C71_
    ld      a, $03
    ld      (Frame2_Select), a
    ld      hl, _DATA_F720_
    ld      de, _RAM_C500_
    ld      c, $02
    call    decompress_interleaved_rle
    ld      hl, $C500
    ld      (_RAM_C0C0_), hl
    ld      a, $03
    ld      (GameMode), a
    ld      a, $81
    ld      (Sound_MusicTrigger), a
    jp      enable_display

_LABEL_9DD_:
    ld      de, $8066
    rst     $28    ; vdp_write_control
    ld      a, (_RAM_C050_)
    neg
    ld      (VDPRegister8), a
    ld      e, a
    ld      d, $88
    rst     $28    ; vdp_write_control
    ld      a, (_RAM_C062_)
    ld      (VDPRegister9), a
    ld      e, a
    ld      d, $89
    rst     $28    ; vdp_write_control
    ld      a, (_RAM_C076_)
    or      a
    ret     nz
    xor     a
    ld      hl, _RAM_C04F_
    ld      (hl), a
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    srl     h
    rr      l
    srl     h
    rr      l
    srl     h
    rr      l
    ld      b, $20
-:
    push    hl
    push    bc
    call    _LABEL_A71_
    pop     bc
    pop     hl
    inc     hl
    djnz    -
    ret


_LABEL_A1F_:
    ld      (_RAM_C052_), de
    ld      hl, (_RAM_C04F_)
    ld      a, (_RAM_C051_)
    add     hl, de
    bit     7, d
    ld      de, (_RAM_C055_)
    jr      nz, +
    ld      bc, $0020
    adc     a, b
    cp      e
    jp      c, ++
-:
    ld      hl, $0000
    ld      (_RAM_C052_), hl
    scf
    ret

+:
    ld      bc, $0001
    ccf
    sbc     a, b
    cp      d
    jp      m, -
++:
    ld      (_RAM_C051_), a
    ld      a, h
    neg
    ld      (VDPRegister8), a
    ld      a, (_RAM_C050_)
    ld      (_RAM_C04F_), hl
    xor     h
    and     $04
    ret     z
    ld      (_RAM_C05C_), a
    ld      hl, (_RAM_C050_)
    srl     h
    rr      l
    srl     h
    rr      l
    srl     h
    rr      l
    add     hl, bc
_LABEL_A71_:
    ld      a, l
    add     a, a
    and     $3E
    exx
    ld      hl, (_RAM_C062_)
    ld      h, $00
    add     hl, hl
    add     hl, hl
    add     hl, hl
    ld      d, $D6
    ld      e, a
    add     hl, de
    ld      (_RAM_C05D_), hl
    ex      de, hl
    ld      bc, _RAM_D000_
    and     $02
    jr      z, +
    inc     bc
    inc     bc
+:
    exx
    srl     h
    rr      l
    add     hl, hl
    add     hl, hl
    ld      e, l
    ld      d, h
    add     hl, hl
    add     hl, de
    ld      a, (_RAM_DD2D_)
    or      a
    ld      de, $CA00
    jr      z, +
    ld      de, $C940
+:
    add     hl, de
    ld      a, (_RAM_C07C_)
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      b, $0C
-:
    ld      a, (hl)
    exx
    ld      l, a
    ld      h, $00
    add     hl, hl
    add     hl, hl
    add     hl, hl
    add     hl, bc
    ldi
    inc     bc
    ldi
    inc     bc
    inc     hl
    inc     hl
    ld      a, $3E
    add     a, e
    ld      e, a
    jr      nc, +
    inc     d
+:
    ldi
    inc     bc
    ld      a, (hl)
    ld      (de), a
    ld      a, $3F
    add     a, e
    ld      e, a
    jr      nc, +
    inc     d
+:
    ld      a, d
    cp      $DD
    jr      nz, +
    ld      d, $D6
+:
    exx
    inc     hl
    ld      a, (_RAM_C07C_)
    dec     b
    cp      b
    jr      nz, +
    neg
    add     a, $BE
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    inc     b
    djnz    -
    or      a
    ret

; TODO: looks like this could be updating a column of mapping data in VRAM
_LABEL_AF3_:
    ld      a, (VDPRegister8)
    out     ($BF), a
    ld      a, $88
    out     ($BF), a
    ld      hl, _RAM_C05C_
    ld      a, (hl)
    or      a
    ret     z
    ld      de, $003F
    ld      (hl), d
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ld      bc, $18BF
-:
    out     (c), l
    ld      a, h
    sub     $5E
    cp      $7F
    jr      c, +
    sub     $07
+:
    out     ($BF), a
    ld      a, (hl)
    inc     hl
    nop
    out     ($BE), a
    ld      a, (hl)
    add     hl, de
    out     ($BE), a
    ld      a, h
    cp      $DD
    jr      c, +
    sub     $07
    ld      h, a
+:
    djnz    -
    ret

_LABEL_B2F_:
    ld      a, (_RAM_C062_)
    ld      (VDPRegister9), a
    ld      e, a
    ld      d, $89
    rst     $28    ; vdp_write_control
    ld      a, (_RAM_C076_)
    or      a
    ret     z
    xor     a
    ld      hl, _RAM_C061_
    ld      (hl), a
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ld      a, h
    or      a
    jr      z, +
    xor     a
-:
    add     a, $1C
    dec     h
    jr      nz, -
+:
    ld      h, $00
    srl     l
    srl     l
    srl     l
    add     a, l
    jr      nc, +
    inc     h
+:
    ld      l, a
    ld      b, $18
-:
    push    hl
    push    bc
    call    _LABEL_BDB_
    pop     bc
    pop     hl
    inc     hl
    djnz    -
    ret

_LABEL_B6C_:
    ld      (_RAM_C067_), de
    call    _LABEL_7B9_
    ld      (_RAM_C067_), de
    ld      a, d
    or      e
    jr      nz, +
    scf
    ret

+:
    ld      hl, (_RAM_C061_)
    add     hl, de
    bit     7, d
    jr      nz, ++
    ld      bc, $E000
    or      a
    sbc     hl, bc
    jr      nc, +
    add     hl, bc
+:
    ccf
    ld      a, (_RAM_C063_)
    ld      bc, $0018
    adc     a, b
    jr      +++

++:
    ld      bc, $E000
    or      a
    sbc     hl, bc
    jr      c, +
    ld      bc, $C000
+:
    add     hl, bc
    ld      a, (_RAM_C063_)
    ld      bc, $0000
    ccf
    sbc     a, b
+++:
    ld      (_RAM_C063_), a
    ld      a, h
    ld      (VDPRegister9), a
    ld      a, (_RAM_C062_)
    ld      (_RAM_C061_), hl
    xor     h
    and     $04
    ret     z
    ld      (_RAM_C071_), a
    ld      hl, (_RAM_C062_)
    ld      a, h
    or      a
    jr      z, +
    xor     a
-:
    add     a, $1C
    dec     h
    jr      nz, -
+:
    ld      h, $00
    srl     l
    srl     l
    srl     l
    add     a, l
    jr      nc, +
    inc     h
+:
    ld      l, a
    add     hl, bc
_LABEL_BDB_:
    push    hl
    ld      de, 28
-:
    or      a
    sbc     hl, de
    jr      nc, -
    add     hl, de
    add     hl, hl
    add     hl, hl
    add     hl, hl
    add     hl, hl
    add     hl, hl
    add     hl, hl
    ld      (_RAM_C072_), hl
    ld      a, l
    ld      de, ScreenMapBuffer
    add     hl, de
    ex      de, hl
    ld      bc, _RAM_D000_
    and     $40
    jr      z, +
    inc     bc
    inc     bc
    inc     bc
    inc     bc
+:
    exx
    pop     hl
    ld      de, $0018
    xor     a
-:
    sbc     hl, de
    inc     a
    jr      nc, -
    add     hl, de
    srl     h
    rr      l
    ex      de, hl
    ld      bc, $00C0
    ld      h, b
    ld      l, b
    dec     a
    jr      z, +
-:
    add     hl, bc
    dec     a
    jr      nz, -
+:
    add     hl, de
    ld      de, $CA00
    add     hl, de
    ld      b, $10
-:
    ld      a, (hl)
    exx
    ld      l, a
    ld      h, $00
    add     hl, hl
    add     hl, hl
    add     hl, hl
    add     hl, bc
    ldi
    inc     bc
    ldi
    inc     bc
    ldi
    inc     bc
    ld      a, (hl)
    ld      (de), a
    inc     de
    exx
    ld      de, $000C
    add     hl, de
    djnz    -
    or      a
    ret

_LABEL_C41_:
    ; set the background y-scroll register
    ld      a, (VDPRegister9)
    out     (Ports_VDP_Control), a
    ld      a, $89
    out     (Ports_VDP_Control), a

    ld      hl, _RAM_C071_
    ld      a, (hl)
    or      a
    ret     z
    ld      (hl), $00

    ; read from _RAM_C072_
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)

    ld      hl, ScreenMapBuffer
    add     hl, de
    ld      bc, ($20 << 8) | Ports_VDP_Control
-:
    out     (c), l
    ld      a, h
    sub     94
    out     (Ports_VDP_Control), a
    ld      a, (hl)
    inc     hl
    nop
    out     (Ports_VDP_Data), a
    ld      a, (hl)
    out     (Ports_VDP_Data), a
    inc     hl
    nop
    djnz    -
    ret

; 7th entry of Jump Table from 135 (indexed by unknown)
_LABEL_C71_:
    call    _LABEL_887_
    call    _LABEL_89F_
    ld      a, (_RAM_C229_)
    res     0, a
    ld      (_RAM_C229_), a
    xor     a
    ld      (_RAM_C226_), a
    ld      a, (_RAM_C222_)
    ld      hl, $0000
    bit     1, a
    jr      z, +
    ld      hl, (Alex_SpeedX)
+:
    ld      (_RAM_DD30_), hl
    ld      a, $07
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, $8821
    rst     $18    ; get_array_index
    ld      a, (_RAM_C230_)
    and     $3F
    rst     $18    ; get_array_index
    ld      a, (hl)
    ld      (_RAM_C04E_), a
    inc     hl
    ld      a, (hl)
    or      a
    jp      z, _LABEL_DE5_
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      (_RAM_C078_), hl
    ld      h, (hl)
    ld      l, a
    ld      (_RAM_C07A_), hl
    ld      a, (_RAM_C076_)
    ld      (_RAM_DD2F_), a
    call    _LABEL_E68_
    inc     a
    ld      (_RAM_C076_), a
    ld      a, $08
    ld      (_RAM_C06A_), a
    ld      a, $07
    ld      (GameMode), a
    ret

; 8th entry of Jump Table from 135 (indexed by unknown)
_LABEL_CD2_:
    xor     a
    ld      hl, (_RAM_C07A_)
    ld      de, (_RAM_C062_)
    sbc     hl, de
    ld      e, a
    jr      z, ++++
    ld      a, l
    jr      c, +
    cp      $02
    ld      d, $02
    jr      nc, +++
    jr      ++

+:
    cp      $FE
    ld      d, $FE
    jr      c, +++
++:
    ld      d, a
+++:
    or      a
    ld      hl, (_RAM_C20A_)
    sbc     hl, de
    ld      (_RAM_C20A_), hl
    call    _LABEL_B6C_
    call    _LABEL_E95_
    jp      _LABEL_DAD_

++++:
    ld      a, (_RAM_DD2F_)
    ld      (_RAM_C076_), a
    call    _LABEL_818_
    ld      de, _RAM_DD10_
    ld      bc, $000C
    ldir
    ld      a, $07
    ld      (Frame2_Select), a
    ld      hl, (_RAM_C078_)
    inc     hl
    ld      a, (hl)
    cp      $FF
    jr      nz, +
    ld      (_RAM_DD2D_), a
    inc     a
+:
    ld      (_RAM_C050_), a
    inc     hl
    ld      a, (hl)
    ld      (_RAM_C051_), a
    inc     hl
    ld      b, (hl)
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      (_RAM_C078_), hl
    ld      h, (hl)
    ld      l, a
    ld      a, b
    ld      (Frame2_Select), a
    ld      de, _RAM_CA00_
    ld      c, $0C
    call    decompress_interleaved_rle
    xor     a
    ld      c, a
    call    _LABEL_850_
    ld      (_RAM_DD1E_), hl
    ld      de, _RAM_DD20_
    ld      bc, $000C
    ldir
    ld      a, (_RAM_C20D_)
    bit     7, a
    jr      nz, +
    ld      hl, _RAM_DD10_
    ld      de, (_RAM_DD1E_)
    ld      bc, $000C
    ldir
+:
    call    _LABEL_E68_
    ld      a, $07
    ld      (_RAM_C055_), a
    ld      a, (_RAM_C20D_)
    rlca
    ld      hl, $0400
    ld      de, $FCA0
    jr      c, +
    ld      h, $FC
    ld      de, $0340
+:
    ld      (_RAM_C07A_), hl
    ld      (Alex_SpeedX), de
    ld      a, $08
    ld      (GameMode), a
    ret

; 9th entry of Jump Table from 135 (indexed by unknown)
_LABEL_D8D_:
    ld      a, (_RAM_C077_)
    cp      $40
    jr      z, +
    inc     a
    ld      (_RAM_C077_), a
    ld      hl, (_RAM_C20C_)
    ld      de, (Alex_SpeedX)
    add     hl, de
    ld      de, (_RAM_C07A_)
    ld      (_RAM_C20C_), hl
    call    _LABEL_A1F_
    call    _LABEL_E95_
_LABEL_DAD_:
    ld      a, $04
    jp      wait_for_c041

+:
    ld      a, $03
    ld      (GameMode), a
    call    _LABEL_E68_
    ld      (_RAM_DD2D_), a
    ld      hl, (_RAM_C078_)
    call    _LABEL_EAB_
    ld      hl, _RAM_DD20_
    ld      de, (_RAM_DD1E_)
    ld      bc, $000C
    ldir
    call    _LABEL_F16_
    call    _LABEL_1291_
    call    _LABEL_4CE1_
    xor     a
    ld      (_RAM_DDFE_), a
    ld      a, $07
    ld      (Frame2_Select), a
    call    _LABEL_1D5CF_
    ret

_LABEL_DE5_:
    xor     a
    ld      (_RAM_C222_), a
    ld      (_RAM_C229_), a
    call    _LABEL_2AFF_
    ld      a, (_RAM_C230_)
    and     $3F
    ld      (_RAM_DD4F_), a
    inc     hl
    ld      b, (hl)
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      (_RAM_C078_), hl
    ld      h, (hl)
    ld      l, a
    ld      a, b
    ld      (Frame2_Select), a
    ld      de, _RAM_CA00_
    ld      c, $0C
    call    decompress_interleaved_rle
    call    _LABEL_E68_
    ld      hl, (_RAM_C078_)
    inc     hl
    ld      a, (hl)
    ld      (_RAM_C20D_), a
    inc     hl
    ld      a, (hl)
    ld      (_RAM_C20B_), a
    call    _LABEL_EAB_
    ld      a, (_RAM_DD2E_)
    or      a
    call    z, _LABEL_66F_
    call    _LABEL_9DD_
    call    _LABEL_B2F_
    call    _LABEL_1291_
    call    _LABEL_4CE1_
    xor     a
    ld      (_RAM_DDFE_), a
    call    copy_screenmap_to_vram
    ld      a, (_RAM_DD0B_)
    or      a
    jr      nz, +
    call    load_stage_palettes
+:
    call    _LABEL_F16_
    call    _LABEL_E95_
    xor     a
    ld      (_RAM_DD2E_), a
    ld      a, $03
    ld      (GameMode), a
    ld      a, $04
    ld      (_RAM_C081_), a
    call    wait_for_c041
    call    _LABEL_1C72_
    ld      a, $07
    ld      (Frame2_Select), a
    call    _LABEL_1D5CF_
    jp      enable_display

_LABEL_E68_:
    call    _LABEL_EEB_
    ld      a, $07
    ld      (Frame2_Select), a
    xor     a
    ld      l, a
    ld      h, l
    ld      (_RAM_C04F_), a
    ld      (_RAM_C061_), a
    ld      (_RAM_C064_), a
    ld      (_RAM_C076_), a
    ld      (_RAM_C077_), a
    ld      (_RAM_C230_), a
    ld      (_RAM_C052_), hl
    ld      (_RAM_C067_), hl
    ld      (_RAM_C09A_), hl
    ld      hl, (_RAM_DD30_)
    ld      (Alex_SpeedX), hl
    ret

_LABEL_E95_:
    ld      ix, Alex_State
    call    use_default_working_sat
    bit     OBJECT_F34_DISABLE_ANIM, (ix+34)
    jr      z, +
    call    alex_update_anim_frame
+:
    call    _LABEL_5061_
    jp      disable_sprites_current_working_sat

_LABEL_EAB_:
    inc     hl
    ld      a, (hl)
    ld      (_RAM_C076_), a
    inc     hl
    ld      de, _RAM_C050_
    ldi
    ldi
    ld      de, _RAM_C062_
    ldi
    ldi
    inc     de
    ldi
    ldi
    ld      a, (hl)
    ld      (_RAM_C055_), a
    ld      (_RAM_C06A_), a
    inc     hl
    ld      a, (hl)
    ld      (_RAM_C056_), a
    ld      (_RAM_C06B_), a
    inc     hl
    ld      a, (hl)
    ld      (_RAM_C209_), a
    ld      a, (_RAM_C051_)
    ld      (_RAM_C219_), a
    ld      a, (_RAM_C066_)
    ld      (_RAM_C216_), a
    ld      hl, (_RAM_C20A_)
    ld      (_RAM_C214_), hl
    ret

_LABEL_EEB_:
    ld      a, $0D
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, $A5E4
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    ld      a, (hl)
    or      a
    ret     z
    ld      b, a
-:
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    ld      a, (de)
    or      a
    jr      z, +
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    inc     hl
    ld      a, (hl)
    ld      (de), a
    jr      ++

+:
    inc     hl
    inc     hl
    inc     hl
++:
    djnz    -
    ret

_LABEL_F16_:
    ld      a, (StageNumber)
    ld      hl, $0F3D
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    inc     hl
    ld      (_RAM_DDFB_), de
    ld      a, (hl)
    inc     hl
    ld      (_RAM_DD36_), a
    ld      a, $0D
    ld      (Frame2_Select), a
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ld      de, $7580
    jp      decode_tiles_to_vram

; Data from F3D to FB4 (120 bytes)
.db $45 $0F $4F $0F $5F $0F $6F $0F $85 $0F $85 $0F $85 $0F $85 $0F
.db $85 $0F $8A $0F $8A $0F $8F $0F $94 $0F $94 $0F $94 $0F $94 $0F
.db $94 $0F $99 $0F $99 $0F $99 $0F $99 $0F $99 $0F $99 $0F $99 $0F
.db $99 $0F $9E $0F $9E $0F $9E $0F $A3 $0F $9E $0F $A8 $0F $A8 $0F
.db $A8 $0F $A8 $0F $A8 $0F $A8 $0F $AD $0F $12 $5C $92 $AD $0F $27
.db $A8 $93 $AD $0F $4D $61 $95 $B1 $0F $1C $4A $97 $AD $0F $1E $F6
.db $98 $AD $0F $48 $DD $9B $AD $0F $01 $9D $9A $AD $0F $18 $9D $9A
.db $01 $01 $01 $01 $C1 $C1 $C1 $C1


; ==============================================================================
;  void load_powerup_and_hud_gfx(void)
; ------------------------------------------------------------------------------
;  Decompresses the powerup and HUD tile art into VRAM at $2280
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
; 3rd entry of Jump Table from 135 (indexed by unknown)
load_powerup_and_hud_gfx:                                           ; $0FB5
    ld      a, (_RAM_DDFF_)
    or      a
    ret     nz
    ld      a, :GFX_Powerup_and_HUD
    ld      (Frame2_Select), a
    ld      hl, GFX_Powerup_and_HUD
    ld      de, $4280
    jp      decode_tiles_to_vram


; ==============================================================================
;  void mode_handler_enter_pause_menu(void)
; ------------------------------------------------------------------------------
;  Game state handler for the transition into the pause menu.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
; 4th entry of Jump Table from 135 (indexed by unknown)
; FIXME: this isn't right. It's called each frame
mode_handler_enter_pause_menu:                                      ; $0FC8
    ld      a, (_RAM_C0DD_)
    or      a
    jr      z, +

    ld      a, (InputFlagsChanged)
    and     BTN_1 | BTN_2
    jp      nz, _LABEL_1089_

    ; this is the demo sequence handler
    ld      de, (_RAM_C0C0_)
    inc     de
    inc     de
    ld      hl, $CA00
    or      a
    sbc     hl, de
    jp      z, _LABEL_1089_
    jp      c, _LABEL_1089_
    ld      (_RAM_C0C0_), de
    ex      de, hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    ld      (InputFlags), de
+:
    ld      a, (_RAM_C0DE_)
    or      a
    jr      z, +
    ld      hl, $0808
    ld      (InputFlags), hl
+:
    ld      a, (_RAM_C212_)
    cp      $01
    jr      nz, _LABEL_100F_
    ld      a, (StageNumber)
    cp      $03
    jp      z, _LABEL_1358_
_LABEL_100F_:
    ld      a, (_RAM_C230_)
    or      a
    jr      z, _LABEL_1028_
    rlca
    jp      c, _LABEL_1387_
    rlca
    jr      nc, _LABEL_1022_
    ld      a, (InputFlags)
    dec     a
    jr      nz, _LABEL_1028_
_LABEL_1022_:
    ld      a, Mode_Level
    ld      (GameMode), a
    ret

_LABEL_1028_:
    call    _LABEL_1CDB_
    call    _LABEL_6D26_
    call    _LABEL_6BB6_
    call    _LABEL_180C_
    call    _LABEL_1A47_
    call    _LABEL_10AE_
    ld      hl, _RAM_C0AE_
    ld      a, (hl)
    or      a
    jr      z, ++
    ld      (hl), $00
    ld      a, $03
    ld      (Alex_Health), a
    ld      hl, _RAM_DD07_
    ld      a, (hl)
    cp      $03
    jr      nz, +
    ld      a, $01
    ld      (_RAM_DD0B_), a
    ld      (_RAM_DD2E_), a
+:
    dec     (hl)
    jp      _LABEL_1C2C_

++:
    ld      hl, _RAM_C0AF_
    ld      a, (hl)
    or      a
    jr      z, +
    ld      (hl), $00
    ld      a, $11
    ld      (GameMode), a
    call    disable_sprites
    call    _LABEL_66F_
+:
    ld      a, $02
    call    wait_for_c041
    call    _LABEL_4D29_
    call    _LABEL_2CBB_
    call    _LABEL_18CD_
    call    _LABEL_180C_
    call    _LABEL_10AE_
    ld      a, $03
    jp      wait_for_c041

_LABEL_1089_:
    ld      a, $00
    ld      (GameMode), a
    call    disable_sprites
    call    _LABEL_66F_
    ld      hl, Alex_State
    ld      de, _RAM_C201_
    ld      bc, $002D
    ld      (hl), b
    ldir
    xor     a
    ld      (_RAM_DD00_), a
    ld      (_RAM_DD03_), a
    ld      (_RAM_C0DD_), a
    ld      (_RAM_DD04_), a
    ret

_LABEL_10AE_:
    call    use_default_working_sat
    call    _LABEL_4E7E_
    call    _LABEL_6D08_
    call    _LABEL_66CB_
    call    _LABEL_67E5_
    call    update_hud_icons
    jp      disable_sprites_current_working_sat


; ==============================================================================
;  void use_default_working_sat(void)
; ------------------------------------------------------------------------------
;  Switches the current sprite attribute table to the default working copy.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
use_default_working_sat:                                            ; $10C3
    ld      hl, WorkingSAT
    ld      (WorkingSATPtr), hl
    ld      hl, WorkingSAT_HPos
    ld      (WorkingSAT_HPosPtr), hl
    ret


; ==============================================================================
;  void disable_sprites_current_working_sat(void)
; ------------------------------------------------------------------------------
;  Disables the sprites in the current working SAT.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
disable_sprites_current_working_sat:                                ; $10D0
    ld      hl, (WorkingSATPtr)
    ld      (hl), $D0
    ret


; ==============================================================================
;  void copy_screenmap_to_vram(void)
; ------------------------------------------------------------------------------
;  Writes the screen map data from work RAM to the VDP.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
copy_screenmap_to_vram:                                             ; $10D6
    ld      hl, ScreenMapBuffer
    ld      de, (VDPCTL_WriteAddress << 8) | $3800
    ld      bc, $0700
    jp      copy_to_vram


; Data from 10E2 to 1105 (36 bytes)
.db $78 $06 $00 $C5 $D5 $ED $B0 $D1 $EB $0E $40 $09 $EB $C1 $3D $20
.db $F2 $C9 $78 $06 $00 $C5 $D5 $ED $B0 $D1 $EB $0E $0C $09 $EB $C1
.db $3D $20 $F2 $C9


; ==============================================================================
;  void update_hud_icons(void)
; ------------------------------------------------------------------------------
;  Updates the x/y position and char codes for each of the HUD elements.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
update_hud_icons:                                                   ; $1106
    ld      a, (Alex_Health)
    ld      hl, hud_vpos_attribs
    ld      de, (WorkingSATPtr)
    ld      b, $00
    or      a
    jr      z, +
    ; since each sprite holds 2 energy icons we need to account for when Alex
    ; has an odd energy number. we do this by painting the sprite with only
    ; one energy icon at the bottom
    call    _update_hud_vpos

    ld      a, (Alex_Health)
    ld      hl, hud_vpos_attribs + 1
    ld      de, (WorkingSAT_HPosPtr)
    call    _update_hud_hpos_charcode
+:
    ld      a, (_RAM_DD0B_)
    or      a
    ret     z
    
    ld      a, (StageNumber)
    dec     a
    ret     z

    ld      a, (_RAM_C31E_)
    ld      hl, hud_vpos_attribs
    ld      de, (WorkingSATPtr)
    or      a
    ret     z
    call    _update_hud_vpos
    ld      a, (_RAM_C31E_)
    ld      hl, $1193
    ld      de, (WorkingSAT_HPosPtr)
    jp      _update_hud_hpos_charcode


_update_hud_vpos:
    ; Cap health at max
    cp      (MAX_ENERGY+1)
    jr      c, +
    ld      a, MAX_ENERGY
+:  inc     a
    ; divide by 2 because each sprite is 8x16. i.e. two energy icons
    ; are combined into one sprite.
    srl     a
    ld      c, a
    ldir
    ld      (WorkingSATPtr), de
    ret


_update_hud_hpos_charcode:
    cp      (MAX_ENERGY + 1)
    jr      c, +
    ld      a, MAX_ENERGY
+:  ld      c, a
    inc     c
    res     0, c
    rst     $18    ; get_array_index
    ldir
    ld      (WorkingSAT_HPosPtr), de
    ret

; Data from 116E to 11B8 (75 bytes)
hud_vpos_attribs:                                                   ; $116E
.db $08 $18 $28

; $1171
.db $7D $11
.db $7F $11 
.db $81 $11
.db $85 $11
.db $89 $11
.db $8F $11 

; $117D - hpos & char codes
.db $10 $46 
.db $10 $44 
.db $10 $44
.db $10 $46
.db $10 $44
.db $10 $44
.db $10 $44
.db $10 $44
.db $10 $46
.db $10 $44
.db $10 $44
.db $10 $44

; $1195
.db $A1 $11
.db $A3 $11
.db $A5 $11
.db $A9 $11
.db $AD $11
.db $B3 $11

; $11A1
.db $F0 $46
.db $F0 $44
.db $F0 $44
.db $F0 $46
.db $F0 $44
.db $F0 $44
.db $F0 $44
.db $F0 $44
.db $F0 $46
.db $F0 $44
.db $F0 $44
.db $F0 $44


; ==============================================================================
;  void load_level_tilesets(void)
; ------------------------------------------------------------------------------
;  Loads the tilesets & mapping data for the current level. The level numbers
;  are read from the global variables at C04D and DD07.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
load_level_tilesets:                                                ; $11B9
    ld      a, $07
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, DATA_StageTilesets
    rst     $18    ; get_array_index
    ld      a, (_RAM_DD07_)
    dec     a
    rst     $18    ; get_array_index
    ; read the bank number into B
    ld      a, (hl)
    ld      b, a
    ; read the data pointer into HL
    inc     hl
    ld      a, (hl)
    inc     hl
    push    hl
    ld      h, (hl)
    ld      l, a
    ; swap in the right bank
    ld      a, b
    ld      (Frame2_Select), a
    ; decompress the tiles into VRAM
    ld      de, $6000
    call    decode_tiles_to_vram
    pop     hl

    call    _load_level_mappings
    ret     z
_LABEL_11E1_:
    ; load the auxilliary tileset.
    ; read the bank number into B
    inc     hl
    ld      a, (hl)
    ld      b, a
    ; read the VRAM address into DE
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    ; read the data pointer into HL
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ; swap in the relevant bank and decompress into VRAM
    ld      a, b
    ld      (Frame2_Select), a
    jp      decode_tiles_to_vram


DATA_StageTilesets:                                                 ; $11F4
; Data from 11F4 to 1290 (157 bytes)
.dw DATA_Stage1_Tilesets
.dw DATA_Stage2_Tilesets
.dw DATA_Stage3_Tilesets
.dw DATA_Stage4_Tilesets

DATA_Stage1_Tilesets:                                               ; $11FC
.dw DATA_Stage1_1_Tilesets
.dw DATA_Stage1_1_Tilesets
DATA_Stage2_Tilesets:                                               ; $1200
.dw DATA_Stage2_1_Tilesets
.dw DATA_Stage2_2_Tilesets
DATA_Stage3_Tilesets:                                               ; $1204
.dw DATA_Stage3_1_Tilesets
.dw DATA_Stage3_2_Tilesets
DATA_Stage4_Tilesets:                                               ; $1208
.dw DATA_Stage4_1_Tilesets
.dw DATA_Stage4_2_Tilesets


DATA_Stage1_1_Tilesets:                                               ; $120C
.db :GFX_Stage1_1_Tiles
    .dw GFX_Stage1_1_Tiles
.db :UNK_Stage1_1_mappings
    .dw UNK_Stage1_1_mappings
.db :GFX_Stage1_Enemies1
    .dw GFX_Stage1_Enemies1
.dw $0500                       ; mirror $500 bytes of tile data
.dw $4E00                       ; to $E00
.db $01 
.db :GFX_Stage1_Enemies2
    .dw $5300
    .dw GFX_Stage1_Enemies2 


DATA_Stage2_1_Tilesets:                                             ; $121F
.db :GFX_Stage2_1_Tiles
    .dw GFX_Stage2_1_Tiles
.db :UNK_Stage2_1_mappings
    .dw UNK_Stage2_1_mappings
.db :GFX_Stage2_Enemies1
    .dw GFX_Stage2_Enemies1
.dw $0980 
.dw $5280
.db $01
.db :GFX_Stage2_Enemies2
    .dw $5BC0
    .dw GFX_Stage2_Enemies2


DATA_Stage2_2_Tilesets:                                             ; $1232
.db :GFX_Stage2_2_Tiles
    .dw GFX_Stage2_2_Tiles
.db :UNK_Stage2_2_mappings
    .dw UNK_Stage2_2_mappings
.db :GFX_Stage2_Enemies1
    .dw GFX_Stage2_Enemies1
.dw $0980
.dw $5280
.db $01
.db :GFX_Stage2_Enemies2
    .dw $5BC0
    .dw GFX_Stage2_Enemies2


DATA_Stage3_1_Tilesets:                                             ; $1245
.db :GFX_Stage3_1
    .dw GFX_Stage3_1
.db :UNK_Stage3_1_mappings
    .dw UNK_Stage3_1_mappings
.db :GFX_Stage3_1_Enemies1
    .dw GFX_Stage3_1_Enemies1
.dw $0740
.dw $5040
.db $01
.db :GFX_Stage3_1_Enemies2
    .dw $5A40
    .dw GFX_Stage3_1_Enemies2


DATA_Stage3_2_Tilesets:                                             ; $1258
.db :GFX_Stage3_2
    .dw GFX_Stage3_2
.db :UNK_Stage3_2_mappings
    .dw UNK_Stage3_2_mappings
.db :GFX_Stage3_2_Enemies1
    .dw GFX_Stage3_2_Enemies1
.dw $0040
.dw $4940
.db $01
.db :GFX_Stage3_2_Enemies2
    .dw $4900
    .dw GFX_Stage3_2_Enemies2


DATA_Stage4_1_Tilesets:                                             ; $126B
.db :GFX_Stage4_1
    .dw GFX_Stage4_1
.db :UNK_Stage4_1_mappings
    .dw UNK_Stage4_1_mappings
.db :GFX_Stage4_1_Enemies1
    .dw GFX_Stage4_1_Enemies1
.dw $0900
.dw $5200
.db $01
.db :GFX_Stage4_1_Enemies2  
    .dw $5B80
    .dw GFX_Stage4_1_Enemies2


DATA_Stage4_2_Tilesets:                                             ; $127E
.db :GFX_Stage4_2
    .dw GFX_Stage4_2
.db :UNK_Stage4_2_mappings
    .dw UNK_Stage4_2_mappings
.db :GFX_Stage4_2_Enemies1
    .dw GFX_Stage4_2_Enemies1
.dw $0040
.dw $4940
.db $01
.db :GFX_Stage4_2_Enemies2
    .dw $4900
    .dw GFX_Stage4_2_Enemies2


_LABEL_1291_:
    ld      a, $07
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, $8000
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    ld      a, (hl)
    cp      $FF
    ret     z
    ld      de, _RAM_DD50_
    call    _LABEL_12EB_
    ld      a, (hl)
    or      a
    jr      z, +
    ld      de, _RAM_DD6E_
    call    _LABEL_12EB_
+:
    ld      ix, _RAM_DD50_
    ld      b, $0A
-:
    ld      a, (ix+4)
    or      a
    jr      z, ++
    push    bc
    ld      a, (_RAM_C050_)
    ld      b, a
    ld      a, (ix+3)
    sub     b
    ld      (ix+3), a
    jr      nc, +
    dec     (ix+0)
+:
    ld      a, (_RAM_C065_)
    ld      b, a
    ld      a, (ix+2)
    sub     b
    ld      (ix+2), a
    jr      nc, +
    dec     (ix+0)
+:
    pop     bc
++:
    ld      de, $0006
    add     ix, de
    djnz    -
    ret

_LABEL_12EB_:
    ld      b, a
    inc     hl
-:
    push    bc
    ld      a, (_RAM_C076_)
    or      a
    ld      a, (_RAM_C219_)
    jr      z, +
    ld      a, (_RAM_C216_)
+:
    ld      b, a
    ld      a, (hl)
    sub     b
    ld      (de), a
    inc     hl
    inc     de
    inc     de
    ld      bc, $0004
    ldir
    pop     bc
    djnz    -
    ret

_load_level_mappings:                                               ; $130A
    ; TODO: why do this when the bank is replaced later?
    ld      a, $07
    ld      (Frame2_Select), a
    inc     hl
    ; read the bank number into B
    ld      a, (hl)
    ld      b, a
    ; read the data pointer into HL
    inc     hl
    ld      a, (hl)
    inc     hl
    push    hl
    ld      h, (hl)
    ld      l, a
    ; swap in the relevant bank
    ld      a, b
    ld      (Frame2_Select), a
    ; decompress the mappings into memory
    ld      de, _RAM_D000_
    ld      c, $02
    call    decompress_interleaved_rle

    pop     hl
    ld      a, $07
    ld      (Frame2_Select), a
    inc     hl

_load_enemy_tiles:                                                  ; $132B
    ; read the bank number into B
    ld      a, (hl)
    ld      b, a
    ; read the data pointer into HL
    inc     hl
    ld      a, (hl)
    inc     hl
    push    hl
    ld      h, (hl)
    ld      l, a
    ; swap in the correct bank and copy the tiles to VRAM
    ld      a, b
    ld      (Frame2_Select), a
    ld      de, $4900
    call    decode_tiles_to_vram

    pop     hl
    ld      a, $07
    ld      (Frame2_Select), a
    ; horizontally mirror the enemy tiles.
    ; read the tile byte count into BC
    inc     hl
    ld      c, (hl)
    inc     hl
    ld      b, (hl)
    ; read the VRAM destination address into HL
    inc     hl
    ld      a, (hl)
    inc     hl
    push    hl
    ld      h, (hl)
    ld      l, a
    ; mirror the tile data from $4900
    ld      de, $4900
    call    vdp_mirror_region
    pop     hl
    inc     hl
    ld      a, (hl)
    or      a
    ret


_LABEL_1358_:
    ld      a, (_RAM_C04E_)
    cp      $05
    jp      nc, _LABEL_100F_
    ld      hl, _DATA_137C_
    or      a
    jr      z, +
    ld      hl, $1382
+:
    ld      a, (_RAM_C219_)
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    ld      (_RAM_C230_), a
    xor     a
    ld      (_RAM_C212_), a
    jp      _LABEL_1022_

; Data from 137C to 1386 (11 bytes)
_DATA_137C_:
.db $02 $02 $03 $04 $05 $06 $0D $0E $0F $10 $11

_LABEL_1387_:
    ld      a, (_RAM_C230_)
    cp      $FB
    jp      nc, _LABEL_1C12_
    and     $7F
    ld      hl, $1397
    jp      call_jump_table

; Jump Table from 1397 to 13C6 (24 entries, indexed by unknown)
_DATA_1397_:
.dw _LABEL_13C7_ _LABEL_13D3_ _LABEL_13D8_ _LABEL_13DD_ _LABEL_13E2_ _LABEL_13EA_ _LABEL_13EF_ _LABEL_13F3_
.dw _LABEL_1403_ _LABEL_1412_ _LABEL_141C_ _LABEL_1421_ _LABEL_1426_ _LABEL_142E_ _LABEL_1432_ _LABEL_1436_
.dw _LABEL_143A_ _LABEL_143F_ _LABEL_149C_ _LABEL_14A2_ _LABEL_14A8_ _LABEL_14AE_ _LABEL_1449_ _LABEL_1450_

; 1st entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13C7_:
    ld      a, (_RAM_C300_)
    or      a
    jp      nz, _LABEL_1028_
    ld      a, $02
    jp      _LABEL_1460_

; 2nd entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13D3_:
    ld      b, $05
    jp      _LABEL_1490_

; 3rd entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13D8_:
    ld      a, $08
    jp      _LABEL_1460_

; 4th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13DD_:
    ld      b, $0F
    jp      _LABEL_1490_

; 5th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13E2_:
    ld      a, $04
    ld      (_RAM_C06A_), a
    jp      _LABEL_1028_

; 6th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13EA_:
    ld      a, $05
    jp      _LABEL_1460_

; 7th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13EF_:
    ld      b, $07
    jr      _LABEL_1414_

; 8th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_13F3_:
    ld      b, $09
--:
    ld      a, (_RAM_C20F_)
    rlca
    jp      nc, _LABEL_1459_
-:
    xor     a
    ld      (_RAM_C230_), a
    jp      _LABEL_1028_

; 9th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1403_:
    ld      a, $F7
    ld      (_RAM_C20F_), a
    ld      a, $01
    ld      (_RAM_C0B5_), a
    call    _LABEL_1C72_
    jr      -

; 10th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1412_:
    ld      b, $0A
_LABEL_1414_:
    ld      a, (_RAM_C20F_)
    rlca
    jr      c, _LABEL_1459_
    jr      -

; 11th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_141C_:
    ld      b, $0B
    jp      _LABEL_1490_

; 12th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1421_:
    ld      a, $02
    jp      _LABEL_1460_

; 13th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1426_:
    ld      a, $01
    ld      (_RAM_C0B4_), a
    jp      _LABEL_1028_

; 14th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_142E_:
    ld      b, $06
    jr      --

; 15th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1432_:
    ld      b, $11
    jr      --

; 16th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1436_:
    ld      b, $12
    jr      _LABEL_1414_

; 17th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_143A_:
    ld      a, $13
    jp      _LABEL_1460_

; 18th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_143F_:
    ld      a, $01
    ld      (_RAM_DDFF_), a
    ld      b, $18
    jp      _LABEL_1490_

; 23rd entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1449_:
    xor     a
    ld      (_RAM_C0B4_), a
    jp      _LABEL_1028_

; 24th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_1450_:
    ld      b, $44
    ld      a, (_RAM_C300_)
    or      a
    jp      nz, _LABEL_1028_
_LABEL_1459_:
    ld      a, b
    ld      (_RAM_C230_), a
    jp      _LABEL_1022_

_LABEL_1460_:
    ld      hl, _RAM_DD07_
    inc     (hl)
    ld      (_RAM_C230_), a
    ld      a, (_RAM_DDF0_)
    or      a
    jr      nz, ++
    ld      a, (_RAM_DD04_)
    or      a
    jr      nz, +
    ld      a, $88
+:
    ld      (Sound_MusicTrigger), a
    xor     a
    ld      (_RAM_DD04_), a
    ld      b, $19
    call    _LABEL_720_
++:
    xor     a
    ld      (_RAM_DDF0_), a
    call    disable_sprites
    ld      a, $09
    ld      (GameMode), a
    jp      _LABEL_66F_

_LABEL_1490_:
    ld      a, $01
    ld      (_RAM_DD0B_), a
    ld      (_RAM_DD2E_), a
    ld      a, b
    jp      _LABEL_1460_

; 19th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_149C_:
    ld      c, $14
    ld      b, $00
    jr      +

; 20th entry of Jump Table from 1397 (indexed by unknown)
_LABEL_14A2_:
    ld      c, $15
    ld      b, $01
    jr      +

; 21st entry of Jump Table from 1397 (indexed by unknown)
_LABEL_14A8_:
    ld      c, $16
    ld      b, $02
    jr      +

; 22nd entry of Jump Table from 1397 (indexed by unknown)
_LABEL_14AE_:
    ld      c, $17
    ld      b, $03
    jr      +

+:
    ld      a, (_RAM_C300_)
    or      a
    jp      nz, _LABEL_1028_
    ld      a, c
    ld      (_RAM_DD94_), a
    ld      (_RAM_DD2E_), a
    ld      (_RAM_C230_), a
    ld      a, b
    ld      (_RAM_DD08_), a
    ld      a, $0B
    ld      (GameMode), a
    jp      _LABEL_66F_

; 12th entry of Jump Table from 135 (indexed by unknown)
_LABEL_14D1_:
    ld      a, $0C
    ld      (Frame2_Select), a
    ld      a, (_RAM_DD08_)
    ld      hl, _DATA_14DF_
    jp      call_jump_table

; Jump Table from 14DF to 14E6 (4 entries, indexed by unknown)
_DATA_14DF_:
.dw _LABEL_14E7_ _LABEL_14F2_ _LABEL_14FF_ _LABEL_14F2_

; 1st entry of Jump Table from 14DF (indexed by unknown)
_LABEL_14E7_:
    ld      hl, PAL_Stage1_Boss_BG
    call    ++
    ld      hl, _DATA_151D_
    jr      +

; 2nd entry of Jump Table from 14DF (indexed by unknown)
_LABEL_14F2_:
    ld      a, $0E
    ld      (Frame2_Select), a
    ld      hl, $80B8
    ld      hl, _DATA_152A_
    jr      +

; 3rd entry of Jump Table from 14DF (indexed by unknown)
_LABEL_14FF_:
    ld      hl, PAL_Stage3_Boss_BG
    call    ++
    ld      hl, _DATA_1537_
+:
    call    _load_enemy_tiles
    jp      z, _LABEL_1022_
    call    _LABEL_11E1_
    jp      _LABEL_1022_

++:
    ld      de, _RAM_C030_
    ld      bc, $0010
    ldir
    ret

; Data from 151D to 1529 (13 bytes)
_DATA_151D_:
.db $0C $41 $8D $00 $0B $00 $54 $01 $0C $00 $5F $D5 $95

; Data from 152A to 1536 (13 bytes)
_DATA_152A_:
.db $08 $22 $A5 $40 $00 $40 $49 $01 $08 $00 $49 $2E $A5

; Data from 1537 to 153E (8 bytes)
_DATA_1537_:
.db $0C $EB $B2 $80 $0A $80 $53 $00


; ==============================================================================
;  void mode_handler_load_title_card(void)
; ------------------------------------------------------------------------------
;  Loads the tile art and screen maps for the stage title card.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
; 10th entry of Jump Table from 135 (indexed by unknown)
mode_handler_load_title_card:                                       ; $153F
    ld      a, Mode_TitleCard
    ld      (GameMode), a
    call    _LABEL_8A7_
    call    _LABEL_8C3_
    ld      a, (_RAM_DD07_)
    cp      $03
    jr      nz, +
    call    _LABEL_1DF8_
    jr      ++

+:
    call    _LABEL_1DFD_
++:
    ld      hl, _RAM_DD4E_
    ld      a, (hl)
    or      a
    ld      (hl), $00
    call    z, scorecard_load_tiles
    ; load the palette for the current stage into work RAM
    ld      a, :PAL_ScoreCardPalettes
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, DATA_StagePaletteBufferPointers
    rst     $18    ; get_array_index
    ld      de, PaletteBuffer
    ld      bc, 32
    ldir
    ; load the title card mappings
    ld      a, (StageNumber)
    ld      hl, DATA_TitleCardMappingPointers
    rst     $18    ; get_array_index
    call    decompress_screenmap_to_vram

    call    _LABEL_1785_

    ; draw the Alex picture over the top of Shinobi Kid
    ld      hl, SCR_TitleCardAlex
    ld      de, $7A8E
    ld      bc, $0404
    call    draw_screen_map_element

    ; draw the life counter
    ld      hl, DATA_LifeNumberMappings
    ld      (FontNumberMappings), hl
    ld      hl, $0202
    ld      (FontNumberMappingsSize), hl
    ld      hl, Alex_LifeCounter
    ld      de, (VDPCTL_WriteAddress << 8) | $3B16
    call    font_draw_bcd

    ; draw the score values
    ld      hl, DATA_ScoreNumberMappings
    ld      (FontNumberMappings), hl
    ld      hl, $0102
    ld      (FontNumberMappingsSize), hl
    call    scorecard_draw_score_values

    ld      a, $06
    ld      (_RAM_C0C2_), a
    ld      hl, $4000
    ld      (_RAM_C0C0_), hl
    xor     a
    ld      (_RAM_C0C7_), a
    ld      hl, $0200
    ld      (_RAM_DD92_), hl
    call    disable_sprites
    ld      a, (StageNumber)
    dec     a
    jr      c, +
    jr      z, ++
    jp      enable_display

+:
    ld      hl, _DATA_15FE_
    ld      de, $79AE
    ld      bc, $0206
    call    draw_screen_map_element
    jr      +++

++:
    ld      hl, _DATA_15F4_
    ld      de, $7B6C
    ld      bc, $0005
    call    copy_to_vram
+++:
    jp      enable_display

; Data from 15F4 to 15FD (10 bytes)
_DATA_15F4_:
.db $00 $00 $1A $00 $17 $01 $1E $00 $1B $00

; Data from 15FE to 1609 (12 bytes)
_DATA_15FE_:
.db $A4 $00 $A5 $00 $A6 $00 $B0 $00 $B1 $00 $B2 $00


; ==============================================================================
;  void decompress_screenmap_to_vram(uint8_t *compressed)
; ------------------------------------------------------------------------------
;  Decompresses a stream of mapping data and copies it to the VDP screen map.
; ------------------------------------------------------------------------------
;  In:
;    HL     - Pointer to the compressed data buffer.
; ------------------------------------------------------------------------------
decompress_screenmap_to_vram:                                       ; $160A
    ld      de, ScreenMapBuffer
    ld      c, $02
    call    decompress_interleaved_rle
    jp      copy_screenmap_to_vram


; ==============================================================================
;  void disable_sprites(void)
; ------------------------------------------------------------------------------
;  Disables all sprites in the default SAT by seting the v-pos of the first 
;  to $D0.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
disable_sprites:                                                    ; $1615
    ld      a, $D0
    ld      (WorkingSAT), a
    ret


; ==============================================================================
;  void scorecard_draw_score_values(void)
; ------------------------------------------------------------------------------
;  Draws the current score & hi-score onto the title card.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
scorecard_draw_score_values:                                        ; $161B
    ld      hl, CurrentScore
    ld      de, (VDPCTL_WriteAddress << 8) | $3D26
    call    font_draw_packed_bcd
    ld      hl, HiScore
    ld      de, (VDPCTL_WriteAddress << 8) | $3CA6
    jp      font_draw_packed_bcd


; 11th entry of Jump Table from 135 (indexed by unknown)
_LABEL_162D_:
    ld      hl, (_RAM_DD92_)
    dec     hl
    ld      (_RAM_DD92_), hl
    ld      a, l
    or      h
    jr      z, +
    ld      a, (InputFlagsChanged)
    and     $30
    jr      z, ++
+:
    ld      a, (_RAM_DD07_)
    cp      $04
    jr      nz, +++
    ld      a, (_RAM_C0C0_)
    cp      $07
    jr      nc, +++
++:
    call    _LABEL_169A_
    call    _LABEL_6D26_
    call    use_default_working_sat
    call    _LABEL_6D08_
    call    disable_sprites_current_working_sat
    ld      a, $05
    jp      wait_for_c041

+++:
    call    _LABEL_66F_
    ld      a, (_RAM_DD07_)
    cp      $04
    jr      z, ++
    ld      a, (_RAM_DD0B_)
    or      a
    jr      nz, +
    call    load_powerup_and_hud_gfx
    call    load_level_tilesets
    ; Play the stage's music
    ld      a, (StageNumber)
    add     a, $81
    ld      (Sound_MusicTrigger), a
    jp      _LABEL_100F_

+:
    ld      a, $0C
    ld      (GameMode), a
    ld      a, $87
    ld      (Sound_MusicTrigger), a
    ret

++:
    ld      a, (StageNumber)
    inc     a
    ld      (StageNumber), a
    ld      a, $0F
    ld      (GameMode), a
    ret

_LABEL_169A_:
    ld      a, (_RAM_DD07_)
    cp      $04
    ret     nz
    ld      hl, _RAM_C0C1_
    dec     (hl)
    ret     nz
    ld      (hl), $38
    dec     hl
    ld      a, (hl)
    inc     a
    cp      $07
    ret     nc
    ld      (hl), a
    ld      (_RAM_C0C7_), a
    ld      hl, $16B5
    jp      call_jump_table

; Jump Table from 16B7 to 16C2 (6 entries, indexed by unknown)
_DATA_16B7_:
.dw _LABEL_16C3_ _LABEL_16D4_ _LABEL_16F2_ _LABEL_16D4_ _LABEL_1702_ _LABEL_16E3_

; 1st entry of Jump Table from 16B7 (indexed by unknown)
_LABEL_16C3_:
    ld      a, $04
    ld      (_RAM_C240_), a
    ld      a, (Alex_Health)
    or      a
    jr      z, +
    add     a, $03
+:
    ld      b, a
    jp      _LABEL_4F5_

; 2nd entry of Jump Table from 16B7 (indexed by unknown)
_LABEL_16D4_:
    ld      hl, (_RAM_C0D7_)
    ld      a, h
    or      l
    jr      z, +
    ld      a, $91
    ld      (Sound_MusicTrigger), a
+:
    jp      _LABEL_527_

; 6th entry of Jump Table from 16B7 (indexed by unknown)
_LABEL_16E3_:
    ld      hl, (_RAM_C0D7_)
    ld      a, h
    or      l
    jr      z, +
    ld      a, $97
    ld      (Sound_MusicTrigger), a
+:
    jp      _LABEL_527_

; 3rd entry of Jump Table from 16B7 (indexed by unknown)
_LABEL_16F2_:
    ld      a, (Alex_Health)
    cp      $06
    jr      z, _LABEL_16FD_
    ld      b, $00
    jr      +

_LABEL_16FD_:
    ld      b, $08
+:
    jp      _LABEL_4F5_

; 5th entry of Jump Table from 16B7 (indexed by unknown)
_LABEL_1702_:
    ld      hl, _RAM_C22D_
    ld      a, (hl)
    or      a
    jr      z, _LABEL_16FD_
    xor     a
    ld      (hl), a
    ld      (_RAM_C0C7_), a
    ld      a, $07
    ld      (_RAM_C0C0_), a
    ret

_LABEL_1714_:
    ld      a, (GameMode)
    cp      $0A
    ret     nz
    ld      a, $0A
    ld      (Frame2_Select), a
    ld      a, (_RAM_DD07_)
    cp      $04
    jr      z, +
    ld      hl, _RAM_C0C2_
    dec     (hl)
    ret     nz
    ld      (hl), $06
    call    _LABEL_7042_
_LABEL_1730_:
    ld      bc, $0102
    jp      _LABEL_7032_

+:
    ld      hl, _RAM_C0C7_
    ld      a, (hl)
    or      a
    ret     z
    ld      (hl), $00
    ld      a, (_RAM_C0C0_)
    ld      hl, $1745
    jp      call_jump_table

; Jump Table from 1747 to 1752 (6 entries, indexed by unknown)
_DATA_1747_:
.dw _LABEL_175B_ _LABEL_1758_ _LABEL_1760_ _LABEL_1758_ _LABEL_1770_ _LABEL_1753_

; 6th entry of Jump Table from 1747 (indexed by unknown)
_LABEL_1753_:
    ld      a, $07
    ld      (_RAM_C0C0_), a
; 2nd entry of Jump Table from 1747 (indexed by unknown)
_LABEL_1758_:
    jp      scorecard_draw_score_values

; 1st entry of Jump Table from 1747 (indexed by unknown)
_LABEL_175B_:
    ld      de, $7966
    jr      ++

; 3rd entry of Jump Table from 1747 (indexed by unknown)
_LABEL_1760_:
    ld      a, (Alex_Health)
    cp      $06
    jr      z, +
    ld      hl, _RAM_C0C0_
    inc     (hl)
+:
    ld      de, $79E6
    jr      ++

; 5th entry of Jump Table from 1747 (indexed by unknown)
_LABEL_1770_:
    ld      hl, _DATA_2A88A_
    ld      de, $7A44
    ld      bc, $011A
    call    draw_screen_map_element
    ld      de, $7A66
++:
    ld      hl, $C0D6
    jp      font_draw_packed_bcd

_LABEL_1785_:
    ld      hl, _DATA_2A64E_
    ld      a, (StageNumber)
    cp      $02
    jr      z, ++
    cp      $03
    jr      z, +
    or      a
    jr      nz, ++++
    ld      hl, _DATA_2A638_
    ld      de, $7B6C
    ld      bc, $010E
    jr      +++

+:
    ld      hl, _DATA_2A646_
++:
    ld      de, $78A2
    ld      bc, $0204
+++:
    call    draw_screen_map_element
++++:
    ld      a, (_RAM_DD07_)
    cp      $04
    jr      z, +++
    ld      hl, $A6F8
    ld      (_RAM_C0CA_), hl
    ld      hl, $A6F6
    ld      (_RAM_C0CC_), hl
    ld      a, (_RAM_DD07_)
    dec     a
    jr      z, +
    ld      b, a
-:
    push    bc
    ld      a, b
    call    ++
    xor     a
    call    _LABEL_1730_
    pop     bc
    djnz    -
+:
    ld      a, (_RAM_DD07_)
++:
    ld      hl, $1804
    rst     $18    ; get_array_index
    ld      (_RAM_C0C8_), hl
    ret

+++:
    ld      hl, _DATA_2A856_
    ld      de, $7944
    ld      bc, $011A
    call    draw_screen_map_element
    ld      hl, _DATA_2A870_
    ld      de, $79C4
    ld      bc, $011A
    jp      draw_screen_map_element

; Data from 17F6 to 180B (22 bytes)
DATA_StagePaletteBufferPointers:                                    ; $17F6
.dw PAL_ScoreCard_Stage1
.dw PAL_ScoreCard_Stage2
.dw PAL_ScoreCard_Stage3
.dw PAL_ScoreCard_Stage4


DATA_TitleCardMappingPointers:                                      ; $17FE
.dw SCR_TitleCard0
.dw SCR_TitleCard1
.dw SCR_TitleCard2
.dw SCR_TitleCard3

; TODO: this points into code
.db $D2 $79
.db $D6 $79
.db $DA $79


_LABEL_180C_:
    ld      hl, (_RAM_DD4A_)
    ld      a, l
    or      h
    ret     z
    ld      a, $95
    ld      (Sound_MusicTrigger), a
    ld      a, l
    and     $BD
    ld      l, a
    ld      (_RAM_DD4A_), hl
    ex      de, hl
    ld      hl, (_RAM_DDFB_)
    inc     e
    ldi
    inc     e
    ldi
    ld      a, $3D
    add     a, e
    ld      e, a
    jr      nc, +
    inc     d
+:
    ldi
    inc     e
    ldi
    ld      a, (_RAM_C216_)
    ld      b, a
    ld      a, (_RAM_C076_)
    or      a
    ld      hl, (_RAM_C218_)
    ld      a, (_RAM_C211_)
    jr      z, ++
    ld      hl, (_RAM_C215_)
    ld      a, (_RAM_C20F_)
    bit     7, a
    jr      z, +++
    ld      a, $E6
    add     a, l
    cp      $C0
    jr      c, +
    dec     b
+:
    jr      +++

++:
    bit     7, a
    ld      de, $0008
    jr      z, +
    ld      de, $FFF8
+:
    add     hl, de
    ld      a, h
    jr      ++++

+++:
    ld      a, b
++++:
    ld      hl, $1B2C
    rst     $18    ; get_array_index
    ld      a, b
    ld      (_RAM_DD32_), a
    ld      c, l
    ld      b, h
    ld      hl, (_RAM_DD4A_)
    ld      a, l
    and     $7F
    ld      d, a
    add     a, a
    add     a, d
    ld      l, c
    ld      h, b
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      c, l
    ld      b, h
    or      a
    ld      hl, (_RAM_DD4A_)
    ld      de, ScreenMapBuffer
    sbc     hl, de
    sla     l
    rl      h
    ld      d, h
    ld      a, (_RAM_DD32_)
    ld      hl, $1B50
    rst     $18    ; get_array_index
    ld      a, d
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      l, (hl)
    ld      h, $00
    add     hl, bc
    ld      a, (_RAM_DD36_)
    ld      (hl), a
    ld      hl, (_RAM_DD4A_)
    ld      de, $5E00
    or      a
    sbc     hl, de
    ex      de, hl
    ld      b, $08
    ld      hl, _RAM_DDA0_
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), $08
    inc     hl
    ld      (hl), e
    inc     hl
    ld      (hl), d
    jr      ++

+:
    inc     hl
    inc     hl
    inc     hl
    djnz    -
++:
    ld      hl, $0000
    ld      (_RAM_DD4A_), hl
    ret

_LABEL_18CD_:
    ld      a, (StageNumber)
    ld      hl, $1BCE
    rst     $18    ; get_array_index
    ld      a, (_RAM_DD33_)
    inc     a
    ld      (_RAM_DD33_), a
    and     $0C
    rrca
    rrca
    rst     $18    ; get_array_index
    ld      (_RAM_DD34_), hl
    ld      a, (StageNumber)
    ld      hl, $1BF6
    rst     $18    ; get_array_index
    inc     hl
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      h, a
    ld      l, a
    ld      (_RAM_DDF9_), hl
    ret

+:
    dec     hl
    ld      a, (_RAM_DDF8_)
    inc     a
    ld      (_RAM_DDF8_), a
    and     $0C
    rrca
    rrca
    rst     $18    ; get_array_index
    ld      (_RAM_DDF9_), hl
    ret

_LABEL_1906_:
    ld      a, $0D
    ld      (Frame2_Select), a
    ld      a, $80
    out     ($BF), a
    ld      a, $77
    out     ($BF), a
    ld      c, $BE
    ld      hl, (_RAM_DD34_)
    call    ++
    ld      hl, (_RAM_DDF9_)
    ld      a, h
    or      l
    jr      z, +
    ld      a, $00
    out     ($BF), a
    ld      a, $75
    out     ($BF), a
    call    ++
+:
    ld      de, (_RAM_DD0C_)
    ld      a, d
    or      a
    ret     z
    ld      a, $0D
    ld      (Frame2_Select), a
    xor     a
    ld      (_RAM_DD0D_), a
    ld      hl, (_RAM_DD0E_)
    ld      bc, $0204
    jp      draw_screen_map_element

++:
.repeat 128
    outi
.endr
    ret

_LABEL_1A47_:
    ld      a, (StageNumber)
    ld      hl, $1A50
    jp      call_jump_table

; Jump Table from 1A50 to 1A57 (4 entries, indexed by unknown)
_DATA_1A50_:
.dw _LABEL_1A58_ _LABEL_1A88_ _LABEL_1ABC_ _LABEL_1AEE_

; 1st entry of Jump Table from 1A50 (indexed by unknown)
_LABEL_1A58_:
    ld      a, (_RAM_C04E_)
    or      a
    ret     nz
    ld      a, (_RAM_DD4D_)
    inc     a
    ld      (_RAM_DD4D_), a
    cp      $04
    ret     nz
    xor     a
    ld      (_RAM_DD4D_), a
    ld      a, (_RAM_DD4C_)
    inc     a
    cp      $04
    jr      nz, +
    xor     a
+:
    ld      (_RAM_DD4C_), a
    ld      hl, $1AEF
    rst     $18    ; get_array_index
    ld      de, _RAM_C02E_
    ldi
    ldi
    ld      a, $01
    ld      (_RAM_C01F_), a
    ret

; 2nd entry of Jump Table from 1A50 (indexed by unknown)
_LABEL_1A88_:
    ld      a, (_RAM_C04E_)
    or      a
    ret     nz
    ld      a, (_RAM_DD4D_)
    inc     a
    ld      (_RAM_DD4D_), a
    cp      $0A
    ret     nz
    xor     a
    ld      (_RAM_DD4D_), a
    ld      a, (_RAM_DD4C_)
    inc     a
    cp      $05
    jr      nz, +
    xor     a
+:
    ld      (_RAM_DD4C_), a
    ld      hl, $1AFF
    rst     $18    ; get_array_index
    ld      de, _RAM_C02C_
    ldi
    ldi
    ldi
    ldi
    ld      a, $01
    ld      (_RAM_C01F_), a
    ret

; 3rd entry of Jump Table from 1A50 (indexed by unknown)
_LABEL_1ABC_:
    ld      a, (_RAM_C04E_)
    or      a
    ret     nz
    ld      a, (_RAM_DD4D_)
    inc     a
    ld      (_RAM_DD4D_), a
    cp      $03
    ret     c
    xor     a
    ld      (_RAM_DD4D_), a
    ld      a, (_RAM_DD4C_)
    inc     a
    cp      $03
    jr      c, +
    xor     a
+:
    ld      (_RAM_DD4C_), a
    ld      hl, $1B1D
    rst     $18    ; get_array_index
    ld      de, _RAM_C02D_
    ldi
    ldi
    ldi
    ld      a, $01
    ld      (_RAM_C01F_), a
    ret

; 4th entry of Jump Table from 1A50 (indexed by unknown)
_LABEL_1AEE_:
    ret

; Data from 1AEF to 1C11 (291 bytes)
.db $F7 $1A $F9 $1A $FB $1A $FD $1A $0B $0F $13 $0B $13 $13 $13 $0B
.db $09 $1B $0D $1B $11 $1B $15 $1B $19 $1B $0B $03 $02 $01 $03 $02
.db $01 $00 $02 $01 $00 $00 $03 $02 $01 $00 $0B $03 $02 $01 $29 $1B
.db $26 $1B $23 $1B $38 $25 $10 $10 $38 $25 $25 $10 $38 $00 $CA $C0
.db $CA $80 $CB $40 $CC $00 $CD $C0 $CD $80 $CE $40 $CF $00 $01 $00
.db $01 $00 $01 $00 $01 $00 $01 $00 $01 $00 $01 $00 $01 $00 $01 $00
.db $01 $60 $1B $6E $1B $7C $1B $8A $1B $98 $1B $A6 $1B $B4 $1B $C2
.db $1B $00 $01 $02 $03 $04 $05 $06 $07 $08 $09 $0A $0B $0B $0B $02
.db $03 $04 $05 $06 $07 $08 $09 $0A $0B $0B $0B $00 $01 $04 $05 $06
.db $07 $08 $09 $0A $0B $00 $00 $00 $01 $02 $03 $06 $07 $08 $09 $0A
.db $0B $00 $00 $00 $01 $02 $03 $04 $05 $08 $09 $0A $0B $00 $00 $00
.db $01 $02 $03 $04 $05 $06 $07 $0A $0B $00 $00 $00 $01 $02 $03 $04
.db $05 $06 $07 $08 $09 $00 $00 $00 $01 $02 $03 $04 $05 $06 $07 $08
.db $09 $0A $0B $00 $01 $02 $03 $04 $05 $06 $07 $08 $09 $0A $0B $D6
.db $1B $DE $1B $E6 $1B $EE $1B $5C $86 $DC $86 $5C $87 $DC $87 $5C
.db $88 $DC $88 $5C $89 $DC $89 $5C $8C $DC $8C $5C $8D $DC $8D $5C
.db $8E $DC $8E $5C $8F $DC $8F $FE $1B $00 $1C $08 $1C $0A $1C $00
.db $00 $5C $8A $DC $8A $5C $8B $DC $8B $00 $00 $5C $90 $DC $90 $5C
.db $91 $DC $91

_LABEL_1C12_:
    ld      a, $0D
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, $9EAB
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    ld      a, (_RAM_C230_)
    and     $07
    sub     $03
    jp      call_jump_table

_LABEL_1C2C_:
    ld      a, $86
    ld      (Sound_MusicTrigger), a
    ld      (_RAM_DD04_), a
    ld      a, (StageNumber)
    ld      hl, $1C4A
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    ld      (_RAM_C230_), a
    jp      _LABEL_1460_

; Data from 1C4A to 1C71 (40 bytes)
.db $52 $1C $57 $1C $5F $1C $67 $1C $01 $02 $06 $04 $05 $01 $03 $06
.db $08 $09 $0A $0D $0F $01 $02 $0C $05 $06 $08 $08 $0B $01 $01 $08
.db $0C $0C $13 $92 $15 $94 $17 $18

_LABEL_1C72_:
    ld      ix, Alex_State
    ld      (ix+36), $00
    ld      a, $01
    ld      (_RAM_C081_), a
    call    _LABEL_5DBE_
    ld      hl, _RAM_C0B5_
    ld      a, (hl)
    or      a
    jp      z, alex_set_state_idle
    ld      (hl), $00
    ld      (ix+0), $04
    call    _LABEL_29A5_
    ld      (ix+35), $0F
    call    _LABEL_2AFF_
    jp      _LABEL_2B23_

; Jump Table from 1C9D to 1CDA (31 entries, indexed by unknown)
alex_state_handlers:                                                ; $1C9D
.dw alex_idle_handler
.dw alex_walk_handler
.dw alex_stopping_handler
.dw _LABEL_23BC_
.dw _LABEL_243A_
.dw _LABEL_246D_
.dw _LABEL_1EA5_
.dw _LABEL_1F38_
.dw _LABEL_1EE2_
.dw _LABEL_23DC_
.dw _LABEL_2343_
.dw _LABEL_2647_
.dw _LABEL_1FA8_
.dw _LABEL_200C_
.dw _LABEL_20B6_
.dw _LABEL_252E_
.dw _LABEL_2144_
.dw _LABEL_1FFE_
.dw _LABEL_21C9_
.dw _LABEL_2256_
.dw _LABEL_2005_
.dw _LABEL_22D1_
.dw _LABEL_2479_
.dw _LABEL_2523_
.dw _LABEL_2686_
.dw _LABEL_2553_
.dw _LABEL_257B_
.dw _LABEL_1F35_
.dw _LABEL_267A_
.dw _LABEL_258A_
.dw _LABEL_2627_

_LABEL_1CDB_:
    ld      ix, Alex_State
    call    _LABEL_1DB9_

    ld      a, (Alex_State)
    ld      bc, (InputFlags)
    ld      hl, alex_state_handlers-2
    rst     $08    ; call_jump_table
    res     7, (ix+34)
    ld      hl, (_RAM_C0A8_)
    ld      a, h
    or      l
    jr      z, +
    set     7, (ix+34)
+:
    res     3, (ix+34)
    ld      hl, (Alex_SpeedX)
    ld      de, $0200
    bit     7, h
    call    nz, negate_hl
    or      a
    sbc     hl, de
    jr      c, +
    set     3, (ix+34)
+:
    ld      hl, _RAM_C0DF_
    ld      a, (hl)
    ld      (hl), $00
    or      a
    jp      nz, _LABEL_28C0_
    call    _LABEL_1D8C_
    bit     1, (ix+34)
    jr      nz, +
    bit     1, (ix+41)
    jr      nz, +
    ld      a, (_RAM_C09A_)
    cp      $02
    jp      z, _LABEL_294B_
+:
    bit     3, (ix+41)
    jr      nz, +
    ld      a, (_RAM_C0B4_)
    or      a
    jp      nz, _LABEL_28DC_
+:
    ld      a, (_RAM_C212_)
    cp      $02
    jr      z, ++
    ld      a, (_RAM_C20B_)
    cp      $DF
    jr      nc, ++
    ld      a, (Alex_State)
    cp      $0C
    jr      z, +
    cp      $19
    jr      z, +
    ld      a, (_RAM_C231_)
    or      a
    jp      nz, _LABEL_2964_
+:
    xor     a
    ld      (_RAM_C088_), a
    ld      (_RAM_C094_), a
    ld      (_RAM_C08F_), a
    call    _LABEL_1E0F_
    bit     0, (ix+34)
    jr      nz, +
    call    _LABEL_2AEE_
+:
    jp      alex_update_anim_frame

++:
    call    _LABEL_4D4_
    jr      c, +
    ld      a, $01
    ld      (_RAM_C0AE_), a
    ret

+:
    ld      a, $01
    ld      (_RAM_C0AF_), a
    ret

_LABEL_1D8C_:
    ld      a, (Alex_State)
    cp      $19
    ret     z
    ld      hl, _RAM_C227_
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      hl, _RAM_C0AA_
    ld      a, (hl)
    inc     hl
    or      (hl)
    inc     hl
    or      (hl)
    jp      nz, _LABEL_28F3_
    ld      hl, _RAM_C224_
    ld      a, (hl)
    or      a
    ret     z
    ld      (hl), $00
    jp      _LABEL_28F3_

+:
    inc     hl
    dec     (hl)
    ret     nz
    dec     hl
    xor     a
    ld      (hl), a
    ld      (_RAM_C224_), a
    ret

_LABEL_1DB9_:
    res     0, (ix+41)
    ld      hl, _RAM_C226_
    ld      a, (hl)
    or      a
    ret     z
    ld      (hl), $00
    ld      iy, (_RAM_C0C5_)
    ld      a, (iy+0)
    or      a
    ret     z
    ld      l, (iy+10)
    ld      h, (iy+11)
    ld      de, $F000
    add     hl, de
    ld      (_RAM_C20A_), hl
    ld      l, (iy+14)
    ld      h, (iy+15)
    ld      (Alex_SpeedY), hl
    set     0, (ix+41)
    res     0, (ix+34)
    ret

_LABEL_1DED_:
    ld      hl, $0203
    ld      (Alex_Health), hl
    ld      bc, $002D
    jr      +

_LABEL_1DF8_:
    ld      bc, $002B
    jr      +

_LABEL_1DFD_:
    ld      bc, $002C
+:
    ld      hl, Alex_State
    ld      de, _RAM_C201_
    ld      (hl), b
    ldir
    call    _LABEL_1E0F_
    jp      _LABEL_2AFF_

_LABEL_1E0F_:
    ld      hl, _RAM_C0A2_
    ld      de, _RAM_C0A3_
    ld      bc, $000A
    ld      (hl), b
    ldir
    ret

; 1st entry of Jump Table from 1C9D (indexed by unknown)
alex_idle_handler:                                                  ; $1E1C
    call    _LABEL_2ADC_
    call    _LABEL_2A9E_
    call    _LABEL_2AB3_
    ld      a, c
    and     BTN_LEFT | BTN_RIGHT
    jp      nz, alex_set_state_walking
    bit     BTN_1_BIT, b
    jp      nz, _LABEL_2717_
    bit     0, (ix+34)
    jp      nz, _LABEL_2773_
    bit     BTN_2_BIT, b
    jp      nz, alex_set_state_jump
    bit     BTN_DOWN_BIT, c
    jp      nz, alex_set_state_duck
    ret

; 2nd entry of Jump Table from 1C9D (indexed by unknown)
alex_walk_handler:                                              ; $1E42
    call    _LABEL_2AB3_
    bit     BTN_2_BIT, b
    jp      nz, alex_set_state_jump
    bit     BTN_1_BIT, b
    jp      nz, _LABEL_2717_
    bit     0, (ix+34)
    jp      nz, _LABEL_2773_
    bit     BTN_DOWN_BIT, c
    jp      nz, alex_set_state_duck
    ld      (ix+6), $03
    bit     3, (ix+34)
    jr      z, +
    ld      (ix+6), $01
+:  ; if left/right are not pressed start to decelerate
    ld      a, c
    and     BTN_LEFT | BTN_RIGHT
    jp      z, alex_set_state_stopping
    and     BTN_LEFT
    jr      nz, _LABEL_1E8C_
_LABEL_1E73_:
    ld      (ix + Object_FacingRight), 1
_LABEL_1E77_:
    ld      de, $0180
    bit     7, (ix+34)
    jr      z, +
    ld      de, $0100
+:
    ld      hl, $0020
    ld      (AccelerationValue), hl
    jp      alex_move_right

_LABEL_1E8C_:
    ld      (ix + Object_FacingRight), 0
_LABEL_1E90_:
    ld      de, $0180
    bit     7, (ix+34)
    jr      z, +
    ld      de, $0100
+:
    ld      hl, $0020
    ld      (AccelerationValue), hl
    jp      alex_move_left

; 7th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_1EA5_:
    bit     4, b
    jp      nz, _LABEL_26CD_
    ld      a, c
    and     $0C
    jp      nz, _LABEL_26D6_
    res     6, (ix+34)
    call    _LABEL_2AB3_
    bit     5, b
    jr      z, +
    ld      a, (_RAM_C093_)
    cp      $03
    jp      z, _LABEL_2843_
    jp      alex_set_state_jump

+:
    bit     0, (ix+34)
    jp      nz, _LABEL_2773_
    bit     1, c
    jp      z, alex_set_state_stopping
_LABEL_1ED2_:
    set     6, (ix+34)
    ld      hl, (Alex_SpeedX)
    ld      a, h
    or      l
    ret     z
    ld      de, $0060
    jp      _LABEL_241B_

; 9th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_1EE2_:
    ld      a, c
    and     $0C
    jp      z, alex_set_state_duck
    bit     4, b
    jp      nz, _LABEL_26CD_
    res     6, (ix+34)
    call    _LABEL_2AB3_
    bit     5, b
    jr      z, +
    ld      a, (_RAM_C093_)
    cp      $03
    jp      z, _LABEL_2843_
    jp      alex_set_state_jump

+:
    bit     0, (ix+34)
    jp      nz, _LABEL_2773_
    bit     1, c
    jp      z, alex_set_state_stopping
    set     6, (ix+34)
    ld      de, $00C0
    bit     7, (ix+34)
    jr      z, +
    ld      de, $0080
+:
    ld      hl, $0020
    ld      (AccelerationValue), hl
    xor     a
    bit     2, c
    jr      nz, +
    inc     a
+:
    ld      (_RAM_C209_), a
    or      a
    jp      z, alex_move_left
    jp      alex_move_right

; 28th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_1F35_:
    call    _LABEL_29CF_
; 8th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_1F38_:
    res     6, (ix+34)
    call    _LABEL_2AB3_
    bit     5, b
    jp      nz, _LABEL_26E2_
    bit     0, (ix+34)
    jp      nz, _LABEL_26DD_
    set     6, (ix+34)
    dec     (ix+32)
    jp      nz, _LABEL_1ED2_
    jp      alex_set_state_duck

; 3rd entry of Jump Table from 1C9D (indexed by unknown)
alex_stopping_handler:                                              ; $1F58
    call    _LABEL_2AB3_
    bit     BTN_1_BIT, b
    jp      nz, _LABEL_2717_
    bit     BTN_2_BIT, b
    jp      nz, alex_set_state_jump
    bit     0, (ix+34)
    jp      nz, _LABEL_2773_
    bit     BTN_DOWN_BIT, c
    jp      nz, alex_set_state_duck
    ld      a, c
    and     BTN_LEFT | BTN_RIGHT
    jp      nz, alex_set_state_walking
    ld      (ix+6), $03
    ld      de, $0060
    bit     3, (ix+34)
    jr      z, +
    ld      (ix+6), $01
    ld      de, $0010
+:
    ld      hl, (Alex_SpeedX)
    bit     7, h
    jr      nz, +
    or      a
    sbc     hl, de
    bit     7, h
    jp      nz, alex_set_state_idle
    ld      (Alex_SpeedX), hl
    ret

+:
    add     hl, de
    bit     7, h
    jp      z, alex_set_state_idle
    ld      (Alex_SpeedX), hl
    ret

; 13th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_1FA8_:
    bit     5, b
    jr      nz, _LABEL_1FF7_
    ld      a, (_RAM_C093_)
    cp      $03
    jr      z, +++
    ld      a, (_RAM_C099_)
    or      a
    jr      z, _LABEL_1FF1_
    rrca
    jr      nc, +
    call    _LABEL_5B6D_
    jr      ++

+:
    call    _LABEL_5B5F_
++:
    call    _LABEL_20AB_
    jp      _LABEL_280F_

+++:
    bit     4, c
    jp      nz, _LABEL_27D5_
    ld      (ix+4), $00
    ld      a, c
    and     $0C
    jp      z, _LABEL_2433_
    ld      (ix+4), $01
    ld      hl, $00C0
    ld      e, $01
    and     $04
    jr      z, +
    ld      hl, $FF40
    dec     e
+:
    ld      (Alex_SpeedX), hl
    ld      (ix+9), e
    ret

_LABEL_1FF1_:
    call    _LABEL_20AB_
    jp      _LABEL_2773_

_LABEL_1FF7_:
    bit     0, c
    jr      z, _LABEL_1FF1_
    jp      _LABEL_2835_

; 18th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_1FFE_:
    dec     (ix+32)
    ret     nz
    jp      _LABEL_2773_

; 21st entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2005_:
    dec     (ix+32)
    ret     nz
    jp      _LABEL_27C8_

; 14th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_200C_:
    bit     4, c
    jp      z, _LABEL_2096_
    ld      a, c
    and     $0C
    jr      z, +++
    ld      hl, _RAM_C221_
    ld      a, (hl)
    bit     2, c
    jr      nz, +
    ld      de, $0040
    inc     a
    cp      $10
    jr      z, +++
    push    hl
    ld      hl, _RAM_C093_
    bit     1, (hl)
    pop     hl
    jr      nz, ++
    ld      a, $0F
    ld      de, $FFC0
    jr      ++

+:
    ld      de, $FFC0
    dec     a
    cp      $F0
    jr      z, +++
    push    hl
    ld      hl, _RAM_C093_
    bit     0, (hl)
    pop     hl
    jr      nz, ++
    ld      a, $F1
    ld      de, $0040
++:
    ld      (hl), a
    jr      ++++

+++:
    ld      de, $0000
++++:
    ld      (Alex_SpeedX), de
    ld      hl, (Alex_MaxAnimFrame)
    ld      a, h
    sub     l
    ret     nz
    ld      a, (Alex_FrameCountDown)
    dec     a
    ret     nz
    ld      a, $A5
    ld      (Sound_MusicTrigger), a
    ld      hl, _RAM_C220_
    ld      a, (hl)
    inc     a
    cp      $04
    ret     nc
    ld      (hl), a
    cp      $01
    jr      z, +
    cp      $02
    ret     nz
+:
    ld      hl, _DATA_2C77_
_LABEL_2079_:
    add     a, a
    ld      c, a
    add     a, a
    add     a, c
    ld      c, a
    ld      b, $00
    add     hl, bc
    ld      de, _RAM_C086_
    ldi
    ldi
    ld      de, _RAM_C202_
    ldi
    ldi
    ldi
    ld      a, (hl)
    ld      (_RAM_C206_), a
    ret

_LABEL_2096_:
    ld      a, (_RAM_C220_)
    cp      $03
    jp      nc, _LABEL_27E5_
    cp      $02
    jp      nc, _LABEL_26B0_
    bit     0, c
    call    z, _LABEL_20AB_
    jp      _LABEL_2773_

_LABEL_20AB_:
    ld      hl, (_RAM_C20A_)
    ld      de, $1C00
    add     hl, de
    ld      (_RAM_C20A_), hl
    ret

; 15th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_20B6_:
    call    _LABEL_2AB3_
    ld      hl, _RAM_C221_
    ld      a, (hl)
    or      a
    jr      z, +
    dec     (hl)
    jr      z, _LABEL_213D_
    ld      a, (_RAM_C094_)
    or      a
    jr      nz, _LABEL_213D_
    jr      _LABEL_2122_

+:
    ld      a, (_RAM_C09A_)
    or      a
    jr      nz, +
    ld      a, (_RAM_C0A8_)
    or      a
    ld      a, $02
    jr      nz, +
    ld      a, (_RAM_C212_)
    cp      $FE
    ld      a, $01
    jr      z, +
    ld      a, (_RAM_C094_)
    or      a
    jr      z, ++
+:
    ld      (hl), $2F
    ld      de, $0400
    dec     a
    jr      z, +
    ld      de, $FC00
+:
    ld      (Alex_SpeedY), de
    ld      a, (_RAM_C209_)
    xor     $01
    ld      (_RAM_C209_), a
    ld      hl, _DATA_2C67_
    push    bc
    call    _LABEL_2B4F_
    pop     bc
    set     0, (ix+34)
    jr      _LABEL_2122_

++:
    dec     hl
    dec     (hl)
    jr      z, _LABEL_213D_
    ld      hl, _RAM_C205_
    ld      a, (hl)
    cp      $04
    jr      c, _LABEL_2122_
    ld      a, (Alex_FrameCountDown)
    dec     a
    jr      nz, _LABEL_2122_
    dec     hl
    ld      (hl), $02
_LABEL_2122_:
    ld      a, c
    and     $0C
    jr      z, ++
    ld      hl, $0080
    and     $04
    jr      z, +
    ld      hl, $FF80
+:
    ld      (Alex_SpeedX), hl
    jr      +++

++:
    call    _LABEL_2433_
+++:
    ld      a, b
    and     $30
    ret     z
_LABEL_213D_:
    res     1, (ix+34)
    jp      _LABEL_2773_

; 17th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2144_:
    bit     5, b
    jr      nz, _LABEL_21AF_
    ld      a, c
    and     $0C
    jr      z, ++
    ld      a, (_RAM_C08F_)
    or      a
    jr      z, ++
    ld      hl, (_RAM_C20C_)
    ld      de, $F400
_LABEL_2159_:
    rrca
    jr      c, +
    ld      de, $0C00
+:
    add     hl, de
    ld      (_RAM_C20C_), hl
    res     2, (ix+34)
    jp      _LABEL_27AD_

++:
    ld      a, (_RAM_C095_)
    or      a
    jr      z, _LABEL_21AF_
    bit     4, c
    jp      nz, _LABEL_2854_
    ld      a, c
    and     $0C
    jr      z, ++
    ld      e, $00
    and     $04
    jr      z, +
    inc     e
+:
    ld      (ix+9), e
++:
    ld      (ix+4), $00
    ld      a, c
    ld      hl, _RAM_C095_
    bit     0, (hl)
    jr      nz, +
    res     1, a
+:
    bit     3, (hl)
    jr      nz, +
    res     0, a
+:
    and     $03
    jp      z, _LABEL_2AF8_
    ld      hl, $00C0
    and     $01
    jr      z, +
    ld      hl, $FF40
+:
    ld      (Alex_SpeedY), hl
    ld      (ix+4), $02
    ret

_LABEL_21AF_:
    res     2, (ix+34)
    ld      a, c
    and     $0C
    jp      z, _LABEL_2773_
    ld      hl, $00C0
    and     $08
    jr      nz, +
    ld      hl, $FF40
+:
    ld      (Alex_SpeedX), hl
    jp      _LABEL_2773_

; 19th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_21C9_:
    bit     4, c
    jp      z, _LABEL_2239_
    ld      a, c
    and     $03
    jr      z, +++
    ld      hl, _RAM_C221_
    ld      a, (hl)
    bit     0, c
    jr      nz, +
    ld      de, $0040
    inc     a
    cp      $10
    jr      z, +++
    push    hl
    ld      hl, _RAM_C095_
    bit     0, (hl)
    pop     hl
    jr      nz, ++
    ld      a, $0F
    ld      de, $FFC0
    jr      ++

+:
    ld      de, $FFC0
    dec     a
    cp      $F0
    jr      z, +++
    push    hl
    ld      hl, _RAM_C095_
    bit     2, (hl)
    pop     hl
    jr      nz, ++
    ld      a, $F1
    ld      de, $0040
++:
    ld      (hl), a
    jr      ++++

+++:
    ld      de, $0000
++++:
    ld      (Alex_SpeedY), de
    ld      hl, (Alex_MaxAnimFrame)
    ld      a, h
    sub     l
    ret     nz
    ld      a, (Alex_FrameCountDown)
    dec     a
    ret     nz
    ld      a, $A5
    ld      (Sound_MusicTrigger), a
    ld      hl, _RAM_C220_
    ld      a, (hl)
    inc     a
    cp      $04
    ret     nc
    ld      (hl), a
    cp      $01
    jr      z, +
    cp      $02
    ret     nz
+:
    ld      hl, _DATA_2C89_
    jp      _LABEL_2079_

_LABEL_2239_:
    res     2, (ix+34)
    ld      a, (_RAM_C220_)
    cp      $03
    jp      nc, _LABEL_2870_
    call    +
    jp      _LABEL_2773_

+:
    ld      hl, (_RAM_C20A_)
    ld      de, $0800
    add     hl, de
    ld      (_RAM_C20A_), hl
    ret

; 20th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2256_:
    call    _LABEL_2AB3_
    ld      hl, _RAM_C221_
    ld      a, (hl)
    or      a
    jr      z, +
    bit     0, (ix+34)
    jr      z, _LABEL_22CA_
    dec     (hl)
    jr      z, _LABEL_22CA_
-:
    ld      a, b
    and     $30
    jr      nz, _LABEL_22CA_
    ret

+:
    dec     hl
    dec     (hl)
    jr      z, _LABEL_22BC_
    ld      a, (_RAM_C088_)
    or      a
    jr      nz, ++
    ld      a, (_RAM_C09B_)
    or      a
    jr      nz, ++
    ld      de, (_RAM_C0A8_)
    ld      a, d
    or      e
    jr      nz, ++
    call    _LABEL_2AF8_
    bit     1, c
    jr      z, +
    ld      hl, $0080
    ld      (Alex_SpeedY), hl
+:
    ld      hl, _RAM_C205_
    ld      a, (hl)
    cp      $04
    jr      c, -
    ld      a, (Alex_FrameCountDown)
    dec     a
    jr      nz, -
    dec     hl
    ld      (hl), $02
    jr      -

++:
    ld      de, $0180
    dec     a
    jr      z, +
    ld      de, $FE80
+:
    ld      (Alex_SpeedX), de
    ld      a, (_RAM_C209_)
    xor     $01
    ld      (_RAM_C209_), a
_LABEL_22BC_:
    inc     hl
    ld      (hl), $17
    ld      hl, _DATA_2C6F_
    push    bc
    call    _LABEL_2B4F_
    pop     bc
    jp      _LABEL_2AFF_

_LABEL_22CA_:
    res     1, (ix+34)
    jp      _LABEL_2773_

; 22nd entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_22D1_:
    bit     5, b
    jr      nz, ++
    push    bc
    ld      hl, $2317
    call    _LABEL_29F2_
    pop     bc
    ld      a, c
    and     $0F
    jr      z, +++
    dec     (ix+32)
    jr      nz, +
    ld      (ix+32), $04
    ld      hl, _RAM_C209_
    ld      a, (hl)
    xor     $01
    ld      (hl), a
+:
    bit     0, c
    jr      z, +
    ld      a, (_RAM_C0A4_)
    and     $07
    bit     2, a
    call    z, _LABEL_2AF8_
+:
    ld      a, (_RAM_C0A4_)
    or      a
    ret     nz
++:
    xor     a
    bit     2, c
    jr      nz, +
    inc     a
+:
    ld      (_RAM_C209_), a
    res     4, (ix+34)
    jp      _LABEL_2773_

+++:
    call    _LABEL_2433_
    jp      _LABEL_2AF8_

; Data from 231B to 2342 (40 bytes)
.db $40 $FF $00 $00 $C0 $00 $00 $00 $00 $00 $00 $00 $00 $00 $40 $FF
.db $78 $FF $78 $FF $88 $00 $78 $FF $00 $00 $00 $00 $00 $00 $C0 $00
.db $78 $FF $88 $00 $88 $00 $88 $00

; 11th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2343_:
    call    _LABEL_2ACD_
    call    _LABEL_2ADC_
    call    _LABEL_2AB3_
    bit     4, b
    jp      nz, _LABEL_23B6_
    ld      hl, _RAM_C08A_
    bit     0, (ix+34)
    jr      nz, +
    ld      a, (hl)
    or      a
    jr      z, _LABEL_23AA_
    set     0, (ix+34)
+:
    ld      a, (hl)
    or      a
    jr      nz, ++
    ld      hl, _RAM_C205_
    ld      a, (hl)
    or      a
    jr      nz, +
    dec     hl
    ld      (hl), a
+:
    ld      a, c
    and     $0C
    jp      z, _LABEL_2415_
    and     $04
    jp      nz, _LABEL_1E90_
    jp      _LABEL_1E77_

++:
    bit     7, (hl)
    jr      nz, +
    set     7, (hl)
    inc     hl
    ld      (hl), $08
    dec     hl
    call    _LABEL_2B02_
    ld      de, $50D8
    ld      (_RAM_C086_), de
    ld      de, $0000
    ld      (Alex_MaxAnimFrame), de
+:
    inc     hl
    ld      a, (hl)
    or      a
    jr      z, +
    dec     (hl)
    call    _LABEL_2433_
    call    _LABEL_2AF8_
    bit     5, b
    ret     z
    jp      _LABEL_2784_

_LABEL_23AA_:
    call    _LABEL_2AEB_
    jp      alex_set_state_stopping

+:
    call    _LABEL_2AEB_
    jp      _LABEL_2773_

_LABEL_23B6_:
    call    _LABEL_2AFF_
    jp      _LABEL_270E_

; 4th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_23BC_:
    call    _LABEL_2A83_
    call    _LABEL_2ACD_
    call    _LABEL_2ADC_
    bit     4, b
    jp      nz, _LABEL_270E_
_LABEL_23CA_:
    bit     0, (ix+34)
    jr      nz, +
    ld      a, (_RAM_C088_)
    or      a
    jr      z, ++
    set     0, (ix+34)
    jr      +

; 10th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_23DC_:
    bit     4, b
    jp      nz, _LABEL_270E_
    bit     0, (ix+34)
    jr      z, ++
+:
    call    _LABEL_2A9E_
    call    _LABEL_2AB3_
    ld      a, c
    and     $0C
    jr      z, _LABEL_2415_
    and     $04
    jp      nz, _LABEL_1E8C_
    jp      _LABEL_1E73_

++:
    ld      a, (Alex_State)
    cp      $06
    jr      z, +
    cp      $1B
    jr      nz, ++
+:
    ld      a, (_RAM_C220_)
    or      a
    jp      p, _LABEL_2720_
++:
    ld      a, c
    and     $30
    jp      z, alex_set_state_stopping
    jp      alex_set_state_walking

_LABEL_2415_:
    ld      hl, (Alex_SpeedX)
    ld      de, $0008
_LABEL_241B_:
    bit     7, h
    jr      nz, +
    or      a
    sbc     hl, de
    bit     7, h
    jr      nz, _LABEL_2433_
    ld      (Alex_SpeedX), hl
    ret

+:
    add     hl, de
    bit     7, h
    jr      z, _LABEL_2433_
    ld      (Alex_SpeedX), hl
    ret

_LABEL_2433_:
    ld      hl, $0000
    ld      (Alex_SpeedX), hl
    ret

; 5th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_243A_:
    call    _LABEL_2AB3_
    bit     5, b
    jp      nz, _LABEL_26E2_
    bit     0, (ix+34)
    jp      nz, _LABEL_26DD_
    ld      a, c
    and     $0C
    jr      z, ++
    xor     a
    bit     2, c
    jr      nz, +
    inc     a
+:
    ld      (_RAM_C209_), a
++:
    ld      hl, (Alex_SpeedX)
    dec     (ix+32)
    jr      z, +
    ld      de, $0060
    jp      _LABEL_241B_

+:
    ld      a, h
    or      l
    jp      z, alex_set_state_idle
    jp      alex_set_state_stopping

; 6th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_246D_:
    call    _LABEL_2AB3_
    dec     (ix+32)
    jp      nz, _LABEL_23CA_
    jp      _LABEL_276C_

; 23rd entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2479_:
    call    _LABEL_253C_
    bit     4, b
    jp      nz, _LABEL_28AC_
    ld      (ix+4), $00
    ld      a, c
    and     $0F
    jr      z, _LABEL_24D4_
    ld      (ix+4), $01
    and     $0C
    jr      z, +++
    push    bc
    ld      hl, $0018
    ld      (AccelerationValue), hl
    ld      de, $0140
    bit     5, c
    jr      nz, +
    ld      de, $00C0
+:
    bit     2, c
    jr      nz, +
    ld      (ix+9), $01
    call    alex_move_right
    jr      ++

+:
    ld      (ix+9), $00
    call    alex_move_left
++:
    pop     bc
+++:
    ld      a, c
    and     $03
    jr      z, _LABEL_24D4_
    push    bc
    ld      hl, $0018
    ld      (AccelerationValue), hl
    ld      de, $00C0
    bit     0, c
    jr      nz, +
    call    alex_move_down
    jr      ++

+:
    call    alex_move_up
++:
    pop     bc
_LABEL_24D4_:
    ld      a, c
    and     $0C
    jr      nz, +
_LABEL_24D9_:
    ld      hl, (Alex_SpeedX)
    ld      de, $0008
    call    _LABEL_241B_
+:
    ld      hl, $0004
    ld      (AccelerationValue), hl
    ld      de, $00C0
    push    bc
    call    alex_move_up
    pop     bc
    ld      a, (_RAM_C0A7_)
    or      a
    jr      nz, +
    ld      hl, (_RAM_C0A5_)
    ld      a, l
    or      h
    cp      $03
    jr      nz, ++++
    jr      ++

+:
    bit     3, a
    ret     nz
    bit     5, b
    jr      nz, +++
    bit     2, a
    ret     nz
++:
    ld      hl, $0040
    ld      (Alex_SpeedY), hl
    ret

+++:
    res     5, (ix+34)
    call    _LABEL_2AFF_
    jp      _LABEL_26AB_

++++:
    res     5, (ix+34)
    jp      _LABEL_2773_

; 24th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2523_:
    call    _LABEL_253C_
    dec     (ix+32)
    jr      nz, _LABEL_24D9_
    jp      _LABEL_289D_

; 16th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_252E_:
    call    _LABEL_253C_
    call    _LABEL_29CF_
    dec     (ix+32)
    jr      nz, _LABEL_24D9_
    jp      _LABEL_289D_

_LABEL_253C_:
    ld      a, (_RAM_C0A7_)
    cp      $0F
    ret     nz
    ld      hl, _RAM_C2E0_
    ld      a, (hl)
    or      a
    ret     nz
    dec     (ix+42)
    ret     nz
    ld      (ix+42), $20
    ld      (hl), $08
    ret

; 26th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2553_:
    call    _LABEL_29CF_
    call    _LABEL_2AB3_
    bit     5, b
    jp      nz, _LABEL_26E2_
    bit     0, (ix+34)
    jp      nz, _LABEL_26DD_
    ld      hl, (Alex_SpeedX)
    dec     (ix+32)
    jr      z, +
    ld      de, $0060
    jp      _LABEL_241B_

+:
    ld      a, h
    or      l
    jp      z, alex_set_state_idle
    jp      alex_set_state_stopping

; 27th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_257B_:
    call    _LABEL_29CF_
    call    _LABEL_2AB3_
    dec     (ix+32)
    jp      nz, _LABEL_23CA_
    jp      _LABEL_276C_

; 30th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_258A_:
    ld      a, (_RAM_C21C_)
    ld      hl, $25F5
    rst     $08    ; call_jump_table
    ld      hl, _RAM_C240_
    ld      de, $0020
    ld      b, $05
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), $06
+:
    add     hl, de
    djnz    -
    ret

; 1st entry of Jump Table from 25F5 (indexed by unknown)
_LABEL_25A3_:
    dec     (ix+32)
    ret     nz
    inc     (ix+28)
    ld      (ix+32), $50
    ld      hl, _DATA_2C57_
    jp      _LABEL_2B4F_

; 2nd entry of Jump Table from 25F5 (indexed by unknown)
_LABEL_25B4_:
    ld      a, (Alex_FrameCountDown)
    dec     a
    jr      nz, +
    ld      a, $B1
    ld      (Sound_MusicTrigger), a
+:
    dec     (ix+32)
    jr      nz, _LABEL_25D1_
    inc     (ix+28)
    ld      (ix+32), $23
    ld      hl, _DATA_2C5F_
    jp      _LABEL_2B4F_

_LABEL_25D1_:
    ld      hl, _DATA_25FB_
    call    _LABEL_29F2_
    ld      hl, _RAM_C221_
    ld      a, (hl)
    xor     $01
    ld      (hl), a
    ld      hl, _RAM_C20D_
    jr      z, +
    inc     (hl)
    inc     (hl)
    ret

+:
    dec     (hl)
    dec     (hl)
    ret

; 3rd entry of Jump Table from 25F5 (indexed by unknown)
_LABEL_25E9_:
    dec     (ix+32)
    jr      nz, _LABEL_25D1_
    res     2, (ix+41)
    jp      _LABEL_2773_

; Jump Table from 25F5 to 25FA (3 entries, indexed by unknown)
_DATA_25F5_:
.dw _LABEL_25A3_ _LABEL_25B4_ _LABEL_25E9_

; Data from 25FB to 2626 (44 bytes)
_DATA_25FB_:
.db $00 $00 $00 $00 $80 $FD $00 $00 $80 $02 $00 $00 $00 $00 $00 $00
.db $00 $00 $80 $FD $3C $FE $3C $FE $C4 $01 $3C $FE $00 $00 $00 $00
.db $00 $00 $80 $02 $3C $FE $C4 $01 $C4 $01 $C4 $01

; 31st entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2627_:
    ld      hl, _RAM_C240_
    ld      de, $0020
    ld      b, $05
-:
    ld      a, (hl)
    or      a
    jr      z, +
    add     hl, de
    djnz    -
+:
    ld      (hl), $0C
    ld      a, (_RAM_C0B4_)
    or      a
    ret     nz
    res     3, (ix+41)
    call    _LABEL_2AB3_
    jp      _LABEL_2773_

; 12th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2647_:
    dec     (ix+37)
    jr      z, +
    ld      a, c
    and     $0C
    jp      z, _LABEL_2415_
    and     $04
    jp      nz, _LABEL_1E8C_
    jp      _LABEL_1E73_

+:
    ld      a, (Alex_Health)
    or      a
    jp      z, _LABEL_2964_
    bit     5, (ix+34)
    jp      nz, _LABEL_289D_
    bit     0, (ix+34)
    jp      nz, _LABEL_2773_
    ld      hl, (Alex_SpeedX)
    ld      a, h
    or      l
    jp      nz, alex_set_state_stopping
    jp      alex_set_state_idle

; 29th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_267A_:
    ld      hl, _RAM_C20B_
    ld      a, (hl)
    inc     a
    cp      $D0
    jr      nz, +
    inc     a
+:
    ld      (hl), a
    ret

; 25th entry of Jump Table from 1C9D (indexed by unknown)
_LABEL_2686_:
    ld      hl, _RAM_C20B_
    ld      a, (hl)
    sub     $02
    ld      (hl), a
    ret     nc
    ld      (ix+49), $00
    ret


alex_set_state_idle:                                                ; $2693
    ld      (ix + Object_State), Alex_State_Idle
    call    _LABEL_2433_
    jp      _LABEL_2B23_

alex_set_state_walking:                                             ; $269D
    ld      (ix + Object_State), Alex_State_Walk
    jp      _LABEL_2B23_

alex_set_state_stopping:                                            ; $26A4
    ld      (ix + Object_State), Alex_State_Stopping
    jp      _LABEL_2B23_

_LABEL_26AB_:
    call    _LABEL_29AF_
    jr      +

_LABEL_26B0_:
    call    _LABEL_29B4_
    jr      +

alex_set_state_jump:                                                ; $26B5
    call    _LABEL_299B_
+:
    ld      (ix + Object_State), Alex_State_Jump
    call    _LABEL_2AFF_
    jp      _LABEL_2B23_

alex_set_state_duck:                                                ; $26C2
    ld      (ix + Object_State), Alex_State_Duck
    set     6, (ix+34)
    jp      _LABEL_2B23_

_LABEL_26CD_:
    call    _LABEL_275D_
    call    alex_trigger_attack_sound
    jp      _LABEL_2B23_

_LABEL_26D6_:
    ld      (ix+0), $09
    jp      _LABEL_2B23_

_LABEL_26DD_:
    call    _LABEL_2750_
    jr      +

_LABEL_26E2_:
    call    _LABEL_2750_
    call    _LABEL_299B_
+:
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      nz, +
    ld      hl, $8CA0
    ld      de, $50F0
    jr      _LABEL_2706_

+:
    or      a
    ld      hl, $8D24
    ld      de, $5148
    jr      z, _LABEL_2706_
    ld      hl, $8D38
    ld      de, $515C
_LABEL_2706_:
    ld      (_RAM_C202_), hl
    ld      (_RAM_C086_), de
    ret

_LABEL_270E_:
    call    _LABEL_2750_
    call    alex_trigger_attack_sound
    jp      _LABEL_2B23_

_LABEL_2717_:
    call    ++
    call    alex_trigger_attack_sound
    jp      _LABEL_2B23_

_LABEL_2720_:
    call    ++
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      z, +
    or      a
    ld      hl, $8CE4
    ld      de, $5108
    jr      z, _LABEL_2706_
    ld      hl, $8CF8
    ld      de, $511C
    jr      _LABEL_2706_

+:
    ld      hl, $8C98
    ld      de, $50E8
    jr      _LABEL_2706_

++:
    ld      b, $1A
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      z, +
    ld      b, Alex_State_Sword
    jr      +

_LABEL_2750_:
    ld      b, $1B
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      z, +
    ld      b, $06
    jr      +

_LABEL_275D_:
    ld      b, $1C
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      z, +
    ld      b, $08
+:
    ld      (ix+0), b
    ret

_LABEL_276C_:
    ld      (ix + Object_State), Alex_State_Jump
    jp      _LABEL_2B23_

_LABEL_2773_:
    call    _LABEL_2AFF_
    xor     a
    ld      (_RAM_C222_), a
    ld      (_RAM_C229_), a
    ld      (ix + Object_State), Alex_State_Fall
    jp      _LABEL_2B23_

_LABEL_2784_:
    call    _LABEL_2987_
    xor     a
    ld      (_RAM_C08A_), a
    call    _LABEL_2AFF_
    ld      (ix+0), $0B
    jp      _LABEL_2B23_

_LABEL_2795_:
    ld      (ix+0), $16
    ld      (ix+32), $04
    set     4, (ix+34)
    call    _LABEL_2B02_
    call    _LABEL_2AF8_
    call    _LABEL_2433_
    jp      _LABEL_2B23_

_LABEL_27AD_:
    ld      a, (_RAM_C076_)
    or      a
    ld      a, (_RAM_C062_)
    ld      hl, (_RAM_C090_)
    jr      z, +
    ld      b, a
    ld      a, (_RAM_C092_)
    sub     b
    ld      e, $00
    ld      d, a
    add     hl, de
+:
    ld      (_RAM_C20A_), hl
    call    _LABEL_2B02_
_LABEL_27C8_:
    ld      (ix+0), $0D
    call    _LABEL_2AF8_
    call    _LABEL_2433_
    jp      _LABEL_2B23_

_LABEL_27D5_:
    ld      (ix+0), $0E
    xor     a
    ld      (ix+9), a
    ld      h, a
    ld      l, a
    ld      (_RAM_C220_), hl
    jp      _LABEL_2B23_

_LABEL_27E5_:
    ld      (ix+0), $0F
    ld      hl, $00C0
    ld      (_RAM_C220_), hl
    ld      hl, $0300
    ld      e, $01
    ld      a, (InputFlags)
    bit     BTN_DOWN_BIT, a
    jr      nz, +
    ld      h, $FD
    dec     e
+:
    ld      (Alex_SpeedY), hl
    ld      (ix+9), e
    set     0, (ix+34)
    set     OBJECT_F34_DISABLE_ANIM, (ix+34)
    jp      _LABEL_2B23_

_LABEL_280F_:
    ld      (ix+0), $11
    set     2, (ix+34)
    ld      a, (_RAM_C050_)
    ld      hl, (_RAM_C096_)
    ld      b, a
    ld      a, (_RAM_C098_)
    sub     b
    ld      e, $00
    ld      d, a
    add     hl, de
    ld      (_RAM_C20C_), hl
    call    _LABEL_2B02_
    call    _LABEL_2AF8_
    call    _LABEL_2433_
    jp      _LABEL_2B23_

_LABEL_2835_:
    ld      (ix+0), $12
    ld      (ix+32), $0B
    call    _LABEL_2433_
    jp      _LABEL_2B23_

_LABEL_2843_:
    ld      (ix+0), $15
    ld      (ix+32), $0B
    call    _LABEL_2B02_
    call    _LABEL_2433_
    jp      _LABEL_2B23_

_LABEL_2854_:
    ld      (ix+0), $13
    xor     a
    ld      (ix+9), a
    ld      h, a
    ld      l, a
    ld      (_RAM_C220_), hl
    ld      hl, (_RAM_C20A_)
    ld      de, $0800
    or      a
    sbc     hl, de
    ld      (_RAM_C20A_), hl
    jp      _LABEL_2B23_

_LABEL_2870_:
    ld      (ix+0), $14
    ld      hl, $0034
    ld      (_RAM_C220_), hl
    ld      a, (InputFlags)
    ld      hl, $FD00
    ld      e, $00
    bit     BTN_LEFT_BIT, a
    jr      nz, +
    ld      h, $03
    inc     e
+:
    ld      (Alex_SpeedX), hl
    ld      (ix+9), e
    call    _LABEL_2AF8_
    set     0, (ix+34)
    set     OBJECT_F34_DISABLE_ANIM, (ix+34)
    jp      _LABEL_2B23_

_LABEL_289D_:
    ld      (ix+0), $17
    ld      (ix+42), $20
    ld      (ix+34), $20
    jp      _LABEL_2B23_

_LABEL_28AC_:
    ld      b, $18
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      nz, +
    ld      b, $10
+:
    ld      (ix+0), b
    call    alex_trigger_attack_sound
    jp      _LABEL_2B23_

_LABEL_28C0_:
    ld      (ix+0), $1E
    set     2, (ix+41)
    xor     a
    ld      (_RAM_C21C_), a
    ld      (_RAM_C221_), a
    ld      (ix+32), $18
    call    _LABEL_2AF8_
    call    _LABEL_2433_
    jp      _LABEL_2B23_

_LABEL_28DC_:
    ld      (ix+0), $1F
    set     3, (ix+41)
    call    _LABEL_2B02_
    call    _LABEL_2433_
    ld      hl, $0100
    ld      (Alex_SpeedY), hl
    jp      _LABEL_2B23_

_LABEL_28F3_:
    ld      a, $90
    ld      (Sound_MusicTrigger), a
    ld      (ix+0), $0C
    dec     (ix+46)
    jr      nz, +
    ld      (ix+49), $01
+:
    ld      (ix+45), $01
    ld      (ix+39), $01
    ld      (ix+40), $30
    ld      (ix+37), $10
    ld      (ix+35), $00
    ld      (ix+41), $00
    call    _LABEL_2AFF_
    ld      hl, $0100
    ld      a, (_RAM_C0C4_)
    ld      b, a
    ld      a, (_RAM_C20D_)
    sub     b
    jr      nc, +
    ld      hl, $FF00
+:
    ld      (Alex_SpeedX), hl
    ld      hl, $FC80
    bit     5, (ix+34)
    jr      z, +
    ld      hl, $0040
+:
    ld      (Alex_SpeedY), hl
    jr      nz, +
    ld      (ix+34), $01
+:
    jp      _LABEL_2B23_

_LABEL_294B_:
    ld      a, $B4
    ld      (Sound_MusicTrigger), a
    ld      (ix+0), $1D
    set     1, (ix+41)
    call    _LABEL_2B02_
    call    _LABEL_2AF8_
    call    _LABEL_2433_
    jp      _LABEL_2B23_

_LABEL_2964_:
    ld      a, $B4
    ld      (Sound_MusicTrigger), a
    ld      (ix+0), $19
    xor     a
    ld      (_RAM_C222_), a
    ld      (_RAM_C229_), a
    ld      (_RAM_C227_), a
    call    _LABEL_2AF8_
    call    _LABEL_2433_
    ld      (_RAM_C067_), hl
    ld      (ix+32), $1F
    jp      _LABEL_2B23_

_LABEL_2987_:
    ld      hl, $0180
    ld      a, (_RAM_C089_)
    dec     a
    jr      z, +
    ld      hl, $FE80
+:
    xor     $01
    ld      (_RAM_C209_), a
    ld      (Alex_SpeedX), hl
_LABEL_299B_:
    ld      hl, $FC80
-:
    ld      (Alex_SpeedY), hl
    ld      (ix+35), $00
_LABEL_29A5_:
    set     0, (ix+34)
    ld      a, $9D
    ld      (Sound_MusicTrigger), a
    ret

_LABEL_29AF_:
    ld      hl, $FE40
    jr      -

_LABEL_29B4_:
    ld      hl, $FC00
    jr      -


alex_trigger_attack_sound:                                          ; $29B9
    ld      a, (Alex_PowerupState)
    ld      hl, _DATA_29C9_
    rst     $18    ; get_array_index
    ld      a, l
    ld      (_RAM_C220_), a
    ld      a, h
    ld      (Sound_MusicTrigger), a
    ret

; Data from 29C9 to 29CE (6 bytes)
_DATA_29C9_:
.db $08 SFX_Sword
.db $0A SFX_PowerSword
.db $07 SFX_Arrow


_LABEL_29CF_:
    ld      a, (_RAM_C212_)
    or      a
    ret     nz
    ld      a, (_RAM_C205_)
    or      a
    ret     nz
    ld      a, (Alex_FrameCountDown)
    cp      $03
    ret     nz
    ld      hl, _RAM_C240_
    ld      de, $0020
    ld      b, $02
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), $01
    ret

+:
    add     hl, de
    djnz    -
    ret

_LABEL_29F2_:
    ld      a, c
    and     $0F
    add     a, a
    add     a, a
    ld      e, a
    ld      d, $00
    add     hl, de
    ld      de, Alex_SpeedY
    ldi
    ldi
    ldi
    ldi
    ret


; ==============================================================================
;  void alex_move_right(int16_t adjustment, int16_t limit)
; ------------------------------------------------------------------------------
;  Adjusts Alex' velocity on the X-axis to move right.
; ------------------------------------------------------------------------------
;  In:
;    (AccelerationValue)    - adjustment value
;    DE                     - The velocity will saturate at this value.
; ------------------------------------------------------------------------------
alex_move_right:                                                    ; $2A07
    ld      hl, (Alex_SpeedX)
    call    _add_x_acceleration
    bit     7, h
    jr      nz, _add_x_acceleration
    or      a
    sbc     hl, de
    ret     c
    ld      (Alex_SpeedX), de
    ret


; ==============================================================================
;  void alex_move_left(int16_t adjustment, int16_t limit)
; ------------------------------------------------------------------------------
;  Adjusts Alex' velocity on the X-axis to move left.
; ------------------------------------------------------------------------------
;  In:
;    (AccelerationValue)    - adjustment value
;    DE                     - The velocity will saturate at this value.
; ------------------------------------------------------------------------------
alex_move_left:                                                     ; $2A1A
    ld      hl, (Alex_SpeedX)
    call    _subtract_x_acceleration
    bit     7, h
    jr      z, _subtract_x_acceleration
    call    negate_hl
    or      a
    sbc     hl, de
    ret     c
    call    negate_de
    ld      (Alex_SpeedX), de
    ret

_add_x_acceleration:
    ld      bc, (AccelerationValue)
    add     hl, bc
    jr      +

_subtract_x_acceleration:
    ld      bc, (AccelerationValue)
    or      a       ; clear carry flag
    sbc     hl, bc
+:
    ld      (Alex_SpeedX), hl
    ret


; ==============================================================================
;  void alex_move_down(int16_t adjustment, int16_t limit)
; ------------------------------------------------------------------------------
;  Adjusts Alex' velocity on the Y-axis to move down.
; ------------------------------------------------------------------------------
;  In:
;    (AccelerationValue)    - adjustment value
;    DE                     - The velocity will saturate at this value.
; ------------------------------------------------------------------------------
alex_move_down:                                                     ; $2A45
    ld      hl, (Alex_SpeedY)
    call    _add_y_acceleration
    bit     7, h
    jr      nz, _add_y_acceleration
    or      a
    sbc     hl, de
    ret     c
    ld      (Alex_SpeedY), de
    ret


; ==============================================================================
;  void alex_move_up(int16_t delta, int16_t limit)
; ------------------------------------------------------------------------------
;  Adjusts Alex' velocity on the Y-axis to move up.
; ------------------------------------------------------------------------------
;  In:
;    (AccelerationValue)    - adjustment value
;    DE                     - The velocity will saturate at this value.
; ------------------------------------------------------------------------------
alex_move_up:                                                       ; $2A58
    ld      hl, (Alex_SpeedY)
    call    _subtract_y_acceleration
    bit     7, h
    jr      z, _subtract_y_acceleration
    call    negate_hl
    or      a
    sbc     hl, de
    ret     c
    call    negate_de
    ld      (Alex_SpeedY), de
    ret

_add_y_acceleration:
    ld      bc, (AccelerationValue)
    add     hl, bc
    jr      +

_subtract_y_acceleration:
    ld      bc, (AccelerationValue)
    or      a
    sbc     hl, bc
+:
    ld      (Alex_SpeedY), hl
    ret


_LABEL_2A83_:
    ld      hl, _RAM_C08A_
    ld      a, (hl)
    or      a
    ret     z
    rlca
    jr      c, +
    set     7, (hl)
    inc     hl
    ld      (hl), $06
    ret

+:
    inc     hl
    dec     (hl)
    jp      z, _LABEL_2AEE_
    bit     5, b
    ret     z
    pop     af
    jp      _LABEL_2784_

_LABEL_2A9E_:
    ld      hl, _RAM_C0A2_
    ld      a, (hl)
    inc     hl
    or      (hl)
    cp      $03
    jr      z, +
    inc     hl
    ld      a, (hl)
    or      a
    ret     z
+:
    bit     0, c
    ret     z
    pop     af
    jp      _LABEL_2795_

_LABEL_2AB3_:
    ld      hl, _RAM_C0A5_
    ld      a, (hl)
    inc     hl
    or      (hl)
    cp      $03
    jr      z, +
    inc     hl
    ld      a, (hl)
    and     $0F
    cp      $0F
    ret     nz
+:
    pop     af
    ld      a, $0A
    ld      (_RAM_C2E0_), a
    jp      _LABEL_289D_

_LABEL_2ACD_:
    ld      a, (_RAM_C08F_)
    or      a
    ret     z
    cp      $03
    ret     nz
    bit     0, c
    ret     z
    pop     af
    jp      _LABEL_27AD_

_LABEL_2ADC_:
    ld      a, (_RAM_C095_)
    and     $07
    cp      $07
    ret     nz
    bit     0, c
    ret     z
    pop     af
    jp      _LABEL_280F_

_LABEL_2AEB_:
    call    _LABEL_2AFF_
_LABEL_2AEE_:
    ld      hl, $0000
    ld      (_RAM_C088_), hl
    ld      (_RAM_C08A_), hl
    ret

_LABEL_2AF8_:
    ld      hl, $0000
    ld      (Alex_SpeedY), hl
    ret

_LABEL_2AFF_:
    xor     a
    jr      +

_LABEL_2B02_:
    ld      a, $01
+:
    ld      (_RAM_C08E_), a
    ret


; ==============================================================================
;  void alex_update_anim_frame(object *alex)
; ------------------------------------------------------------------------------
;  Updates Alex's animation to the next frame and loops if required.
; ------------------------------------------------------------------------------
;  In:
;    IX     - pointer to the alex object.
; ------------------------------------------------------------------------------
alex_update_anim_frame:                                             ; $2B08
    ld      a, (_RAM_C206_)
    or      a
    ret     z
    dec     (ix + Object_FrameCountDown)
    ret     nz
    ld      (Alex_FrameCountDown), a            ; Object_ield07
    inc     (ix + Object_AnimFrame)
    ld      a, (Alex_MaxAnimFrame)              ; Object_MaxAnimFrame
    cp      (ix + Object_AnimFrame)
    ret     nc
    ld      (ix + Object_AnimFrame), 0
    ret


_LABEL_2B23_:
    ld      a, (Alex_PowerupState)
    or      a
    ld      a, (Alex_State)
    ld      bc, _DATA_2B5F_ - 8
    ; jump if alex' powerup state is 0
    jr      z, ++

    ld      e, $00
    cp      Alex_State_Sword
    jr      z, +
    inc     e
    cp      $06
    jr      z, +
    inc     e
    cp      $08
    jr      z, +
    inc     e
    cp      $18
    jr      nz, ++
+:
    ld      a, e
    ld      bc, $2C9B

++:
    ld      h, $00
    ld      l, a
    add     hl, hl
    add     hl, hl
    add     hl, hl
    add     hl, bc
_LABEL_2B4F_:
    ld      de, _RAM_C202_
    ld      bc, $0006
    ldir
    ld      de, _RAM_C086_
    ldi
    ldi
    ret

; Data from 2B5F to 2C56 (248 bytes)
_DATA_2B5F_:
.db $70 $8C $00 $00 $00 $00 $B0 $50
.db $70 $8C $03 $00 $03 $03 $AC $50
.db $70 $8C $03 $00 $04 $04 $B0 $50
.db $70 $8C $00 $00 $00 $00 $C8 $50
.db $E4 $8C $04 $00 $02 $02 $08 $51
.db $24 $8D $04 $00 $02 $02 $48 $51
.db $70 $8C $00 $00 $00 $00 $C4 $50
.db $64 $8D $04 $00 $02 $02 $88 $51
.db $70 $8C $01 $00 $04 $04 $C0 $50
.db $70 $8C $00 $00 $00 $00 $5C $53
.db $70 $8C $02 $01 $06 $06 $CC $50
.db $70 $8C $00 $00 $00 $00 $60 $53
.db $C4 $8C $00 $00 $03 $03 $14 $52
.db $E4 $8D $07 $00 $02 $02 $3C $52
.db $94 $8E $04 $03 $01 $01 $EC $52
.db $B0 $8C $01 $00 $04 $04 $00 $51
.db $B8 $8C $00 $00 $03 $03 $08 $52
.db $D0 $8C $02 $00 $04 $04 $44 $53
.db $3C $8E $07 $00 $02 $02 $94 $52
.db $C0 $8E $04 $03 $01 $01 $18 $53
.db $CC $8C $02 $00 $04 $04 $50 $53
.db $70 $8C $00 $00 $04 $04 $DC $50
.db $90 $8C $00 $00 $03 $03 $E0 $50
.db $A4 $8D $04 $00 $02 $02 $C8 $51
.db $DC $8C $01 $00 $04 $04 $84 $53
.db $98 $8C $01 $00 $04 $04 $E8 $50
.db $A0 $8C $01 $00 $04 $04 $F0 $50
.db $A8 $8C $01 $00 $04 $04 $F8 $50
.db $70 $8C $04 $00 $04 $04 $64 $53
.db $EC $8E $02 $00 $08 $08 $1C $52
.db $70 $8C $04 $00 $04 $04 $64 $53

; Data from 2C57 to 2C5E (8 bytes)
_DATA_2C57_:
.db $F8 $8E $01 $00 $02 $02 $34 $52

; Data from 2C5F to 2C66 (8 bytes)
_DATA_2C5F_:
.db $EC $8E $01 $00 $06 $06 $28 $52

; Data from 2C67 to 2C6E (8 bytes)
_DATA_2C67_:
.db $A8 $8E $05 $00 $08 $08 $00 $53

; Data from 2C6F to 2C76 (8 bytes)
_DATA_2C6F_:
.db $D4 $8E $05 $00 $04 $04 $2C $53

; Data from 2C77 to 2C88 (18 bytes)
_DATA_2C77_:
.db $3C $52 $E4 $8D $07 $02 $5C $52 $04 $8E $07 $01 $7C $52 $24 $8E
.db $05 $01

; Data from 2C89 to 2CBA (50 bytes)
_DATA_2C89_:
.db $94 $52 $3C $8E $07 $02 $B4 $52 $5C $8E $07 $01 $D4 $52 $7C $8E
.db $05 $01 $F8 $8C $0A $00 $01 $01 $1C $51 $38 $8D $0A $00 $01 $01
.db $5C $51 $78 $8D $0A $00 $01 $01 $9C $51 $B8 $8D $0A $00 $01 $01
.db $DC $51

_LABEL_2CBB_:
    ld      ix, _RAM_C300_
    ld      b, $0E
-:
    ld      a, $09
    ld      (Frame2_Select), a
    ld      a, (ix+0)
    or      a
    jr      z, +++
    bit     7, a
    jr      z, +
    and     $7F
    jr      ++

+:
    bit     2, (ix+1)
    jr      nz, ++
    ld      a, (ix+19)
    or      (ix+18)
    jr      z, ++
    call    _LABEL_2DAB_
    call    _LABEL_2DB5_
    jr      +++

++:
    push    bc
    ld      a, (ix + Object_State)
    ld      hl, $2DCF
    rst     $08    ; call_jump_table
    call    object_update_animation_counters
    pop     bc
+++:
    ld      de, $0020
    add     ix, de
    djnz    -
    ret

_LABEL_2CFE_:
    xor     a
    ld      b, a
_LABEL_2D00_:
    ld      (ix+2), l
    ld      (ix+3), h
_LABEL_2D06_:
    ld      (ix+4), a
    ld      (ix+5), $00
    ld      (ix+6), b
    ld      (ix+7), b
    ret

_LABEL_2D14_:
    ld      l, (ix+14)
    ld      h, (ix+15)
    call    negate_hl
    jp      _LABEL_2DAE_

_LABEL_2D20_:
    ld      a, (ix+9)
    xor     $01
    ld      (ix+9), a
    ld      l, (ix+16)
    ld      h, (ix+17)
    call    negate_hl
    jp      _LABEL_2DB8_

_LABEL_2D34_:
    ld      (ix+0), $01
    jr      +

_LABEL_2D3A_:
    ld      (ix+0), $6C
+:
    xor     a
    ld      (ix+28), a
    ld      (ix+26), a
    ret

_LABEL_2D46_:
    call    _LABEL_2DB5_
_LABEL_2D49_:
    ld      l, (ix+14)
    ld      h, (ix+15)
    add     hl, de
    ld      a, h
    bit     7, a
    jr      nz, +
    cp      $04
    jr      nz, +
    ld      hl, $03F0
+:
    jp      _LABEL_2DAE_

_LABEL_2D5F_:
    call    _LABEL_2DB5_
_LABEL_2D62_:
    call    _LABEL_2DAB_
    ld      h, (ix+11)
    ld      l, (ix+10)
    ld      de, (_RAM_C061_)
    call    negate_de
    or      a
    sbc     hl, de
    ld      a, h
    and     $F8
    ld      h, a
    ld      l, $00
    add     hl, de
    ld      (ix+11), h
    ld      (ix+10), l
    ret

_LABEL_2D83_:
    ld      a, (_RAM_C20D_)
    sub     (ix+13)
    ld      a, $01
    jr      nc, +
    dec     a
+:
    ld      (ix+9), a
    ret

_LABEL_2D92_:
    ld      a, (_RAM_C20D_)
    sub     (ix+13)
    jr      nc, +
    neg
+:
    sub     b
    ret

_LABEL_2D9E_:
    call    _LABEL_2D83_
    or      a
    ld      hl, $FF80
    jr      z, +
    ld      h, $00
+:
    jr      _LABEL_2DB8_

_LABEL_2DAB_:
    ld      hl, $0000
_LABEL_2DAE_:
    ld      (ix+14), l
    ld      (ix+15), h
    ret

_LABEL_2DB5_:
    ld      hl, $0000
_LABEL_2DB8_:
    ld      (ix+16), l
    ld      (ix+17), h
    ret

_LABEL_2DBF_:
    ld      l, (ix+14)
    ld      h, (ix+15)
    add     hl, de
    jr      _LABEL_2DAE_

; Data from 2DC8 to 2DD0 (9 bytes)
.db $DD $6E $10 $DD $66 $11 $19 $18 $E7

; Jump Table from 2DD1 to 2EA8 (108 entries, indexed by unknown)
_DATA_2DD1_:
.dw _LABEL_250CC_ 
.dw _LABEL_2518D_
.dw _LABEL_2F3F_
.dw _LABEL_251CF_
.dw _LABEL_3052_
.dw _LABEL_2523D_
.dw _LABEL_30E1_
.dw _LABEL_343B_
.dw _LABEL_349B_
.dw _LABEL_351A_
.dw _LABEL_252EC_
.dw _LABEL_25356_
.dw _LABEL_3DC4_
.dw _LABEL_3DC4_
.dw _LABEL_3DC4_
.dw _LABEL_3DC4_
.dw _LABEL_3933_
.dw _LABEL_3C56_
.dw _LABEL_3DC4_
.dw _LABEL_3A2F_
.dw _LABEL_3AC5_
.dw _LABEL_3DC4_
.dw _LABEL_3DC4_
.dw _LABEL_3DC4_
.dw _LABEL_3DC4_
.dw _LABEL_253C9_
.dw _LABEL_261EA_
.dw _LABEL_2FF5_
.dw _LABEL_260FE_
.dw mode_handler_load_boss_room
.dw mode_handler_load_boss_room
.dw mode_handler_load_boss_room
.dw mode_handler_load_boss_room
.dw mode_handler_load_boss_room
.dw mode_handler_load_boss_room
.dw mode_handler_load_boss_room
.dw _LABEL_4BB5_
.dw _LABEL_4C16_
.dw _LABEL_4C47_
.dw _LABEL_26154_
.dw _LABEL_4CE1_
.dw _LABEL_261B2_
.dw _LABEL_2617A_
.dw _LABEL_2622B_
.dw _LABEL_4CE1_
.dw _LABEL_4CE1_
.dw _LABEL_493B_
.dw _LABEL_4CE1_
.dw _LABEL_4CE1_
.dw _LABEL_4967_
.dw _LABEL_4A59_
.dw _LABEL_4CE1_
.dw _LABEL_4CE1_
.dw _LABEL_4C7A_
.dw _LABEL_4CAE_
.dw _LABEL_4B31_
.dw _LABEL_26268_
.dw _LABEL_262E7_
.dw _LABEL_26196_
.dw _LABEL_25465_
.dw _LABEL_7A41_
.dw _LABEL_79BA_
.dw _LABEL_7A99_
.dw _LABEL_4009_
.dw _LABEL_4089_
.dw _LABEL_411D_
.dw _LABEL_41AF_
.dw _LABEL_3FB4_
.dw _LABEL_3E8D_
.dw _LABEL_3EC9_
.dw _LABEL_3F2A_
.dw _LABEL_3F5A_
.dw _LABEL_25521_
.dw _LABEL_255C0_
.dw _LABEL_25619_
.dw _LABEL_256A6_
.dw _LABEL_25713_
.dw _LABEL_2577E_
.dw _LABEL_257E1_
.dw _LABEL_2580E_
.dw _LABEL_25667_
.dw _LABEL_25909_
.dw _LABEL_42ED_
.dw _LABEL_45D5_
.dw _LABEL_462B_
.dw _LABEL_46D4_
.dw _LABEL_25AFD_
.dw _LABEL_25BF3_
.dw _LABEL_25983_
.dw _LABEL_25A01_
.dw _LABEL_25A59_
.dw _LABEL_4755_
.dw _LABEL_47F4_
.dw _LABEL_486D_
.dw _LABEL_48D0_
.dw _LABEL_25C63_
.dw _LABEL_25D1D_
.dw _LABEL_25D67_
.dw _LABEL_25E26_
.dw _LABEL_25E72_
.dw _LABEL_25EDB_
.dw _LABEL_25F22_
.dw _LABEL_25FE2_
.dw _LABEL_26058_
.dw mode_handler_load_boss_room
.dw mode_handler_load_boss_room
.dw _LABEL_2608E_
.dw _LABEL_2510B_

; ==============================================================================
;  void mode_handler_load_boss_room(void)
; ------------------------------------------------------------------------------
;  Loads the palettes, tile art & mappings for the current stage's boss room.
;  Transitions to game state 3.
; ------------------------------------------------------------------------------
;  In:
;    (StageNumber)      - Loads the boss room for this stage.
; ------------------------------------------------------------------------------
mode_handler_load_boss_room:                                        ; $2EA9
    ld      a, (StageNumber)
    ld      hl, DATA_BossTilesets
    rst     $18    ; get_array_index
    ; load the palettes into RAM
    ld      a, (hl)
    ld      (Frame2_Select), a
    inc     hl
    ld      a, (hl)
    inc     hl
    push    hl
    ld      h, (hl)
    ld      l, a
    ld      de, PaletteBuffer
    ld      bc, $0020
    ldir
    ; decompress the background tiles to VRAM
    ld      de, $6000
    call    decode_tiles_to_vram
    pop     hl
    ; load the screen map & primary sprite tileset
    call    _load_level_mappings
    ; decompress the auxilliary tileset, if it exists
    jr      z, +
    inc     hl
    ld      a, (hl)
    ld      (Frame2_Select), a
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    inc     hl
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    call    decode_tiles_to_vram
+:
    ld      a, $03
    ld      (GameMode), a
    ld      a, :GFX_ExplosionCloud
    ld      (Frame2_Select), a
    ld      hl, GFX_ExplosionCloud
    ld      de, $4380
    call    decode_tiles_to_vram
    jp      load_powerup_and_hud_gfx


DATA_BossTilesets:                                                  ; $2EF5
.dw DATA_BossTilesets_Stage1
.dw DATA_BossTilesets_Stage2
.dw DATA_BossTilesets_Stage3
.dw DATA_BossTilesets_Stage4

DATA_BossTilesets_Stage1                                            ; $2EFD
.db :PAL_Stage1_Boss_BG
    .dw PAL_Stage1_Boss_BG
.db :UNK_Stage1_Boss_mappings
    .dw UNK_Stage1_Boss_mappings
.db :GFX_Stage1_Boss_Kabuto
    .dw GFX_Stage1_Boss_Kabuto
.dw $0B00
.dw $5400
.db $01
.db :GFX_Stage1_Boss_Aux
    .dw $5F00
    .dw GFX_Stage1_Boss_Aux

DATA_BossTilesets_Stage2:                                           ; $2F10
.db :PAL_Stage2_Boss_BG
    .dw PAL_Stage2_Boss_BG
.db :UNK_Stage2_Boss_mappings
    .dw UNK_Stage2_Boss_mappings
.db :GFX_Stage2_Boss_SmallHeli
    .dw GFX_Stage2_Boss_SmallHeli
.dw $0140
.dw $4A40
.db $00     ; no auxilliary tileset

DATA_BossTilesets_Stage3:                                           ; $2F1E
.db :PAL_Stage3_Boss_BG
    .dw PAL_Stage3_Boss_BG
.db :UNK_Stage3_Boss_mappings
    .dw UNK_Stage3_Boss_mappings
.db :GFX_Stage3_Boss_Robster
    .dw GFX_Stage3_Boss_Robster
.dw $0A80
.dw $5380
.db $00


DATA_BossTilesets_Stage4:                                           ; $2F2C
.db :PAL_Stage4_Boss_BG
    .dw PAL_Stage4_Boss_BG
.db :UNK_Stage4_Boss_mappings
    .dw UNK_Stage4_Boss_mappings
.db :GFX_Stage4_Boss_Empty
    .dw GFX_Stage4_Boss_Empty
.dw $0040
.dw $4A00
.db $01
.db :GFX_Stage4_Boss_DarkNinja
    .dw $4500
    .dw GFX_Stage4_Boss_DarkNinja

; 14th entry of Jump Table from 135 (indexed by unknown)
_LABEL_2F3F_:
    ld      b, $00
    ld      a, (ix+28)
    cp      $02
    jr      z, +
    inc     b
+:
    ld      (ix+23), b
    call    _LABEL_6954_
    jr      nc, +
    ld      a, (ix+28)
    cp      $02
    jp      z, _LABEL_2D34_
    ld      a, $A2
    ld      (Sound_MusicTrigger), a
+:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, _DATA_2F6A_
    jp      call_jump_table

; Jump Table from 2F6A to 2F6F (3 entries, indexed by unknown)
_DATA_2F6A_:
.dw _LABEL_2F70_ _LABEL_2F81_ _LABEL_2FE3_

; 1st entry of Jump Table from 2F6A (indexed by unknown)
_LABEL_2F70_:
    inc     (ix+28)
    ld      (ix+29), $20
    ld      hl, $A939
    ld      a, $01
    ld      b, $10
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 2F6A (indexed by unknown)
_LABEL_2F81_:
    ld      bc, $0000
    call    _LABEL_6B8B_
    ld      de, $0020
    cp      $01
    jr      z, +
    cp      $02
    jr      z, +
    cp      $05
    jp      nz, _LABEL_2D46_
+:
    call    _LABEL_2D62_
    dec     (ix+29)
    jr      z, ++
    ld      a, (ix+30)
    or      a
    ld      hl, $0080
    jr      nz, +
    ld      hl, $FF80
+:
    ld      (ix+16), l
    ld      (ix+17), h
    ret

++:
    ld      (ix+29), $20
    ld      a, (ix+30)
    xor     $01
    ld      (ix+30), a
    call    _LABEL_3045_
    or      a
    ret     nz
    ld      (iy+0), $1C
    ld      a, (ix+13)
    ld      (iy+13), a
    ld      a, (ix+11)
    sub     $0A
    ld      (iy+11), a
    inc     (ix+28)
    call    _LABEL_2DB5_
    ld      hl, $A95D
    xor     a
    ld      b, a
    jp      _LABEL_2D00_

; 3rd entry of Jump Table from 2F6A (indexed by unknown)
_LABEL_2FE3_:
    call    _LABEL_3045_
    or      a
    ret     nz
    dec     (ix+28)
    ld      hl, $A939
    ld      a, $01
    ld      b, $10
    jp      _LABEL_2D00_

; 28th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2FF5_:
    call    _LABEL_6954_
    jr      nc, +
    ld      a, $A2
    ld      (Sound_MusicTrigger), a
+:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $300B
    jp      call_jump_table

; Jump Table from 300B to 300E (2 entries, indexed by unknown)
_DATA_300B_:
.dw _LABEL_300F_ _LABEL_302C_

; 1st entry of Jump Table from 300B (indexed by unknown)
_LABEL_300F_:
    inc     (ix+28)
    ld      hl, $FE80
    ld      (ix+16), l
    ld      (ix+17), h
    ld      (ix+29), $20
    set     5, (ix+1)
    ld      hl, $A972
    ld      a, $02
    ld      b, a
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 300B (indexed by unknown)
_LABEL_302C_:
    push    ix
    pop     iy
    ld      de, $FF40
    add     iy, de
    ld      a, (iy+13)
    sub     (ix+13)
    jp      c, _LABEL_88F_
    dec     (ix+29)
    ret     nz
    jp      _LABEL_2D20_

_LABEL_3045_:
    push    ix
    pop     iy
    ld      de, $00C0
    add     iy, de
    ld      a, (iy+0)
    ret

; 5th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3052_:
    ld      a, (Alex_PowerupState)
    or      a
    jr      z, +
    call    _LABEL_6954_
    jr      c, _LABEL_30AB_
+:
    ld      a, (ix+28)
    ld      hl, $3066
    jp      call_jump_table

; Jump Table from 3066 to 306B (3 entries, indexed by unknown)
_DATA_3066_:
.dw _LABEL_306C_ _LABEL_3095_ _LABEL_30CA_

; 1st entry of Jump Table from 3066 (indexed by unknown)
_LABEL_306C_:
    call    _LABEL_30D0_
    ld      a, $93
    ld      (Sound_MusicTrigger), a
    ld      a, (iy+13)
    ld      (ix+13), a
    ld      a, (iy+11)
    sub     $11
    ld      (ix+11), a
    ld      a, (iy+9)
    ld      (ix+9), a
    call    _LABEL_7205_
    ld      hl, $ABF7
    ld      a, $01
    ld      b, $02
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 3066 (indexed by unknown)
_LABEL_3095_:
    call    _LABEL_69E5_
    ld      a, (ix+19)
    or      a
    jp      nz, _LABEL_88F_
    ld      bc, $0000
    call    _LABEL_6B8B_
    cp      $02
    ret     nz
    jp      _LABEL_88F_

_LABEL_30AB_:
    ld      a, $A2
    ld      (Sound_MusicTrigger), a
    ld      a, (ix+9)
    or      a
    ld      hl, $0180
    jr      z, +
    ld      h, $FE
+:
    ld      (ix+16), l
    ld      (ix+17), h
    ld      (ix+15), $FE
    ld      (ix+28), $02
    ret

; 3rd entry of Jump Table from 3066 (indexed by unknown)
_LABEL_30CA_:
    ld      de, $0040
    jp      _LABEL_2D49_

_LABEL_30D0_:
    inc     (ix+28)
    set     5, (ix+1)
    push    ix
    pop     iy
    ld      de, $FF40
    add     iy, de
    ret

; 7th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_30E1_:
    ld      a, (_RAM_C31C_)
    cp      $05
    jr      nc, +
    call    _LABEL_6954_
    jp      c, _LABEL_3173_
+:
    call    _LABEL_69E5_
    ld      a, (_RAM_C31C_)
    ld      hl, $30FA
    jp      call_jump_table

; Jump Table from 30FA to 3109 (8 entries, indexed by unknown)
_DATA_30FA_:
.dw _LABEL_310A_ _LABEL_3131_ _LABEL_314A_ _LABEL_3220_ _LABEL_3256_ _LABEL_31A5_ _LABEL_31F3_ _LABEL_3276_

; 1st entry of Jump Table from 30FA (indexed by unknown)
_LABEL_310A_:
    bit     4, (ix+1)
    jr      nz, +
    set     4, (ix+1)
    set     6, (ix+1)
    ld      (ix+30), $04
+:
    ld      (ix+28), $01
    call    _LABEL_2D9E_
    ld      (ix+29), $20
    ld      hl, $3291
    ld      a, $01
    ld      b, $08
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 30FA (indexed by unknown)
_LABEL_3131_:
    dec     (ix+29)
    jr      z, +
    jp      _LABEL_2D9E_

+:
    call    _LABEL_2DB5_
    ld      (ix+28), $02
    ld      hl, $3299
    ld      a, $01
    ld      b, $08
    jp      _LABEL_2D00_

; 3rd entry of Jump Table from 30FA (indexed by unknown)
_LABEL_314A_:
    ld      a, (_RAM_C305_)
    cp      $01
    ret     nz
    ld      a, (_RAM_C307_)
    ld      c, $08
    cp      $07
    jr      z, _LABEL_3161_
    cp      $01
    ret     nz
    ld      (ix+28), $00
    ret

_LABEL_3161_:
    ld      hl, _RAM_C320_
    ld      de, $0020
    ld      b, $0D
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), c
    ret

+:
    add     hl, de
    djnz    -
    ret

_LABEL_3173_:
    ld      a, $A8
    ld      (Sound_MusicTrigger), a
    dec     (ix+30)
    jr      z, _LABEL_31D5_
    ld      (ix+28), $05
    call    _LABEL_2D83_
    ld      hl, $02C0
    or      a
    jr      z, +
    ld      hl, $FD40
+:
    ld      (_RAM_C310_), hl
    ld      a, (_RAM_C31F_)
    or      a
    jr      z, +
    ld      (ix+29), $06
    ret

+:
    ld      (ix+29), $10
    ld      hl, $32A1
    jp      _LABEL_2CFE_

; 6th entry of Jump Table from 30FA (indexed by unknown)
_LABEL_31A5_:
    dec     (ix+29)
    jr      z, +++
    bit     7, (ix+17)
    ld      a, (_RAM_C30D_)
    jr      nz, +
    cp      $E0
    ret     c
    ld      a, $E0
    jr      ++

+:
    cp      $20
    ret     nc
    ld      a, $20
++:
    ld      (_RAM_C30D_), a
    jp      _LABEL_2D20_

+++:
    res     3, (ix+1)
    ld      a, (_RAM_C31F_)
    or      a
    jr      z, +
    jr      _LABEL_320B_

+:
    ld      (_RAM_C31C_), a
    ret

_LABEL_31D5_:
    ld      hl, _RAM_C31F_
    ld      a, (hl)
    or      a
    jp      nz, _LABEL_326C_
    inc     (hl)
    ld      (ix+28), $06
    ld      (ix+30), $03
    call    _LABEL_2DB5_
    ld      hl, $32A5
    ld      a, $08
    ld      b, $04
    jp      _LABEL_2D00_

; 7th entry of Jump Table from 30FA (indexed by unknown)
_LABEL_31F3_:
    ld      a, (_RAM_C301_)
    xor     $08
    ld      (_RAM_C301_), a
    ld      a, (_RAM_C305_)
    cp      $08
    ret     nz
    ld      a, (_RAM_C307_)
    cp      $01
    ret     nz
    res     3, (ix+1)
_LABEL_320B_:
    ld      (ix+28), $03
    call    _LABEL_2D9E_
    ld      (ix+29), $10
    ld      hl, $32C9
    ld      a, $01
    ld      b, $04
    jp      _LABEL_2D00_

; 4th entry of Jump Table from 30FA (indexed by unknown)
_LABEL_3220_:
    dec     (ix+29)
    jr      z, ++
    ld      a, (_RAM_C30D_)
    cp      $20
    ld      b, $01
    ld      h, $00
    ld      l, $80
    jr      c, +
    cp      $E0
    dec     b
    dec     h
    jr      nc, +
    call    _LABEL_5FF_
    cp      $10
    jp      c, _LABEL_2D20_
    ret

+:
    ld      (ix+9), b
    ld      (_RAM_C310_), hl
    ret

++:
    ld      (ix+28), $04
    ld      hl, $32D1
    ld      a, $01
    ld      b, $06
    jp      _LABEL_2D00_

; 5th entry of Jump Table from 30FA (indexed by unknown)
_LABEL_3256_:
    ld      a, (_RAM_C305_)
    cp      $01
    ret     nz
    ld      a, (_RAM_C307_)
    ld      c, $09
    cp      $05
    jp      z, _LABEL_3161_
    cp      $01
    ret     nz
    jp      _LABEL_320B_

_LABEL_326C_:
    ld      hl, $FA00
    ld      (_RAM_C30E_), hl
    ld      (ix+28), $07
; 8th entry of Jump Table from 30FA (indexed by unknown)
_LABEL_3276_:
    ld      de, $0080
    call    _LABEL_2D46_
    bit     7, h
    ret     nz
_LABEL_327F_:
    ld      a, (_RAM_DD94_)
    or      a
    jr      nz, +
    ld      a, $01
    ld      (_RAM_DD37_), a
+:
    xor     a
    ld      (_RAM_DD94_), a
    jp      _LABEL_2D3A_

; Data from 3291 to 343A (426 bytes)
.db $D9 $32 $6A $33 $F6 $32 $87 $33 $13 $33 $A4 $33 $30 $33 $C1 $33
.db $4D $33 $DE $33 $FB $33 $1B $34 $30 $33 $C1 $33 $FB $33 $1B $34
.db $30 $33 $C1 $33 $FB $33 $1B $34 $30 $33 $C1 $33 $FB $33 $1B $34
.db $30 $33 $C1 $33 $FB $33 $1B $34 $FB $33 $1B $34 $03 $34 $23 $34
.db $0B $34 $2B $34 $13 $34 $33 $34 $09 $04 $D1 $F4 $48 $D1 $FC $4A
.db $D1 $04 $4C $E1 $F4 $4E $E1 $FC $50 $E1 $04 $52 $F1 $F4 $54 $F1
.db $FC $56 $F1 $04 $58 $09 $04 $D0 $F4 $5A $D0 $FC $5C $D0 $04 $4C
.db $E0 $F4 $5E $E0 $FC $60 $E0 $04 $62 $F0 $F4 $64 $F0 $FC $66 $F0
.db $04 $68 $09 $04 $D1 $F4 $6A $D1 $FC $6C $D1 $04 $4C $E1 $F4 $6E
.db $E1 $FC $70 $E1 $04 $52 $F1 $F4 $72 $F1 $FC $74 $F1 $04 $76 $09
.db $04 $D1 $F4 $78 $D1 $FC $7A $D1 $04 $4C $E1 $F4 $7C $E1 $FC $7E
.db $E1 $04 $52 $F1 $F4 $72 $F1 $FC $74 $F1 $04 $76 $09 $04 $D0 $F4
.db $80 $D0 $FC $82 $D0 $04 $84 $E0 $F4 $86 $E0 $FC $88 $E0 $04 $8A
.db $F0 $F4 $8C $F0 $FC $8E $F0 $04 $90 $09 $04 $D1 $F4 $A4 $D1 $FC
.db $A2 $D1 $04 $A0 $E1 $F4 $AA $E1 $FC $A8 $E1 $04 $A6 $F1 $F4 $B0
.db $F1 $FC $AE $F1 $04 $AC $09 $04 $D0 $F4 $A4 $D0 $FC $B4 $D0 $04
.db $B2 $E0 $F4 $BA $E0 $FC $B8 $E0 $04 $B6 $F0 $F4 $C0 $F0 $FC $BE
.db $F0 $04 $BC $09 $04 $D1 $F4 $A4 $D1 $FC $C4 $D1 $04 $C2 $E1 $F4
.db $AA $E1 $FC $C8 $E1 $04 $C6 $F1 $F4 $CE $F1 $FC $CC $F1 $04 $CA
.db $09 $04 $D1 $F4 $A4 $D1 $FC $D2 $D1 $04 $D0 $E1 $F4 $AA $E1 $FC
.db $D6 $E1 $04 $D4 $F1 $F4 $CE $F1 $FC $CC $F1 $04 $CA $09 $04 $D0
.db $F4 $DC $D0 $FC $DA $D0 $04 $D8 $E0 $F4 $E2 $E0 $FC $E0 $E0 $04
.db $DE $F0 $F4 $E8 $F0 $FC $E6 $F0 $04 $E4 $02 $05 $F0 $F8 $92 $F0
.db $00 $94 $02 $05 $F0 $F8 $96 $F0 $00 $98 $02 $05 $F0 $F8 $9A $F0
.db $00 $94 $02 $05 $F0 $F8 $9C $F0 $00 $94 $02 $05 $F0 $F8 $EC $F0
.db $00 $EA $02 $05 $F0 $F8 $F0 $F0 $00 $EE $02 $05 $F0 $F8 $EC $F0
.db $00 $F2 $02 $05 $F0 $F8 $EC $F0 $00 $F4

; 8th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_343B_:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $3447
    jp      call_jump_table

; Jump Table from 3447 to 344A (2 entries, indexed by unknown)
_DATA_3447_:
.dw _LABEL_344B_ _LABEL_347D_

; 1st entry of Jump Table from 3447 (indexed by unknown)
_LABEL_344B_:
    set     5, (ix+1)
    inc     (ix+28)
    ld      a, (_RAM_C309_)
    ld      (ix+9), a
    or      a
    ld      hl, $FEC0
    ld      b, $EC
    jr      z, +
    ld      hl, $0140
    ld      b, $14
+:
    call    _LABEL_2DB8_
    ld      a, (_RAM_C30D_)
    add     a, b
    ld      (ix+13), a
    ld      a, (_RAM_C30B_)
    sub     $1C
    ld      (ix+11), a
    ld      hl, $AC09
    jp      _LABEL_2CFE_

; 2nd entry of Jump Table from 3447 (indexed by unknown)
_LABEL_347D_:
    ld      a, (ix+11)
    ld      b, a
    ld      a, (StageNumber)
    or      a
    ld      a, b
    jr      nz, +
    cp      $A0
    jr      ++

+:
    cp      $98
++:
    jr      nc, +
    ld      de, $0040
    jp      _LABEL_2D49_

+:
    ld      (ix+15), $FC
    ret

; 9th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_349B_:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $34A7
    jp      call_jump_table

; Jump Table from 34A7 to 34AA (2 entries, indexed by unknown)
_DATA_34A7_:
.dw _LABEL_34AB_ _LABEL_34DD_

; 1st entry of Jump Table from 34A7 (indexed by unknown)
_LABEL_34AB_:
    set     5, (ix+1)
    inc     (ix+28)
    ld      a, (_RAM_C309_)
    ld      (ix+9), a
    or      a
    ld      hl, $FE00
    ld      b, $F6
    jr      z, +
    ld      hl, $0200
    ld      b, $0A
+:
    call    _LABEL_2DB8_
    ld      a, (_RAM_C30D_)
    add     a, b
    ld      (ix+13), a
    ld      a, (_RAM_C30B_)
    sub     $08
    ld      (ix+11), a
    ld      hl, $AC15
    jp      _LABEL_2CFE_

; 2nd entry of Jump Table from 34A7 (indexed by unknown)
_LABEL_34DD_:
    ld      a, (ix+11)
    ld      b, a
    ld      a, (StageNumber)
    or      a
    ld      a, b
    jr      nz, +
    cp      $A8
    jr      ++

+:
    cp      $98
++:
    jr      nc, +
    ld      de, $00A0
    jp      _LABEL_2D49_

+:
    ld      (ix+15), $FC
    ret

_LABEL_34FB_:
    ld      (ix+13), $80
    ld      (ix+11), $A0
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $FF
    jr      nz, +
    ld      a, $D4
+:
    ld      (_RAM_C31D_), a
    ld      hl, $B867
    ld      a, $05
    ld      b, $10
    jp      _LABEL_2D00_

; 10th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_351A_:
    ld      a, (_RAM_C31C_)
    ld      hl, $3523
    jp      call_jump_table

; Jump Table from 3523 to 3544 (17 entries, indexed by unknown)
_DATA_3523_:
.dw _LABEL_3545_ _LABEL_3580_ _LABEL_3591_ _LABEL_35BA_ _LABEL_35D5_ _LABEL_35F3_ _LABEL_360C_ _LABEL_362E_
.dw _LABEL_3663_ _LABEL_3670_ _LABEL_368F_ _LABEL_36A0_ _LABEL_36D1_ _LABEL_36DD_ _LABEL_3723_ _LABEL_3759_
.dw _LABEL_355B_

; 1st entry of Jump Table from 3523 (indexed by unknown)
_LABEL_3545_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $10
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $60
    jr      nz, +
    ld      a, $50
+:
    ld      (_RAM_C31D_), a
    ret

; 17th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_355B_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $01
    ld      hl, _DATA_375A_
    ld      de, PaletteBuffer
    ld      bc, $0011
    ldir
    ld      (ix+7), $00
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $20
    jr      nz, +
    ld      a, $1A
+:
    ld      (_RAM_C31D_), a
    ret

; 2nd entry of Jump Table from 3523 (indexed by unknown)
_LABEL_3580_:
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $04
    inc     (ix+28)
    ld      hl, $B963
    jp      _LABEL_2CFE_

; 3rd entry of Jump Table from 3523 (indexed by unknown)
_LABEL_3591_:
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $04
    ld      a, (_RAM_C31F_)
    inc     a
    cp      $09
    jr      z, +
    ld      (_RAM_C31F_), a
    ld      hl, $3799
    rst     $18    ; get_array_index
    ld      de, PaletteBuffer
    ld      bc, $0011
    ldir
    ret

+:
    inc     (ix+28)
    ld      a, $15
    ld      (_RAM_C320_), a
    ret

; 4th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_35BA_:
    ld      a, (_RAM_C33F_)
    or      a
    ret     z
    inc     (ix+28)
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $40
    jr      nz, +
    ld      a, $35
+:
    ld      (_RAM_C31D_), a
    ld      hl, $B98D
    jp      _LABEL_2CFE_

; 5th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_35D5_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $40
    jr      nz, +
    ld      a, $35
+:
    ld      (_RAM_C31D_), a
    ld      hl, $B9AB
    ld      a, $01
    ld      b, $0C
    jp      _LABEL_2D00_

; 6th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_35F3_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+7), $00
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $40
    jr      nz, +
    ld      a, $35
+:
    ld      (_RAM_C31D_), a
    ret

; 7th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_360C_:
    dec     (ix+29)
    ret     nz
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $A0
    jr      nz, +
    ld      a, $85
+:
    ld      (_RAM_C31D_), a
    ld      (ix+31), $00
    inc     (ix+28)
    ld      hl, $B9DB
    ld      a, $20
    ld      b, $02
    jp      _LABEL_2D00_

; 8th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_362E_:
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $04
    ld      a, (_RAM_C31F_)
    inc     a
    cp      $0A
    jr      z, +
    ld      (_RAM_C31F_), a
    ld      hl, $3799
    rst     $18    ; get_array_index
    ld      de, PaletteBuffer
    ld      bc, $0011
    ldir
    ret

+:
    inc     (ix+28)
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $C0
    jr      nz, +
    ld      a, $A0
+:
    ld      (_RAM_C31D_), a
    ld      (ix+31), $00
    ret

; 9th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_3663_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      a, $12
    ld      (_RAM_C320_), a
    ret

; 10th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_3670_:
    ld      a, (_RAM_C31F_)
    or      a
    ret     z
    inc     (ix+28)
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $12
    jr      nz, +
    ld      a, $0F
+:
    ld      (_RAM_C31D_), a
    ld      hl, $BA7B
    ld      a, $03
    ld      b, $08
    jp      _LABEL_2D00_

; 11th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_368F_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $01
    ld      hl, $BAED
    jp      _LABEL_2CFE_

; 12th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_36A0_:
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $06
    ld      a, (_RAM_C31F_)
    inc     a
    cp      $0A
    jr      z, +
    ld      (_RAM_C31F_), a
    ld      hl, $37AB
    rst     $18    ; get_array_index
    ld      de, PaletteBuffer
    ld      bc, $0011
    ldir
    ret

+:
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $30
    jr      nz, +
    ld      a, $28
+:
    ld      (_RAM_C31D_), a
    inc     (ix+28)
    ret

; 13th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_36D1_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+31), $00
    ret

; 14th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_36DD_:
    ld      a, (_RAM_C31F_)
    inc     a
    ld      (_RAM_C31F_), a
    cp      $1F
    jr      z, +
    ld      hl, $38F5
    rst     $18    ; get_array_index
    ld      (_RAM_DD02_), hl
    ret

+:
    ld      hl, $0000
    ld      (_RAM_DD02_), hl
    ld      (ix+31), $00
    inc     (ix+28)
    ld      hl, _DATA_376B_
    ld      de, PaletteBuffer
    ld      bc, $0010
    ldir
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $0D
    jr      nz, +
    ld      a, $10
+:
    ld      (_RAM_C31D_), a
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $FF
    jr      nz, +
    ld      a, $D4
+:
    ld      (_RAM_C31E_), a
    ret

; 15th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_3723_:
    dec     (ix+30)
    jr      z, ++
    dec     (ix+29)
    ret     nz
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $10
    jr      nz, +
    ld      a, $0D
+:
    ld      (_RAM_C31D_), a
    ld      a, (_RAM_C31F_)
    inc     a
    ld      (_RAM_C31F_), a
    and     $01
    ld      hl, _DATA_377B_
    jr      z, +
    ld      hl, _DATA_378B_
+:
    ld      de, PaletteBuffer
    ld      bc, $0010
    ldir
    ret

++:
    ld      a, $01
    ld      (_RAM_DD04_), a
    ret

; 16th entry of Jump Table from 3523 (indexed by unknown)
_LABEL_3759_:
    ret

; Data from 375A to 376A (17 bytes)
_DATA_375A_:
.db $24 $26 $16 $06 $04 $05 $38 $04 $14 $24 $24 $24 $05 $04 $24 $24
.db $24

; Data from 376B to 377A (16 bytes)
_DATA_376B_:
.db $00 $12 $11 $11 $21 $10 $10 $10 $20 $03 $0F $3F $3F $3F $00 $00

; Data from 377B to 378A (16 bytes)
_DATA_377B_:
.db $00 $12 $11 $11 $21 $10 $10 $10 $20 $03 $0F $00 $3F $3F $00 $00

; Data from 378B to 3932 (424 bytes)
_DATA_378B_:
.db $00 $12 $11 $11 $21 $10 $10 $10 $20 $03 $0F $3F $3F $3F $00 $00
.db $C5 $37 $D6 $37 $E7 $37 $F8 $37 $09 $38 $1A $38 $2B $38 $3C $38
.db $4D $38 $5E $38 $6F $38 $80 $38 $91 $38 $A2 $38 $B3 $38 $C4 $38
.db $D5 $38 $E6 $38 $F7 $38 $F7 $38 $F7 $38 $24 $26 $16 $06 $04 $05
.db $38 $04 $14 $24 $24 $24 $05 $04 $0F $24 $24 $30 $30 $30 $30 $30
.db $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $39 $3B $2B $1B
.db $09 $0A $3E $19 $29 $39 $39 $39 $0A $09 $39 $39 $39 $24 $26 $16
.db $06 $04 $05 $38 $04 $14 $24 $24 $24 $05 $04 $24 $24 $24 $24 $26
.db $16 $06 $04 $05 $38 $04 $14 $24 $24 $24 $05 $04 $24 $0F $24 $30
.db $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30
.db $39 $3B $2B $1B $09 $0A $3E $19 $29 $39 $39 $39 $0A $09 $39 $39
.db $39 $24 $26 $16 $06 $04 $05 $38 $04 $14 $24 $24 $24 $05 $04 $24
.db $24 $24 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00
.db $00 $00 $00 $00 $26 $25 $24 $14 $10 $10 $10 $14 $00 $00 $00 $10
.db $14 $0F $0F $00 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30
.db $30 $30 $30 $30 $30 $00 $26 $25 $24 $14 $10 $10 $10 $14 $00 $00
.db $00 $10 $14 $00 $00 $00 $24 $26 $16 $06 $04 $05 $38 $04 $14 $03
.db $24 $24 $05 $04 $24 $0F $00 $30 $30 $30 $30 $30 $30 $30 $30 $30
.db $30 $30 $30 $30 $30 $30 $30 $30 $00 $12 $11 $11 $21 $10 $10 $10
.db $20 $03 $00 $00 $00 $00 $00 $00 $00 $10 $25 $26 $16 $04 $14 $20
.db $14 $24 $03 $10 $10 $14 $04 $0F $10 $10 $30 $30 $30 $30 $30 $30
.db $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $30 $00 $12 $11 $11 $21
.db $10 $10 $10 $20 $03 $0F $00 $00 $00 $00 $00 $00 $00 $80 $00 $81
.db $00 $82 $00 $83 $00 $84 $00 $85 $00 $86 $00 $87 $00 $88 $00 $89
.db $00 $8A $00 $8B $00 $8C $00 $8D $00 $8E $00 $8F $00 $90 $00 $91
.db $00 $92 $00 $93 $00 $94 $00 $95 $00 $96 $00 $97 $00 $98 $00 $99
.db $00 $9A $00 $9B $00 $9C $00 $9D

; 17th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3933_:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $393F
    jp      call_jump_table

; Jump Table from 393F to 3948 (5 entries, indexed by unknown)
_DATA_393F_:
.dw _LABEL_3949_ _LABEL_3954_ _LABEL_396A_ _LABEL_3995_ _LABEL_39C0_

; 1st entry of Jump Table from 393F (indexed by unknown)
_LABEL_3949_:
    inc     (ix+28)
    ld      hl, $39DB
    xor     a
    ld      b, a
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 393F (indexed by unknown)
_LABEL_3954_:
    ld      b, $1A
    call    _LABEL_2D92_
    ret     nc
    inc     (ix+28)
    ld      (ix+14), $00
    ld      (ix+15), $04
    ld      (ix+5), $01
    ret

; 3rd entry of Jump Table from 393F (indexed by unknown)
_LABEL_396A_:
    ld      a, (ix+11)
    cp      $A0
    ret     c
    ld      (ix+11), $A0
    ld      (ix+15), $00
    ld      (ix+14), $00
    ld      (ix+5), $00
    ld      (ix+29), $08
    inc     (ix+28)
    ld      de, $89DC
    rst     $28    ; vdp_write_control
    ld      a, e
    ld      (VDPRegister9), a
    ld      a, $B3
    ld      (Sound_MusicTrigger), a
    ret

; 4th entry of Jump Table from 393F (indexed by unknown)
_LABEL_3995_:
    ld      a, (VDPRegister9)
    cp      $DF
    jr      z, +
    inc     a
    ld      (VDPRegister9), a
    ld      d, $89
    ld      e, a
    rst     $28    ; vdp_write_control
+:
    ld      de, $8900
    rst     $28    ; vdp_write_control
    ld      a, e
    ld      (VDPRegister9), a
    dec     (ix+29)
    ret     nz
    ld      (ix+14), $C0
    ld      (ix+15), $FF
    ld      (ix+5), $01
    inc     (ix+28)
    ret

; 5th entry of Jump Table from 393F (indexed by unknown)
_LABEL_39C0_:
    ld      a, (ix+11)
    cp      $60
    ret     nc
    ld      (ix+14), $00
    ld      (ix+15), $00
    ld      (ix+11), $60
    ld      (ix+28), $01
    ld      (ix+5), $00
    ret

; Data from 39DB to 3A2E (84 bytes)
.db $E3 $39 $E3 $39 $09 $3A $09 $3A $0C $0A $D0 $F0 $48 $D0 $F8 $4A
.db $D0 $00 $4C $D0 $08 $4E $E0 $F0 $50 $E0 $F8 $52 $E0 $00 $54 $E0
.db $08 $56 $F0 $F0 $58 $F0 $F8 $5A $F0 $00 $5C $F0 $08 $5E $0C $0A
.db $D0 $F0 $60 $D0 $F8 $62 $D0 $00 $64 $D0 $08 $66 $E0 $F0 $68 $E0
.db $F8 $6A $E0 $00 $6C $E0 $08 $6E $F0 $F0 $58 $F0 $F8 $70 $F0 $00
.db $72 $F0 $08 $5E

; 20th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3A2F_:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $3A3B
    jp      call_jump_table

; Jump Table from 3A3B to 3A44 (5 entries, indexed by unknown)
_DATA_3A3B_:
.dw _LABEL_3A45_ _LABEL_3A56_ _LABEL_3A6C_ _LABEL_3A8F_ _LABEL_3AAA_

; 1st entry of Jump Table from 3A3B (indexed by unknown)
_LABEL_3A45_:
    ld      a, (ix+13)
    ld      (ix+23), a
    inc     (ix+28)
    ld      hl, $3AB3
    xor     a
    ld      b, a
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 3A3B (indexed by unknown)
_LABEL_3A56_:
    call    _LABEL_5FF_
    bit     7, a
    ld      hl, $0200
    jr      z, +
    ld      h, $FE
+:
    ld      (ix+16), l
    ld      (ix+17), h
    inc     (ix+28)
    ret

; 3rd entry of Jump Table from 3A3B (indexed by unknown)
_LABEL_3A6C_:
    ld      a, (ix+13)
    sub     (ix+23)
    jr      nc, +
    neg
+:
    cp      $10
    ret     c
    ld      a, (ix+17)
    bit     7, a
    ld      hl, $0080
    jr      nz, +
    ld      h, $FF
+:
    ld      (ix+16), l
    ld      (ix+17), h
    inc     (ix+28)
    ret

; 4th entry of Jump Table from 3A3B (indexed by unknown)
_LABEL_3A8F_:
    ld      a, (ix+13)
    sub     (ix+23)
    ret     nz
    ld      (ix+12), $00
    ld      (ix+16), $00
    ld      (ix+17), $00
    ld      (ix+29), $40
    inc     (ix+28)
    ret

; 5th entry of Jump Table from 3A3B (indexed by unknown)
_LABEL_3AAA_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $01
    ret

; Data from 3AB3 to 3AC4 (18 bytes)
.db $B7 $3A $B7 $3A $04 $0C $F8 $F0 $74 $F8 $F8 $76 $F8 $00 $7A $F8
.db $08 $7C

; 21st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3AC5_:
    ld      a, (_RAM_C33C_)
    ld      hl, $3ACE
    jp      call_jump_table

; Jump Table from 3ACE to 3AD9 (6 entries, indexed by unknown)
_DATA_3ACE_:
.dw _LABEL_3ADA_ _LABEL_3AF7_ _LABEL_3B12_ _LABEL_3B23_ _LABEL_3B3E_ _LABEL_3B53_

; 1st entry of Jump Table from 3ACE (indexed by unknown)
_LABEL_3ADA_:
    inc     (ix+28)
    ld      (ix+13), $94
    ld      (ix+11), $08
    ld      (ix+15), $04
    set     5, (ix+1)
    ld      hl, $3B54
    ld      a, $02
    ld      b, $01
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 3ACE (indexed by unknown)
_LABEL_3AF7_:
    ld      a, (_RAM_C32B_)
    cp      $A0
    ret     c
    inc     (ix+28)
    ld      (ix+11), $A0
    ld      (ix+15), $00
    ld      (ix+29), $10
    ld      hl, $3B54
    jp      _LABEL_2CFE_

; 3rd entry of Jump Table from 3ACE (indexed by unknown)
_LABEL_3B12_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      hl, $3B60
    ld      a, $01
    ld      b, $04
    jp      _LABEL_2D00_

; 4th entry of Jump Table from 3ACE (indexed by unknown)
_LABEL_3B23_:
    ld      a, (_RAM_C325_)
    cp      $01
    ret     nz
    inc     (ix+31)
    ld      a, (_RAM_C327_)
    cp      $03
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $10
    ld      (ix+7), $00
    ret

; 5th entry of Jump Table from 3ACE (indexed by unknown)
_LABEL_3B3E_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+15), $FC
    ld      hl, $3B68
    ld      a, $02
    ld      b, $01
    jp      _LABEL_2D00_

; 6th entry of Jump Table from 3ACE (indexed by unknown)
_LABEL_3B53_:
    ret

; Data from 3B54 to 3C55 (258 bytes)
.db $74 $3B $74 $3B $9A $3B $9A $3B $C0 $3B $C0 $3B $E6 $3B $E6 $3B
.db $1E $3C $1E $3C $9A $3B $9A $3B $C0 $3B $C0 $3B $74 $3B $74 $3B
.db $0C $00 $D0 $F8 $5C $D0 $00 $5D $D8 $F8 $5E $D8 $00 $5F $E0 $F8
.db $60 $E0 $00 $61 $E8 $F8 $62 $E8 $00 $63 $F0 $F8 $64 $F0 $00 $65
.db $F8 $F8 $66 $F8 $00 $67 $0C $00 $D0 $F8 $78 $D0 $00 $79 $D8 $F8
.db $7A $D8 $00 $7B $E0 $F8 $7C $E0 $00 $7D $E8 $F8 $72 $E8 $00 $63
.db $F0 $F8 $75 $F0 $00 $7E $F8 $F8 $75 $F8 $00 $7F $0C $00 $D0 $F8
.db $80 $D0 $00 $81 $D8 $F8 $82 $D8 $00 $83 $E0 $F8 $84 $E0 $00 $85
.db $E8 $F8 $72 $E8 $00 $63 $F0 $F8 $75 $F0 $00 $7E $F8 $F8 $75 $F8
.db $00 $7F $12 $00 $D0 $F0 $68 $D0 $F8 $5C $D0 $00 $5D $D8 $F0 $69
.db $D8 $F8 $5E $D8 $00 $5F $E0 $F0 $6A $E0 $F8 $6B $E0 $00 $61 $E8
.db $F0 $6C $E8 $F8 $6C $E8 $00 $63 $F0 $F0 $6D $F0 $F8 $6C $F0 $00
.db $65 $F8 $F0 $6E $F8 $F8 $6C $F8 $00 $67 $12 $00 $D0 $F8 $5C $D0
.db $00 $5D $D0 $08 $25 $D8 $F8 $5E $D8 $00 $5F $D8 $08 $25 $E0 $F8
.db $6F $E0 $00 $70 $E0 $08 $71 $E8 $F8 $72 $E8 $00 $73 $E8 $08 $74
.db $F0 $F8 $75 $F0 $00 $76 $F0 $08 $25 $F8 $F8 $77 $F8 $00 $67 $F8
.db $08 $25

; 18th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3C56_:
    ld      a, (_RAM_C33C_)
    ld      hl, $3C5F
    jp      call_jump_table

; Jump Table from 3C5F to 3C72 (10 entries, indexed by unknown)
_DATA_3C5F_:
.dw _LABEL_3C73_ _LABEL_3C88_ _LABEL_3CA2_ _LABEL_3CAF_ _LABEL_3CB4_ _LABEL_3CC2_ _LABEL_3CD0_ _LABEL_3CFD_
.dw _LABEL_3D1D_ _LABEL_3D36_

; 1st entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3C73_:
    inc     (ix+28)
    ld      (ix+11), $30
    ld      (ix+13), $D0
    ld      (ix+29), $10
    ld      hl, $3D44
    jp      _LABEL_2CFE_

; 2nd entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3C88_:
    dec     (ix+29)
    ret     nz
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $40
    jr      nz, +
    ld      a, $35
+:
    ld      (_RAM_C33D_), a
    inc     (ix+28)
    ld      hl, $3D48
    jp      _LABEL_2CFE_

; 3rd entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3CA2_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      a, $3F
    ld      (_RAM_C03F_), a
    ret

; 4th entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3CAF_:
    ld      hl, $9F77
    jr      +

; 5th entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3CB4_:
    ld      a, (_RAM_C0BF_)
    or      a
    ret     nz
    dec     (ix+29)
    ret     nz
    ld      hl, $A049
    jr      +

; 6th entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3CC2_:
    ld      a, (_RAM_C0BF_)
    or      a
    ret     nz
    dec     (ix+29)
    ret     nz
    ld      hl, $A11B
    jr      +

; 7th entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3CD0_:
    ld      a, (_RAM_C0BF_)
    or      a
    ret     nz
    dec     (ix+29)
    ret     nz
    ld      hl, $A1ED
+:
    ld      de, $78C6
    ld      bc, $1515
    ld      a, $05
    ld      (_RAM_C0BF_), a
    inc     (ix+28)
    ld      a, (_RAM_C0FE_)
    or      a
    ld      a, $FF
    jr      nz, +
    ld      a, $C4
+:
    ld      (_RAM_C33D_), a
    ld      a, (_RAM_C0BF_)
    jp      _LABEL_754A_

; 8th entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3CFD_:
    ld      a, (_RAM_C0BF_)
    or      a
    ret     nz
    ld      a, (_RAM_C33D_)
    dec     a
    ld      (_RAM_C33D_), a
    cp      $20
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $26
    ld      hl, $3D4C
    ld      a, $03
    ld      b, $0A
    jp      _LABEL_2D00_

; 9th entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3D1D_:
    dec     (ix+29)
    ret     nz
    ld      hl, $FF00
    ld      (_RAM_C330_), hl
    ld      hl, $0130
    ld      (_RAM_C32E_), hl
    inc     (ix+28)
    ld      hl, $3D58
    jp      _LABEL_2CFE_

; 10th entry of Jump Table from 3C5F (indexed by unknown)
_LABEL_3D36_:
    ld      a, (_RAM_C32D_)
    cp      $80
    ret     nc
    ld      a, $01
    ld      (_RAM_C31F_), a
    jp      _LABEL_88F_

; Data from 3D44 to 3DC3 (128 bytes)
.db $5C $3D $5C $3D $82 $3D $82 $3D $82 $3D $82 $3D $5C $3D $5C $3D
.db $A8 $3D $A8 $3D $B6 $3D $B6 $3D $0C $00 $E8 $F8 $92 $E8 $00 $93
.db $F0 $F8 $94 $F0 $00 $95 $F8 $F8 $96 $F8 $00 $97 $00 $F8 $98 $00
.db $00 $99 $08 $F8 $9A $08 $00 $9B $10 $F8 $9C $10 $00 $9D $0C $00
.db $E8 $F8 $86 $E8 $00 $87 $F0 $F8 $88 $F0 $00 $89 $F8 $F8 $8A $F8
.db $00 $8B $00 $F8 $8C $00 $00 $8D $08 $F8 $8E $08 $00 $8F $10 $F8
.db $90 $10 $00 $91 $04 $00 $F8 $F8 $9E $F8 $00 $9F $00 $F8 $A0 $00
.db $00 $A1 $04 $00 $F8 $F8 $A2 $F8 $00 $A3 $00 $F8 $A4 $00 $00 $A5

; 13th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3DC4_:
    ld      a, (ix+28)
    ld      hl, $3DCD
    jp      call_jump_table

; Jump Table from 3DCD to 3DD2 (3 entries, indexed by unknown)
_DATA_3DCD_:
.dw _LABEL_3DD3_ _LABEL_3DE2_ _LABEL_3E0D_

; 1st entry of Jump Table from 3DCD (indexed by unknown)
_LABEL_3DD3_:
    inc     (ix+28)
    ld      (ix+29), $20
    ld      hl, $3E7B
    xor     a
    ld      b, a
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 3DCD (indexed by unknown)
_LABEL_3DE2_:
    dec     (ix+29)
    ret     nz
    ld      a, (_RAM_C30D_)
    ld      (_RAM_C317_), a
    ld      a, (_RAM_C30B_)
    ld      (_RAM_C318_), a
    ld      a, (_RAM_C316_)
    inc     a
    and     $07
    ld      (_RAM_C316_), a
    ld      hl, $3E4B
    rst     $18    ; get_array_index
    ld      (ix+20), h
    ld      (ix+21), l
    inc     (ix+28)
    ld      (ix+29), $18
    ret

; 3rd entry of Jump Table from 3DCD (indexed by unknown)
_LABEL_3E0D_:
    dec     (ix+29)
    jr      z, ++
    bit     0, (ix+29)
    ld      hl, (_RAM_C314_)
    jr      nz, +
    ld      hl, (_RAM_C317_)
+:
    ld      (ix+13), l
    ld      (ix+11), h
    ret

++:
    nop
    ld      a, (_RAM_C316_)
    ld      de, _DATA_3E6B_
    call    +
    ld      a, (_RAM_C316_)
    dec     a
    and     $07
    ld      de, _DATA_3E73_
    call    +
    ld      (ix+28), $00
    ret

+:
    ld      hl, $3E5B
    rst     $18    ; get_array_index
    ex      de, hl
    ld      bc, $0008
    ldir
    ret

; Data from 3E4B to 3E6A (32 bytes)
.db $90 $D0 $78 $B0 $60 $D0 $60 $80 $38 $78 $28 $40 $60 $80 $40 $B0
.db $70 $DA $A8 $D9 $F0 $D8 $DC $D8 $9A $D7 $0C $D7 $DC $D8 $E8 $D7

; Data from 3E6B to 3E72 (8 bytes)
_DATA_3E6B_:
.db $00 $40 $00 $40 $00 $40 $00 $40

; Data from 3E73 to 3E8C (26 bytes)
_DATA_3E73_:
.db $00 $00 $00 $00 $00 $00 $00 $00 $7F $3E $7F $3E $04 $00 $F0 $F0
.db $7E $F0 $F8 $7E $F0 $00 $7E $F0 $08 $7E

; 69th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3E8D_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $3E9F
    jp      call_jump_table

; Jump Table from 3E9F to 3EA2 (2 entries, indexed by unknown)
_DATA_3E9F_:
.dw _LABEL_40A6_ _LABEL_3EA3_

; 2nd entry of Jump Table from 3E9F (indexed by unknown)
_LABEL_3EA3_:
    call    _LABEL_2D83_
    call    _LABEL_423A_
    ld      b, $40
    call    _LABEL_2D92_
    ld      hl, $FF00
    call    nc, _LABEL_4252_
    call    _LABEL_425C_
    call    c, _LABEL_2D20_
    call    _LABEL_428D_
    cp      $02
    call    z, _LABEL_2DB5_
    call    _LABEL_427C_
    jp      z, _LABEL_2DB5_
    ret

; 70th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3EC9_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $3EDB
    jp      call_jump_table

; Jump Table from 3EDB to 3EDE (2 entries, indexed by unknown)
_DATA_3EDB_:
.dw _LABEL_3EDF_ _LABEL_3EFE_

; 1st entry of Jump Table from 3EDB (indexed by unknown)
_LABEL_3EDF_:
    inc     (ix+28)
    call    _LABEL_42C1_
--:
    call    _LABEL_5FF_
    and     $03
    ld      (ix+31), a
-:
    ld      hl, $3EF4
    rst     $18    ; get_array_index
    jp      _LABEL_4252_

; Data from 3EF4 to 3EFD (10 bytes)
.db $C0 $FE $E0 $FE $20 $FF $30 $FF $00 $FF

; 2nd entry of Jump Table from 3EDB (indexed by unknown)
_LABEL_3EFE_:
    call    _LABEL_423A_
_LABEL_3F01_:
    ld      a, (ix+16)
    or      (ix+17)
    call    z, --
    call    _LABEL_2D83_
    ld      b, $40
    call    _LABEL_2D92_
    ld      a, (ix+31)
    call    nc, -
    call    _LABEL_425C_
    call    c, _LABEL_2D20_
    call    _LABEL_428D_
    cp      $02
    ret     nz
    ld      hl, $FE80
    jp      _LABEL_2DAE_

; 71st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3F2A_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $3F3C
    jp      call_jump_table

; Jump Table from 3F3C to 3F3F (2 entries, indexed by unknown)
_DATA_3F3C_:
.dw _LABEL_3F40_ _LABEL_3F4C_

; 1st entry of Jump Table from 3F3C (indexed by unknown)
_LABEL_3F40_:
    call    _LABEL_40A6_
    ld      (ix+31), $04
    xor     a
    ld      b, a
    jp      _LABEL_2D06_

; 2nd entry of Jump Table from 3F3C (indexed by unknown)
_LABEL_3F4C_:
    call    _LABEL_423A_
    jr      nz, +
    ld      hl, $FF80
    call    _LABEL_2DAE_
+:
    jp      _LABEL_3F01_

; 72nd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3F5A_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $3F6C
    jp      call_jump_table

; Jump Table from 3F6C to 3F73 (4 entries, indexed by unknown)
_DATA_3F6C_:
.dw _LABEL_3F74_ _LABEL_3F7E_ _LABEL_3F8F_ _LABEL_3EFE_

; 1st entry of Jump Table from 3F6C (indexed by unknown)
_LABEL_3F74_:
    inc     (ix+28)
    set     3, (ix+1)
    jp      _LABEL_42C1_

; 2nd entry of Jump Table from 3F6C (indexed by unknown)
_LABEL_3F7E_:
    call    _LABEL_2D83_
    ld      b, $20
    call    +++
    ret     nc
    inc     (ix+28)
    ld      (ix+29), $20
    ret

; 3rd entry of Jump Table from 3F6C (indexed by unknown)
_LABEL_3F8F_:
    res     3, (ix+1)
    dec     (ix+29)
    jr      z, ++
    ld      a, (ix+29)
    rrca
    jr      c, +
    set     3, (ix+1)
+:
    jp      _LABEL_3EFE_

++:
    inc     (ix+28)
    ret

+++:
    ld      a, (_RAM_C20D_)
    sub     (ix+13)
    jr      c, +
    sub     b
+:
    ccf
    ret

; 68th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_3FB4_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    call    _LABEL_2D83_
    ld      a, (ix+28)
    ld      hl, $3FC9
    jp      call_jump_table

; Jump Table from 3FC9 to 3FCE (3 entries, indexed by unknown)
_DATA_3FC9_:
.dw _LABEL_3FCF_ _LABEL_3FE3_ _LABEL_4001_

; 1st entry of Jump Table from 3FC9 (indexed by unknown)
_LABEL_3FCF_:
    inc     (ix+28)
    call    _LABEL_5FF_
    rlca
    ld      hl, $FC80
    jr      nc, +
    ld      h, $FE
+:
    call    _LABEL_2DAE_
    jp      _LABEL_42A9_

; 2nd entry of Jump Table from 3FC9 (indexed by unknown)
_LABEL_3FE3_:
    call    _LABEL_4060_
    ld      a, (ix+15)
    or      a
    jr      z, +
    call    _LABEL_426C_
    jr      c, +
    call    _LABEL_429B_
    cp      $02
    ret     nz
+:
    inc     (ix+28)
    call    _LABEL_2522F_
    jp      z, _LABEL_404E_
    ret

; 3rd entry of Jump Table from 3FC9 (indexed by unknown)
_LABEL_4001_:
    dec     (ix+29)
    jp      z, _LABEL_42A9_
    jr      _LABEL_4060_

; 64th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4009_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    call    _LABEL_2D83_
    ld      a, (ix+28)
    ld      hl, $401E
    jp      call_jump_table

; Jump Table from 401E to 4023 (3 entries, indexed by unknown)
_DATA_401E_:
.dw _LABEL_4024_ _LABEL_4033_ _LABEL_405A_

; 1st entry of Jump Table from 401E (indexed by unknown)
_LABEL_4024_:
    inc     (ix+28)
    ld      hl, $FB80
    call    _LABEL_2DAE_
    call    _LABEL_4252_
    jp      _LABEL_42A9_

; 2nd entry of Jump Table from 401E (indexed by unknown)
_LABEL_4033_:
    call    _LABEL_4060_
    call    _LABEL_425C_
    jr      c, +
    call    _LABEL_428D_
    cp      $02
    ret     nz
    jp      _LABEL_2D20_

+:
    call    _LABEL_2DB5_
    inc     (ix+28)
    call    _LABEL_2522F_
    ret     nz
_LABEL_404E_:
    ld      b, $05
    call    _LABEL_25223_
    ld      (ix+29), $07
    jp      _LABEL_42B3_

; 3rd entry of Jump Table from 401E (indexed by unknown)
_LABEL_405A_:
    dec     (ix+29)
    jp      z, _LABEL_42A9_
_LABEL_4060_:
    ld      bc, $0000
    call    _LABEL_6B8B_
    cp      $02
    jr      z, +
    cp      $01
    jr      z, +
    ld      de, $0030
    call    _LABEL_2DBF_
    bit     7, h
    ret     nz
    ld      de, $03F0
    or      a
    sbc     hl, de
    ret     c
    ex      de, hl
    jp      _LABEL_2DAE_

+:
    ld      (ix+28), $00
    jp      _LABEL_2D5F_

; 65th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4089_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    call    _LABEL_2D83_
    ld      a, (ix+28)
    ld      hl, $409E
    jp      call_jump_table

; Jump Table from 409E to 40A5 (4 entries, indexed by unknown)
_DATA_409E_:
.dw _LABEL_40A6_ _LABEL_40B2_ _LABEL_40EF_ _LABEL_4112_

; 1st entry of Jump Table from 3E9F (indexed by unknown)
_LABEL_40A6_:
    inc     (ix+28)
    ld      hl, $FF00
    call    _LABEL_4252_
    jp      _LABEL_42C1_

; 2nd entry of Jump Table from 409E (indexed by unknown)
_LABEL_40B2_:
    ld      de, $0020
    call    _LABEL_2D49_
    ld      bc, $0000
    call    _LABEL_6B8B_
    cp      $02
    call    z, _LABEL_2D62_
    ld      hl, $FF00
    call    _LABEL_4252_
    call    _LABEL_428D_
    cp      $02
    call    z, _LABEL_2DB5_
    call    _LABEL_427C_
    call    nz, _LABEL_2DB5_
    ld      b, $30
    call    _LABEL_2D92_
    ret     nc
    inc     (ix+28)
    ld      hl, $FC00
    call    _LABEL_2DAE_
    ld      hl, $0100
    call    _LABEL_4252_
    jp      _LABEL_42A9_

; 3rd entry of Jump Table from 409E (indexed by unknown)
_LABEL_40EF_:
    call    _LABEL_2522F_
    jr      nz, _LABEL_40FA_
    ld      a, (ix+15)
    or      a
    jr      z, +
_LABEL_40FA_:
    call    _LABEL_4060_
    call    _LABEL_428D_
    cp      $02
    call    z, _LABEL_2DB5_
    call    _LABEL_425C_
    ret     nc
    jp      _LABEL_2DB5_

+:
    inc     (ix+28)
    jp      _LABEL_404E_

; 4th entry of Jump Table from 409E (indexed by unknown)
_LABEL_4112_:
    dec     (ix+29)
    jr      nz, _LABEL_40FA_
    dec     (ix+28)
    jp      _LABEL_42A9_

; 66th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_411D_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $412F
    jp      call_jump_table

; Jump Table from 412F to 4136 (4 entries, indexed by unknown)
_DATA_412F_:
.dw _LABEL_4137_ _LABEL_40B2_ _LABEL_413D_ _LABEL_415D_

; 1st entry of Jump Table from 412F (indexed by unknown)
_LABEL_4137_:
    call    _LABEL_2D83_
    jp      _LABEL_40A6_

; 3rd entry of Jump Table from 412F (indexed by unknown)
_LABEL_413D_:
    inc     (ix+28)
    call    _LABEL_5FF_
    and     $3F
    ld      (ix+29), a
    and     $07
    ld      hl, _DATA_418F_
    call    _LABEL_7A8_
    ld      (ix+14), e
    ld      (ix+15), d
    ld      (ix+16), l
    ld      (ix+17), h
    ret

; 4th entry of Jump Table from 412F (indexed by unknown)
_LABEL_415D_:
    ld      a, (ix+9)
    xor     $01
    ld      (ix+9), a
    call    _LABEL_425C_
    call    c, _LABEL_2D20_
    call    _LABEL_426C_
    call    c, _LABEL_2D14_
    call    _LABEL_428D_
    or      a
    call    nz, _LABEL_2D20_
    call    _LABEL_429B_
    or      a
    call    nz, _LABEL_2D14_
    dec     (ix+29)
    ret     nz
    dec     (ix+28)
    call    _LABEL_2522F_
    ret     nz
    ld      b, $05
    jp      _LABEL_25223_

; Data from 418F to 41AE (32 bytes)
_DATA_418F_:
.db $C0 $FD $80 $00 $40 $02 $80 $FF $80 $00 $C0 $FD $C0 $FE $80 $FE
.db $40 $01 $80 $FE $80 $FF $40 $02 $C0 $FE $80 $01 $80 $01 $40 $01

; 67th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_41AF_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    call    _LABEL_2D83_
    ld      a, (ix+28)
    ld      hl, $41C4
    jp      call_jump_table

; Data from 41C4 to 4239 (118 bytes)
.db $A6 $40 $B2 $40 $D0 $41 $DF $41 $04 $42 $16 $42 $DD $34 $1C $21
.db $00 $04 $CD $52 $42 $21 $00 $FE $C3 $AE $2D $CD $5C $42 $38 $0C
.db $CD $6C $42 $DC $14 $2D $CD $8D $42 $FE $02 $C0 $DD $34 $1C $DD
.db $36 $1D $10 $3E $03 $06 $01 $CD $06 $2D $CD $AB $2D $C3 $B5 $2D
.db $DD $35 $1D $C0 $DD $34 $1C $AF $47 $CD $06 $2D $21 $00 $FD $C3
.db $52 $42 $11 $E0 $FF $DD $CB $11 $7E $20 $03 $11 $20 $00 $CD $C8
.db $2D $CD $5C $42 $38 $06 $CD $8D $42 $FE $02 $C0 $DD $36 $1C $00
.db $CD $AB $2D $C3 $B5 $2D

_LABEL_423A_:
    ld      de, $0020
_LABEL_423D_:
    call    _LABEL_2D49_
    ld      bc, $0000
    call    _LABEL_6B8B_
    cp      $01
    jr      z, +
    cp      $02
    ret     nz
+:
    call    _LABEL_2D62_
    xor     a
    ret

_LABEL_4252_:
    ld      a, (ix+9)
    or      a
    call    nz, negate_hl
    jp      _LABEL_2DB8_

_LABEL_425C_:
    ld      a, (ix+13)
    bit     7, (ix+17)
    jr      nz, +
    cp      $E0
    ccf
    ret

+:
    cp      $20
    ret

_LABEL_426C_:
    ld      a, (ix+11)
    bit     7, (ix+15)
    jr      nz, +
    cp      $B0
    ccf
    ret

+:
    cp      $20
    ret

_LABEL_427C_:
    ld      bc, $0808
    bit     7, (ix+17)
    jr      z, +
    ld      b, $F8
+:
    call    _LABEL_6B8B_
    cp      $02
    ret

_LABEL_428D_:
    ld      bc, $10F0
    bit     7, (ix+17)
    jr      z, +
    ld      b, $F0
+:
    jp      _LABEL_6B8B_

_LABEL_429B_:
    ld      bc, $0000
    bit     7, (ix+15)
    jr      z, +
    ld      c, $E0
+:
    jp      _LABEL_6B8B_

_LABEL_42A9_:
    ld      a, (StageNumber)
    ld      hl, $42CF
    rst     $18    ; get_array_index
    jp      _LABEL_2CFE_

_LABEL_42B3_:
    ld      a, (StageNumber)
    ld      hl, $42D9
    rst     $18    ; get_array_index
    ld      a, $01
    ld      b, $04
    jp      _LABEL_2D00_

_LABEL_42C1_:
    ld      a, (StageNumber)
    ld      hl, $42E3
    rst     $18    ; get_array_index
    ld      a, $03
    ld      b, $03
    jp      _LABEL_2D00_

; Data from 42CF to 42EC (30 bytes)
.db $DC $A9 $86 $AA $30 $AB $DC $A9 $DC $A9 $96 $A9 $40 $AA $EA $AA
.db $96 $A9 $96 $A9 $DC $A9 $86 $AA $30 $AB $DC $A9 $DC $A9

; 83rd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_42ED_:
    ld      a, (_RAM_C313_)
    or      a
    jr      z, ++
    call    _LABEL_2DAB_
    ld      hl, $0140
    bit     7, a
    jr      nz, +
    ld      hl, $FEC0
+:
    jp      _LABEL_2DB8_

++:
    call    _LABEL_2D83_
    ld      a, (ix+28)
    ld      hl, $430F
    jp      call_jump_table

; Jump Table from 430F to 4332 (18 entries, indexed by unknown)
_DATA_430F_:
.dw _LABEL_4333_ _LABEL_4348_ _LABEL_4368_ _LABEL_43D4_ _LABEL_43DD_ _LABEL_440F_ _LABEL_442A_ _LABEL_4468_
.dw _LABEL_4486_ _LABEL_44B5_ _LABEL_44EE_ _LABEL_43DD_ _LABEL_440F_ _LABEL_44F9_ _LABEL_451E_ _LABEL_4564_
.dw _LABEL_456D_ _LABEL_458E_

; 1st entry of Jump Table from 430F (indexed by unknown)
_LABEL_4333_:
    inc     (ix+28)
    xor     a
    ld      (_RAM_C0C7_), a
    ld      (ix+30), $04
    set     2, (ix+1)
    ld      hl, $AE59
    jp      _LABEL_2CFE_

; 2nd entry of Jump Table from 430F (indexed by unknown)
_LABEL_4348_:
    call    _LABEL_459D_
    call    _LABEL_425C_
    call    c, _LABEL_2D20_
    ld      de, $0060
    call    _LABEL_423D_
    ret     nz
    inc     (ix+28)
    call    _LABEL_2DB5_
    ld      (ix+29), $30
    ld      hl, $AE85
    jp      _LABEL_2CFE_

; 3rd entry of Jump Table from 430F (indexed by unknown)
_LABEL_4368_:
    call    _LABEL_459D_
    ld      hl, _RAM_C31D_
    dec     (hl)
    jr      z, _LABEL_4380_
    ld      a, (hl)
    cp      $0C
    ret     nc
    call    _LABEL_6954_
    jr      c, +
    ld      a, $01
    ld      (_RAM_C0C7_), a
    ret

_LABEL_4380_:
    dec     (ix+28)
    xor     a
    ld      (_RAM_C0C7_), a
    ld      hl, $AE59
    call    _LABEL_2CFE_
    ld      hl, $FE40
    call    _LABEL_4252_
    ld      hl, $FB00
    jp      _LABEL_2DAE_

+:
    ld      hl, _RAM_C3C0_
    call    _LABEL_892_
    dec     (ix+30)
    jr      nz, +
    ld      (ix+28), $04
    set     3, (ix+1)
    ld      (ix+29), $20
    ret

+:
    inc     (ix+28)
_LABEL_43B4_:
    ld      (ix+29), $10
    ld      a, $A8
    ld      (Sound_MusicTrigger), a
    call    _LABEL_2DAB_
    ld      hl, $0100
    ld      a, (_RAM_C309_)
    or      a
    jr      z, +
    ld      h, $FF
+:
    call    _LABEL_2DB8_
    ld      hl, $AE59
    jp      _LABEL_2CFE_

; 4th entry of Jump Table from 430F (indexed by unknown)
_LABEL_43D4_:
    dec     (ix+29)
    ret     nz
    dec     (ix+28)
    jr      _LABEL_4380_

; 5th entry of Jump Table from 430F (indexed by unknown)
_LABEL_43DD_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      b, $20
    ld      (ix+11), b
    ld      a, (_RAM_C050_)
    rlca
    jr      c, +
    ld      b, $E0
+:
    ld      (ix+13), b
    ld      (ix+30), $04
    ld      (ix+23), $01
    call    _LABEL_2DAB_
    call    _LABEL_2DB5_
    xor     a
    ld      (ix+18), a
    ld      (ix+19), a
    ld      hl, $AE59
    jp      _LABEL_2CFE_

; 6th entry of Jump Table from 430F (indexed by unknown)
_LABEL_440F_:
    ld      hl, _RAM_C301_
    ld      a, (hl)
    xor     $08
    ld      (hl), a
    ld      de, $0060
    call    _LABEL_423D_
    ret     nz
    inc     (ix+28)
    res     3, (ix+1)
    xor     a
    ld      (_RAM_C0C1_), a
    jr      _LABEL_4440_

; 7th entry of Jump Table from 430F (indexed by unknown)
_LABEL_442A_:
    ld      b, $60
    call    _LABEL_2D92_
    jr      nc, +
    call    _LABEL_425C_
    jr      c, +
    call    _LABEL_2D83_
    ld      de, $0060
    call    _LABEL_423D_
    ret     nz
_LABEL_4440_:
    ld      hl, $FE80
    call    _LABEL_2DAE_
    ld      hl, $0180
    jp      _LABEL_4252_

+:
    inc     (ix+28)
    ld      a, $01
    ld      (_RAM_C0C0_), a
    call    _LABEL_2DAB_
    call    _LABEL_2DB5_
    ld      (ix+29), $0B
    ld      a, $03
    ld      b, $04
    ld      hl, $AF05
    jp      _LABEL_2D00_

; 8th entry of Jump Table from 430F (indexed by unknown)
_LABEL_4468_:
    call    _LABEL_694E_
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      hl, $FE00
    call    _LABEL_4252_
    ld      (ix+29), $30
    ld      a, $01
    ld      b, $02
    ld      hl, $AF65
    jp      _LABEL_2D00_

; 9th entry of Jump Table from 430F (indexed by unknown)
_LABEL_4486_:
    call    _LABEL_45A6_
    call    _LABEL_694E_
    call    _LABEL_693E_
    jr      c, +
    ld      de, $0060
    call    _LABEL_423D_
    call    _LABEL_425C_
    call    c, _LABEL_2D20_
    call    _LABEL_2D83_
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $0B
    ld      a, $03
    ld      b, $04
    ld      hl, $AEA5
    jp      _LABEL_2D00_

; 10th entry of Jump Table from 430F (indexed by unknown)
_LABEL_44B5_:
    call    _LABEL_694E_
    dec     (ix+29)
    ret     nz
    xor     a
    ld      (_RAM_C0C0_), a
    ld      (ix+28), $06
    ld      hl, $AE59
    call    _LABEL_2CFE_
    jp      _LABEL_4440_

+:
    xor     a
    ld      (_RAM_C0C0_), a
    dec     (ix+30)
    jr      nz, +
    ld      (ix+28), $0B
    set     3, (ix+1)
    ld      (ix+29), $20
    ld      (ix+23), $00
    ret

+:
    ld      (ix+28), $0A
    jp      _LABEL_43B4_

; 11th entry of Jump Table from 430F (indexed by unknown)
_LABEL_44EE_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $06
    jp      _LABEL_4440_

; 14th entry of Jump Table from 430F (indexed by unknown)
_LABEL_44F9_:
    call    _LABEL_694E_
    call    _LABEL_693E_
    jr      c, _LABEL_4542_
    call    _LABEL_425C_
    call    c, _LABEL_2D20_
    ld      de, $0060
    call    _LABEL_423D_
    ret     nz
    inc     (ix+28)
    call    _LABEL_2DB5_
    ld      (ix+29), $10
    ld      hl, $AE85
    jp      _LABEL_2CFE_

; 15th entry of Jump Table from 430F (indexed by unknown)
_LABEL_451E_:
    call    _LABEL_694E_
    call    _LABEL_693E_
    jr      c, _LABEL_4542_
    dec     (ix+29)
    ret     nz
    dec     (ix+28)
    ld      hl, $AE59
    call    _LABEL_2CFE_
    call    _LABEL_45BD_
    ld      hl, $FEC0
    call    _LABEL_4252_
    ld      hl, $FA00
    jp      _LABEL_2DAE_

_LABEL_4542_:
    dec     (ix+30)
    jr      nz, +
    ld      (ix+28), $10
    ld      hl, $FD00
    call    _LABEL_2DAE_
    ld      hl, $0140
    call    _LABEL_4252_
    ld      hl, $AFE1
    jp      _LABEL_2CFE_

+:
    ld      (ix+28), $0F
    jp      _LABEL_43B4_

; 16th entry of Jump Table from 430F (indexed by unknown)
_LABEL_4564_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $0D
    ret

; 17th entry of Jump Table from 430F (indexed by unknown)
_LABEL_456D_:
    call    _LABEL_425C_
    call    c, _LABEL_2DB5_
    ld      de, $0060
    call    _LABEL_423D_
    ret     nz
    inc     (ix+28)
    call    _LABEL_2DB5_
    ld      (ix+29), $11
    ld      a, $02
    ld      b, $06
    ld      hl, $B00D
    jp      _LABEL_2D00_

; 18th entry of Jump Table from 430F (indexed by unknown)
_LABEL_458E_:
    dec     (ix+29)
    ret     nz
    xor     a
    ld      (_RAM_DD94_), a
    inc     a
    ld      (_RAM_DD00_), a
    jp      _LABEL_327F_

_LABEL_459D_:
    ld      hl, _RAM_C3C0_
    ld      a, (hl)
    or      a
    ret     nz
    ld      (hl), $54
    ret

_LABEL_45A6_:
    ld      a, (_RAM_C313_)
    or      a
    ret     nz
    ld      hl, _RAM_C3C0_
    ld      de, $0020
    ld      b, $03
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), $55
+:
    add     hl, de
    djnz    -
    ret

_LABEL_45BD_:
    ld      a, (_RAM_C313_)
    or      a
    ret     nz
    ld      hl, _RAM_C320_
    ld      de, $0020
    ld      b, $03
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), $56
    ret

+:
    add     hl, de
    djnz    -
    ret

; 84th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_45D5_:
    call    _LABEL_694E_
    call    _LABEL_69E5_
    ld      a, (_RAM_C3D3_)
    or      (ix+19)
    jp      nz, _LABEL_88F_
    ld      a, (ix+28)
    ld      hl, $45F7
    rst     $08    ; call_jump_table
    ld      hl, _RAM_C30A_
    ld      de, _RAM_C3CA_
    ld      bc, $0004
    ldir
    ret

; Jump Table from 45F7 to 45FC (3 entries, indexed by unknown)
_DATA_45F7_:
.dw _LABEL_45FD_ _LABEL_4614_ _LABEL_4621_

; 1st entry of Jump Table from 45F7 (indexed by unknown)
_LABEL_45FD_:
    inc     (ix+28)
    set     2, (ix+1)
    res     3, (ix+1)
    ld      a, $01
    ld      (ix+23), a
    ld      b, a
    ld      hl, $AE19
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 45F7 (indexed by unknown)
_LABEL_4614_:
    ld      a, (_RAM_C0C7_)
    or      a
    ret     z
    inc     (ix+28)
    set     3, (ix+1)
    ret

; 3rd entry of Jump Table from 45F7 (indexed by unknown)
_LABEL_4621_:
    ld      a, (_RAM_C0C7_)
    or      a
    ret     nz
    ld      (ix+28), $00
    ret

; 85th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_462B_:
    ld      a, (_RAM_C31C_)
    cp      $0A
    call    nz, _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $463C
    jp      call_jump_table

; Jump Table from 463C to 4641 (3 entries, indexed by unknown)
_DATA_463C_:
.dw _LABEL_4642_ _LABEL_4675_ _LABEL_46B5_

; 1st entry of Jump Table from 463C (indexed by unknown)
_LABEL_4642_:
    inc     (ix+28)
    set     2, (ix+1)
    ld      bc, _RAM_C0C1_
    ld      a, (bc)
    ld      hl, $46C8
    rst     $18    ; get_array_index
    ld      (ix+26), l
    ld      (ix+27), h
    xor     a
    rlc     h
    jr      c, +
    inc     a
+:
    ld      (ix+9), a
    ld      a, (bc)
    inc     a
    cp      $06
    jr      c, +
    xor     a
+:
    ld      (bc), a
    call    _LABEL_46BD_
    ld      a, $01
    ld      b, $02
    ld      hl, $AE3D
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 463C (indexed by unknown)
_LABEL_4675_:
    ld      a, (_RAM_C0C0_)
    or      a
    jp      z, _LABEL_88F_
    ld      hl, _RAM_C30B_
    ld      a, (hl)
    add     a, (ix+26)
    ld      (ix+11), a
    inc     hl
    inc     hl
    ld      a, (hl)
    ld      b, (ix+27)
    bit     7, b
    jr      z, +
    add     a, b
    cp      $F0
    jp      nc, _LABEL_88F_
    jr      ++

+:
    add     a, b
    jp      c, _LABEL_88F_
++:
    ld      (ix+13), a
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    call    _LABEL_2D83_
    ld      hl, $FD80
    or      a
    jr      z, +
    ld      hl, $0280
+:
    jp      _LABEL_2DB8_

; 3rd entry of Jump Table from 463C (indexed by unknown)
_LABEL_46B5_:
    ld      a, (ix+19)
    or      a
    jp      nz, _LABEL_88F_
    ret

_LABEL_46BD_:
    call    _LABEL_5FF_
    and     $07
    add     a, $10
    ld      (ix+29), a
    ret

; Data from 46C8 to 46D3 (12 bytes)
.db $D8 $F4 $DF $10 $E4 $EC $EC $10 $F4 $FC $F8 $0A

; 86th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_46D4_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    ld      a, (_RAM_C31C_)
    cp      $10
    jp      nc, _LABEL_88F_
    ld      a, (ix+19)
    or      a
    jp      nz, _LABEL_88F_
    ld      a, (ix+28)
    ld      hl, $46F5
    jp      call_jump_table

; Jump Table from 46F5 to 46FA (3 entries, indexed by unknown)
_DATA_46F5_:
.dw _LABEL_46FB_ _LABEL_4716_ _LABEL_4738_

; 1st entry of Jump Table from 46F5 (indexed by unknown)
_LABEL_46FB_:
    inc     (ix+28)
    set     2, (ix+1)
    ld      a, (_RAM_C30B_)
    ld      (ix+11), a
    ld      a, (_RAM_C30D_)
    ld      (ix+13), a
    ld      a, (_RAM_C309_)
    ld      (ix+9), a
    jr      +

; 2nd entry of Jump Table from 46F5 (indexed by unknown)
_LABEL_4716_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      hl, $AF95
    call    _LABEL_2CFE_
    ld      hl, $FEC0
    call    _LABEL_4252_
    ld      hl, $FA00
    call    _LABEL_5FF_
    and     $03
    ld      e, $00
    ld      d, a
    add     hl, de
    jp      _LABEL_2DAE_

; 3rd entry of Jump Table from 46F5 (indexed by unknown)
_LABEL_4738_:
    call    _LABEL_425C_
    call    c, _LABEL_2D20_
    ld      de, $0060
    call    _LABEL_423D_
    ret     nz
    dec     (ix+28)
+:
    call    _LABEL_2DB5_
    ld      (ix+29), $0C
    ld      hl, $AFC1
    jp      _LABEL_2CFE_

; 92nd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4755_:
    call    _LABEL_6954_
    jr      nc, +
    ld      hl, _RAM_C3C0_
    ld      bc, $0080
    call    _LABEL_8AD_
    ld      hl, _RAM_C3C0_
    ld      de, $0020
    ld      b, $04
-:
    ld      (hl), $5D
    add     hl, de
    djnz    -
    xor     a
    ld      (_RAM_C0B1_), a
    ld      a, (ix+11)
    ld      (_RAM_C0B2_), a
    ld      a, (ix+13)
    ld      (_RAM_C0B3_), a
    jp      _LABEL_88F_

+:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $478F
    jp      call_jump_table

; Jump Table from 478F to 4794 (3 entries, indexed by unknown)
_DATA_478F_:
.dw _LABEL_4795_ _LABEL_47A8_ _LABEL_47D7_

; 1st entry of Jump Table from 478F (indexed by unknown)
_LABEL_4795_:
    inc     (ix+28)
    ld      (ix+29), $20
    ld      a, $03
    ld      b, $08
    ld      hl, $B19D
    call    _LABEL_2D00_
    jr      _LABEL_47B1_

; 2nd entry of Jump Table from 478F (indexed by unknown)
_LABEL_47A8_:
    dec     (ix+29)
    jr      z, +
    dec     (ix+30)
    ret     nz
_LABEL_47B1_:
    ld      (ix+30), $02
    inc     (ix+31)
    ld      a, (ix+31)
    rrca
    jp      c, _LABEL_2D20_
    ld      hl, $0080
    jp      _LABEL_2DB8_

+:
    inc     (ix+28)
    call    _LABEL_2DB5_
    ld      hl, $FD80
    call    _LABEL_2DAE_
    ld      hl, $B1E5
    jp      _LABEL_2CFE_

; 3rd entry of Jump Table from 478F (indexed by unknown)
_LABEL_47D7_:
    call    _LABEL_47E0_
    ret     nz
    ld      (ix+28), $00
    ret

_LABEL_47E0_:
    ld      de, $0020
    call    _LABEL_2D49_
    ld      bc, $0000
    call    _LABEL_6B8B_
    cp      $02
    ret     nz
    call    _LABEL_2D5F_
    xor     a
    ret

; 93rd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_47F4_:
    call    _LABEL_69E5_
    ld      a, (ix+11)
    cp      $A8
    jp      nc, _LABEL_88F_
    ld      a, (ix+28)
    ld      hl, $4808
    jp      call_jump_table

; Jump Table from 4808 to 480D (3 entries, indexed by unknown)
_DATA_4808_:
.dw _LABEL_480E_ _LABEL_4843_ _LABEL_4850_

; 1st entry of Jump Table from 4808 (indexed by unknown)
_LABEL_480E_:
    inc     (ix+28)
    ld      a, (_RAM_C0B2_)
    ld      (ix+11), a
    ld      a, (_RAM_C0B3_)
    ld      (ix+13), a
    push    ix
    pop     hl
    ld      de, $000E
    add     hl, de
    ex      de, hl
    ld      hl, _RAM_C0B1_
    ld      a, (hl)
    inc     (hl)
    ld      hl, $485D
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      bc, $0004
    ldir
    ld      a, $01
    ld      b, $06
    ld      hl, $B1F7
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 4808 (indexed by unknown)
_LABEL_4843_:
    call    _LABEL_47E0_
    ret     nz
    inc     (ix+28)
    call    _LABEL_2DB5_
    jp      _LABEL_47B1_

; 3rd entry of Jump Table from 4808 (indexed by unknown)
_LABEL_4850_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    dec     (ix+30)
    ret     nz
    jp      _LABEL_47B1_

; Data from 485D to 486C (16 bytes)
.db $00 $FE $80 $FF $80 $FD $C0 $FF $00 $FE $40 $00 $80 $FD $80 $00

; 94th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_486D_:
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $4879
    jp      call_jump_table

; Jump Table from 4879 to 487E (3 entries, indexed by unknown)
_DATA_4879_:
.dw _LABEL_487F_ _LABEL_48A5_ _LABEL_48C7_

; 1st entry of Jump Table from 4879 (indexed by unknown)
_LABEL_487F_:
    inc     (ix+28)
    res     3, (ix+1)
    call    _LABEL_5FF_
    ld      hl, $FA00
    rlca
    jr      c, +
    ld      h, $FB
+:
    and     $1F
    add     a, $10
    ld      (ix+29), a
    call    _LABEL_2DAE_
    ld      a, $01
    ld      b, $03
    ld      hl, $B20F
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 4879 (indexed by unknown)
_LABEL_48A5_:
    ld      de, $0050
    call    _LABEL_2D49_
    bit     7, h
    ret     nz
    ld      hl, $B227
    ld      (ix+2), l
    ld      (ix+3), h
    call    _LABEL_429B_
    cp      $07
    ret     nz
    inc     (ix+28)
    set     3, (ix+1)
    jp      _LABEL_2DAB_

; 3rd entry of Jump Table from 4879 (indexed by unknown)
_LABEL_48C7_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $00
    ret

; 95th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_48D0_:
    ld      a, (ix+28)
    ld      hl, $48D9
    jp      call_jump_table

; Jump Table from 48D9 to 48E2 (5 entries, indexed by unknown)
_DATA_48D9_:
.dw _LABEL_48E3_ _LABEL_48ED_ _LABEL_4904_ _LABEL_4911_ _LABEL_492F_

; 1st entry of Jump Table from 48D9 (indexed by unknown)
_LABEL_48E3_:
    inc     (ix+28)
    ld      a, (ix+11)
    ld      (ix+27), a
    ret

; 2nd entry of Jump Table from 48D9 (indexed by unknown)
_LABEL_48ED_:
    inc     (ix+28)
    ld      a, (ix+27)
    ld      (ix+11), a
    ld      (ix+29), $17
    ld      a, $02
    ld      b, $08
    ld      hl, $B23F
    jp      _LABEL_2D00_

; 3rd entry of Jump Table from 48D9 (indexed by unknown)
_LABEL_4904_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      hl, $B25D
    jp      _LABEL_2CFE_

; 4th entry of Jump Table from 48D9 (indexed by unknown)
_LABEL_4911_:
    call    _LABEL_69E5_
    ld      de, $0020
    call    _LABEL_2D49_
    call    _LABEL_429B_
    or      a
    ret     z
    inc     (ix+28)
    call    _LABEL_2DAB_
    ld      (ix+29), $08
    ld      hl, $B266
    jp      _LABEL_2CFE_

; 5th entry of Jump Table from 48D9 (indexed by unknown)
_LABEL_492F_:
    call    _LABEL_69E5_
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $01
    ret

; 47th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_493B_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    call    _LABEL_2D83_
    ld      b, $0C
    ld      a, (ix+28)
    ld      hl, $4952
    jp      call_jump_table

; Jump Table from 4952 to 4959 (4 entries, indexed by unknown)
_DATA_4952_:
.dw _LABEL_495A_ _LABEL_251F5_ _LABEL_25201_ _LABEL_2533F_

; 1st entry of Jump Table from 4952 (indexed by unknown)
_LABEL_495A_:
    inc     (ix+28)
    ld      a, (StageNumber)
    ld      hl, $B2D0
    rst     $18    ; get_array_index
    jp      _LABEL_2CFE_

; 50th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4967_:
    ld      a, $0C
    ld      (Frame2_Select), a
    ld      a, (_RAM_C31C_)
    ld      hl, $4975
    jp      call_jump_table

; Jump Table from 4975 to 497E (5 entries, indexed by unknown)
_DATA_4975_:
.dw _LABEL_497F_ _LABEL_4990_ _LABEL_49A3_ _LABEL_49E0_ _LABEL_49EA_

; 1st entry of Jump Table from 4975 (indexed by unknown)
_LABEL_497F_:
    inc     (ix+28)
    ld      (ix+30), $0D
    ld      hl, $B356
    ld      a, $0B
    ld      b, $04
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 4975 (indexed by unknown)
_LABEL_4990_:
    ld      a, (_RAM_C305_)
    cp      $0B
    jr      z, +
    ld      hl, $9C90
    rst     $18    ; get_array_index
    ld      (_RAM_DDF0_), hl
    ret

+:
    inc     (ix+28)
    ret

; 3rd entry of Jump Table from 4975 (indexed by unknown)
_LABEL_49A3_:
    call    _LABEL_5FF_
    cp      $10
    jp      c, _LABEL_4A38_
    ret

_LABEL_49AC_:
    ld      a, (_RAM_C31E_)
    dec     a
    jr      nz, +
    ld      a, $03
    ld      (_RAM_C31C_), a
    xor     a
    ld      (_RAM_C320_), a
    ld      (_RAM_C340_), a
    ld      (_RAM_C360_), a
    ld      (_RAM_C380_), a
    ld      (_RAM_C3A0_), a
    ld      (_RAM_C3C0_), a
    ld      (_RAM_C3E0_), a
    ld      (_RAM_C400_), a
+:
    ld      hl, Frame2_Select
    ld      (hl), $0C
    ld      (_RAM_C31E_), a
    ld      hl, $9CA6
    rst     $18    ; get_array_index
    ld      (_RAM_DDF4_), hl
    ret

; 4th entry of Jump Table from 4975 (indexed by unknown)
_LABEL_49E0_:
    inc     (ix+28)
    ld      hl, $000B
    ld      (_RAM_C304_), hl
    ret

; 5th entry of Jump Table from 4975 (indexed by unknown)
_LABEL_49EA_:
    ld      a, (_RAM_C305_)
    cp      $0B
    jr      z, +
    ld      hl, $9CC6
    rst     $18    ; get_array_index
    ld      (_RAM_DDF0_), hl
    ret

+:
    xor     a
    ld      (_RAM_DD37_), a
    ld      (_RAM_DD0B_), a
    jp      _LABEL_1460_

; Data from 4A03 to 4A37 (53 bytes)
.db $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A
.db $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A
.db $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A $33 $4A
.db $01 $02 $F4 $FC $48

_LABEL_4A38_:
    ld      iy, _RAM_C320_
    ld      b, $08
    ld      de, $0020
-:
    ld      a, (iy+0)
    or      a
    jr      nz, +
    ld      (iy+0), $33
    ld      (iy+13), $A4
    ld      (iy+11), $50
    ret

+:
    add     iy, de
    djnz    -
    ret

; 51st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4A59_:
    call    _LABEL_6954_
    jr      c, +
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $4A6A
    jp      call_jump_table

; Jump Table from 4A6A to 4A6F (3 entries, indexed by unknown)
_DATA_4A6A_:
.dw _LABEL_4A76_ _LABEL_4A93_ _LABEL_4AC1_

+:
    call    _LABEL_49AC_
    jp      _LABEL_2D34_

; 1st entry of Jump Table from 4A6A (indexed by unknown)
_LABEL_4A76_:
    inc     (ix+28)
    call    _LABEL_5FF_
    and     $03
    or      $FC
    ld      (ix+17), a
    call    _LABEL_5FF_
    ld      (ix+16), a
    ld      hl, $B38B
    ld      a, $01
    ld      b, $02
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 4A6A (indexed by unknown)
_LABEL_4A93_:
    ld      a, (ix+17)
    or      a
    jr      z, +
    ld      l, (ix+16)
    ld      h, (ix+17)
    ld      de, $0020
    add     hl, de
    jp      _LABEL_2DB8_

+:
    inc     (ix+28)
    call    _LABEL_5FF_
    and     $0F
    ld      hl, $4ADC
    rst     $18    ; get_array_index
    call    _LABEL_2DB8_
    call    _LABEL_5FF_
    and     $0F
    ld      hl, $4ADC
    rst     $18    ; get_array_index
    jp      _LABEL_2DAE_

; 3rd entry of Jump Table from 4A6A (indexed by unknown)
_LABEL_4AC1_:
    ld      a, (ix+13)
    cp      $0C
    jr      c, +
    cp      $F8
    jr      c, ++
+:
    call    _LABEL_2D20_
++:
    ld      a, (ix+11)
    cp      $18
    jr      c, +
    cp      $A8
    ret     c
+:
    jp      _LABEL_2D14_

; Data from 4ADC to 4AFB (32 bytes)
.db $60 $FD $80 $FD $C0 $FD $00 $FE $40 $FE $60 $FE $80 $FE $C0 $FE
.db $A0 $02 $80 $02 $40 $02 $00 $02 $C0 $01 $A0 $01 $80 $01 $40 $01

_LABEL_4AFC_:
    ld      a, (StageNumber)
    cp      $01
    ret     nz
    ld      a, $0C
    ld      (Frame2_Select), a
    ld      hl, (_RAM_DDF4_)
    ld      a, h
    or      a
    jr      z, +
    ld      de, $7808
    ld      bc, $0238
    call    draw_screen_map_element
+:
    ld      hl, (_RAM_DDF0_)
    ld      a, h
    or      a
    jr      z, +
    ld      de, $78EA
    ld      bc, $0816
    call    draw_screen_map_element
+:
    ld      hl, $0000
    ld      (_RAM_DDF0_), hl
    ld      (_RAM_DDF4_), hl
    ret

; 56th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4B31_:
    call    _LABEL_6954_
    jp      c, _LABEL_2D34_
    call    _LABEL_69E5_
    ld      a, (ix+28)
    ld      hl, $4B43
    jp      call_jump_table

; Jump Table from 4B43 to 4B46 (2 entries, indexed by unknown)
_DATA_4B43_:
.dw _LABEL_40A6_ _LABEL_4B47_

; 2nd entry of Jump Table from 4B43 (indexed by unknown)
_LABEL_4B47_:
    ld      bc, $0000
    call    _LABEL_6B8B_
    ld      de, $0020
    cp      $01
    jr      z, +
    cp      $02
    jr      z, +
    cp      $05
    jp      nz, _LABEL_2D46_
+:
    call    _LABEL_2D62_
    ld      a, (ix+17)
    or      a
    jr      z, _LABEL_4BA7_
    ld      a, (ix+9)
    ld      bc, $FC04
    or      a
    jr      z, +
    ld      b, $04
+:
    call    _LABEL_6B8B_
    cp      $01
    jr      z, +
    cp      $02
    jr      z, +
    cp      $05
    jp      nz, _LABEL_2D20_
+:
    ld      a, (ix+9)
    ld      bc, $FBFC
    or      a
    jr      z, +
    ld      b, $04
+:
    call    _LABEL_6B8B_
    cp      $02
    jp      z, _LABEL_2D20_
    cp      $05
    jp      z, _LABEL_2D20_
    ld      a, (ix+29)
    inc     a
    ld      (ix+29), a
    cp      $40
    ret     c
    ld      (ix+29), $00
_LABEL_4BA7_:
    call    _LABEL_2D83_
    or      a
    ld      hl, $0100
    jr      nz, +
    ld      h, $FF
+:
    jp      _LABEL_2DB8_

; 37th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4BB5_:
    ld      a, (ix+28)
    cp      $02
    jr      c, +
    call    _LABEL_69E5_
    jr      c, ++
    ld      a, (ix+28)
+:
    ld      hl, $4BCA
    jp      call_jump_table

; Data from 4BCA to 4BD1 (8 bytes)
.db $E9 $4B $F6 $4B $06 $4C $15 $4C

++:
    ld      a, $9A
    ld      (Sound_MusicTrigger), a
    xor     a
    ld      (_RAM_C224_), a
    ld      a, (Alex_Health)
    cp      $06
    jr      nc, +
    inc     a
+:
    ld      (Alex_Health), a
    jp      _LABEL_88F_

_LABEL_4BE9_:
    inc     (ix+28)
    ld      hl, $A903
    ld      a, $02
    ld      b, $03
    jp      _LABEL_2D00_

_LABEL_4BF6_:
    ld      a, (ix+5)
    cp      $02
    ret     nz
    ld      a, (ix+7)
    cp      $01
    ret     nz
    inc     (ix+28)
    ret

_LABEL_4C06_:
    ld      hl, $B3B3
    inc     (ix+28)
    set     5, (ix+1)
    xor     a
    ld      b, a
    jp      _LABEL_2D00_

_LABEL_4C15_:
    ret

; 38th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4C16_:
    ld      a, (ix+28)
    cp      $02
    jr      c, +
    call    _LABEL_69E5_
    jr      c, ++
    ld      a, (ix+28)
+:
    ld      hl, $4C2B
    jp      call_jump_table

; Data from 4C2B to 4C32 (8 bytes)
.db $E9 $4B $F6 $4B $42 $4C $15 $4C

++:
    ld      a, SFX_NewLife
    ld      (Sound_MusicTrigger), a
    xor     a
    ld      (_RAM_C224_), a
    call    alex_add_life
    jp      _LABEL_88F_

; Data from 4C42 to 4C46 (5 bytes)
.db $21 $BF $B3 $18 $C2

; 39th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4C47_:
    ld      a, (ix+28)
    cp      $02
    jr      c, +
    call    _LABEL_69E5_
    jr      c, ++
    ld      a, (ix+28)
+:
    ld      hl, $4C5C
    jp      call_jump_table

; Data from 4C5C to 4C63 (8 bytes)
.db $E9 $4B $F6 $4B $74 $4C $15 $4C

++:
    ld      a, $9B
    ld      (Sound_MusicTrigger), a
    xor     a
    ld      (_RAM_C224_), a
    inc     a
    ld      (Alex_PowerupState), a
    jp      _LABEL_88F_

; Data from 4C74 to 4C79 (6 bytes)
.db $21 $CB $B3 $C3 $09 $4C

; 54th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4C7A_:
    ld      a, (ix+28)
    cp      $02
    jr      c, +
    call    _LABEL_69E5_
    jr      c, ++
    ld      a, (ix+28)
+:
    ld      hl, $4C8F
    jp      call_jump_table

; Data from 4C8F to 4C96 (8 bytes)
.db $E9 $4B $F6 $4B $A8 $4C $15 $4C

++:
    ld      a, $91
    ld      (Sound_MusicTrigger), a
    xor     a
    ld      (_RAM_C224_), a
    ld      a, $02
    ld      (Alex_PowerupState), a
    jp      _LABEL_88F_

; Data from 4CA8 to 4CAD (6 bytes)
.db $21 $D7 $B3 $C3 $09 $4C

; 55th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4CAE_:
    ld      a, (ix+28)
    cp      $02
    jr      c, +
    call    _LABEL_69E5_
    jr      c, ++
    ld      a, (ix+28)
+:
    ld      hl, $4CC3
    jp      call_jump_table

; Data from 4CC3 to 4CCA (8 bytes)
.db $E9 $4B $F6 $4B $DB $4C $15 $4C

++:
    ld      a, $B1
    ld      (Sound_MusicTrigger), a
    xor     a
    ld      (_RAM_C224_), a
    inc     a
    ld      (_RAM_C0DF_), a
    jp      _LABEL_88F_

; Data from 4CDB to 4CE0 (6 bytes)
.db $21 $E3 $B3 $C3 $09 $4C

; 41st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_4CE1_:
    ld      a, $07
    ld      (Frame2_Select), a
    ld      a, (StageNumber)
    ld      hl, $829B
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    ld      (_RAM_DD09_), hl
    ld      a, (_RAM_C076_)
    or      a
    jp      nz, _LABEL_4DC8_
    ld      a, $01
    ld      (_RAM_DDFE_), a
    ld      a, (_RAM_C051_)
    ld      (_RAM_DD1D_), a
    ld      a, (_RAM_C051_)
    or      a
    jr      z, +
    dec     a
    ld      c, $FF
    call    _LABEL_4D83_
    ld      a, (_RAM_C051_)
+:
    ld      c, $00
    call    _LABEL_4D80_
    ld      a, (_RAM_C051_)
    inc     a
    ld      b, a
    ld      a, (_RAM_C055_)
    cp      b
    ret     c
    ld      a, b
    ld      c, $01
    jr      _LABEL_4D80_

_LABEL_4D29_:
    ld      a, $07
    ld      (Frame2_Select), a
    ld      a, (_RAM_C076_)
    or      a
    jp      nz, _LABEL_4DF1_
    ld      a, (_RAM_C051_)
    ld      b, a
    ld      a, (_RAM_DD1D_)
    xor     b
    ret     z
    ld      a, (StageNumber)
    ld      hl, $829B
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    push    hl
    ld      a, (_RAM_C053_)
    and     $80
    jr      nz, +
    dec     b
+:
    ld      a, b
    cp      $FF
    jr      z, +
    rst     $18    ; get_array_index
    ld      a, (hl)
    or      a
    jr      z, +
    ld      c, $FF
    call    ++
+:
    ld      a, (_RAM_C053_)
    ld      b, a
    ld      a, (_RAM_C051_)
    ld      (_RAM_DD1D_), a
    pop     hl
    inc     a
    bit     7, b
    jr      z, +
    inc     a
+:
    ld      b, a
    rst     $18    ; get_array_index
    ld      a, (_RAM_C055_)
    cp      b
    ret     c
    ld      a, (hl)
    or      a
    ret     z
    ld      c, $01
    jr      ++

_LABEL_4D80_:
    ld      hl, (_RAM_DD09_)
_LABEL_4D83_:
    rst     $18    ; get_array_index
    ld      a, (hl)
    or      a
    ret     z
++:
    ld      b, a
-:
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    push    de
    pop     ix
    ld      a, (ix+0)
    or      a
    jr      nz, +++
    inc     hl
    ld      a, (hl)
    ld      (ix+0), a
    ld      (ix+19), c
    inc     hl
    ld      a, (_RAM_C050_)
    ld      d, a
    ld      a, (_RAM_DDFE_)
    or      a
    ld      a, (hl)
    jr      nz, +
    sub     d
    jr      ++

+:
    sub     d
    jr      nc, ++
    dec     (ix+19)
++:
    ld      (ix+13), a
    inc     hl
    ld      a, (hl)
    ld      (ix+11), a
    jr      ++++

; Data from 4DBC to 4DC1 (6 bytes)
.db $DD $36 $00 $00 $2B $2B

+++:
    inc     hl
    inc     hl
    inc     hl
++++:
    djnz    -
    ret

_LABEL_4DC8_:
    ld      a, (_RAM_C066_)
    ld      (_RAM_DD1D_), a
    ld      a, (_RAM_C066_)
    or      a
    jr      z, +
    dec     a
    ld      c, $FF
    call    _LABEL_4E3F_
    ld      a, (_RAM_C066_)
+:
    ld      c, $00
    call    _LABEL_4E3C_
    ld      a, (_RAM_C066_)
    inc     a
    ld      b, a
    ld      a, (_RAM_C06A_)
    cp      b
    ret     c
    ld      a, b
    ld      c, $01
    jr      _LABEL_4E3C_

_LABEL_4DF1_:
    ld      a, (_RAM_C066_)
    ld      b, a
    ld      a, (_RAM_DD1D_)
    xor     b
    ret     z
    ld      a, (StageNumber)
    ld      hl, $829B
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    push    hl
    ld      a, (_RAM_C068_)
    and     $80
    jr      nz, +
    dec     b
+:
    ld      a, b
    cp      $FF
    jr      z, +
    rst     $18    ; get_array_index
    ld      a, (hl)
    or      a
    jr      z, +
    ld      c, $FF
    call    ++
+:
    ld      a, (_RAM_C068_)
    ld      b, a
    ld      a, (_RAM_C066_)
    ld      (_RAM_DD1D_), a
    pop     hl
    inc     a
    bit     7, b
    jr      z, +
    inc     a
+:
    ld      b, a
    rst     $18    ; get_array_index
    ld      a, (_RAM_C06A_)
    cp      b
    ret     c
    ld      a, (hl)
    or      a
    ret     z
    ld      c, $01
    jr      ++

_LABEL_4E3C_:
    ld      hl, (_RAM_DD09_)
_LABEL_4E3F_:
    rst     $18    ; get_array_index
    ld      a, (hl)
    or      a
    ret     z
++:
    ld      b, a
-:
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    push    de
    pop     ix
    ld      a, (ix+0)
    or      a
    jr      nz, ++
    inc     hl
    ld      a, (hl)
    ld      (ix+0), a
    inc     hl
    ld      a, (hl)
    ld      (ix+13), a
    inc     hl
    ld      a, (_RAM_C065_)
    bit     7, a
    jr      z, +
    sub     $C0
    neg
+:
    ld      d, a
    ld      a, (hl)
    sub     d
    ld      (ix+11), a
    ld      (ix+18), c
    jr      +++

; Data from 4E72 to 4E77 (6 bytes)
.db $DD $36 $00 $00 $2B $2B

++:
    inc     hl
    inc     hl
    inc     hl
+++:
    djnz    -
    ret

_LABEL_4E7E_:
    ld      ix, Alex_State
    ld      hl, $0000
    ld      (_RAM_C09A_), hl
    ld      a, (_RAM_C076_)
    or      a
    jp      nz, _LABEL_4F73_
    ld      a, (_RAM_C050_)
    neg
    ld      (VDPRegister8), a
    call    _LABEL_569B_
    ld      hl, (_RAM_C20C_)
    ld      de, (Alex_SpeedX)
    add     hl, de
    ld      a, d
    or      e
    jr      nz, _LABEL_4EF8_
    ld      a, h
    ld      de, $0100
    cp      $58
    jr      c, +
    ld      de, $FF00
    cp      $A8
    jr      nc, ++
-:
    ld      de, $0000
    jr      _LABEL_4EF8_

+:
    ld      bc, (_RAM_C050_)
    ld      a, b
    or      c
    jr      z, -
    add     hl, de
    ld      de, $FF00
    push    hl
    call    _LABEL_A1F_
    pop     hl
    ld      a, h
    cp      $68
    jr      c, _LABEL_4F34_
    jp      _LABEL_4F37_

++:
    ld      a, (_RAM_C055_)
    or      a
    jr      z, -
    ld      b, a
    ld      c, $00
    dec     bc
    push    hl
    ld      hl, (_RAM_C050_)
    or      a
    sbc     hl, bc
    pop     hl
    jr      nc, -
    add     hl, de
    ld      de, $0100
    push    hl
    call    _LABEL_A1F_
    pop     hl
    ld      a, h
    cp      $98
    jr      nc, _LABEL_4F34_
    jp      _LABEL_4F37_

_LABEL_4EF8_:
    ld      a, h
    bit     7, d
    jr      nz, +
    cp      $98
    jr      c, ++
    push    hl
    call    _LABEL_A1F_
    pop     hl
    jr      nc, _LABEL_4F37_
    ld      a, h
    cp      $F6
    jr      c, _LABEL_4F34_
    ld      a, $02
    ld      (_RAM_C09B_), a
    jp      _LABEL_4F37_

+:
    cp      $68
    jr      nc, ++
    push    hl
    call    _LABEL_A1F_
    pop     hl
    jr      nc, _LABEL_4F37_
    ld      a, h
    cp      $12
    jr      nc, _LABEL_4F34_
    ld      a, $01
    ld      (_RAM_C09B_), a
    jp      _LABEL_4F37_

++:
    ld      de, $0000
    ld      (_RAM_C052_), de
_LABEL_4F34_:
    ld      (_RAM_C20C_), hl
_LABEL_4F37_:
    ld      hl, (_RAM_C20C_)
    ld      de, (_RAM_C04F_)
    add     hl, de
    ld      (_RAM_C217_), hl
    ld      a, (_RAM_C051_)
    jr      nc, +
    inc     a
+:
    ld      (_RAM_C219_), a
    ld      hl, (_RAM_C20A_)
    ld      de, (Alex_SpeedY)
    add     hl, de
    ld      (_RAM_C20A_), hl
    ld      a, h
    cp      $C0
_LABEL_4F59_:
    jp      c, _LABEL_5061_
    bit     7, d
    ld      hl, _RAM_C212_
    jr      nz, +
    inc     (hl)
    ld      hl, $0000
    jr      ++

+:
    dec     (hl)
    ld      hl, $BF00
++:
    ld      (_RAM_C20A_), hl
    jp      _LABEL_5061_

_LABEL_4F73_:
    ld      a, (_RAM_C062_)
    ld      (VDPRegister9), a
    call    _LABEL_569B_
    ld      a, (Alex_State)
    cp      $19
    jp      z, _LABEL_5061_
    ld      hl, (_RAM_C20A_)
    ld      de, (Alex_SpeedY)
    add     hl, de
    ld      a, d
    or      e
    jr      nz, _LABEL_4FE2_
    ld      a, h
    ld      de, $0100
    cp      $48
    jr      c, +
    ld      de, $FE00
    cp      $98
    jr      nc, ++
-:
    ld      de, $0000
    jr      _LABEL_4FE2_

+:
    ld      bc, (_RAM_C062_)
    ld      a, b
    or      c
    jr      z, -
    add     hl, de
    ld      de, $FF00
    push    hl
    call    _LABEL_B6C_
    pop     hl
    ld      a, h
    cp      $58
    jr      c, _LABEL_501E_
    jp      _LABEL_5021_

++:
    ld      a, (_RAM_C06A_)
    or      a
    jr      z, -
    ld      b, a
    dec     b
    ld      c, $BF
    push    hl
    ld      hl, (_RAM_C065_)
    or      a
    sbc     hl, bc
    pop     hl
    jr      nc, -
    add     hl, de
    ld      de, $0200
    push    hl
    call    _LABEL_B6C_
    pop     hl
    ld      a, h
    cp      $90
    jr      nc, _LABEL_501E_
    jp      _LABEL_5021_

_LABEL_4FE2_:
    ld      a, h
    bit     7, d
    jr      nz, +
    cp      $90
    jr      c, ++
    push    hl
    call    _LABEL_B6C_
    pop     hl
    jr      nc, _LABEL_5021_
    ld      a, h
    cp      $C0
    jr      c, _LABEL_501E_
    ld      a, $02
    ld      (_RAM_C09A_), a
    jp      _LABEL_5021_

+:
    cp      $58
    jr      nc, ++
    push    hl
    call    _LABEL_B6C_
    pop     hl
    jr      nc, _LABEL_5021_
    ld      a, h
    cp      $10
    jr      nc, _LABEL_501E_
    ld      a, $01
    ld      (_RAM_C09A_), a
    jp      _LABEL_5021_

++:
    ld      de, $0000
    ld      (_RAM_C067_), de
_LABEL_501E_:
    ld      (_RAM_C20A_), hl
_LABEL_5021_:
    ld      hl, (_RAM_C20A_)
    ld      de, (_RAM_C064_)
    add     hl, de
    ld      a, h
    jr      nc, +
    add     a, $40
    ld      h, a
    jr      ++

+:
    ld      a, (_RAM_C066_)
    ld      b, a
    ld      a, h
    cp      $C0
    jr      c, +
    sub     $C0
    ld      h, a
    inc     b
+:
    ld      a, b
    ld      (_RAM_C216_), a
++:
    ld      (_RAM_C214_), hl
    ld      de, (_RAM_C20C_)
    ld      hl, (Alex_SpeedX)
    add     hl, de
    ld      a, h
    ld      b, $01
    cp      $12
    jr      c, +
    inc     b
    cp      $F6
    jr      c, ++
+:
    ex      de, hl
    ld      a, b
    ld      (_RAM_C09B_), a
++:
    ld      (_RAM_C20C_), hl
_LABEL_5061_:
    ld      a, $07
    ld      (Frame2_Select), a
    ld      a, (_RAM_C205_)
    ld      hl, (_RAM_C086_)
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C209_)
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ld      a, (hl)
    ld      (_RAM_C085_), a
    inc     hl
    ld      a, (_RAM_C082_)
    cp      (hl)
    jr      z, +
    ld      a, (hl)
    ld      (_RAM_C081_), a
    ld      (_RAM_C082_), a
    inc     hl
    ld      de, _RAM_C083_
    ldi
    ldi
+:
    ld      hl, _RAM_C227_
    bit     0, (hl)
    jp      z, _LABEL_6876_
    ld      a, (hl)
    xor     $80
    ld      (hl), a
    bit     7, a
    ret     nz
    jp      _LABEL_6876_

; Data from 50AC to 565B (1456 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_50ac.inc"

_LABEL_565C_:
    ld      a, (_RAM_C08E_)
    or      a
    ret     nz
    ld      a, (_RAM_C229_)
    and     $05
    ret     nz
    bit     5, (ix+34)
    ret     nz
    ld      hl, (Alex_SpeedY)
    ld      de, $0044
    ld      a, (InputFlags)
    and     BTN_2
    jr      z, +
    bit     7, h
    jr      z, +
    ld      a, (_RAM_C223_)
    inc     a
    cp      $10
    jr      z, +
    ld      (_RAM_C223_), a
    ld      de, $0014
+:
    add     hl, de
    ld      a, h
    cp      $04
    jr      c, +
    rlca
    jr      c, +
    ld      hl, $03FF
+:
    ld      (Alex_SpeedY), hl
    ret

_LABEL_569B_:
    ld      a, (Alex_State)
    cp      Alex_State_Dead
    ret     z
    call    _LABEL_565C_
    ld      a, (_RAM_C212_)
    or      a
    ret     nz
    ld      b, $B9
    ld      a, (_RAM_C076_)
    or      a
    jr      z, +
    dec     b
+:
    ld      a, (_RAM_C20B_)
    cp      b
    ret     nc
    ld      hl, $0000
    ld      (_RAM_C09C_), hl
    ld      a, (_RAM_C20F_)
    rlca
    jr      c, +
    call    ++
    jp      _LABEL_58DE_

+:
    call    _LABEL_58DE_
    jp      _LABEL_5819_

++:
    xor     a
    ld      (_RAM_C093_), a
    ld      (_RAM_C099_), a
    call    _LABEL_5CF2_
    call    _LABEL_5D84_
    call    _LABEL_5D5F_
    ex      de, hl
    ld      hl, (_RAM_C0E0_)
    jp      call_jump_table

_LABEL_56E6_:
    call    _LABEL_5CF2_
    call    _LABEL_5DA4_
    call    _LABEL_5D5F_
    ex      de, hl
    ld      hl, (_RAM_C0E2_)
    rst     $08    ; call_jump_table
    ld      a, (_RAM_C0A8_)
    or      a
    jr      z, +
    res     0, (ix+34)
    ld      a, (_RAM_C09D_)
    or      a
    call    z, _LABEL_2AF8_
    ld      a, $02
    ld      (_RAM_C094_), a
+:
    ld      hl, (_RAM_C09C_)
    ld      a, h
    or      l
    ret     z
    jp      _LABEL_57F1_

; Data from 5713 to 57A4 (146 bytes)
.db $21 $A8 $C0 $CB $C6 $CD $02 $5C $18 $C9 $21 $A8 $C0 $CB $CE $C9
.db $21 $A8 $C0 $CB $D6 $18 $BC $21 $A8 $C0 $CB $DE $C3 $E6 $5B $CD
.db $AB $57 $DD $CB $29 $56 $20 $AB $21 $AB $C0 $CB $C6 $18 $A4 $CD
.db $AB $57 $DD $CB $29 $56 $C0 $21 $AB $C0 $CB $CE $C9 $21 $A6 $C0
.db $CB $C6 $18 $8F $21 $A6 $C0 $CB $CE $18 $60 $21 $A3 $C0 $CB $C6
.db $C3 $E6 $56 $21 $A3 $C0 $CB $CE $18 $51 $DD $CB $22 $4E $20 $14
.db $DD $CB $08 $7E $28 $06 $DD $CB $22 $76 $20 $08 $21 $9C $C0 $36
.db $01 $C3 $E6 $56 $CD $9F $57 $C3 $E6 $56 $DD $CB $22 $4E $20 $0C
.db $DD $CB $08 $7E $28 $58 $DD $CB $22 $76 $28 $52 $1B $ED $53 $4A
.db $DD $C9

_LABEL_57A5_:
    call    +
    jp      _LABEL_56E6_

+:
    ld      a, $01
    ld      (_RAM_C09D_), a
    ret

_LABEL_57B1_:
    ld      hl, _RAM_C099_
    set     0, (hl)
    jp      _LABEL_56E6_

_LABEL_57B9_:
    ld      hl, _RAM_C099_
    set     1, (hl)
_LABEL_57BE_:
    ld      hl, _RAM_C093_
    bit     0, (hl)
    jr      nz, _LABEL_57F1_
    set     0, (ix+34)
    ret

; Data from 57CA to 57F0 (39 bytes)
.db $DD $CB $22 $4E $C2 $E6 $56 $DD $CB $22 $56 $C2 $E6 $56 $21 $93
.db $C0 $CB $C6 $C3 $E6 $56 $DD $CB $22 $4E $20 $D8 $DD $CB $22 $56
.db $20 $D2 $21 $93 $C0 $CB $CE

_LABEL_57F1_:
    ld      hl, (_RAM_C20A_)
    ld      de, (Alex_SpeedY)
    add     hl, de
    ld      de, (_RAM_C061_)
    call    negate_de
    or      a
    sbc     hl, de
    ld      a, h
    and     $F8
    ld      h, a
    ld      l, $00
    add     hl, de
    ld      (_RAM_C20A_), hl
    ld      a, $02
    ld      (_RAM_C094_), a
    res     0, (ix+34)
    jp      _LABEL_2AF8_

_LABEL_5819_:
    ld      a, (_RAM_C20B_)
    cp      $20
    ret     c
    call    _LABEL_5CFC_
    call    _LABEL_5D84_
    call    _LABEL_5D5F_
    ex      de, hl
    ld      hl, (_RAM_C0E4_)
    rst     $08    ; call_jump_table
    call    _LABEL_5CFC_
    call    _LABEL_5DA4_
    call    _LABEL_5D5F_
    ex      de, hl
    ld      hl, (_RAM_C0E6_)
    rst     $08    ; call_jump_table
    ld      hl, (_RAM_C09C_)
    ld      a, h
    or      l
    ret     z
    jr      _LABEL_589F_

; Data from 5843 to 589E (92 bytes)
.db $CD $AB $57 $DD $CB $29 $56 $C0 $21 $AA $C0 $CB $C6 $C9 $CD $AB
.db $57 $DD $CB $29 $56 $C0 $21 $AA $C0 $CB $CE $C9 $21 $A5 $C0 $CB
.db $C6 $C9 $21 $A5 $C0 $CB $CE $C9 $21 $A2 $C0 $CB $C6 $C9 $21 $A2
.db $C0 $CB $CE $C9 $DD $CB $22 $4E $C2 $9F $57 $DD $CB $08 $7E $C2
.db $9F $57 $21 $9C $C0 $36 $01 $C9 $DD $CB $22 $4E $C2 $9F $57 $DD
.db $CB $08 $7E $C2 $9F $57 $21 $9C $C0 $36 $01 $C9

_LABEL_589F_:
    ld      a, $01
    ld      (_RAM_C094_), a
    jp      _LABEL_2AF8_

; Data from 58A7 to 58DC (54 bytes)
.db $21 $8F $C0 $CB $C6 $DD $CB $22 $56 $C4 $B9 $58 $C9 $21 $8F $C0
.db $CB $CE $2A $0A $C2 $ED $5B $0E $C2 $19 $11 $00 $1C $B7 $ED $52
.db $3A $62 $C0 $32 $92 $C0 $E6 $07 $57 $1E $00 $19 $7C $E6 $F8 $92
.db $67 $2E $00 $22 $90 $C0

_LABEL_58DD_:
    ret

_LABEL_58DE_:
    ld      b, $03
    ld      hl, _RAM_C0AD_
    ld      a, (_RAM_C20B_)
    cp      $18
    jr      nc, +
    dec     b
    cp      $10
    jr      nc, +
    dec     b
    cp      $08
    ret     c
+:
    ld      (hl), b
    xor     a
    ld      h, a
    ld      l, a
    ld      (_RAM_C095_), a
    ld      (_RAM_C09E_), hl
    ld      hl, (Alex_SpeedX)
    ld      a, h
    or      l
    jr      nz, ++
    bit     7, (ix+8)
    jp      z, _LABEL_5C86_
    call    _LABEL_5D1F_
    bit     0, (ix+9)
    jr      nz, +
    call    _LABEL_5D80_
    jp      _LABEL_5B18_

+:
    call    _LABEL_5DA0_
    jr      +++

++:
    ld      a, h
    rlca
    jp      c, _LABEL_5B12_
    call    _LABEL_5D2C_
    call    _LABEL_5DA8_
+++:
    and     $F8
    rrca
    rrca
    add     a, l
    ld      l, a
    ld      de, $FFC0
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0E8_)
    jp      call_jump_table

_LABEL_593D_:
    ld      hl, _RAM_C0AD_
    dec     (hl)
    jr      z, +
    exx
    add     hl, de
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0EA_)
    jp      call_jump_table

_LABEL_594F_:
    ld      hl, _RAM_C0AD_
    dec     (hl)
    jr      z, +
    exx
    add     hl, de
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0EC_)
    rst     $08    ; call_jump_table
+:
    ld      a, (_RAM_C222_)
    and     $30
    call    nz, _LABEL_5C86_
    ld      hl, _RAM_C09E_
    ld      a, (hl)
    inc     hl
    or      (hl)
    ret     z
    jp      _LABEL_5A77_

; Data from 5971 to 5A08 (152 bytes)
.db $DD $CB $08 $7E $20 $05 $21 $AC $C0 $CB $C6 $CD $49 $5A $18 $BC
.db $DD $CB $08 $7E $20 $05 $21 $AC $C0 $CB $CE $CD $49 $5A $18 $BE
.db $DD $CB $08 $7E $20 $05 $21 $AC $C0 $CB $D6 $C3 $49 $5A $3E $02
.db $32 $A9 $C0 $CD $96 $5B $18 $94 $3E $01 $32 $A9 $C0 $CD $AC $5B
.db $C3 $2B $5B $DD $CB $22 $4E $20 $0C $DD $CB $08 $7E $20 $06 $CD
.db $FA $59 $C3 $3D $59 $CD $00 $5A $C3 $3D $59 $DD $CB $22 $4E $20
.db $0C $DD $CB $08 $7E $20 $06 $CD $FA $59 $C3 $4F $59 $CD $00 $5A
.db $C3 $4F $59 $DD $CB $22 $76 $20 $37 $DD $CB $22 $4E $CA $77 $5A
.db $DD $CB $08 $7E $C4 $00 $5A $D9 $C9 $21 $9E $C0 $36 $01 $C9 $D9
.db $E5 $2B $22 $4A $DD $E1 $D9 $C9

_LABEL_5A09_:
    ld      hl, _RAM_C095_
    set     0, (hl)
    jp      _LABEL_593D_

_LABEL_5A11_:
    ld      hl, _RAM_C095_
    set     1, (hl)
    jp      _LABEL_594F_

_LABEL_5A19_:
    ld      hl, _RAM_C095_
    set     2, (hl)
    call    _LABEL_5B5F_
_LABEL_5A21_:
    exx
    ret

_LABEL_5A23_:
    bit     OBJECT_F34_DISABLE_ANIM, (ix+34)
    jr      z, _LABEL_5A77_
    call    +
    jp      _LABEL_593D_

; Data from 5A2F to 5A48 (26 bytes)
.db $DD $CB $22 $4E $28 $42 $CD $49 $5A $C3 $4F $59 $DD $CB $22 $76
.db $20 $E0 $DD $CB $22 $4E $20 $DA $18 $2E

+:
    ld      a, $01
    ld      (_RAM_C09F_), a
    ret

; Data from 5A4F to 5A72 (36 bytes)
.db $DD $CB $22 $4E $28 $1E $CD $49 $5A $C3 $2B $5B $DD $CB $22 $4E
.db $28 $12 $CD $49 $5A $C3 $3D $5B $DD $CB $22 $76 $20 $B4 $DD $CB
.db $22 $4E $20 $AE

_LABEL_5A73_:
    ld      a, $01
    jr      +

_LABEL_5A77_:
    ld      a, $02
+:
    ld      hl, _RAM_C088_
    ld      (hl), a
    inc     hl
    cp      (hl)
    jr      z, +
    ld      (hl), a
    inc     hl
    ld      (hl), a
+:
    exx
    jp      _LABEL_2433_

; Data from 5A88 to 5B11 (138 bytes)
.db $DD $CB $08 $7E $20 $05 $21 $AC $C0 $CB $C6 $CD $49 $5A $C3 $2B
.db $5B $DD $CB $08 $7E $20 $05 $21 $AC $C0 $CB $CE $CD $49 $5A $C3
.db $3D $5B $DD $CB $08 $7E $20 $05 $21 $AC $C0 $CB $D6 $18 $92 $21
.db $95 $C0 $CB $C6 $18 $6D $21 $95 $C0 $CB $CE $18 $78 $21 $95 $C0
.db $CB $D6 $CD $6D $5B $D9 $C9 $DD $CB $22 $4E $20 $0B $DD $CB $08
.db $7E $20 $05 $CD $FA $59 $18 $4B $CD $00 $5A $18 $46 $DD $CB $22
.db $4E $20 $0B $DD $CB $08 $7E $20 $05 $CD $FA $59 $18 $47 $CD $00
.db $5A $18 $42 $DD $CB $22 $76 $C2 $21 $5A $DD $CB $22 $4E $CA $73
.db $5A $DD $CB $08 $7E $C4 $00 $5A $D9 $C9

_LABEL_5B12_:
    call    _LABEL_5D2C_
    call    _LABEL_5D88_
_LABEL_5B18_:
    and     $F8
    rrca
    rrca
    add     a, l
    ld      l, a
    ld      de, $FFC0
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0EE_)
    jp      call_jump_table

_LABEL_5B2B_:
    ld      hl, _RAM_C0AD_
    dec     (hl)
    jr      z, +
    exx
    add     hl, de
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0F0_)
    jp      call_jump_table

_LABEL_5B3D_:
    ld      hl, _RAM_C0AD_
    dec     (hl)
    jr      z, +
    exx
    add     hl, de
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0F2_)
    rst     $08    ; call_jump_table
+:
    ld      a, (_RAM_C222_)
    and     $30
    call    nz, _LABEL_5C86_
    ld      hl, _RAM_C09E_
    ld      a, (hl)
    inc     hl
    or      (hl)
    ret     z
    jp      _LABEL_5A73_

_LABEL_5B5F_:
    ld      hl, (_RAM_C20C_)
    ld      de, (Alex_SpeedX)
    add     hl, de
    ld      de, $0700
    add     hl, de
    jr      +

_LABEL_5B6D_:
    ld      hl, (_RAM_C20C_)
    ld      de, (Alex_SpeedX)
    add     hl, de
    ld      de, $0700
    or      a
    sbc     hl, de
+:
    ld      a, (_RAM_C050_)
    ld      (_RAM_C098_), a
    and     $07
    ld      d, a
    ld      e, $00
    add     hl, de
    ld      a, h
    and     $F8
    sub     d
    ld      h, a
    ld      l, $00
    ld      de, $0400
    add     hl, de
    ld      (_RAM_C096_), hl
    ret

; Data from 5B96 to 5C85 (240 bytes)
.db $2A $0C $C2 $ED $5B $10 $C2 $19 $11 $00 $07 $19 $ED $5B $4F $C0
.db $19 $7C $E6 $07 $18 $1B $2A $0C $C2 $ED $5B $10 $C2 $19 $11 $00
.db $07 $B7 $ED $52 $ED $5B $4F $C0 $CD $9A $07 $7A $E6 $07 $94 $E6
.db $07 $57 $1E $00 $3A $62 $C0 $ED $44 $E6 $07 $6F $3A $0B $C2 $D6
.db $02 $95 $C6 $07 $E6 $F8 $85 $67 $6B $B7 $ED $52 $22 $0A $C2 $C9
.db $DD $CB $11 $7E $C8 $21 $00 $FE $22 $10 $C2 $CD $4A $5C $B7 $28
.db $09 $21 $00 $FF $22 $10 $C2 $CD $4A $5C $18 $1E $2A $10 $C2 $7C
.db $B5 $C8 $CB $7C $C0 $21 $00 $02 $22 $10 $C2 $CD $64 $5C $B7 $28
.db $09 $21 $00 $01 $22 $10 $C2 $CD $64 $5C $D5 $2A $0A $C2 $24 $ED
.db $5B $61 $C0 $CD $9A $07 $B7 $ED $52 $7D $B7 $28 $01 $24 $7C $C6
.db $07 $E6 $F8 $67 $2E $00 $19 $D1 $7A $B7 $28 $04 $15 $ED $52 $25
.db $22 $0A $C2 $C9 $2A $0C $C2 $ED $5B $10 $C2 $19 $11 $00 $06 $19
.db $ED $5B $4F $C0 $19 $7C $E6 $07 $57 $1E $00 $E6 $01 $C9 $2A $0C
.db $C2 $ED $5B $10 $C2 $19 $11 $00 $06 $B7 $ED $52 $ED $5B $4F $C0
.db $CD $9A $07 $EB $B7 $ED $52 $7C $E6 $07 $57 $1E $00 $E6 $01 $C9

_LABEL_5C86_:
    call    _LABEL_5D2C_
    push    hl
    ld      hl, (_RAM_C20C_)
    ld      de, (_RAM_C04F_)
    add     hl, de
    ld      a, h
    pop     hl
    and     $F8
    rrca
    rrca
    add     a, l
    ld      l, a
    ld      de, $FFC0
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0F4_)
    rst     $18    ; get_array_index
    set     0, (hl)
    exx
    add     hl, de
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0F6_)
    rst     $18    ; get_array_index
    set     1, (hl)
    exx
    add     hl, de
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0F8_)
    rst     $18    ; get_array_index
    set     2, (hl)
    exx
    add     hl, de
    call    _LABEL_5D65_
    exx
    ld      hl, (_RAM_C0FA_)
    rst     $18    ; get_array_index
    set     3, (hl)
    exx
    bit     2, (ix+34)
    ret     nz
    bit     4, (ix+34)
    ret     nz
    ld      hl, (_RAM_C20C_)
    ld      a, (_RAM_C050_)
    ld      (_RAM_C098_), a
    and     $07
    ld      e, a
    ld      a, h
    add     a, e
    and     $F8
    sub     e
    ld      h, a
    ld      l, $00
    ld      de, $0400
    add     hl, de
    ld      (_RAM_C096_), hl
    ret

_LABEL_5CF2_:
    ld      hl, (_RAM_C20A_)
    ld      de, (Alex_SpeedY)
    add     hl, de
    jr      _LABEL_5D42_

_LABEL_5CFC_:
    ld      hl, (_RAM_C20A_)
    ld      de, (Alex_SpeedY)
    add     hl, de
    ld      l, $00
    ld      de, $1000
    bit     OBJECT_F34_DISABLE_ANIM, (ix+34)
    jr      nz, +
    ld      de, $1C00
+:
    or      a
    sbc     hl, de
    jr      nc, _LABEL_5D42_
    ld      de, $2000
    or      a
    sbc     hl, de
    jr      _LABEL_5D42_

_LABEL_5D1F_:
    ld      hl, (_RAM_C20A_)
    ld      de, (Alex_SpeedY)
    add     hl, de
    ld      de, $0C00
    jr      10

_LABEL_5D2C_:
    ld      hl, (_RAM_C20A_)
    ld      de, (Alex_SpeedY)
    add     hl, de
    ld      de, $0300
    or      a
    sbc     hl, de
    jr      nc, _LABEL_5D42_
    ld      de, $2000
    or      a
    sbc     hl, de
_LABEL_5D42_:
    ld      de, (_RAM_C061_)
    add     hl, de
    ld      a, h
    jr      nc, +
    add     a, $20
+:
    cp      $E0
    jr      c, +
    sub     $E0
+:
    and     $F8
    ld      l, a
    ld      h, $00
    add     hl, hl
    add     hl, hl
    add     hl, hl
    ld      de, $D601
    add     hl, de
    ret

_LABEL_5D5F_:
    and     $F8
    rrca
    rrca
    add     a, l
    ld      l, a
_LABEL_5D65_:
    push    de
    ex      de, hl
    ld      hl, ScreenMapBuffer
    or      a
    sbc     hl, de
    jr      c, +
    ex      de, hl
    ld      hl, $DD00
    or      a
    sbc     hl, de
    ex      de, hl
+:
    ex      de, hl
    pop     de
    ld      a, (hl)
    rlca
    rlca
    rlca
    and     $07
    ret

_LABEL_5D80_:
    ld      b, $0C
    jr      +

_LABEL_5D84_:
    ld      b, $06
    jr      +

_LABEL_5D88_:
    ld      b, $07
+:
    ld      c, $00
    push    hl
    ld      hl, (_RAM_C20C_)
    ld      de, (Alex_SpeedX)
    add     hl, de
    ld      de, (_RAM_C04F_)
    add     hl, de
    or      a
    sbc     hl, bc
    ld      a, h
    pop     hl
    ret

_LABEL_5DA0_:
    ld      b, $0C
    jr      +

_LABEL_5DA4_:
    ld      b, $06
    jr      +

_LABEL_5DA8_:
    ld      b, $07
+:
    ld      c, $00
    push    hl
    ld      hl, (_RAM_C20C_)
    ld      de, (Alex_SpeedX)
    add     hl, de
    ld      de, (_RAM_C04F_)
    add     hl, de
    add     hl, bc
    ld      a, h
    pop     hl
    ret

_LABEL_5DBE_:
    ld      a, (StageNumber)
    cp      $03
    jr      nz, ++
    ld      b, $03
    ld      a, (_RAM_DD07_)
    cp      $02
    jr      nz, +
    inc     b
+:
    ld      a, b
++:
    ld      hl, $5DDD
    rst     $18    ; get_array_index
    ld      de, _RAM_C0E0_
    ld      bc, $001C
    ldir
    ret

; Data from 5DDD to 5F7F (419 bytes)
.db $E7 $5D $03 $5E $E7 $5D $1F $5E $3B $5E $57 $5E $67 $5E $D7 $5E
.db $E7 $5E $57 $5F $67 $5F $77 $5F $E7 $5F $F7 $5F $07 $60 $77 $60
.db $87 $60 $97 $60 $A7 $60 $B7 $5E $C7 $5E $37 $5F $47 $5F $B7 $5F
.db $C7 $5F $D7 $5F $47 $60 $57 $60 $67 $60 $37 $61 $47 $61 $57 $61
.db $67 $61 $77 $5E $87 $5E $F7 $5E $07 $5F $87 $5F $97 $5F $A7 $5F
.db $17 $60 $27 $60 $37 $60 $B7 $60 $C7 $60 $D7 $60 $E7 $60 $97 $5E
.db $A7 $5E $17 $5F $27 $5F $87 $5F $97 $5F $A7 $5F $17 $60 $27 $60
.db $37 $60 $F7 $60 $07 $61 $17 $61 $27 $61 $E6 $56 $A5 $57 $A5 $57
.db $CA $57 $B1 $57 $6D $57 $5E $57 $32 $57 $BE $57 $AB $57 $AB $57
.db $E0 $57 $B9 $57 $8D $57 $66 $57 $42 $57 $E6 $56 $32 $57 $A5 $57
.db $CA $57 $50 $57 $6D $57 $13 $57 $23 $57 $BE $57 $42 $57 $AB $57
.db $E0 $57 $57 $57 $8D $57 $1D $57 $2A $57 $E6 $56 $32 $57 $A5 $57
.db $CA $57 $5E $57 $6D $57 $13 $57 $23 $57 $BE $57 $42 $57 $AB $57
.db $E0 $57 $66 $57 $8D $57 $1D $57 $2A $57 $E6 $56 $A5 $57 $A5 $57
.db $CA $57 $5E $57 $6D $57 $50 $57 $32 $57 $BE $57 $AB $57 $AB $57
.db $E0 $57 $66 $57 $8D $57 $57 $57 $42 $57 $DD $58 $DD $58 $AB $57
.db $A7 $58 $DD $58 $77 $58 $6B $58 $43 $58 $DD $58 $DD $58 $AB $57
.db $B4 $58 $DD $58 $8B $58 $71 $58 $51 $58 $DD $58 $43 $58 $AB $57
.db $A7 $58 $5F $58 $77 $58 $AB $57 $AB $57 $DD $58 $51 $58 $AB $57
.db $B4 $58 $65 $58 $8B $58 $AB $57 $AB $57 $DD $58 $43 $58 $AB $57
.db $A7 $58 $6B $58 $77 $58 $AB $57 $AB $57 $DD $58 $51 $58 $AB $57
.db $B4 $58 $71 $58 $8B $58 $AB $57 $AB $57 $DD $58 $DD $58 $AB $57
.db $A7 $58 $6B $58 $77 $58 $5F $58 $43 $58 $DD $58 $DD $58 $AB $57
.db $B4 $58 $71 $58 $8B $58 $65 $58 $51 $58 $3D $59 $3D $59 $23 $5A
.db $3D $59 $09 $5A $B4 $59 $3D $59 $71 $59 $4F $59 $4F $59 $2F $5A
.db $4F $59 $11 $5A $CC $59 $4F $59 $81 $59 $21 $5A $21 $5A $3B $5A
.db $21 $5A $19

; 11th entry of Jump Table from 8D10 (indexed by _RAM_DE03_)
_LABEL_5F80_:
    ld      e, d
    call    po, _LABEL_2159_
    ld      e, d
    sub     c
    ld      e, c
    dec     a
    ld      e, c
    ld      (hl), c
    ld      e, c
    inc     hl
    ld      e, d
    dec     a
    ld      e, c
    dec     a
    ld      e, c
    or      h
    ld      e, c
    dec     a
    ld      e, c
    sbc     a, a
    ld      e, c
    ld      c, a
    ld      e, c
    add     a, c
    ld      e, c
    cpl
    ld      e, d
    ld      c, a
    ld      e, c
    ld      c, a
    ld      e, c
    call    z, _LABEL_4F59_
    ld      e, c
    cpl
    ld      e, d
    ld      hl, $915A
    ld      e, c
    dec     sp
    ld      e, d
    ld      hl, $215A
    ld      e, d
    call    po, _LABEL_2159_
    ld      e, d
    dec     sp
    ld      e, d
    dec     a
    ld      e, c
    dec     a
    ld      e, c
    inc     hl
    ld      e, d
    dec     a
    ld      e, c
    dec     a
    ld      e, c
    or      h
    ld      e, c
    dec     a
    ld      e, c
    ld      (hl), c
    ld      e, c
    ld      c, a
    ld      e, c
    ld      c, a
    ld      e, c
    cpl
    ld      e, d
    ld      c, a
    ld      e, c
    ld      c, a
    ld      e, c
    call    z, _LABEL_4F59_
    ld      e, c
    add     a, c
    ld      e, c
    ld      hl, $215A
    ld      e, d
    dec     sp
    ld      e, d
    ld      hl, $215A
    ld      e, d
    call    po, _LABEL_2159_
    ld      e, d
    sub     c
    ld      e, c
    dec     hl
    ld      e, e
    dec     hl
    ld      e, e
    ld      c, a
    ld      e, d
    dec     hl
    ld      e, e
    or      a
    ld      e, d
    rst     $08    ; call_jump_table
    ld      e, d
    dec     hl
    ld      e, e
    adc     a, b
    ld      e, d
    dec     a
    ld      e, e
    dec     a
    ld      e, e
    ld      e, e
    ld      e, d
    dec     a
    ld      e, e
    cp      (hl)
    ld      e, d
    push    hl
    ld      e, d
    dec     a
    ld      e, e
    sbc     a, c
    ld      e, d
    ld      hl, $215A
    ld      e, d
    ld      h, a
    ld      e, d
    ld      hl, $C55A
    ld      e, d
    ei
    ld      e, d
    ld      hl, $AA5A
    ld      e, d
    dec     hl
    ld      e, e
    adc     a, b
    ld      e, d
    ld      c, a
    ld      e, d
    dec     hl
    ld      e, e
    dec     hl
    ld      e, e
    rst     $08    ; call_jump_table
    ld      e, d
    xor     c
    ld      e, c
    dec     hl
    ld      e, e
    dec     a
    ld      e, e
    sbc     a, c
    ld      e, d
    ld      e, e
    ld      e, d
    dec     a
    ld      e, e
    dec     a
    ld      e, e
    push    hl
    ld      e, d
    ld      e, e
    ld      e, d
    dec     a
    ld      e, e
    ld      hl, $AA5A
    ld      e, d
    ld      h, a
    ld      e, d
    ld      hl, $215A
    ld      e, d
    ei
    ld      e, d
    ld      h, a
    ld      e, d
    ld      hl, $2B5A
    ld      e, e
    dec     hl
    ld      e, e
    ld      c, a
    ld      e, d
    dec     hl
    ld      e, e
    dec     hl
    ld      e, e
    rst     $08    ; call_jump_table
    ld      e, d
    dec     hl
    ld      e, e
    adc     a, b
    ld      e, d
    dec     a
    ld      e, e
    dec     a
    ld      e, e
    ld      e, e
    ld      e, d
    dec     a
    ld      e, e
    dec     a
    ld      e, e
    push    hl
    ld      e, d
    dec     a
    ld      e, e
    sbc     a, c
    ld      e, d
    ld      hl, $215A
    ld      e, d
    ld      h, a
    ld      e, d
    ld      hl, $215A
    ld      e, d
    ei
    ld      e, d
    ld      hl, $AA5A
    ld      e, d
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    sub     l
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    sub     l
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    sub     l
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    sub     l
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    adc     a, h
    ret     nz
    and     a
    ret     nz
    adc     a, h
    ret     nz
_LABEL_6177_:
    ld      hl, _RAM_C081_
    ld      a, (hl)
    or      a
    ret     z
    ld      (hl), $00
    ld      b, $05
    bit     7, a
    jr      nz, +
    ld      b, $03
    ld      a, (_RAM_C209_)
    or      a
    jr      z, +
    inc     b
+:
    ld      a, b
    ld      (Frame2_Select), a
    ld      a, $00
    out     (Ports_VDP_Control), a
    ld      a, VDPCTL_WriteAddress 
    out     (Ports_VDP_Control), a
    ld      c, Ports_VDP_Data

    ld      hl, (_RAM_C083_)
    ld      a, (_RAM_C085_)
    cp      $0A
    jp      z, +
    cp      $09
    jp      z, _LABEL_6244_
    cp      $08
    jp      z, _LABEL_62C5_
    cp      $07
    jp      z, _LABEL_6346_
    cp      $04
    jp      z, _LABEL_64C8_
    cp      $02
    jp      z, _LABEL_65C9_
    jp      _LABEL_63C7_

+:
    xor     a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
_LABEL_6244_:
    xor     a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
_LABEL_62C5_:
    xor     a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
_LABEL_6346_:
    xor     a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
_LABEL_63C7_:
    xor     a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
_LABEL_64C8_:
    xor     a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
_LABEL_65C9_:
    xor     a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    outi
    outi
    outi
    out     (c), a
    ret

_LABEL_66CB_:
    ld      a, $09
    ld      (Frame2_Select), a
    ld      a, (_RAM_C080_)
    dec     a
    ld      (_RAM_C080_), a
    ld      b, $0E
    ld      ix, _RAM_C300_
    rrca
    jr      c, _LABEL_66F1_
    ld      ix, _RAM_C4A0_
-:
    push    bc
    call    +
    pop     bc
    ld      de, $FFE0
    add     ix, de
    djnz    -
    ret

_LABEL_66F1_:
    push    bc
    call    +
    pop     bc
    ld      de, $0020
    add     ix, de
    djnz    _LABEL_66F1_
    ret

+:
    ld      a, (ix+0)
    or      a
    ret     z
    call    _LABEL_6721_
    bit     2, (ix+1)
    jr      z, +
    jp      _LABEL_67B2_

+:
    call    _LABEL_679C_
    ld      a, (ix+19)
    or      (ix+18)
    ret     z
    bit     5, (ix+1)
    jp      nz, _LABEL_88F_
    ret

_LABEL_6721_:
    or      a
    ld      e, (ix+12)
    ld      d, (ix+13)
    ld      l, (ix+16)
    ld      h, (ix+17)
    bit     7, (ix+1)
    jr      nz, +
    ld      bc, (_RAM_C052_)
    sbc     hl, bc
    ld      b, h
    add     hl, de
    ld      (ix+12), l
    ld      (ix+13), h
    ld      a, h
    rra
    xor     b
    jp      p, +++
    bit     7, b
    jr      z, ++
    dec     (ix+19)
    jr      +++

+:
    add     hl, de
    ld      (ix+12), l
    ld      (ix+13), h
    jr      +++

++:
    inc     (ix+19)
+++:
    or      a
    ld      e, (ix+10)
    ld      d, (ix+11)
    ld      l, (ix+14)
    ld      h, (ix+15)
    ld      bc, (_RAM_C067_)
    sbc     hl, bc
    ld      b, h
    add     hl, de
    ld      (ix+10), l
    ld      (ix+11), h
    ld      a, h
    cp      $C0
    jp      c, _LABEL_6876_
    bit     7, b
    ld      a, (ix+18)
    ld      de, $4000
    jr      z, +
    dec     a
    ld      d, $C0
    jr      ++

+:
    inc     a
++:
    add     hl, de
    ld      (ix+10), l
    ld      (ix+11), h
    or      a
    ld      (ix+18), a
    jp      _LABEL_6876_

_LABEL_679C_:
    ld      a, (ix+19)
    or      a
    jr      z, +
    and     $01
    jp      z, _LABEL_88F_
+:
    ld      a, (ix+18)
    or      a
    ret     z
    and     $01
    jp      z, _LABEL_88F_
    ret

_LABEL_67B2_:
    ld      e, (ix+20)
    ld      d, (ix+21)
    ld      l, (ix+14)
    ld      h, (ix+15)
    ld      b, h
    add     hl, de
    ld      (ix+20), l
    ld      (ix+21), h
    ld      a, h
    cp      $C0
    ret     c
    bit     7, b
    ld      a, (ix+22)
    ld      de, $4000
    jr      z, +
    dec     a
    ld      d, $C0
    jr      ++

+:
    inc     a
++:
    add     hl, de
    ld      (ix+20), l
    ld      (ix+21), h
    ld      (ix+22), a
    ret

_LABEL_67E5_:
    ld      ix, _RAM_DD50_
    ld      b, $0A
-:
    ld      a, (ix+4)
    or      a
    jr      z, +++
    ld      a, (_RAM_C076_)
    or      a
    push    bc
    jr      nz, +
    call    ++++
    jr      ++

+:
    call    +++++
++:
    pop     bc
+++:
    ld      de, $0006
    add     ix, de
    djnz    -
    ret

++++:
    ld      e, (ix+1)
    ld      d, (ix+3)
    ld      hl, $0000
    ld      bc, (_RAM_C052_)
    sbc     hl, bc
    ld      b, h
    add     hl, de
    ld      (ix+1), l
    ld      (ix+3), h
    ld      a, h
    rra
    xor     b
    ret     p
    bit     7, b
    jr      z, +
    dec     (ix+0)
    ret

+:
    inc     (ix+0)
    ret

+++++:
    or      a
    ld      l, (ix+1)
    ld      h, (ix+2)
    ld      de, (_RAM_C067_)
    sbc     hl, de
    ld      a, h
    cp      $C0
    jr      c, +++
    bit     7, d
    ld      bc, $C000
    jr      nz, +
    dec     (ix+0)
    ld      b, $40
    jr      ++

+:
    inc     (ix+0)
++:
    or      a
    sbc     hl, bc
+++:
    ld      (ix+1), l
    ld      (ix+2), h
    ret


; ==============================================================================
;  void object_update_animation_counters(object *obj)
; ------------------------------------------------------------------------------
;  Decrements the display counter for the current frame. If the counter expires
;  the next animation frame is displayed.
; ------------------------------------------------------------------------------
;  In:
;    IX     - pointer to the object
; ------------------------------------------------------------------------------
object_update_animation_counters:                                   ; $685D
    dec     (ix + Object_FrameCountDown)
    ret     nz
    ld      a, (ix + Object_FrameCountReset)
    ld      (ix + Object_FrameCountDown), a
    inc     (ix + Object_AnimFrame)
    ld      a, (ix + Object_MaxAnimFrame)
    cp      (ix + Object_AnimFrame)
    ret     nc
    ld      (ix + Object_AnimFrame), 0
    ret

_LABEL_6876_:
    bit     3, (ix+1)
    ret     nz
    ld      h, (ix+18)
    ld      l, (ix+11)
    ld      de, $0120
    or      a
    sbc     hl, de
    ret     nc
    ld      a, (ix+19)
    or      a
    ret     nz
    ld      l, (ix+2)
    ld      h, (ix+3)
    ld      a, h
    or      l
    ret     z
    ld      a, (ix+5)
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (ix+9)
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, a
    ld      a, (hl)
    or      a
    ret     z
    exx
    ld      b, a
    exx
    inc     hl
    ld      a, (hl)
    ld      (ix+8), a
    inc     hl
    ld      de, (WorkingSATPtr)
    ld      bc, (WorkingSAT_HPosPtr)
    exx
-:
    exx
    ld      a, (ix+11)
    add     a, (hl)
    ex      af, af'
    ld      a, (ix+18)
    or      a
    jr      z, +
    ex      af, af'
    jr      c, +++
    sub     $40
    jr      ++

+:
    ex      af, af'
++:
    cp      $D0
    jr      nz, +
    dec     a
+:
    ld      (de), a
    inc     hl
    ld      a, (ix+13)
    add     a, (hl)
    ld      (bc), a
    rr      a
    xor     (hl)
    jp      p, +++++
    jr      ++++

+++:
    inc     hl
++++:
    inc     hl
    inc     hl
    jr      ++++++

+++++:
    inc     hl
    inc     bc
    ld      a, (hl)
    ld      (bc), a
    inc     hl
    inc     de
    inc     bc
++++++:
    exx
    djnz    -
    exx
    ld      (WorkingSATPtr), de
    ld      (WorkingSAT_HPosPtr), bc
    ret


_LABEL_6903_:
    ld      a, (ix+8)
    add     a, a
    add     a, a
    ld      hl, _DATA_6C7C_
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    add     a, (ix+13)
    ret     nc
    cp      c
    ret     nc
    add     a, (hl)
    jr      c, +
    cp      b
    jr      c, +
    inc     hl
    ld      a, (hl)
    add     a, (ix+11)
    ret     nc
    cp      e
    ret     nc
    inc     hl
    add     a, (hl)
    jr      c, +
    cp      d
+:
    ccf
    ret

_LABEL_692D_:
    ld      a, (_RAM_C212_)
    or      a
    ret     nz
    ld      a, (ix+19)
    or      (ix+18)
    ret     nz
    bit     3, (ix+1)
    ret

_LABEL_693E_:
    call    _LABEL_692D_
    ret     nz
    ld      a, (_RAM_C208_)
    bit     7, a
    ret     z
    call    _LABEL_69BE_
    jp      _LABEL_6903_

_LABEL_694E_:
    call    _LABEL_692D_
    ret     nz
    jr      +

_LABEL_6954_:
    call    _LABEL_692D_
    ret     nz
    ld      a, (_RAM_C208_)
    bit     7, a
    jr      z, +
    call    _LABEL_69BE_
    call    _LABEL_6903_
    ret     c
+:
    ld      a, (_RAM_C229_)
    bit     2, a
    jr      nz, +
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      nz, _LABEL_69BC_
+:
    ld      iy, _RAM_C240_
    ld      b, $05
_LABEL_697A_:
    push    bc
    ld      a, (iy+0)
    cp      $02
    jr      z, +
    cp      $07
    jr      nz, ++
+:
    ld      a, (iy+13)
    sub     $06
    jr      nc, +
    xor     a
+:
    ld      b, a
    add     a, $0C
    jr      nc, +
    ld      a, $FF
+:
    ld      c, a
    ld      a, (iy+11)
    ld      e, a
    sub     $08
    jr      nc, +
    xor     a
+:
    ld      d, a
    call    _LABEL_6903_
    jr      nc, ++
    ld      b, $01
    ld      a, (ix+23)
    or      a
    jr      z, +
    inc     b
+:
    ld      (iy+26), b
    pop     bc
    scf
    ret

++:
    pop     bc
    ld      de, $0020
    add     iy, de
    djnz    _LABEL_697A_
_LABEL_69BC_:
    or      a
    ret

_LABEL_69BE_:
    ld      hl, $6AD3
    add     a, a
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C209_)
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C20D_)
    add     a, (hl)
    ld      b, a
    inc     hl
    add     a, (hl)
    ld      c, a
    inc     hl
    ld      a, (_RAM_C20B_)
    add     a, (hl)
    ld      d, a
    inc     hl
    add     a, (hl)
    ld      e, a
    ret

_LABEL_69E5_:
    call    _LABEL_692D_
    ret     nz
    ld      a, (_RAM_C227_)
    or      a
    ret     nz
    ld      a, (_RAM_C224_)
    or      a
    ret     nz
    ld      a, (_RAM_C222_)
    bit     1, a
    ret     nz
    ld      a, (_RAM_C229_)
    bit     2, a
    ret     nz
    ld      a, (_RAM_C208_)
    ld      hl, $6A7B
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C20D_)
    add     a, (hl)
    ld      b, a
    inc     hl
    add     a, (hl)
    ld      c, a
    inc     hl
    ld      a, (_RAM_C20B_)
    add     a, (hl)
    ld      d, a
    inc     hl
    add     a, (hl)
    ld      e, a
    call    _LABEL_6903_
    ret     nc
    ld      a, $01
    ld      (_RAM_C224_), a
    ld      a, (ix+13)
    ld      (_RAM_C0C4_), a
    ret

; Data from 6A2D to 6B8A (350 bytes)
.db $3A $12 $C2 $B7 $C0 $DD $7E $13 $DD $B6 $12 $C0 $3A $22 $C2 $CB
.db $4F $C0 $AF $21 $77 $6A $87 $87 $85 $6F $30 $01 $24 $3A $0D $C2
.db $86 $47 $23 $86 $4F $23 $3A $0B $C2 $86 $57 $23 $86 $5F $CD $03
.db $69 $DD $36 $17 $00 $D0 $3E $01 $DD $CB $01 $56 $20 $01 $3C $32
.db $26 $C2 $DD $22 $C5 $C0 $DD $77 $17 $C9 $FA $0C $F8 $08 $F8 $10
.db $E4 $1C $F8 $10 $E4 $1C $F8 $10 $E4 $1C $F8 $10 $E4 $1C $F8 $10
.db $FA $14 $F8 $10 $EC $14 $F8 $10 $EC $14 $F8 $10 $EC $14 $F8 $10
.db $00 $16 $F8 $10 $EA $16 $EC $14 $F2 $0C $F0 $10 $F2 $0C $00 $10
.db $F2 $0C $00 $14 $F2 $0C $00 $00 $00 $00 $00 $00 $00 $00 $00 $00
.db $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00
.db $00 $00 $F8 $10 $E4 $1C $E8 $20 $DC $0C $F8 $20 $DC $0C $E8 $10
.db $DC $1C $08 $10 $DC $1C $E4 $24 $D4 $14 $F8 $24 $D4 $14 $E4 $24
.db $D4 $24 $F8 $24 $D4 $24 $E4 $18 $02 $14 $04 $18 $02 $14 $F2 $14
.db $F2 $14 $FA $14 $F2 $14 $EA $1C $F2 $14 $FA $1C $F2 $14 $E4 $28
.db $F0 $18 $F4 $28 $F0 $18 $F0 $20 $F6 $20 $F0 $20 $F6 $20 $F0 $20
.db $E6 $20 $F0 $20 $E6 $20 $E4 $20 $F0 $10 $E4 $20 $F0 $10 $EC $18
.db $F0 $10 $EC $18 $F0 $10 $FC $18 $F0 $10 $FC $18 $F0 $10 $FC $20
.db $F0 $10 $FC $20 $F0 $10 $F8 $10 $E8 $20 $F8 $10 $E8 $20 $F8 $10
.db $F0 $20 $F8 $10 $F0 $20 $F8 $10 $F0 $10 $F8 $10 $F0 $10 $F8 $10
.db $E0 $20 $F8 $10 $E0 $20 $E8 $20 $F0 $10 $F8 $20 $F0 $10 $F4 $18
.db $D6 $50 $F4 $18 $D6 $50 $D4 $58 $EC $18 $D4 $58 $EC $18 $E8 $10
.db $E0 $18 $08 $10 $E0 $18 $E8 $20 $F0 $18 $F8 $20 $F0 $18

_LABEL_6B8B_:
    ld      a, (ix+19)
    or      (ix+18)
    jr      z, +
    xor     a
    ret

+:
    ld      h, (ix+11)
    ld      l, (ix+10)
    ld      d, c
    ld      e, $00
    add     hl, de
    call    _LABEL_5D42_
    ex      de, hl
    ld      h, (ix+13)
    ld      l, (ix+12)
    ld      c, $00
    add     hl, bc
    ld      bc, (_RAM_C04F_)
    add     hl, bc
    ld      a, h
    ex      de, hl
    jp      _LABEL_5D5F_

_LABEL_6BB6_:
    ld      a, (_RAM_C231_)
    or      a
    ret     nz
    ld      a, (_RAM_C212_)
    ld      b, a
    ld      a, (_RAM_C213_)
    or      b
    ret     nz
    ld      a, (_RAM_C20D_)
    add     a, $FC
    ld      b, a
    add     a, $08
    ld      c, a
    ld      a, (_RAM_C20B_)
    add     a, $FE
    ld      d, a
    add     a, $04
    ld      e, a
    ld      ix, _RAM_DD50_
    exx
    ld      de, $0006
    ld      b, $05
-:
    exx
    call    +
    or      a
    ret     nz
    exx
    add     ix, de
    djnz    -
    ld      a, (_RAM_C208_)
    bit     7, a
    ret     z
    call    _LABEL_69BE_
    ld      ix, _RAM_DD6E_
    exx
    ld      de, $0006
    ld      b, $05
-:
    exx
    call    +
    or      a
    ret     nz
    exx
    add     ix, de
    djnz    -
    ret

+:
    ld      a, (ix+4)
    or      a
    jr      z, _LABEL_6C4F_
    ld      a, (ix+0)
    or      a
    jr      nz, _LABEL_6C4F_
    ld      a, (ix+5)
    add     a, a
    add     a, a
    ld      hl, $6C54
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    add     a, (ix+3)
    cp      c
    jr      nc, _LABEL_6C4F_
    add     a, (hl)
    cp      b
    jr      c, _LABEL_6C4F_
    inc     hl
    ld      a, (hl)
    add     a, (ix+2)
    cp      e
    jr      nc, _LABEL_6C4F_
    inc     hl
    add     a, (hl)
    cp      d
    jr      c, _LABEL_6C4F_
    ld      a, (ix+2)
    ld      (_RAM_C21B_), a
    ld      a, (ix+3)
    ld      (_RAM_C21A_), a
    ld      a, (ix+4)
    ld      (_RAM_C230_), a
    ret

_LABEL_6C4F_:
    xor     a
    ld      (_RAM_C230_), a
    ret

; Data from 6C54 to 6C7B (40 bytes)
.db $F0 $20 $F0 $20 $FE $04 $08 $08 $FD $06 $D0 $60 $FD $06 $E8 $30
.db $FD $06 $FD $06 $E8 $30 $FE $04 $F0 $20 $FE $04 $F0 $20 $FB $0A
.db $FA $0C $FA $0C $E0 $40 $F8 $10

; Data from 6C7C to 6D07 (140 bytes)
_DATA_6C7C_:
.db $FA $0C $E4 $1C $F1 $1E $EE $04 $FC $08 $FC $08 $F4 $18 $E4 $18
.db $F8 $10 $D4 $2C $FC $08 $FA $06 $FA $0C $FA $0C $FC $08 $FC $08
.db $F8 $10 $F8 $10 $F4 $18 $F4 $18 $F0 $1E $D0 $30 $FA $0C $F0 $10
.db $F1 $1E $F6 $04 $FA $0C $F2 $0C $F2 $1C $00 $1E $F9 $0E $EE $04
.db $EA $2C $EE $04 $FE $04 $FE $04 $FC $08 $FC $08 $F2 $1C $F4 $0C
.db $FA $0C $E0 $1E $FA $0C $E0 $20 $EE $24 $E8 $10 $FD $06 $F9 $06
.db $FA $0C $F4 $0C $F2 $1C $F2 $0C $F6 $14 $F4 $0C $F6 $14 $E0 $20
.db $FC $08 $F4 $0A $F6 $14 $EE $04 $FA $0C $E8 $16 $F2 $1C $E8 $16
.db $FE $04 $E2 $1C $FD $06 $FB $02 $F2 $1C $E8 $0A

_LABEL_6D08_:
    ld      a, $09
    ld      (Frame2_Select), a
    ld      ix, _RAM_C240_
    ld      b, $06
-:
    ld      a, (ix+0)
    or      a
    jr      z, +
    push    bc
    call    _LABEL_6721_
    pop     bc
+:
    ld      de, $0020
    add     ix, de
    djnz    -
    ret

_LABEL_6D26_:
    ld      ix, _RAM_C240_
    ld      b, $06
-:
    ld      a, $09
    ld      (Frame2_Select), a
    ld      a, (ix+0)
    or      a
    jr      z, ++
    ld      a, (ix+19)
    or      (ix+18)
    jr      z, +
    push    bc
    call    _LABEL_88F_
    pop     bc
    jr      ++

+:
    push    bc
    ld      a, (ix+0)
    ld      hl, $6D58
    rst     $08    ; call_jump_table
    call    object_update_animation_counters
    pop     bc
++:
    ld      de, $0020
    add     ix, de
    djnz    -
    ret

; Jump Table from 6D5A to 6D71 (12 entries, indexed by unknown)
_DATA_6D5A_:
.dw _LABEL_6D72_ _LABEL_6DC4_ _LABEL_6DEB_ _LABEL_6DF8_ _LABEL_6E0C_ _LABEL_6E0D_ _LABEL_6E53_ _LABEL_6E88_
.dw _LABEL_6EC1_ _LABEL_6F15_ _LABEL_6F3C_ _LABEL_6E66_

; 1st entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6D72_:
    inc     (ix+0)
    ld      hl, (_RAM_C20C_)
    ld      de, $1000
    ld      a, (_RAM_C209_)
    or      a
    jr      nz, +
    sbc     hl, de
    jr      ++

+:
    add     hl, de
++:
    jp      c, _LABEL_88F_
    ld      (ix+12), l
    ld      (ix+13), h
    ld      de, $0C00
    ld      a, (_RAM_C222_)
    bit     6, a
    jr      z, +
    ld      d, $04
+:
    ld      hl, (_RAM_C20A_)
    or      a
    sbc     hl, de
    jp      c, _LABEL_88F_
    ld      (ix+10), l
    ld      (ix+11), h
    ld      hl, $FD80
    ld      a, (_RAM_C209_)
    or      a
    jr      z, +
    ld      h, $02
+:
    ld      (ix+9), a
    ld      (ix+16), l
    ld      (ix+17), h
    ld      hl, $6F43
    jp      _LABEL_2CFE_

; 2nd entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6DC4_:
    ld      a, (ix+26)
    dec     a
    jp      z, _LABEL_88F_
    dec     a
    jr      z, +
    ld      a, (ix+13)
    cp      $09
    ret     c
    ld      bc, $00F8
    call    _LABEL_6B8B_
    cp      $02
    jr      z, +
    cp      $05
    ret     nz
+:
    inc     (ix+0)
    ld      (ix+29), $0C
    jp      _LABEL_30AB_

; 3rd entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6DEB_:
    ld      de, $0080
    call    _LABEL_2D49_
    dec     (ix+29)
    ret     nz
    jp      _LABEL_88F_

; 4th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6DF8_:
    inc     (ix+0)
    ld      hl, $5000
    ld      (_RAM_C24A_), hl
    ld      h, $B8
    ld      (_RAM_C24C_), hl
    ld      hl, $6F57
    jp      _LABEL_2CFE_

; 5th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6E0C_:
    ret

; 6th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6E0D_:
    inc     (ix+0)
    ld      hl, (_RAM_C20A_)
    ld      de, $DC00
    add     hl, de
    ld      (ix+10), l
    ld      (ix+11), h
    ld      hl, (_RAM_C20C_)
    ld      (ix+12), l
    ld      (ix+13), h
    call    +
    ld      (ix+14), l
    ld      (ix+15), h
    call    +
    ld      (ix+16), l
    ld      (ix+17), h
    ld      a, $01
    ld      b, $03
    ld      hl, $6F75
    jp      _LABEL_2D00_

+:
    call    _LABEL_5FF_
    ld      hl, $0300
    rrca
    jr      c, +
    ld      h, $04
+:
    ld      l, a
    rrca
    ret     c
    jp      negate_hl

; 7th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6E53_:
    ld      a, (_RAM_C229_)
    bit     2, a
    ret     z
    call    _LABEL_426C_
    call    c, _LABEL_2D14_
    call    _LABEL_425C_
    jp      c, _LABEL_2D20_
    ret

; 12th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6E66_:
    ld      (ix+0), $09
    call    _LABEL_5FF_
    and     $03
    ld      b, a
    ld      a, (_RAM_C20B_)
    sub     b
    ld      (ix+11), a
    ld      a, (_RAM_C20D_)
    sub     $08
    ld      b, a
    call    _LABEL_5FF_
    and     $0F
    add     a, b
    ld      (ix+13), a
    jr      ++

; 8th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6E88_:
    inc     (ix+0)
    ld      hl, (_RAM_C20A_)
    ld      de, $E800
    add     hl, de
    ld      (_RAM_C2EA_), hl
    ld      a, (_RAM_C209_)
    or      a
    ld      d, $F8
    or      a
    jr      z, +
    ld      d, $08
+:
    ld      hl, (_RAM_C20C_)
    add     hl, de
    ld      (_RAM_C2EC_), hl
++:
    ld      (ix+30), $01
    ld      hl, $6F8D
    call    _LABEL_2CFE_
    ld      hl, $0000
    jr      +

-:
    ld      hl, $0120
+:
    call    _LABEL_2DAE_
    ld      (ix+29), $30
    ret

; 9th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6EC1_:
    ld      bc, $00F8
    call    _LABEL_6B8B_
    ex      af, af'
    ld      b, $06
    ld      a, (StageNumber)
    cp      $01
    jr      z, +
    ld      b, $04
+:
    ex      af, af'
    cp      b
    jp      nz, _LABEL_88F_
    dec     (ix+29)
    call    z, -
    ld      de, $FFE0
    call    _LABEL_2DBF_
    bit     7, h
    jr      z, +
    ld      de, $FF00
    or      a
    sbc     hl, de
    jr      nc, +
    ex      de, hl
    call    _LABEL_2DAE_
+:
    dec     (ix+30)
    ret     nz
    ld      (ix+30), $02
    inc     (ix+31)
    ld      a, (ix+31)
    rrca
    jp      c, _LABEL_2D20_
    call    _LABEL_5FF_
    rlca
    ld      hl, $0080
    jr      nc, +
    ld      hl, $0180
+:
    jp      _LABEL_2DB8_

; 10th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6F15_:
    inc     (ix+0)
    ld      a, $AC
    ld      (Sound_MusicTrigger), a
    call    _LABEL_2DAB_
    call    _LABEL_2DB5_
    ld      hl, _RAM_C20A_
    ld      de, _RAM_C2EA_
    ld      bc, $0004
    ldir
    ld      (ix+29), $07
    ld      a, $01
    ld      b, $04
    ld      hl, $AD26
    jp      _LABEL_2D00_

; 11th entry of Jump Table from 6D5A (indexed by unknown)
_LABEL_6F3C_:
    dec     (ix+29)
    ret     nz
    jp      _LABEL_88F_

; Data from 6F43 to 6F95 (83 bytes)
.db $47 $6F $4F $6F $02 $00 $F0 $F8 $14 $F0 $00 $16 $02 $00 $F0 $F8
.db $18 $F0 $00 $1A $5B $6F $5B $6F $08 $00 $F0 $F0 $02 $F0 $F8 $04
.db $F0 $00 $06 $F0 $08 $08 $00 $F0 $0A $00 $F8 $0C $00 $00 $0E $00
.db $08 $10 $7D $6F $7D $6F $85 $6F $85 $6F $02 $00 $F0 $F8 $3C $F0
.db $00 $3E $02 $00 $F0 $F8 $40 $F0 $00 $42 $91 $6F $91 $6F $01 $00
.db $F8 $FC $FA

; 16th entry of Jump Table from 135 (indexed by unknown)
_LABEL_6F96_:
    ld      a, $10
    ld      (GameMode), a
    call    scorecard_load_tiles
    ld      a, $0A
    ld      (Frame2_Select), a
    ld      hl, _DATA_280A0_
    ld      de, PaletteBuffer
    ld      bc, $0020
    ldir
    ld      hl, _DATA_2AD1E_
    call    decompress_screenmap_to_vram
    call    _LABEL_704A_
    xor     a
    ld      (_RAM_C0C7_), a
    ld      a, $06
    ld      (_RAM_C0C2_), a
    call    disable_sprites
    call    _LABEL_8C3_
    call    _LABEL_1DFD_
    ld      a, $85
    ld      (Sound_MusicTrigger), a
    ld      hl, $0200
    ld      (_RAM_DD92_), hl
    jp      enable_display

; 17th entry of Jump Table from 135 (indexed by unknown)
_LABEL_6FD7_:
    ld      hl, (_RAM_DD92_)
    dec     hl
    ld      (_RAM_DD92_), hl
    ld      a, l
    or      h
    jr      z, +
    ld      a, (InputFlagsChanged)
    and     BTN_1 | BTN_2
    jr      z, ++
+:
    call    +++
++:
    ld      a, $05
    jp      wait_for_c041

+++:
    ld      a, $B5
    ld      (Sound_MusicTrigger), a
    ld      a, $0A
    ld      (Frame2_Select), a
    di
    xor     a
    call    _LABEL_702F_
    ei
    ld      a, $03
    ld      (Alex_Health), a
_LABEL_7006_:
    xor     a
    ld      (_RAM_C212_), a
    ld      (_RAM_DD07_), a
    ld      (_RAM_C04E_), a
    inc     a
    ld      (_RAM_DD4E_), a
    ld      (_RAM_DDF0_), a
    jp      _LABEL_1460_

_LABEL_701A_:
    ld      a, (GameMode)
    cp      $10
    ret     nz
    ld      hl, _RAM_C0C2_
    dec     (hl)
    ret     nz
    ld      (hl), $06
    ld      a, $0A
    ld      (Frame2_Select), a
    call    _LABEL_7042_
_LABEL_702F_:
    ld      bc, $0408
_LABEL_7032_:
    ld      hl, (_RAM_C0CA_)
    or      a
    jr      z, +
    ld      hl, (_RAM_C0CC_)
+:
    ld      de, (_RAM_C0C8_)
    jp      draw_screen_map_element

_LABEL_7042_:
    ld      hl, _RAM_C0C7_
    ld      a, (hl)
    xor     $01
    ld      (hl), a
    ret

_LABEL_704A_:
    ld      a, (StageNumber)
    ld      hl, $A656
    rst     $18    ; get_array_index
    ld      de, $786C
    ld      bc, $0204
    call    draw_screen_map_element
    ld      a, (StageNumber)
    or      a
    jr      z, +
    ld      b, a
-:
    push    bc
    ld      a, b
    dec     a
    call    ++
    xor     a
    call    _LABEL_702F_
    pop     bc
    djnz    -
+:
    ld      a, (StageNumber)
++:
    ld      hl, $7086
    add     a, a
    ld      c, a
    add     a, a
    add     a, c
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      de, _RAM_C0C8_
    ld      bc, $0006
    ldir
    ret

; Data from 7086 to 709D (24 bytes)
.db $84 $79 $06 $A7 $26 $A7 $8C $7B $46 $A7 $66 $A7 $E2 $7B $86 $A7
.db $A6 $A7 $B0 $79 $06 $A8 $26 $A8


; ==============================================================================
;  void scorecard_load_tiles(void)
; ------------------------------------------------------------------------------
;  Loads the tiles required for the title/score/game over screens. Also handles
;  patching some of the mario parody art out of memory.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
scorecard_load_tiles:                                               ; $709E
    ; load the "complete" kanji tiles
    ld      a, :GFX_Finish_Kanji
    ld      (Frame2_Select), a
    ld      hl, GFX_Finish_Kanji
    ld      de, $4000
    call    decode_tiles_to_vram

    ld      a, :GFX_ScoreCard_Map_Tiles
    ld      (Frame2_Select), a
    ld      hl, GFX_ScoreCard_Map_Tiles 
    ld      de, $4240
    call    decode_tiles_to_vram

    ld      hl, _DATA_2A32C_
    ld      de, $75E0
    call    decode_tiles_to_vram

    ld      hl, GFX_ScoreCard_Stage2_Icon
    ld      de, $7160
    call    decode_tiles_to_vram

    ; replace shinobi kid with alex
    ld      hl, GFX_ScoreCard_Alex
    ld      de, $76A0
    call    decode_tiles_to_vram

    ; replace the mario parody with something more lawyer-friendly
    ld      a, :GFX_ScoreCard_HideMario1
    ld      (Frame2_Select), a
    ld      hl, GFX_ScoreCard_HideMario1
    ld      de, $54A0
    call    decode_tiles_to_vram
    ld      hl, GFX_ScoreCard_HideMario2
    ld      de, $5600
    jp      decode_tiles_to_vram


; ==============================================================================
;  void mode_handler_load_game_over(void)
; ------------------------------------------------------------------------------
;  Game mode handler to load the Game Over screen.
; ------------------------------------------------------------------------------
;
; ------------------------------------------------------------------------------
; 18th entry of Jump Table from 135 (indexed by unknown)
mode_handler_load_game_over:                                        ; $70EC
    ld      a, Mode_GameOver
    ld      (GameMode), a
    call    scorecard_load_tiles

    ; load the palette data into the working copy
    ld      a, :PAL_ScoreCard_Stage1
    ld      (Frame2_Select), a
    ld      hl, PAL_ScoreCard_Stage1
    ld      de, PaletteBuffer
    ld      bc, 32
    ldir

    ; load the game over screen map
    ld      hl, SCR_GameOver
    call    decompress_screenmap_to_vram
    call    disable_sprites
    call    _LABEL_8C3_

    ld      hl, CurrentScore
    ld      de, $7C6A
    call    font_draw_packed_bcd
    ld      hl, HiScore
    ld      de, $7BEA
    call    font_draw_packed_bcd

    ld      hl, _RAM_C0B6_
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      hl, ScreenMapBuffer
    ld      de, $7A54
    ld      bc, $051A
    call    draw_screen_map_element
    ld      a, $8C
    jr      ++

+:
    ld      de, $7AA8
    call    font_draw_bcd
    ld      a, $8A
++:
    ld      (Sound_MusicTrigger), a
    ld      hl, $0200
    ld      (_RAM_DD92_), hl
    call    _LABEL_7155_
    inc     h
    inc     l
    ld      (_RAM_C0C0_), hl
    jp      enable_display

_LABEL_7155_:
    ld      hl, $0000
    ld      (_RAM_C0CE_), hl
    ld      (_RAM_C0D0_), hl
    ret

; 19th entry of Jump Table from 135 (indexed by unknown)
_LABEL_715F_:
    ld      a, (_RAM_C0B6_)
    or      a
    jr      nz, +
    ld      hl, (_RAM_DD92_)
    dec     hl
    ld      (_RAM_DD92_), hl
    ld      a, l
    or      h
    jr      z, ++
+:
    ld      a, (InputFlagsChanged)
    and     BTN_UP | BTN_DOWN
    call    nz, +
    ld      a, (InputFlagsChanged)
    and     BTN_1 | BTN_2
    call    nz, ++
    ld      a, $05
    jp      wait_for_c041

+:
    ld      a, (_RAM_C0B6_)
    or      a
    ret     z
    ld      a, (InputFlags)
    bit     BTN_UP_BIT, a
    ld      a, $01
    jr      nz, +
    inc     a
+:
    ld      h, a
    ld      l, a
    ld      (_RAM_C0C0_), hl
    ret

++:
    ld      a, $B5
    ld      (Sound_MusicTrigger), a
    ld      a, (_RAM_C0B6_)
    or      a
    jr      z, _LABEL_71B3_
    ld      a, (_RAM_C0C1_)
    dec     a
    jr      nz, _LABEL_71B3_
    ld      hl, _RAM_C0B6_
    dec     (hl)
    ld      a, $0F
    jr      +

_LABEL_71B3_:
    xor     a
    ld      (StageNumber), a
    ld      a, $00
+:
    ld      (GameMode), a
    xor     a
    ld      (_RAM_DD07_), a
    ld      (_RAM_C04E_), a
    ld      (_RAM_DD0B_), a
    ld      (_RAM_DD37_), a
    ld      (_RAM_DD00_), a
    ld      (_RAM_DD2E_), a
    ld      (_RAM_DD94_), a
    ld      (_RAM_DDFF_), a
    call    _LABEL_1DED_
    jp      reset_screen

_LABEL_71DB_:
    ld      a, (GameMode)
    cp      $12
    ret     nz
    ld      a, (_RAM_C0B6_)
    or      a
    ret     z
    ld      hl, _RAM_C0C0_
    ld      a, (hl)
    or      a
    ret     z
    ld      (hl), $00
    ld      hl, _DATA_2A6FA_
    dec     a
    jr      z, +
    ld      hl, $A700
+:
    ld      a, $0A
    ld      (Frame2_Select), a
    ld      de, $7A96
    ld      bc, $0302
    jp      draw_screen_map_element

_LABEL_7205_:
    ld      b, $00
    ld      hl, (_RAM_C20B_)
    ld      de, $FFF0
    add     hl, de
    ld      h, $01
    ld      e, (ix+11)
    ld      d, (ix+18)
    inc     d
    or      a
    sbc     hl, de
    jr      c, +
    jr      nz, ++
    inc     l
    jr      ++

+:
    ex      de, hl
    call    negate_de
    ex      de, hl
    ld      b, $01
++:
    ld      c, l
    ld      hl, (_RAM_C20D_)
    ld      h, $01
    ld      e, (ix+13)
    ld      d, (ix+19)
    inc     d
    or      a
    sbc     hl, de
    jr      c, +
    jr      nz, ++
    inc     l
    jr      ++

+:
    ex      de, hl
    call    negate_de
    ex      de, hl
    set     1, b
++:
    ld      h, c
    ld      e, l
    ld      a, e
    cp      h
    jr      c, +++
    jr      nz, +
    ld      e, $1E
    jr      ++

+:
    ld      l, $00
    call    _LABEL_72BB_
    and     $F0
    rrca
    rrca
    rrca
    ld      e, a
++:
    ld      d, $00
    ld      hl, $72F5
    add     hl, de
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    ex      de, hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    bit     1, b
    call    nz, negate_de
    ld      (ix+16), e
    ld      (ix+17), d
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    bit     0, b
    call    nz, negate_de
    ld      (ix+14), e
    ld      (ix+15), d
    ret

+++:
    ld      l, h
    ld      h, e
    ld      e, l
    ld      l, $00
    call    _LABEL_72BB_
    and     $F0
    rrca
    rrca
    rrca
    ld      e, a
    ld      d, $00
    ld      hl, $72F5
    add     hl, de
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    ex      de, hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    bit     0, b
    call    nz, negate_de
    ld      (ix+14), e
    ld      (ix+15), d
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    bit     1, b
    call    nz, negate_de
    ld      (ix+16), e
    ld      (ix+17), d
    ret

_LABEL_72BB_:
    push    bc
    ld      b, $08
    or      a
-:
    adc     hl, hl
    ld      a, h
    jr      c, +
    cp      e
    jr      c, ++
+:
    sub     e
    ld      h, a
    or      a
++:
    djnz    -
    ld      a, l
    rla
    cpl
    pop     bc
    ret

; Data from 72D1 to 7314 (68 bytes)
.db $00 $02 $00 $00 $FC $01 $32 $00 $F6 $01 $62 $00 $E8 $01 $94 $00
.db $D8 $01 $C2 $00 $C2 $01 $F0 $00 $A8 $01 $1C $01 $8A $01 $44 $01
.db $6A $01 $6A $01 $D1 $72 $D5 $72 $D5 $72 $D9 $72 $D9 $72 $DD $72
.db $DD $72 $E1 $72 $E1 $72 $E5 $72 $E5 $72 $E9 $72 $E9 $72 $ED $72
.db $ED $72 $F1 $72

; 20th entry of Jump Table from 135 (indexed by unknown)
_LABEL_7315_:
    call    reset_screen
    call    _LABEL_923_
    ld      a, $14
    ld      (GameMode), a
    ld      a, $06
    ld      (Frame2_Select), a
    ld      hl, _DATA_19E00_
    ld      de, _RAM_C500_
    ld      bc, $0020
    ldir
    ld      de, $4000
    call    decode_tiles_to_vram
    ld      hl, _DATA_1B69E_
    ld      de, $5AE0
    call    decode_tiles_to_vram
    ld      hl, _RAM_C511_
    ld      de, _RAM_C031_
    ld      bc, $000F
    ldir
    xor     a
    ld      (_RAM_C0C7_), a
    ld      a, $BD
    ld      (_RAM_C340_), a
    ld      a, $01
    ld      (_RAM_C353_), a
    ld      a, $A0
    ld      (_RAM_C34B_), a
    ld      a, $10
    ld      (_RAM_C34D_), a
    ld      a, $3E
    ld      (_RAM_C300_), a
    ld      a, $A0
    ld      (_RAM_C30B_), a
    ld      a, $40
    ld      (_RAM_C30D_), a
    ld      a, $3F
    ld      (_RAM_C320_), a
    ld      a, $90
    ld      (_RAM_C32B_), a
    ld      a, $48
    ld      (_RAM_C32D_), a
    ld      a, $8D
    ld      (Sound_MusicTrigger), a
    jp      enable_display

; 21st entry of Jump Table from 135 (indexed by unknown)
_LABEL_7388_:
    ld      a, (_RAM_C0C7_)
    sub     $03
    jr      c, +
    ld      hl, $73A4
    rst     $08    ; call_jump_table
+:
    call    _LABEL_2CBB_
    call    use_default_working_sat
    call    _LABEL_66CB_
    call    disable_sprites_current_working_sat
    ld      a, $07
    jp      wait_for_c041

; Jump Table from 73A4 to 73BD (13 entries, indexed by unknown)
_DATA_73A4_:
.dw _LABEL_73BE_ _LABEL_73D9_ _LABEL_73F7_ _LABEL_7407_ _LABEL_7415_ _LABEL_7415_ _LABEL_7416_ _LABEL_7440_
.dw _LABEL_7453_ _LABEL_747F_ _LABEL_74BC_ _LABEL_74DE_ _LABEL_74FB_

; 1st entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_73BE_:
    ld      a, $04
    ld      (_RAM_C0C7_), a
    ld      (_RAM_C0BF_), a
    ld      a, $3F
    ld      (_RAM_C02F_), a
    ld      hl, $75A1
    ld      de, $78C6
    ld      bc, $1515
    ld      a, $03
    jp      _LABEL_754A_

; 2nd entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_73D9_:
    ld      hl, _RAM_C0BF_
    ld      a, (hl)
    or      a
    ret     nz
    ld      (hl), $01
    ld      a, $05
    ld      (_RAM_C0C7_), a
    ld      (_RAM_C0BF_), a
    ld      hl, $761F
    ld      de, $7B06
    ld      bc, $1515
    ld      a, $02
    jp      _LABEL_754A_

; 3rd entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_73F7_:
    ld      a, (_RAM_C0BF_)
    or      a
    ret     nz
    ld      a, $80
    ld      (_RAM_C0C2_), a
    ld      a, $06
    ld      (_RAM_C0C7_), a
    ret

; 4th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_7407_:
    ld      hl, _RAM_C0C2_
    dec     (hl)
    ret     nz
    ld      a, $07
    ld      (_RAM_C0C7_), a
    xor     a
    ld      (_RAM_C02F_), a
; 5th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_7415_:
    ret

; 7th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_7416_:
    ld      a, $0A
    ld      (_RAM_C0C7_), a
    ld      a, $80
    ld      (_RAM_C0C2_), a
    di
    ld      a, $06
    ld      (Frame2_Select), a
    ld      hl, _DATA_1B4DA_
    ld      de, ScreenMapBuffer
    ld      c, $02
    call    decompress_interleaved_rle
    call    copy_screenmap_to_vram
    ei
    ld      a, $8F
    ld      (Sound_MusicTrigger), a
    call    enable_display
    jp      _LABEL_683_

; 8th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_7440_:
    ld      hl, _RAM_C0C2_
    dec     (hl)
    ret     nz
    ld      (hl), $01
    ld      hl, $0000
    ld      (_RAM_C0C0_), hl
    ld      a, $0B
    ld      (_RAM_C0C7_), a
    ret

; 9th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_7453_:
    ld      hl, _RAM_C0C2_
    dec     (hl)
    ret     nz
    ld      a, (_RAM_C0C0_)
    ld      c, a
    ld      b, $00
    ld      l, a
    ld      h, b
    add     hl, hl
    add     hl, hl
    add     hl, hl
    add     hl, bc
    ex      de, hl
    ld      hl, _DATA_769D_
    add     hl, de
    ld      de, _RAM_C0B7_
    ld      bc, $0008
    ldir
    ld      a, (hl)
    ld      (_RAM_C0C2_), a
    xor     a
    ld      (_RAM_C0C1_), a
    ld      a, $0C
    ld      (_RAM_C0C7_), a
    ret

; 10th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_747F_:
    ld      hl, _RAM_C0BD_
    dec     (hl)
    jr      z, ++
    ld      a, $01
    ld      (_RAM_C0BF_), a
    ld      hl, _RAM_C0C1_
    ld      a, (hl)
    inc     (hl)
    add     a, a
    ld      e, a
    ld      d, $00
    ld      hl, _RAM_C0BE_
    bit     7, (hl)
    ld      hl, (_RAM_C0B9_)
    jr      nz, +
    add     hl, de
    ld      (_RAM_C0FC_), hl
    ret

+:
    or      a
    sbc     hl, de
    ld      (_RAM_C0FC_), hl
    ret

++:
    ld      b, $0B
    ld      hl, _RAM_C0C0_
    ld      a, (hl)
    inc     a
    ld      (hl), a
    cp      $33
    jr      c, +
    ld      b, $0D
+:
    ld      a, b
    ld      (_RAM_C0C7_), a
    ret

; 11th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_74BC_:
    xor     a
    ld      (_RAM_C0C0_), a
    ld      a, $40
    ld      (_RAM_C0C2_), a
    ld      a, $0E
    ld      (_RAM_C0C7_), a
    di
    ld      a, $06
    ld      (Frame2_Select), a
    ld      hl, _DATA_1B61A_
    ld      de, $7996
    ld      bc, $0616
    call    draw_screen_map_element
    ei
    ret

; 12th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_74DE_:
    ld      hl, _RAM_C0C2_
    dec     (hl)
    ret     nz
    ld      (hl), $20
    ld      hl, _RAM_C0C0_
    ld      a, (hl)
    inc     (hl)
    cp      $04
    jr      nc, +
    ld      hl, $7514
    rst     $18    ; get_array_index
    ld      (hl), $23
    ret

+:
    ld      a, $0F
    ld      (_RAM_C0C7_), a
    ret

; 13th entry of Jump Table from 73A4 (indexed by unknown)
_LABEL_74FB_:
    ld      hl, InputFlagsChanged
    ld      a, (hl)
    and     BTN_1 | BTN_2
    ret     z
    ld      (hl), $00
    dec     hl
    ld      (hl), $00
    ld      a, $B5
    ld      (Sound_MusicTrigger), a
    ld      b, $28
    call    _LABEL_720_
    jp      _LABEL_71B3_

; Data from 7514 to 751B (8 bytes)
.db $29 $C0 $2A $C0 $2B $C0 $2E $C0

_LABEL_751C_:
    ld      a, (_RAM_C0C7_)
    cp      $0C
    jr      nz, _LABEL_7559_
    ld      hl, _RAM_C0BF_
    ld      a, (hl)
    or      a
    ret     z
    ld      (hl), $00
    ld      hl, _DATA_797A_
    ld      de, (_RAM_C0B9_)
    ld      a, e
    and     $C0
    ld      e, a
    ld      bc, $0140
    call    draw_screen_map_element
    ld      hl, (_RAM_C0B7_)
    ld      de, (_RAM_C0FC_)
    ld      bc, (_RAM_C0BB_)
    jp      draw_screen_map_element

_LABEL_754A_:
    ld      (_RAM_C0B7_), hl
    ld      (_RAM_C0B9_), de
    ld      (_RAM_C0BB_), bc
    ld      (_RAM_C0BD_), a
    ret

_LABEL_7559_:
    ld      a, (_RAM_C0BF_)
    or      a
    ret     z
    ld      hl, (_RAM_C0B9_)
    ld      bc, (_RAM_C0BB_)
    dec     c
    bit     7, c
    jr      z, +
    ld      a, (_RAM_C0BD_)
    dec     a
    jr      z, ++
    ld      (_RAM_C0BD_), a
    ld      c, b
    dec     c
    ld      e, b
    rlc     e
    ld      d, $00
    or      a
    sbc     hl, de
    ld      de, $0040
    add     hl, de
+:
    ld      d, h
    ld      e, l
    inc     hl
    inc     hl
    ld      (_RAM_C0B9_), hl
    ld      (_RAM_C0BB_), bc
    ld      hl, (_RAM_C0B7_)
    rst     $28    ; vdp_write_control
    ld      c, $BE
    outi
    nop
    outi
    nop
    ld      (_RAM_C0B7_), hl
    ret

++:
    xor     a
    ld      (_RAM_C0BF_), a
    ret

; Data from 75A1 to 769C (252 bytes)
.db $D9 $00 $E5 $00 $E4 $00 $DD $00 $E8 $00 $D7 $00 $EA $00 $EB $00
.db $E2 $00 $D7 $00 $EA $00 $DF $00 $E5 $00 $E4 $00 $E9 $00 $69 $00
.db $EA $00 $E5 $00 $69 $00 $69 $00 $69 $00 $EF $00 $E5 $00 $EB $00
.db $FE $00 $D7 $00 $E2 $00 $DB $00 $EE $00 $FE $00 $D7 $00 $E2 $00
.db $E2 $00 $69 $00 $DF $00 $E9 $00 $69 $00 $DA $00 $E5 $00 $E4 $00
.db $DB $00 $F1 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $EA $00
.db $DE $00 $D7 $00 $E4 $00 $E1 $00 $69 $00 $EF $00 $E5 $00 $EB $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $DC $00 $E5 $00 $E8 $00 $69 $00
.db $EF $00 $E5 $00 $EB $00 $E8 $00 $69 $00 $DD $00 $EB $00 $DF $00
.db $DA $00 $D7 $00 $E4 $00 $D9 $00 $DB $00 $F1 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00

; Data from 769D to 7979 (733 bytes)
_DATA_769D_:
.db $68 $78 $40 $79 $0A $01 $0F $00 $30 $72 $78 $C0 $79 $16 $01 $0C
.db $00 $30 $F0 $78 $70 $7A $10 $01 $0E $FF $20 $00 $79 $C0 $7A $0E
.db $01 $0F $00 $D0 $F0 $78 $58 $7A $10 $01 $0E $FF $01 $7A $79 $40
.db $7A $3E $01 $02 $00 $20 $00 $79 $DA $7A $0E $01 $0E $00 $01 $7A
.db $79 $C0 $7A $3E $01 $02 $00 $20 $72 $78 $D4 $79 $16 $01 $0D $00
.db $01 $7A $79 $C0 $79 $3E $01 $02 $00 $30 $88 $78 $C0 $79 $16 $01
.db $0D $00 $30 $0E $79 $74 $7A $0C $01 $0F $FF $20 $1A $79 $C0 $7A
.db $08 $01 $10 $00 $D0 $0E $79 $5A $7A $0C $01 $0F $FF $01 $7A $79
.db $40 $7A $3E $01 $02 $00 $20 $1A $79 $DC $7A $08 $01 $10 $00 $01
.db $7A $79 $C0 $7A $3E $01 $02 $00 $20 $88 $78 $D6 $79 $16 $01 $0C
.db $00 $01 $7A $79 $C0 $79 $3E $01 $02 $00 $30 $9E $78 $C0 $79 $1C
.db $01 $0C $00 $30 $22 $79 $76 $7A $0A $01 $0F $FF $20 $2C $79 $C0
.db $7A $06 $01 $10 $00 $20 $32 $79 $6C $7B $14 $01 $0C $FF $D0 $22
.db $79 $5C $7A $0A $01 $10 $FF $01 $7A $79 $40 $7A $3E $01 $02 $00
.db $20 $2C $79 $DC $7A $06 $01 $11 $00 $01 $7A $79 $C0 $7A $3E $01
.db $02 $00 $20 $32 $79 $58 $7B $08 $01 $0E $FF $01 $7A $79 $40 $7B
.db $3E $01 $02 $00 $20 $9E $78 $D4 $79 $1C $01 $0A $00 $01 $7A $79
.db $C0 $79 $3E $01 $02 $00 $30 $BA $78 $C0 $79 $1A $01 $0C $00 $30
.db $46 $79 $7A $7A $06 $01 $11 $FF $D0 $46 $79 $5C $7A $06 $01 $10
.db $FF $01 $7A $79 $40 $7A $3E $01 $02 $00 $20 $BA $78 $D4 $79 $1A
.db $01 $0B $00 $01 $7A $79 $C0 $79 $3E $01 $02 $00 $30 $D4 $78 $C0
.db $79 $1C $01 $0C $00 $30 $4C $79 $78 $7A $08 $01 $10 $FF $20 $5E
.db $79 $C0 $7A $1C $01 $0C $00 $20 $54 $79 $76 $7B $0A $01 $0F $FF
.db $D0 $4C $79 $5C $7A $08 $01 $10 $FF $01 $7A $79 $40 $7A $3E $01
.db $02 $00 $20 $5E $79 $D4 $7A $1C $01 $0A $00 $01 $7A $79 $C0 $7A
.db $3E $01 $02 $00 $20 $54 $79 $5C $7B $0A $01 $10 $FF $01 $7A $79
.db $40 $7B $3E $01 $02 $00 $20 $D4 $78 $D4 $79 $1C $01 $0A $00 $01
.db $7A $79 $C0 $79 $3E $01 $02 $00 $30 $68 $78 $5A $79 $0A $01 $10
.db $00 $01 $7A $79 $40 $79 $3E $01 $02 $00 $30 $E9 $00 $EA $00 $D7
.db $00 $DC $00 $DC $00 $DD $00 $D7 $00 $E3 $00 $DB $00 $69 $00 $DA
.db $00 $DB $00 $E9 $00 $DF $00 $DD $00 $E4 $00 $E6 $00 $E8 $00 $E5
.db $00 $DD $00 $E8 $00 $D7 $00 $E3 $00 $E3 $00 $DF $00 $E4 $00 $DD
.db $00 $DD $00 $E8 $00 $D7 $00 $E6 $00 $DE $00 $DF $00 $D9 $00 $69
.db $00 $DA $00 $DB $00 $E9 $00 $DF $00 $DD $00 $E4 $00 $E3 $00 $EB
.db $00 $E9 $00 $DF $00 $D9 $00 $69 $00 $D9 $00 $E5 $00 $E3 $00 $E6
.db $00 $E5 $00 $E9 $00 $DB $00 $E9 $00 $E6 $00 $DB $00 $D9 $00 $DF
.db $00 $D7 $00 $E2 $00 $69 $00 $EA $00 $DE $00 $D7 $00 $E4 $00 $E1
.db $00 $E9 $00 $EF $00 $E5 $00 $E9 $00 $DE $00 $DF $00 $E5 $00 $F1
.db $00 $EF $00 $D9 $00 $D7 $00 $E6 $00 $F2 $00 $DB $00 $E2 $00 $DC
.db $00 $EA $00 $D7 $00 $E1 $00 $EB $00 $D8 $00 $E5 $00 $E1 $00 $EB
.db $00 $E3 $00 $D7 $00 $EF $00 $E5 $00 $EB $00 $E1 $00 $E5 $00 $DD
.db $00 $DB $00 $E4 $00 $E9 $00 $EA $00 $E8 $00 $DB $00 $E9 $00 $EA
.db $00 $DB $00 $E2 $00 $DB $00 $E9 $00 $EE $00 $E5 $00 $E8 $00 $E6
.db $00 $D7 $00 $EA $00 $D7 $00 $E5 $00 $E1 $00 $D7 $00 $E8 $00 $EB
.db $00 $EA $00 $DB $00 $E8 $00 $DF $00 $EF $00 $D7 $00 $E1 $00 $DF
.db $00 $F1 $00 $DC $00 $D7 $00 $E4 $00 $E1 $00 $E9 $00

; Data from 797A to 79B9 (64 bytes)
_DATA_797A_:
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00
.db $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00 $69 $00

; 62nd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_79BA_:
    ld      a, (ix+28)
    ld      hl, $79C3
    jp      call_jump_table

; Jump Table from 79C3 to 79D0 (7 entries, indexed by unknown)
_DATA_79C3_:
.dw _LABEL_79D1_ _LABEL_79DA_ _LABEL_79F0_ _LABEL_7A0A_ _LABEL_7A17_ _LABEL_7A34_ _LABEL_7A40_

; 1st entry of Jump Table from 79C3 (indexed by unknown)
_LABEL_79D1_:
    inc     (ix+28)
    ld      hl, $7BA8
    jp      _LABEL_2CFE_

; 2nd entry of Jump Table from 79C3 (indexed by unknown)
_LABEL_79DA_:
    ld      a, (_RAM_C0C7_)
    dec     a
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $2F
    ld      a, $01
    ld      b, $03
    ld      hl, $7BA8
    jp      _LABEL_2D00_

; 3rd entry of Jump Table from 79C3 (indexed by unknown)
_LABEL_79F0_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $17
    ld      a, $02
    ld      (_RAM_C0C7_), a
    ld      a, $02
    ld      b, $08
    ld      hl, $7BAC
    jp      _LABEL_2D00_

; 4th entry of Jump Table from 79C3 (indexed by unknown)
_LABEL_7A0A_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      hl, $7BB8
    jp      _LABEL_2CFE_

; 5th entry of Jump Table from 79C3 (indexed by unknown)
_LABEL_7A17_:
    ld      a, (_RAM_C0C7_)
    cp      $08
    ret     nz
    inc     (ix+28)
    ld      hl, $8000
    ld      (_RAM_C30C_), hl
    ld      (ix+29), $30
    ld      a, $05
    ld      b, $10
    ld      hl, $7B90
    jp      _LABEL_2D00_

; 6th entry of Jump Table from 79C3 (indexed by unknown)
_LABEL_7A34_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      a, $09
    ld      (_RAM_C0C7_), a
; 7th entry of Jump Table from 79C3 (indexed by unknown)
_LABEL_7A40_:
    ret

; 61st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_7A41_:
    ld      a, (ix+28)
    ld      hl, $7A4A
    jp      call_jump_table

; Jump Table from 7A4A to 7A51 (4 entries, indexed by unknown)
_DATA_7A4A_:
.dw _LABEL_7A52_ _LABEL_7A65_ _LABEL_7A84_ _LABEL_7A90_

; 1st entry of Jump Table from 7A4A (indexed by unknown)
_LABEL_7A52_:
    inc     (ix+28)
    ld      hl, $FF40
    call    _LABEL_2DB8_
    ld      a, $01
    ld      b, $06
    ld      hl, $7B88
    jp      _LABEL_2D00_

; 2nd entry of Jump Table from 7A4A (indexed by unknown)
_LABEL_7A65_:
    ld      a, (ix+19)
    or      a
    ret     nz
    ld      a, (ix+13)
    cp      $A8
    ret     nc
    inc     (ix+28)
    call    _LABEL_2DB5_
    ld      (ix+29), $28
    ld      a, $09
    ld      b, $06
    ld      hl, $7B60
    jp      _LABEL_2D00_

; 3rd entry of Jump Table from 7A4A (indexed by unknown)
_LABEL_7A84_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      a, $01
    ld      (_RAM_C0C7_), a
; 4th entry of Jump Table from 7A4A (indexed by unknown)
_LABEL_7A90_:
    ld      a, (_RAM_C0C7_)
    cp      $08
    ret     nz
    jp      _LABEL_88F_

; 63rd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_7A99_:
    ld      a, (ix+28)
    ld      hl, $7AA2
    jp      call_jump_table

; Jump Table from 7AA2 to 7AB1 (8 entries, indexed by unknown)
_DATA_7AA2_:
.dw _LABEL_7AB2_ _LABEL_7AC3_ _LABEL_7AE0_ _LABEL_7AED_ _LABEL_7B1C_ _LABEL_7B2D_ _LABEL_7B3A_ _LABEL_7B51_

; 1st entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7AB2_:
    inc     (ix+28)
    set     3, (ix+1)
    set     2, (ix+1)
    ld      hl, $7BC4
    jp      _LABEL_2CFE_

; 2nd entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7AC3_:
    ld      a, (_RAM_C0C7_)
    cp      $02
    ret     nz
    inc     (ix+28)
    res     3, (ix+1)
    ld      (ix+29), $18
    ld      hl, $FFC0
    call    _LABEL_2DAE_
    ld      hl, $0110
    jp      _LABEL_2DB8_

; 3rd entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7AE0_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      hl, $7BC8
    jp      _LABEL_2CFE_

; 4th entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7AED_:
    ld      de, $FFF8
    call    _LABEL_2DBF_
    ld      de, $0100
    add     hl, de
    jr      c, +
    ld      hl, $FF00
    call    _LABEL_2DAE_
+:
    ld      a, (_RAM_C32B_)
    cp      $28
    ret     nc
    inc     (ix+28)
    call    _LABEL_2DAB_
    call    _LABEL_2DB5_
    ld      (ix+29), $17
    ld      a, $01
    ld      b, $0C
    ld      hl, $7BBC
    jp      _LABEL_2D00_

; 5th entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7B1C_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $18
    ld      hl, $7BC0
    jp      _LABEL_2CFE_

; 6th entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7B2D_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      a, $03
    ld      (_RAM_C0C7_), a
    ret

; 7th entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7B3A_:
    ld      a, (_RAM_C0C7_)
    cp      $07
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $2F
    ld      a, $03
    ld      b, $0C
    ld      hl, $7BCC
    jp      _LABEL_2D00_

; 8th entry of Jump Table from 7AA2 (indexed by unknown)
_LABEL_7B51_:
    dec     (ix+29)
    ret     nz
    ld      a, $08
    ld      (_RAM_C0C7_), a
    call    _LABEL_88F_
    jp      _LABEL_6A5_

; Data from 7B60 to 7FEF (1168 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_7b60.inc"

.BANK 1 SLOT 1
.ORG $0000

; Data from 7FF0 to 7FFF (16 bytes)
.db $54 $4D $52 $20 $53 $45 $47 $41 $FF $FF $07 $B8 $44 $70 $00 $40

.BANK 2
.ORG $0000

; Data from 8000 to 83F0 (1009 bytes)
.db $FF $01 $80 $C0 $01 $0B $80 $00 $00 $00 $02 $F0 $01 $01 $FA $5F
.db $A5 $02 $BD $01 $F4 $00 $F2 $02 $80 $A0 $01 $2A $80 $D7 $02 $03
.db $01 $80 $C0 $01 $35 $80 $DA $02 $03 $01 $C4 $04 $C9 $04 $C4 $04
.db $C9 $04 $CB $10 $F2 $80 $06 $C4 $04 $C9 $04 $C4 $04 $C9 $04 $CB
.db $0A $F2 $02 $88 $C0 $01 $55 $80 $F4 $00 $05 $0F $88 $E0 $01 $55
.db $80 $F4 $00 $05 $01 $F3 $07 $F0 $02 $02 $FE $1F $00 $20 $05 $00
.db $2D $03 $F2 $02 $80 $C0 $02 $76 $80 $00 $00 $02 $02 $80 $E0 $02
.db $76 $80 $00 $00 $02 $02 $EB $FD $B6 $09 $F2 $01 $80 $E0 $01 $85
.db $80 $0E $03 $05 $01 $F3 $05 $BD $08 $F2 $02 $88 $C0 $01 $9D $80
.db $00 $00 $08 $0F $88 $E0 $01 $9D $80 $00 $00 $08 $01 $F3 $07 $00
.db $98 $04 $00 $88 $08 $F2 $01 $80 $E0 $02 $B0 $80 $00 $00 $02 $00
.db $F3 $06 $B6 $02 $F3 $05 $B6 $02 $F3 $06 $B6 $02 $F2 $02 $80 $A0
.db $01 $D0 $80 $ED $02 $0A $02 $80 $C0 $01 $E1 $80 $F4 $03 $0A $02
.db $A5 $04 $A9 $04 $AC $04 $B0 $04 $B1 $04 $B0 $04 $AC $04 $A9 $07
.db $F2 $80 $02 $A5 $04 $A9 $04 $AC $04 $B0 $04 $B1 $04 $B0 $04 $AC
.db $04 $A9 $05 $F2 $02 $80 $A0 $01 $07 $81 $D0 $00 $02 $01 $80 $C0
.db $01 $07 $81 $D7 $00 $02 $01 $C4 $04 $C9 $04 $C4 $04 $C9 $04 $CB
.db $10 $F2 $01 $80 $C0 $01 $1C $81 $00 $00 $00 $01 $F0 $05 $01 $F9
.db $2A $A5 $12 $F2 $01 $80 $C0 $01 $2E $81 $00 $00 $02 $01 $B1 $02
.db $80 $03 $B5 $02 $80 $03 $B5 $02 $80 $03 $B5 $02 $F2 $02 $88 $A0
.db $01 $50 $81 $F5 $05 $0A $01 $80 $C0 $01 $5B $81 $F5 $05 $0A $01
.db $00 $B1 $03 $FB $F8 $F7 $00 $0C $50 $81 $F2 $80 $01 $FD $01 $00
.db $B1 $03 $FB $01 $F7 $00 $0C $5F $81 $FD $00 $F2 $02 $88 $C0 $01
.db $7F $81 $00 $00 $00 $0F $88 $E0 $01 $7F $81 $00 $00 $00 $01 $F3
.db $07 $00 $A5 $02 $00 $E8 $02 $01 $60 $06 $00 $F3 $23 $F2 $01 $80
.db $A0 $01 $98 $81 $00 $00 $00 $01 $F0 $02 $01 $F8 $2F $A5 $06 $F2
.db $02 $88 $C0 $01 $B3 $81 $2F $04 $04 $0F $88 $E0 $01 $B3 $81 $00
.db $00 $04 $01 $F3 $07 $00 $80 $02 $00 $60 $02 $00 $40 $02 $F2 $02
.db $88 $C0 $01 $D2 $81 $F7 $00 $06 $0F $88 $E0 $01 $D2 $81 $F2 $00
.db $06 $01 $F3 $07 $00 $20 $01 $00 $15 $06 $F2 $02 $88 $C0 $01 $EE
.db $81 $F7 $00 $06 $0F $88 $E0 $01 $EE $81 $F7 $00 $06 $01 $F3 $07
.db $00 $20 $05 $00 $2D $03 $F2 $02 $80 $A0 $01 $0A $82 $00 $01 $02
.db $01 $80 $C0 $01 $0A $82 $F9 $00 $04 $01 $BD $03 $C4 $08 $F2 $01
.db $80 $A0 $01 $19 $82 $00 $00 $04 $01 $F0 $04 $02 $02 $0E $99 $04
.db $A0 $04 $99 $04 $A0 $04 $F2 $01 $80 $A0 $01 $31 $82 $00 $00 $00
.db $01 $F0 $02 $01 $03 $4F $A5 $21 $F2 $02 $88 $C0 $01 $4C $82 $00
.db $00 $00 $0F $88 $E0 $01 $4C $82 $00 $00 $00 $03 $F3 $07 $F0 $02
.db $01 $03 $1F $00 $04 $08 $00 $40 $06 $F2 $02 $88 $C0 $01 $6D $82
.db $00 $00 $08 $0F $88 $E0 $01 $6D $82 $00 $00 $03 $01 $F3 $07 $00
.db $98 $04 $00 $88 $08 $F2 $02 $88 $C0 $01 $89 $82 $00 $00 $00 $0F
.db $88 $E0 $01 $89 $82 $00 $00 $00 $00 $F3 $07 $00 $E8 $02 $01 $60
.db $04 $F5 $02 $00 $3F $05 $00 $3F $03 $00 $3F $04 $F5 $06 $00 $3F
.db $22 $F2 $01 $88 $C0 $01 $AC $82 $40 $00 $00 $01 $02 $01 $02 $03
.db $9E $05 $F2 $02 $88 $C0 $01 $C6 $82 $00 $00 $00 $0F $88 $E0 $01
.db $C6 $82 $00 $01 $00 $01 $F3 $07 $00 $98 $02 $00 $88 $40 $F2 $02
.db $88 $C0 $01 $E2 $82 $00 $00 $00 $06 $88 $E0 $01 $E2 $82 $00 $00
.db $06 $01 $F3 $07 $00 $90 $03 $00 $80 $06 $F2 $02 $88 $C0 $01 $FE
.db $82 $FD $00 $01 $0F $88 $E0 $01 $FE $82 $FD $00 $01 $00 $00 $10
.db $04 $F5 $0C $00 $09 $0B $F2 $02 $88 $C0 $01 $1A $83 $00 $02 $00
.db $02 $88 $E0 $01 $1A $83 $00 $03 $00 $03 $F3 $07 $02 $50 $05 $02
.db $66 $10 $F2 $01 $88 $C0 $01 $2D $83 $20 $04 $04 $02 $00 $F0 $04
.db $01 $00 $06 $F2 $01 $80 $C0 $02 $3E $83 $E5 $00 $03 $03 $F0 $01
.db $01 $FD $17 $9C $04 $A3 $03 $A3 $04 $A3 $06 $F2 $02 $88 $C0 $01
.db $5F $83 $00 $00 $00 $0F $88 $E0 $01 $5F $83 $00 $00 $0C $01 $F3
.db $07 $00 $0D $0E $F2 $02 $88 $A0 $01 $78 $83 $4B $05 $06 $01 $88
.db $C0 $01 $97 $83 $4B $05 $06 $01 $00 $98 $03 $00 $60 $06 $00 $5C
.db $06 $00 $68 $03 $00 $50 $06 $00 $6C $05 $00 $58 $06 $00 $8C $09
.db $00 $94 $09 $00 $8C $0E $F2 $00 $1C $06 $00 $14 $09 $00 $10 $09
.db $00 $0C $0E $F2 $02 $88 $C0 $01 $B7 $83 $00 $E7 $00 $0F $88 $E0
.db $01 $B7 $83 $00 $00 $05 $01 $F3 $07 $01 $09 $04 $00 $00 $02 $F5
.db $06 $00 $FF $18 $F2 $02 $80 $A0 $01 $D8 $83 $00 $00 $05 $02 $80
.db $C0 $01 $D8 $83 $07 $00 $06 $02 $F0 $01 $01 $FD $17 $93 $10 $92
.db $10 $91 $10 $90 $10 $8F $10 $8E $10 $8D $10 $8C $10 $8B $1B $F2
.db $FF

_LABEL_83F1_:
    call    _LABEL_8434_
    call    _LABEL_845B_
    call    _LABEL_849A_
_LABEL_83FA_:
    call    _LABEL_850C_
    ld      ix, _RAM_DE20_
    ld      b, $07
--:
    push    bc
    ld      a, $04
    cp      b
    jr      z, +
    bit     7, (ix+0)
    call    nz, _LABEL_87F2_
-:
    ld      de, $0020
    add     ix, de
    pop     bc
    djnz    --
    ld      a, (_RAM_DE1F_)
    or      a
    ld      (_RAM_DE1E_), a
    call    nz, _LABEL_8937_
    ret

+:
    set     6, (ix+0)
    bit     7, (ix+0)
    call    nz, _LABEL_87F2_
    res     6, (ix+0)
    jr      -

_LABEL_8434_:
    ld      a, (_RAM_DE02_)
    cp      $FF
    jr      z, +
    ld      hl, _RAM_DE01_
    ld      a, (hl)
    or      a
    ret     z
    dec     (hl)
    ret     nz
    ld      a, (_RAM_DE02_)
    ld      (hl), a
--:
    ld      hl, _RAM_DE2A_
    ld      de, $0020
    ld      b, $07
-:
    inc     (hl)
    add     hl, de
    djnz    -
    ret

+:
    ld      a, r
    or      a
    jp      po, --
    ret

_LABEL_845B_:
    ld      de, Sound_MusicTrigger
    call    +
    inc     de
    call    +
    inc     de
    call    +
    ret

+:
    ld      a, (de)
    bit     7, a
    ret     z
    and     $7F
    ld      hl, $8D16
    dec     a
    ld      c, a
    call    _LABEL_874B_
    bit     7, a
    jr      z, +
    ld      a, (de)
    ld      (_RAM_DE03_), a
    xor     a
    ld      (de), a
    pop     hl
    pop     hl
    jp      _LABEL_83FA_

+:
    ld      a, (_RAM_DE07_)
    cp      (hl)
    jr      z, +
    jr      nc, ++
+:
    ld      a, (de)
    ld      (_RAM_DE03_), a
    ld      a, (hl)
    ld      (_RAM_DE07_), a
++:
    xor     a
    ld      (de), a
    ret

_LABEL_849A_:
    ld      a, (_RAM_DE0A_)
    or      a
    ret     z
    ld      a, (_RAM_DE0B_)
    dec     a
    jr      z, +
    ld      (_RAM_DE0B_), a
    ret

+:
    ld      a, $12
    ld      (_RAM_DE0B_), a
    ld      a, (_RAM_DE0A_)
    dec     a
    ld      (_RAM_DE0A_), a
    jp      z, _LABEL_870B_
    ld      hl, _RAM_DE28_
    ld      de, $0020
    ld      b, $07
-:
    ld      a, $01
    cp      b
    jr      nz, +
    push    de
    ld      de, $0003
    sbc     hl, de
    pop     de
+:
    ld      a, (hl)
    inc     a
    cp      $10
    jr      z, +
    ld      (hl), a
+:
    add     hl, de
    djnz    -
    ret

; 1st entry of Jump Table from 8D10 (indexed by _RAM_DE03_)
_LABEL_84D7_:
    ld      a, $0C
    ld      (_RAM_DE0A_), a
    ld      a, $12
    ld      (_RAM_DE0B_), a
    jp      _LABEL_85D2_

; 2nd entry of Jump Table from 8D10 (indexed by _RAM_DE03_)
_LABEL_84E4_:
    xor     a
    ld      (_RAM_DE07_), a
    ld      (_RAM_DEA0_), a
    ld      (_RAM_DEC0_), a
    ld      (_RAM_DEE0_), a
    ld      hl, _RAM_DE40_
    res     2, (hl)
    ld      hl, _RAM_DE60_
    res     2, (hl)
    ld      hl, _RAM_DE80_
    res     2, (hl)
    ld      hl, _DATA_8736_
    ld      c, $7F
    ld      b, $03
    otir
    jp      _LABEL_85D2_

_LABEL_850C_:
    ld      a, (_RAM_DE03_)
    bit     7, a
    jp      z, _LABEL_870B_
    cp      $90
    jr      c, +
    cp      $B5
    jr      c, ++
    cp      $BA
    jp      nc, _LABEL_870B_
    sub     $B5
    ld      hl, _DATA_8D10_
    call    _LABEL_8741_
    jp      (hl)

+:
    sub     $81
    ret     m
    push    af
    call    _LABEL_870B_
    pop     af
    push    af
    ld      hl, $8D50
    ld      c, a
    call    _LABEL_874B_
    ld      (_RAM_DE01_), a
    ld      (_RAM_DE02_), a
    ld      hl, $8CA8
    pop     af
    call    _LABEL_8741_
    ld      de, _RAM_DE20_
    jr      _LABEL_85A9_

++:
    sub     $90
    ld      hl, $8CC6
    call    _LABEL_8741_
    ld      b, h
    ld      c, l
    push    bc
    ld      b, (hl)
    inc     hl
    inc     hl
    ld      a, (hl)
    cp      $A0
    jr      z, ++
    cp      $C0
    jr      z, +
    ld      de, _RAM_DEE0_
    jr      _LABEL_8570_

+:
    ld      de, _RAM_DEC0_
    jr      _LABEL_8570_

++:
    ld      de, _RAM_DEA0_
_LABEL_8570_:
    push    hl
    cp      $A0
    jr      z, ++
    cp      $C0
    jr      z, +
    ld      a, $E4
    out     ($7F), a
    ld      hl, _RAM_DE80_
    set     2, (hl)
    xor     a
    ld      (_RAM_DEFA_), a
    jr      +++

+:
    ld      hl, _RAM_DE60_
    set     2, (hl)
    xor     a
    ld      (_RAM_DEDA_), a
    jr      +++

++:
    ld      hl, _RAM_DE40_
    set     2, (hl)
    xor     a
    ld      (_RAM_DEBA_), a
+++:
    pop     hl
    push    de
    ld      de, $0009
    add     hl, de
    pop     de
    ld      a, (hl)
    djnz    _LABEL_8570_
    pop     bc
    ld      h, b
    ld      l, c
_LABEL_85A9_:
    ld      b, (hl)
    inc     hl
-:
    push    bc
    push    hl
    pop     ix
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ldi
    ld      a, $20
    ld      (de), a
    inc     de
    ld      a, $01
    ld      (de), a
    push    hl
    ld      hl, $0016
    add     hl, de
    ex      de, hl
    pop     hl
    pop     bc
    djnz    -
_LABEL_85D2_:
    ld      a, $80
    ld      (_RAM_DE03_), a
    ret

_LABEL_85D8_:
    bit     7, (ix+6)
    ret     z
    bit     1, (ix+0)
    ret     nz
    ld      e, (ix+17)
    ld      d, (ix+18)
    push    ix
    pop     hl
    ld      b, $00
    ld      c, $15
    add     hl, bc
    ex      de, hl
    ldi
    ldi
    ldi
    ld      a, (hl)
    srl     a
    ld      (de), a
    xor     a
    ld      (ix+19), a
    ld      (ix+20), a
    ret

_LABEL_8603_:
    ld      l, (ix+11)
    ld      h, (ix+12)
    ld      a, (ix+6)
    or      a
    ret     z
    cp      $80
    jr      nz, _LABEL_865A_
    dec     (ix+21)
    ret     nz
    inc     (ix+21)
    push    hl
    ld      l, (ix+19)
    ld      h, (ix+20)
    dec     (ix+22)
    jr      nz, +
    ld      e, (ix+17)
    ld      d, (ix+18)
    push    de
    pop     iy
    ld      a, (iy+1)
    ld      (ix+22), a
    ld      a, (ix+23)
    ld      c, a
    and     $80
    rlca
    neg
    ld      b, a
    add     hl, bc
    ld      (ix+19), l
    ld      (ix+20), h
+:
    pop     bc
    add     hl, bc
    dec     (ix+24)
    ret     nz
    ld      a, (iy+3)
    ld      (ix+24), a
    ld      a, (ix+23)
    neg
    ld      (ix+23), a
    ret

_LABEL_865A_:
    dec     a
    ex      de, hl
    ld      hl, _DATA_8C25_
    call    _LABEL_8741_
    jp      _LABEL_8894_

_LABEL_8665_:
    ld      a, (ix+7)
    or      a
    ret     z
    dec     a
    ld      hl, _DATA_8B70_
    call    _LABEL_8741_
    jp      _LABEL_8861_

_LABEL_8674_:
    bit     1, (ix+0)
    ret     nz
    jp      _LABEL_88C3_

_LABEL_867C_:
    res     1, (ix+0)
    res     4, (ix+0)
    ld      e, (ix+3)
    ld      d, (ix+4)
_LABEL_868A_:
    ld      a, (de)
    inc     de
    cp      $E0
    jp      nc, _LABEL_89E2_
    bit     3, (ix+0)
    jp      nz, _LABEL_86ED_
    cp      $DA
    jp      nc, _LABEL_8995_
    cp      $80
    jr      c, _LABEL_86C2_
    jr      z, _LABEL_86E8_
    bit     6, (ix+0)
    jp      nz, _LABEL_88D4_
    call    _LABEL_8739_
    ld      (ix+11), l
    ld      (ix+12), h
_LABEL_86B3_:
    ld      a, (de)
    inc     de
    or      a
    jp      p, _LABEL_86C2_
    ld      a, (ix+13)
    ld      (ix+10), a
    dec     de
    jr      +

_LABEL_86C2_:
    call    _LABEL_8750_
    ld      (ix+10), a
    ld      (ix+13), a
+:
    ld      (ix+3), e
    ld      (ix+4), d
    bit     1, (ix+0)
    ret     nz
    res     5, (ix+0)
    xor     a
    ld      (ix+25), a
    ld      (ix+22), a
    ld      a, (ix+15)
    ld      (ix+14), a
    ret

_LABEL_86E8_:
    call    _LABEL_8674_
    jr      _LABEL_86B3_

_LABEL_86ED_:
    ld      h, a
    ld      a, (de)
    inc     de
    ld      l, a
    or      h
    jr      z, ++
    ld      b, $00
    ld      a, (ix+5)
    or      a
    ld      c, a
    jp      p, +
    dec     b
+:
    add     hl, bc
++:
    ld      (ix+11), l
    ld      (ix+12), h
    ld      a, (de)
    inc     de
    jp      _LABEL_86C2_

_LABEL_870B_:
    push    hl
    push    bc
    push    de
    ld      hl, _RAM_DE03_
    ld      de, Sound_MusicTrigger
    ld      bc, $00FC
    ld      (hl), $00
    ldir
    pop     de
    pop     bc
    pop     hl
_LABEL_871E_:
    push    hl
    push    bc
    ld      hl, _DATA_872E_
    ld      b, $0B
    ld      c, $7F
    otir
    pop     bc
    pop     hl
    jp      _LABEL_85D2_

; Data from 872E to 8735 (8 bytes)
_DATA_872E_:
.db $80 $00 $A0 $00 $C0 $00 $E4 $9F

; Data from 8736 to 8738 (3 bytes)
_DATA_8736_:
.db $BF $DF $FF

_LABEL_8739_:
    and     $7F
    add     a, (ix+5)
    ld      hl, _DATA_875A_
_LABEL_8741_:
    ld      c, a
    ld      b, $00
    add     hl, bc
    add     hl, bc
    ld      c, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, c
    ret

_LABEL_874B_:
    ld      b, $00
    add     hl, bc
    ld      a, (hl)
    ret

_LABEL_8750_:
    ld      b, (ix+2)
    dec     b
    ret     z
    ld      c, a
-:
    add     a, c
    djnz    -
    ret

; Data from 875A to 87F1 (152 bytes)
_DATA_875A_:
.db $00 $00 $FF $03 $C7 $03 $90 $03 $5D $03 $2D $03 $FF $02 $D4 $02
.db $AB $02 $85 $02 $61 $02 $3F $02 $1E $02 $00 $02 $E3 $01 $C8 $01
.db $AF $01 $96 $01 $80 $01 $6A $01 $56 $01 $43 $01 $30 $01 $1F $01
.db $0F $01 $00 $01 $F2 $00 $E4 $00 $D7 $00 $CB $00 $C0 $00 $B5 $00
.db $AB $00 $A1 $00 $98 $00 $90 $00 $88 $00 $80 $00 $79 $00 $72 $00
.db $6C $00 $66 $00 $60 $00 $5B $00 $55 $00 $51 $00 $4C $00 $48 $00
.db $44 $00 $40 $00 $3C $00 $39 $00 $36 $00 $33 $00 $30 $00 $2D $00
.db $2B $00 $28 $00 $26 $00 $24 $00 $22 $00 $20 $00 $1E $00 $1C $00
.db $1B $00 $19 $00 $18 $00 $16 $00 $15 $00 $14 $00 $13 $00 $12 $00
.db $11 $00 $10 $00 $0F $00 $0E $00

_LABEL_87F2_:
    dec     (ix+10)
    jr      nz, +
    call    _LABEL_867C_
    bit     4, (ix+0)
    ret     nz
    call    _LABEL_85D8_
    jr      ++

+:
    ld      a, (ix+14)
    or      a
    jr      z, ++
    dec     (ix+14)
    jp      z, _LABEL_8674_
++:
    ld      a, (ix+1)
    cp      $E0
    jr      z, ++
    call    _LABEL_8603_
    bit     2, (ix+0)
    ret     nz
    bit     5, (ix+0)
    jr      nz, ++
    ld      d, $00
    ld      a, (ix+16)
    or      a
    jp      p, +
    dec     d
+:
    ld      e, a
    add     hl, de
    ld      a, (ix+1)
    ld      c, a
    ld      a, l
    and     $0F
    or      c
    out     ($7F), a
    ld      a, l
    and     $F0
    or      h
    rrca
    rrca
    rrca
    rrca
    out     ($7F), a
++:
    call    _LABEL_8665_
    bit     4, (ix+0)
    ret     nz
    add     a, (ix+8)
    bit     4, a
    jr      z, +
    ld      a, $0F
+:
    or      (ix+1)
    add     a, $10
    out     ($7F), a
    ret

-:
    ld      (ix+25), a
_LABEL_8861_:
    push    hl
    ld      c, (ix+25)
    call    _LABEL_874B_
    pop     hl
    bit     7, a
    jr      z, ++++
    cp      $82
    jr      z, +
    cp      $81
    jr      z, +++
    cp      $80
    jr      z, ++
    inc     de
    ld      a, (de)
    jr      -

+:
    set     4, (ix+0)
    pop     hl
    jr      _LABEL_88C3_

++:
    xor     a
    jr      -

+++:
    set     4, (ix+0)
    pop     hl
    ret

++++:
    inc     (ix+25)
    ret

-:
    ld      (ix+22), a
_LABEL_8894_:
    push    hl
    ld      c, (ix+22)
    call    _LABEL_874B_
    pop     hl
    bit     7, a
    jr      z, ++++
    cp      $83
    jr      z, +
    jr      nc, +++
    cp      $80
    jr      z, ++
    set     5, (ix+0)
    pop     hl
    ret

+:
    inc     de
    ld      a, (de)
    jr      -

++:
    xor     a
    jr      -

+++:
    ld      h, $FF
    jr      +++++

++++:
    ld      h, $00
+++++:
    ld      l, a
    add     hl, de
    inc     (ix+22)
    ret

_LABEL_88C3_:
    set     4, (ix+0)
    bit     2, (ix+0)
    ret     nz
    ld      a, $1F
    add     a, (ix+1)
    out     ($7F), a
    ret

_LABEL_88D4_:
    bit     0, a
    jr      nz, ++++++
    bit     1, a
    jr      nz, ++
    bit     2, a
    jr      nz, +++++
    bit     3, a
    jr      nz, +
    bit     4, a
    jr      nz, ++++
    bit     5, a
    jr      nz, +++
+:
    ld      a, $02
    ld      b, $03
    ld      c, $E5
    jr      +++++++

++:
    ld      a, $03
    ld      b, $03
    ld      c, $E4
    jr      +++++++

+++:
    ld      a, $04
    ld      b, $04
    ld      c, $E4
    jr      +++++++

++++:
    ld      a, $03
    ld      b, $03
    ld      c, $E6
    jr      +++++++

+++++:
    ld      a, $01
    ld      b, $01
    ld      c, $E6
    jr      +++++++

++++++:
    ld      a, $01
    ld      b, $04
    ld      c, $E4
+++++++:
    ld      (ix+7), a
    ld      a, b
    add     a, (ix+5)
    ld      (ix+8), a
    bit     2, (ix+0)
    jp      nz, _LABEL_86B3_
    ld      a, c
    add     a, (ix+6)
    ld      (_RAM_DE08_), a
    out     ($7F), a
    jp      _LABEL_86B3_

; 3rd entry of Jump Table from 8D10 (indexed by _RAM_DE03_)
_LABEL_8937_:
    ld      a, (_RAM_DE1D_)
    or      a
    ret     nz
    ld      a, $C1
    out     ($7F), a
    ld      a, $00
    out     ($7F), a
_LABEL_8944_:
    ld      hl, _RAM_DE18_
    ld      d, (hl)
    inc     hl
    ld      e, (hl)
    inc     hl
    ld      b, (hl)
    inc     hl
    ld      c, (hl)
    ld      a, (_RAM_DE1C_)
    ld      l, a
    push    bc
    ld      b, $08
    ld      a, (de)
_LABEL_8944__1_:
    push    bc
    ld      b, l
-:
    djnz    -
    pop     bc
    rlca
    ld      h, a
    ld      a, (_RAM_DE68_)
    jr      c, +
    or      $D0
    jr      ++

+:
    or      $DF
++:
    out     ($7F), a
    ld      a, h
    djnz    _LABEL_8944__1_
    inc     de
    pop     bc
    dec     bc
    ld      hl, _RAM_DE18_
    ld      (hl), d
    inc     hl
    ld      (hl), e
    inc     hl
    ld      (hl), b
    inc     hl
    ld      (hl), c
    ld      a, c
    or      b
    jp      z, +
    ld      a, (_RAM_DE1E_)
    dec     a
    ld      (_RAM_DE1E_), a
    or      a
    jp      nz, _LABEL_8944_
    jp      _LABEL_85D2_

+:
    ld      a, $01
    ld      (_RAM_DE1D_), a
    jp      _LABEL_85D2_

_LABEL_8995_:
    set     4, (ix+0)
    push    de
    cp      $DA
    jr      z, +
    cp      $DB
    jr      z, ++
    cp      $DC
    jr      z, +++
    cp      $DD
    jr      z, +++
    cp      $DE
    jr      z, +++
    ld      a, $01
    ld      (_RAM_DE1D_), a
    pop     de
    jp      _LABEL_86B3_

+:
    ld      bc, $89E1
    ld      de, $0000
    jp      ++++

++:
    ld      bc, $89E1
    ld      de, $0000
    jp      ++++

+++:
    ld      bc, $89E1
    ld      de, $0000
++++:
    ld      hl, _RAM_DE18_
    ld      (hl), b
    inc     hl
    ld      (hl), c
    inc     hl
    ld      (hl), d
    inc     hl
    ld      (hl), e
    xor     a
    ld      (_RAM_DE1D_), a
    pop     de
    jp      _LABEL_86B3_

; Data from 89E1 to 89E1 (1 bytes)
.db $FF

_LABEL_89E2_:
    ld      hl, +    ; Overriding return address
    push    hl
    sub     $E0
    ld      hl, _DATA_89FA_
    add     a, a
    ld      c, a
    ld      b, $00
    add     hl, bc
    ld      c, (hl)
    inc     hl
    ld      h, (hl)
    ld      l, c
    ld      a, (de)
    jp      (hl)

+:
    inc     de
    jp      _LABEL_868A_

; Jump Table from 89FA to 8A39 (32 entries, indexed by unknown)
_DATA_89FA_:
.dw _LABEL_8B5B_ _LABEL_8B55_ _LABEL_8B59_ _LABEL_8B59_ _LABEL_8B59_ _LABEL_8B5C_ _LABEL_8A3A_ _LABEL_8B4F_
.dw _LABEL_8B37_ _LABEL_8B5B_ _LABEL_8B59_ _LABEL_8B5A_ _LABEL_8B59_ _LABEL_8B5D_ _LABEL_8B59_ _LABEL_8B64_
.dw _LABEL_8B41_ _LABEL_8B6C_ _LABEL_8A9E_ _LABEL_8A6E_ _LABEL_8A86_ _LABEL_8A82_ _LABEL_8A8A_ _LABEL_8B20_
.dw _LABEL_8AF3_ _LABEL_8B0D_ _LABEL_8A6A_ _LABEL_8A63_ _LABEL_8B5C_ _LABEL_8A90_ _LABEL_8B59_ _LABEL_8B59_

; 7th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A3A_:
    ex      af, af'
    ld      a, (_RAM_DE0A_)
    or      a
    ret     nz
    ld      a, (ix+1)
    cp      $E0
    jr      z, +
    ex      af, af'
    res     4, (ix+0)
    add     a, (ix+8)
    and     $0F
    ld      (ix+8), a
    ret

+:
    ex      af, af'
    res     4, (ix+0)
    add     a, (ix+5)
    and     $0F
    ld      (ix+5), a
    ret

; 28th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A63_:
    add     a, (ix+5)
    ld      (ix+5), a
    ret

; 27th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A6A_:
    ld      (ix+2), a
    ret

; 20th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A6E_:
    or      $E0
    out     ($7F), a
    ld      hl, _RAM_DE60_
    or      $FC
    inc     a
    jr      nz, +
    ld      a, $DF
    out     ($7F), a
    set     2, (hl)
    ret

+:
    ret

; 22nd entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A82_:
    ld      (ix+7), a
    ret

; 21st entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A86_:
    ld      (ix+6), a
    ret

; 23rd entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A8A_:
    ex      de, hl
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    dec     de
    ret

; 30th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A90_:
    cp      $01
    jr      nz, +
    set     3, (ix+0)
    ret

+:
    res     3, (ix+0)
    ret

; 19th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8A9E_:
    xor     a
    ld      (_RAM_DE07_), a
    ld      (ix+0), a
    ld      a, (ix+1)
    or      $1F
    out     ($7F), a
    sub     $1F
    cp      $E0
    jp      z, +++
    cp      $C0
    jp      z, ++
    cp      $A0
    jp      z, +
    ld      hl, _RAM_DE20_
    res     2, (hl)
    set     5, (hl)
    set     4, (hl)
    jr      ++++

+:
    ld      hl, _RAM_DE40_
    res     2, (hl)
    set     5, (hl)
    set     4, (hl)
    jr      ++++

++:
    ld      hl, _RAM_DE60_
    res     2, (hl)
    set     5, (hl)
    set     4, (hl)
    jr      ++++

+++:
    ld      hl, _RAM_DE80_
    set     4, (hl)
    res     2, (hl)
    ld      a, (_RAM_DE0D_)
    or      a
    jr      z, ++++
    ld      hl, _RAM_DE60_
    res     2, (hl)
++++:
    pop     hl
    pop     hl
    ret

; 25th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8AF3_:
    ld      c, a
    inc     de
    ld      a, (de)
    ld      b, a
    push    bc
    push    ix
    pop     hl
    dec     (ix+9)
    ld      c, (ix+9)
    dec     (ix+9)
    ld      b, $00
    add     hl, bc
    ld      (hl), d
    dec     hl
    ld      (hl), e
    pop     de
    dec     de
    ret

; 26th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B0D_:
    push    ix
    pop     hl
    ld      c, (ix+9)
    ld      b, $00
    add     hl, bc
    ld      e, (hl)
    inc     hl
    ld      d, (hl)
    inc     (ix+9)
    inc     (ix+9)
    ret

; 24th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B20_:
    inc     de
    add     a, $1A
    ld      c, a
    ld      b, $00
    push    ix
    pop     hl
    add     hl, bc
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      a, (de)
    ld      (hl), a
+:
    inc     de
    dec     (hl)
    jp      nz, _LABEL_8A8A_
    inc     de
    ret

; 9th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B37_:
    call    _LABEL_8750_
    ld      (ix+14), a
    ld      (ix+15), a
    ret

; 17th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B41_:
    ld      (ix+17), e
    ld      (ix+18), d
    ld      (ix+6), $80
    inc     de
    inc     de
    inc     de
    ret

; 8th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B4F_:
    set     1, (ix+0)
    dec     de
    ret

; 2nd entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B55_:
    ld      (ix+16), a
    ret

; 3rd entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B59_:
    ret

; 12th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B5A_:
    ret

; 1st entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B5B_:
    ret

; 6th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B5C_:
    ret

; 14th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B5D_:
    ld      (_RAM_DE02_), a
    ld      (_RAM_DE01_), a
    ret

; 16th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B64_:
    ld      (_RAM_DE1F_), a
    xor     a
    ld      (_RAM_DE1D_), a
    ret

; 18th entry of Jump Table from 89FA (indexed by unknown)
_LABEL_8B6C_:
    ld      (_RAM_DE1C_), a
    ret

; Pointer Table from 8B70 to 8B89 (13 entries, indexed by unknown)
_DATA_8B70_:
.dw _DATA_8B8A_ _DATA_8B8D_ _DATA_8B96_ _DATA_8B9F_ _DATA_8BA8_ _DATA_8BB3_ _DATA_8BD2_ _DATA_8BDD_
.dw _DATA_8BEC_ _DATA_8BF8_ _DATA_8BFE_ _DATA_8C09_ _DATA_8C1A_

; 1st entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8B8A to 8B8C (3 bytes)
_DATA_8B8A_:
.db $02 $06 $82

; 2nd entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8B8D to 8B95 (9 bytes)
_DATA_8B8D_:
.db $00 $01 $02 $04 $05 $06 $07 $0A $82

; 3rd entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8B96 to 8B9E (9 bytes)
_DATA_8B96_:
.db $01 $00 $01 $01 $03 $04 $07 $0A $82

; 4th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8B9F to 8BA7 (9 bytes)
_DATA_8B9F_:
.db $02 $00 $00 $00 $01 $02 $03 $04 $82

; 5th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8BA8 to 8BB2 (11 bytes)
_DATA_8BA8_:
.db $02 $01 $00 $01 $02 $02 $03 $03 $04 $04 $81

; 6th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8BB3 to 8BD1 (31 bytes)
_DATA_8BB3_:
.db $05 $02 $00 $00 $01 $01 $02 $02 $02 $02 $03 $03 $03 $03 $04 $04
.db $04 $04 $05 $05 $05 $05 $06 $06 $06 $06 $07 $07 $07 $08 $81

; 7th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8BD2 to 8BDC (11 bytes)
_DATA_8BD2_:
.db $04 $04 $03 $03 $02 $02 $01 $01 $02 $02 $81

; 8th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8BDD to 8BEB (15 bytes)
_DATA_8BDD_:
.db $00 $00 $01 $01 $02 $02 $02 $03 $03 $03 $03 $04 $04 $05 $81

; 9th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8BEC to 8BF7 (12 bytes)
_DATA_8BEC_:
.db $00 $00 $01 $01 $01 $02 $04 $03 $02 $02 $83 $04

; 10th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8BF8 to 8BFD (6 bytes)
_DATA_8BF8_:
.db $02 $02 $03 $03 $0F $81

; 11th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8BFE to 8C08 (11 bytes)
_DATA_8BFE_:
.db $02 $01 $00 $00 $01 $01 $02 $03 $04 $05 $81

; 12th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8C09 to 8C19 (17 bytes)
_DATA_8C09_:
.db $07 $07 $06 $06 $05 $05 $04 $04 $03 $03 $02 $02 $02 $01 $01 $01
.db $82

; 13th entry of Pointer Table from 8B70 (indexed by unknown)
; Data from 8C1A to 8C24 (11 bytes)
_DATA_8C1A_:
.db $00 $00 $01 $02 $03 $04 $03 $02 $01 $00 $82

; Pointer Table from 8C25 to 8C34 (8 entries, indexed by _RAM_DE26_)
_DATA_8C25_:
.dw _DATA_8C35_ _DATA_8C4C_ _DATA_8C65_ _DATA_8C7D_ _DATA_8C86_ _DATA_8C8D_ _DATA_8C93_ _DATA_8C9E_

; 1st entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C35 to 8C4B (23 bytes)
_DATA_8C35_:
.db $01 $01 $01 $01 $01 $01 $01 $01 $01 $01 $00 $00 $01 $01 $01 $01
.db $01 $01 $00 $00 $01 $00 $80

; 2nd entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C4C to 8C64 (25 bytes)
_DATA_8C4C_:
.db $00 $00 $01 $00 $03 $00 $01 $01 $01 $00 $00 $01 $02 $03 $02 $02
.db $02 $01 $00 $00 $00 $01 $03 $07 $02

; 3rd entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C65 to 8C7C (24 bytes)
_DATA_8C65_:
.db $00 $01 $02 $03 $05 $03 $01 $00 $01 $02 $04 $02 $01 $00 $01 $02
.db $03 $05 $03 $02 $01 $00 $83 $40

; 4th entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C7D to 8C85 (9 bytes)
_DATA_8C7D_:
.db $00 $50 $00 $01 $01 $00 $00 $83 $04

; 5th entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C86 to 8C8C (7 bytes)
_DATA_8C86_:
.db $57 $18 $11 $00 $3F $01 $80

; 6th entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C8D to 8C92 (6 bytes)
_DATA_8C8D_:
.db $11 $41 $01 $41 $01 $80

; 7th entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C93 to 8C9D (11 bytes)
_DATA_8C93_:
.db $00 $00 $01 $02 $01 $00 $00 $FF $FE $FF $80

; 8th entry of Pointer Table from 8C25 (indexed by _RAM_DE26_)
; Data from 8C9E to 8D0F (114 bytes)
_DATA_8C9E_:
.db $00 $00 $00 $01 $01 $00 $00 $FF $FF $80 $60 $8D $0F $91 $B3 $92
.db $25 $94 $8E $95 $D5 $97 $59 $98 $2A $9A $AB $9A $9E $9B $2F $9C
.db $86 $9C $EA $9C $37 $9D $04 $9F $01 $80 $17 $80 $42 $80 $63 $80
.db $7B $80 $8A $80 $A6 $80 $BD $80 $F4 $80 $12 $81 $24 $81 $3D $81
.db $6C $81 $8E $81 $A0 $81 $BF $81 $DB $81 $DB $81 $F7 $81 $0F $82
.db $27 $82 $39 $82 $5A $82 $76 $82 $A2 $82 $B3 $82 $CF $82 $CF $82
.db $EB $82 $07 $83 $23 $83 $34 $83 $34 $83 $4C $83 $65 $83 $A4 $83
.db $C5 $83

; Jump Table from 8D10 to 8D25 (11 entries, indexed by _RAM_DE03_)
_DATA_8D10_:
.dw _LABEL_84D7_ _LABEL_84E4_ _LABEL_8937_ $8080 $8080 $8080 $8080 $8080
.dw $8080 $8080 _LABEL_5F80_

; Data from 8D26 to BFFF (13018 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_8d26.inc"

.BANK 3
.ORG $0000

; Data from C000 to F71F (14112 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_c000.inc"

; Data from F720 to FFFF (2272 bytes)
_DATA_F720_:
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_f720.inc"

.BANK 4
.ORG $0000

; Data from 10000 to 13FFF (16384 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_10000.inc"

.BANK 5
.ORG $0000

; Data from 14000 to 17FFF (16384 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_14000.inc"

.BANK 6
.ORG $0000

; Data from 18000 to 19DFF (7680 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_18000.inc"

; Data from 19E00 to 1B4D9 (5850 bytes)
_DATA_19E00_:
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_19e00.inc"

; Data from 1B4DA to 1B619 (320 bytes)
_DATA_1B4DA_:
.db $7F $69 $7F $69 $7F $69 $7F $69 $84 $69 $A6 $A7 $A8 $16 $69 $92
.db $A6 $A7 $A8 $A9 $69 $A6 $AA $AB $AC $AC $AD $AE $AF $B0 $AD $AE
.db $AF $B0 $0A $B1 $8B $AD $AE $B2 $B3 $B4 $B5 $B5 $B6 $B7 $B8 $B4
.db $03 $B5 $20 $B9 $FF $BA $BB $BC $BA $BB $BC $BA $BB $BC $BA $BB
.db $BC $BA $BB $BC $BA $BB $BC $BA $BB $BC $BA $BB $BC $BA $BB $BC
.db $BA $BB $BC $BA $BB $BD $BE $BF $C0 $C1 $C2 $BD $BE $BF $C0 $C1
.db $C2 $BD $BE $BF $C0 $C1 $C2 $BD $BE $BF $C0 $C1 $C2 $BD $BE $BF
.db $C0 $C1 $C2 $BD $BE $C3 $C4 $C5 $C6 $C7 $C8 $C9 $C4 $C3 $CA $CB
.db $CC $CD $C4 $C3 $C4 $C3 $C4 $C3 $C4 $C3 $C4 $C3 $C4 $C3 $C4 $C3
.db $C4 $C3 $C4 $C3 $C4 $CE $CF $D0 $D1 $D2 $D3 $CE $CF $D0 $D1 $D2
.db $D3 $CE $CF $D0 $D1 $D2 $D3 $CE $CF $D0 $D1 $D2 $D3 $CE $CF $D0
.db $D1 $D2 $D3 $CE $A1 $CF $D4 $D5 $D6 $D4 $D5 $D6 $D4 $D5 $D6 $D4
.db $D5 $D6 $D4 $D5 $D6 $D4 $D5 $D6 $D4 $D5 $D6 $D4 $D5 $D6 $D4 $D5
.db $D6 $D4 $D5 $D6 $D4 $D5 $7F $69 $01 $69 $00 $7F $00 $7F $00 $7F
.db $00 $7F $00 $7F $00 $7F $00 $7F $00 $07 $00 $00 $D7 $00 $D8 $00
.db $D9 $00 $DA $00 $DB $00 $DC $00 $DD $00 $DE $00 $DF $00 $E0 $00
.db $E1 $00 $E2 $00 $E3 $00 $E4 $00 $E5 $00 $E6 $00 $E7 $00 $E8 $00
.db $E9 $00 $EA $00 $EB $00 $EC $00 $ED $00 $EE $00 $EF $00 $F0 $00
.db $F1 $00 $F2 $00 $F3 $00 $F4 $00 $F5 $00 $F6 $00 $F7 $00 $F8 $00
.db $F9 $00 $FA $00 $FB $00 $E5 $00 $FC $00 $FD $00 $FE $00 $69 $00

; Data from 1B61A to 1B69D (132 bytes)
_DATA_1B61A_:
.db $69 $00 $6A $00 $6B $00 $6C $00 $6D $00 $6E $00 $6F $00 $70 $00
.db $71 $00 $72 $00 $69 $00 $73 $00 $74 $00 $75 $00 $76 $00 $77 $00
.db $78 $00 $79 $00 $7A $00 $7B $00 $7C $00 $7D $00 $69 $00 $7E $00
.db $7F $00 $80 $00 $81 $00 $82 $00 $83 $00 $84 $00 $85 $00 $86 $00
.db $87 $00 $88 $00 $89 $00 $8A $00 $8B $00 $8C $00 $8D $00 $8E $00
.db $8F $00 $90 $00 $91 $00 $92 $00 $93 $00 $94 $00 $95 $00 $96 $00
.db $7E $00 $97 $00 $98 $00 $99 $00 $9A $00 $9B $00 $9C $00 $9D $00
.db $9E $00 $9F $00 $A0 $00 $A1 $00 $69 $00 $A2 $00 $A3 $00 $A4 $00
.db $A5 $00 $69 $00

; Data from 1B69E to 1BFFF (2402 bytes)
_DATA_1B69E_:
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_1b69e.inc"

.BANK 7
.ORG $0000

; Data from 1C000 to 1D5CE (5583 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_1c000.inc"

_LABEL_1D5CF_:
    ld      a, (StageNumber)
    ld      hl, $95E2
    rst     $18    ; get_array_index
    ld      a, (_RAM_C04E_)
    rst     $18    ; get_array_index
    ld      a, (hl)
    ld      (_RAM_C030_), a
    ld      (_RAM_C01F_), a
    ret

; Data from 1D5E2 to 1FFFF (10782 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_1d5e2.inc"

.BANK 8
.ORG $0000

; Data from 20000 to 21CD8 (7385 bytes)
_DATA_20000_:
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_20000.inc"

GFX_Intro_Sprites:                                                  ; $8020
.incbin "graphics/intro_sprites.rle.bin"

SCR_Intro_Screen:                                                   ; $9CD9
.incbin "screenmap/intro_screen.map"

_DATA_21F77_:
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_21cd9.inc"

UNK_Stage4_Boss_mappings:                                           ; $A2BF
.incbin "stage4_boss_mappings.rle.bin"

_DATA_22433_:                                                       ; $A433
.incbin "unknown_B8_A433.bin"

GFX_Stage4_2_Enemies1:                                              ; $A522
.incbin "graphics/stage4_2_enemies1.rle.bin"

GFX_Stage4_2_Enemies2:                                              ; $A52E
.incbin "graphics/stage4_2_enemies2.rle.bin"

GFX_Intro_Letters:                                                  ; $AC10
.incbin "graphics/intro_letters.rle.bin"

.BANK 9
.ORG $0000

GFX_Stage3_2:                                                       ; $8000
.incbin "graphics/stage3_2.rle.bin"


GFX_ExplosionCloud:                                                 ; $8FFF
.incbin "graphics/explosion_cloud.rle.bin"


; 1st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_250CC_:
    ld      a, (ix+28)
    ld      hl, $90D5
    jp      call_jump_table

; Jump Table from 250D5 to 250D8 (2 entries, indexed by unknown)
_DATA_250D5_:
.dw _LABEL_250D9_ _LABEL_250FC_

; 1st entry of Jump Table from 250D5 (indexed by unknown)
_LABEL_250D9_:
    ld      a, $95
    ld      (Sound_MusicTrigger), a
    xor     a
    ld      (ix+16), a
    ld      (ix+17), a
    ld      (ix+14), a
    ld      (ix+15), a
    inc     a
    call    _LABEL_2641A_
    inc     (ix+28)
    ld      hl, $A8DF
    ld      a, $02
    ld      b, $03
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 250D5 (indexed by unknown)
_LABEL_250FC_:
    ld      a, (ix+5)
    cp      $02
    ret     nz
    ld      a, (ix+7)
    cp      $01
    ret     nz
    jp      _LABEL_263F4_

; 108th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2510B_:
    ld      a, (ix+28)
    ld      hl, $9114
    jp      call_jump_table

; Jump Table from 25114 to 2511B (4 entries, indexed by unknown)
_DATA_25114_:
.dw _LABEL_2511C_ _LABEL_25139_ _LABEL_25167_ _LABEL_25183_

; 1st entry of Jump Table from 25114 (indexed by unknown)
_LABEL_2511C_:
    inc     (ix+28)
    ld      a, $95
    ld      (Sound_MusicTrigger), a
    call    _LABEL_263C0_
    call    _LABEL_263CA_
    ld      a, $08
    call    _LABEL_2641A_
    ld      a, $0B
    ld      b, $04
    ld      hl, $B431
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25114 (indexed by unknown)
_LABEL_25139_:
    ld      a, (ix+7)
    dec     a
    jr      nz, +
    ld      a, $95
    ld      (Sound_MusicTrigger), a
+:
    ld      a, (ix+5)
    cp      $0B
    ret     nz
    ld      a, (_RAM_DD37_)
    or      a
    jp      z, _LABEL_263F4_
    xor     a
    ld      (_RAM_DD37_), a
    ld      (_RAM_DD0B_), a
    inc     (ix+28)
    ld      (ix+29), $30
    res     6, (ix+1)
    set     3, (ix+1)
; 3rd entry of Jump Table from 25114 (indexed by unknown)
_LABEL_25167_:
    dec     (ix+29)
    ret     nz
    ld      a, (_RAM_DD00_)
    or      a
    jp      nz, +
    xor     a
    jp      _LABEL_23_

+:
    inc     (ix+28)
    ld      (ix+29), $90
    ld      a, $B5
    ld      (Sound_MusicTrigger), a
    ret

; 4th entry of Jump Table from 25114 (indexed by unknown)
_LABEL_25183_:
    dec     (ix+29)
    ret     nz
    ld      a, $13
    ld      (GameMode), a
    ret

; 2nd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2518D_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, _DATA_25199_
    jp      call_jump_table

; Jump Table from 25199 to 2519C (2 entries, indexed by unknown)
_DATA_25199_:
.dw _LABEL_2519D_ _LABEL_251B5_

; 1st entry of Jump Table from 25199 (indexed by unknown)
_LABEL_2519D_:
    set     2, (ix+1)
    ld      hl, $0100
    ld      (ix+14), l
    ld      (ix+15), h
    inc     (ix+28)
    ld      hl, $A927
    xor     a
    ld      b, a
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25199 (indexed by unknown)
_LABEL_251B5_:
    ld      a, (ix+11)
    cp      $50
    jr      c, +
    cp      $B8
    ret     c
+:
    ld      l, (ix+14)
    ld      h, (ix+15)
    call    _LABEL_263ED_
    ld      (ix+14), l
    ld      (ix+15), h
    ret

; 4th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_251CF_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $91E4
    jp      call_jump_table

; Jump Table from 251E4 to 251EB (4 entries, indexed by unknown)
_DATA_251E4_:
.dw _LABEL_251EC_ _LABEL_251F5_ _LABEL_25201_ _LABEL_2520F_

; 1st entry of Jump Table from 251E4 (indexed by unknown)
_LABEL_251EC_:
    ld      hl, $A996
    inc     (ix+28)
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 4952 (indexed by unknown)
_LABEL_251F5_:
    call    _LABEL_2522F_
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $18
    ret

; 3rd entry of Jump Table from 4952 (indexed by unknown)
_LABEL_25201_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      a, $01
    ld      b, $04
    jp      _LABEL_2631B_

; 4th entry of Jump Table from 251E4 (indexed by unknown)
_LABEL_2520F_:
    ld      a, (ix+5)
    dec     a
    ret     nz
    ld      a, (ix+7)
    dec     a
    jr      nz, +
    ld      (ix+28), a
    ret

+:
    cp      $03
    ret     nz
    ld      b, $05
_LABEL_25223_:
    push    ix
    pop     hl
    ld      de, $00C0
    add     hl, de
    ld      a, (hl)
    or      a
    ret     nz
    ld      (hl), b
    ret

_LABEL_2522F_:
    push    ix
    pop     iy
    ld      de, $00C0
    add     iy, de
    ld      a, (iy+0)
    or      a
    ret

; 6th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2523D_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $924F
    jp      call_jump_table

; Jump Table from 2524F to 25258 (5 entries, indexed by unknown)
_DATA_2524F_:
.dw _LABEL_25259_ _LABEL_2526D_ _LABEL_25283_ _LABEL_252A5_ _LABEL_252D5_

; 1st entry of Jump Table from 2524F (indexed by unknown)
_LABEL_25259_:
    inc     (ix+28)
    set     3, (ix+1)
    ld      hl, $AB94
    ld      a, $03
    ld      b, $00
    call    _LABEL_26315_
    jp      _LABEL_26398_

; 2nd entry of Jump Table from 2524F (indexed by unknown)
_LABEL_2526D_:
    ld      b, $20
    call    _LABEL_263A7_
    ret     nc
    res     3, (ix+1)
    ld      (ix+6), $04
    ld      (ix+7), $04
    inc     (ix+28)
    ret

; 3rd entry of Jump Table from 2524F (indexed by unknown)
_LABEL_25283_:
    ld      a, (ix+5)
    cp      $03
    ret     nz
    ld      a, (ix+7)
    cp      $01
    ret     nz
    ld      hl, $FE00
_LABEL_25292_:
    ld      (ix+14), l
    ld      (ix+15), h
    inc     (ix+28)
    ld      hl, $ABD3
    ld      a, $01
    ld      b, $00
    jp      _LABEL_26315_

; 4th entry of Jump Table from 2524F (indexed by unknown)
_LABEL_252A5_:
    ld      bc, $0000
    call    _LABEL_266F8_
    cp      $01
    jr      z, +
    cp      $02
    jr      z, +
    ld      de, $0080
    call    _LABEL_2635E_
    ld      bc, $FFF8
    call    _LABEL_266F8_
    cp      $02
    jp      z, _LABEL_26335_
    ret

+:
    call    _LABEL_26374_
    inc     (ix+28)
    inc     (ix+5)
    ld      (ix+29), $04
    jp      _LABEL_26398_

; 5th entry of Jump Table from 2524F (indexed by unknown)
_LABEL_252D5_:
    dec     (ix+29)
    ret     nz
    ld      a, (ix+9)
    or      a
    jr      nz, +
    dec     a
+:
    ld      (ix+17), a
    ld      (ix+28), $02
    ld      hl, $FC00
    jr      _LABEL_25292_

; 11th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_252EC_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $92FE
    jp      call_jump_table

; Jump Table from 252FE to 25303 (3 entries, indexed by unknown)
_DATA_252FE_:
.dw _LABEL_25304_ _LABEL_2532E_ _LABEL_2533F_

; 1st entry of Jump Table from 252FE (indexed by unknown)
_LABEL_25304_:
    inc     (ix+28)
    ld      (ix+31), $00
    ld      a, (_RAM_C20B_)
    sub     (ix+11)
    ld      c, a
    jr      c, +
    ld      b, $40
    call    _LABEL_263A7_
    jr      nc, +
    ld      a, c
    cp      $30
    jr      c, +
    inc     (ix+31)
    ld      hl, $AC5E
    jr      ++

+:
    ld      hl, $AC1E
++:
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 252FE (indexed by unknown)
_LABEL_2532E_:
    call    _LABEL_26398_
    call    _LABEL_2522F_
    ret     nz
    inc     (ix+28)
    ld      a, $01
    ld      b, $04
    jp      _LABEL_2631B_

; 3rd entry of Jump Table from 252FE (indexed by unknown)
_LABEL_2533F_:
    ld      a, (ix+5)
    dec     a
    ret     nz
    ld      a, (ix+7)
    dec     a
    jr      nz, +
    ld      (ix+28), a
    ret

+:
    cp      $03
    ret     nz
    ld      b, $0C
    jp      _LABEL_25223_

; 12th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25356_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9362
    jp      call_jump_table

; Jump Table from 25362 to 25365 (2 entries, indexed by unknown)
_DATA_25362_:
.dw _LABEL_25366_ _LABEL_253B7_

; 1st entry of Jump Table from 25362 (indexed by unknown)
_LABEL_25366_:
    call    _LABEL_253B8_
    ld      a, $94
    ld      (Sound_MusicTrigger), a
    ld      a, (iy+31)
    or      a
    jr      nz, ++
    ld      a, (iy+13)
    ld      (ix+13), a
    ld      a, (iy+11)
    sub     $08
    ld      (ix+11), a
    ld      a, (iy+9)
    or      a
    ld      a, $02
    jr      nz, +
    ld      a, $FE
+:
    ld      (ix+17), a
    jr      +++

++:
    ld      a, (iy+13)
    add     a, $04
    ld      (ix+13), a
    ld      a, (iy+11)
    add     a, $04
    ld      (ix+11), a
    ld      (ix+15), $02
+++:
    ld      a, (StageNumber)
    ld      hl, $AC82
    cp      $02
    jr      nz, +
    ld      hl, $AC8B
+:
    xor     a
    ld      b, a
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25362 (indexed by unknown)
_LABEL_253B7_:
    ret

_LABEL_253B8_:
    inc     (ix+28)
    set     5, (ix+1)
    push    ix
    pop     iy
    ld      de, $FF40
    add     iy, de
    ret

; 26th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_253C9_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, $93D5
    jp      call_jump_table

; Jump Table from 253D5 to 253DE (5 entries, indexed by unknown)
_DATA_253D5_:
.dw _LABEL_253DF_ _LABEL_253FD_ _LABEL_25406_ _LABEL_25430_ _LABEL_25457_

; 1st entry of Jump Table from 253D5 (indexed by unknown)
_LABEL_253DF_:
    ld      hl, $AC94
_LABEL_253E2_:
    set     2, (ix+1)
    inc     (ix+28)
    ld      a, (ix+11)
    ld      (ix+24), a
    ld      a, (_RAM_C216_)
    add     a, (ix+18)
    ld      (ix+25), a
    xor     a
    ld      b, a
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 253D5 (indexed by unknown)
_LABEL_253FD_:
    ld      a, (ix+23)
    or      a
    ret     z
    inc     (ix+28)
    ret

; 3rd entry of Jump Table from 253D5 (indexed by unknown)
_LABEL_25406_:
    ld      a, (ix+18)
    or      a
    jr      nz, _LABEL_25419_
    ld      a, (ix+14)
    add     a, $40
    jr      nc, +
    ld      a, $FF
+:
    ld      (ix+14), a
    ret

_LABEL_25419_:
    ld      a, (_RAM_C076_)
    or      a
    jr      nz, +
    ld      (ix+29), $10
    inc     (ix+28)
    ret

+:
    ld      (ix+29), $30
    ld      (ix+28), $04
    ret

; 4th entry of Jump Table from 253D5 (indexed by unknown)
_LABEL_25430_:
    dec     (ix+29)
    ret     nz
    ld      a, (ix+24)
    ld      (ix+11), a
    ld      l, a
    ld      h, (ix+25)
    ld      de, (_RAM_C065_)
    or      a
    sbc     hl, de
    ld      (ix+18), h
    ld      (ix+11), l
    xor     a
    ld      (ix+14), a
    ld      (ix+15), a
-:
    ld      (ix+28), $01
    ret

; 5th entry of Jump Table from 253D5 (indexed by unknown)
_LABEL_25457_:
    ld      a, (ix+18)
    or      a
    jr      nz, +
    jr      -

+:
    dec     (ix+29)
    ret     nz
    jr      _LABEL_25419_

; 60th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25465_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, _DATA_25471_
    jp      call_jump_table

; Jump Table from 25471 to 25478 (4 entries, indexed by unknown)
_DATA_25471_:
.dw _LABEL_25479_ _LABEL_25488_ _LABEL_25491_ _LABEL_254B1_

; 1st entry of Jump Table from 25471 (indexed by unknown)
_LABEL_25479_:
    ld      hl, $AC94
    set     2, (ix+1)
    inc     (ix+28)
    xor     a
    ld      b, a
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25471 (indexed by unknown)
_LABEL_25488_:
    ld      a, (ix+23)
    or      a
    ret     z
    inc     (ix+28)
    ret

; 3rd entry of Jump Table from 25471 (indexed by unknown)
_LABEL_25491_:
    ld      a, (ix+18)
    cp      $10
    jr      z, ++
    ld      a, (ix+14)
    add     a, $40
    jr      nc, +
    ld      a, $FF
+:
    ld      (ix+14), a
    ret

++:
    ld      (ix+14), $00
    ld      (ix+15), $00
    inc     (ix+28)
    ret

; 4th entry of Jump Table from 25471 (indexed by unknown)
_LABEL_254B1_:
    ret

_LABEL_254B2_:
    ld      de, $0020
_LABEL_254B5_:
    call    _LABEL_2635E_
    ld      bc, $0000
    call    _LABEL_266F8_
    cp      $01
    jr      z, +
    cp      $02
    ret     nz
+:
    call    _LABEL_26377_
    xor     a
    ret

_LABEL_254CA_:
    ld      a, (ix+9)
    or      a
    call    nz, _LABEL_263ED_
    jp      _LABEL_263CD_

_LABEL_254D4_:
    ld      a, (ix+13)
    bit     7, (ix+17)
    jr      nz, +
    cp      $E0
    ccf
    ret

+:
    cp      $20
    ret

_LABEL_254E4_:
    ld      a, (ix+11)
    bit     7, (ix+15)
    jr      nz, +
    cp      $B0
    ccf
    ret

+:
    cp      $20
    ret

_LABEL_254F4_:
    ld      bc, $0808
    bit     7, (ix+17)
    jr      z, +
    ld      b, $F8
+:
    call    _LABEL_266F8_
    cp      $02
    ret

_LABEL_25505_:
    ld      bc, $10F0
    bit     7, (ix+17)
    jr      z, +
    ld      b, $F0
+:
    jp      _LABEL_266F8_

_LABEL_25513_:
    ld      bc, $0000
    bit     7, (ix+15)
    jr      z, +
    ld      c, $E0
+:
    jp      _LABEL_266F8_

; 73rd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25521_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $9536
    jp      call_jump_table

; Jump Table from 25536 to 2553B (3 entries, indexed by unknown)
_DATA_25536_:
.dw _LABEL_2553C_ _LABEL_25550_ _LABEL_255B7_

; 1st entry of Jump Table from 25536 (indexed by unknown)
_LABEL_2553C_:
    inc     (ix+28)
    call    _LABEL_25580_
    ld      (ix+29), $20
    ld      a, $01
    ld      b, $03
    ld      hl, $ACE6
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25536 (indexed by unknown)
_LABEL_25550_:
    ld      b, $50
    call    _LABEL_263A7_
    jr      c, _LABEL_25580_
    call    _LABEL_2522F_
    jr      nz, _LABEL_25580_
    dec     (ix+29)
    jr      nz, _LABEL_25580_
    inc     (ix+28)
    ld      hl, $0020
    call    _LABEL_263C3_
    call    _LABEL_263CA_
    ld      b, $05
    call    _LABEL_25223_
    ld      (ix+29), $07
    ld      hl, $ACA6
    ld      a, $01
    ld      b, $04
    jp      _LABEL_26315_

_LABEL_25580_:
    call    _LABEL_26857_
    and     $03
    jr      nz, +
    ld      hl, $FF80
    call    _LABEL_254CA_
+:
    call    _LABEL_26857_
    and     $03
    call    z, +
    call    _LABEL_25505_
    cp      $06
    call    nz, _LABEL_263CA_
    call    _LABEL_25513_
    cp      $06
    jp      nz, _LABEL_263C0_
    ret

+:
    ld      a, (_RAM_C20B_)
    sub     (ix+11)
    ld      hl, $FF90
    jr      c, +
    ld      hl, $0070
+:
    jp      _LABEL_263C3_

; 3rd entry of Jump Table from 25536 (indexed by unknown)
_LABEL_255B7_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $00
    ret

; 74th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_255C0_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $95D5
    jp      call_jump_table

; Jump Table from 255D5 to 255E0 (6 entries, indexed by unknown)
_DATA_255D5_:
.dw _LABEL_255E1_ _LABEL_255EB_ _LABEL_255F9_ _LABEL_2553C_ _LABEL_25550_ _LABEL_25610_

; 1st entry of Jump Table from 255D5 (indexed by unknown)
_LABEL_255E1_:
    inc     (ix+28)
    set     3, (ix+1)
    jp      _LABEL_2563D_

; 2nd entry of Jump Table from 255D5 (indexed by unknown)
_LABEL_255EB_:
    ld      b, $30
    call    _LABEL_263A7_
    ret     nc
    inc     (ix+28)
    res     3, (ix+1)
    ret

; 3rd entry of Jump Table from 255D5 (indexed by unknown)
_LABEL_255F9_:
    ld      de, $0020
    call    _LABEL_2635E_
    ld      bc, $0000
    call    _LABEL_266F8_
    cp      $06
    ret     nz
    inc     (ix+28)
    ld      b, $51
    jp      _LABEL_25223_

; 6th entry of Jump Table from 255D5 (indexed by unknown)
_LABEL_25610_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $03
    ret

; 75th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25619_:
    ld      a, (ix+18)
    or      a
    jp      nz, _LABEL_263F4_
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $962C
    jp      call_jump_table

; Jump Table from 2562C to 25631 (3 entries, indexed by unknown)
_DATA_2562C_:
.dw _LABEL_25632_ _LABEL_25643_ _LABEL_2565B_

; 1st entry of Jump Table from 2562C (indexed by unknown)
_LABEL_25632_:
    inc     (ix+28)
    set     3, (ix+1)
    set     2, (ix+1)
_LABEL_2563D_:
    ld      hl, $ACE6
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 2562C (indexed by unknown)
_LABEL_25643_:
    ld      b, $30
    call    _LABEL_263A7_
    ret     nc
    inc     (ix+28)
    res     3, (ix+1)
    ld      b, $51
    call    _LABEL_25223_
    ld      hl, $FC80
    jp      _LABEL_263C3_

; 3rd entry of Jump Table from 2562C (indexed by unknown)
_LABEL_2565B_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    jp      _LABEL_268BA_

; 81st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25667_:
    ld      a, (ix+28)
    ld      hl, $9670
    jp      call_jump_table

; Jump Table from 25670 to 25673 (2 entries, indexed by unknown)
_DATA_25670_:
.dw _LABEL_25674_ _LABEL_2569F_

; 1st entry of Jump Table from 25670 (indexed by unknown)
_LABEL_25674_:
    inc     (ix+28)
    ld      a, $AC
    ld      (Sound_MusicTrigger), a
    push    ix
    pop     iy
    ld      de, $FF40
    add     iy, de
    ld      a, (iy+13)
    ld      (ix+13), a
    ld      a, (iy+11)
    ld      (ix+11), a
    ld      (ix+29), $07
    ld      a, $01
    ld      b, $04
    ld      hl, $AD26
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25670 (indexed by unknown)
_LABEL_2569F_:
    dec     (ix+29)
    ret     nz
    jp      _LABEL_263F4_

; 76th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_256A6_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $96B8
    jp      call_jump_table

; Jump Table from 256B8 to 256BF (4 entries, indexed by unknown)
_DATA_256B8_:
.dw _LABEL_256C0_ _LABEL_256CD_ _LABEL_256DE_ _LABEL_2570A_

; 1st entry of Jump Table from 256B8 (indexed by unknown)
_LABEL_256C0_:
    inc     (ix+28)
    ld      (ix+29), $40
    ld      hl, $AD3E
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 256B8 (indexed by unknown)
_LABEL_256CD_:
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $0A
    inc     (ix+28)
    ld      hl, $FF00
    jp      _LABEL_263CD_

; 3rd entry of Jump Table from 256B8 (indexed by unknown)
_LABEL_256DE_:
    call    _LABEL_26335_
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $20
    call    _LABEL_263CA_
    inc     (ix+28)
    xor     a
    ld      (_RAM_C0B0_), a
    ld      hl, _RAM_C3C0_
    ld      de, $0020
    ld      b, $04
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), $4D
+:
    add     hl, de
    djnz    -
    ld      hl, $AD42
    jp      _LABEL_26313_

; 4th entry of Jump Table from 256B8 (indexed by unknown)
_LABEL_2570A_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $00
    ret

; 77th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25713_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $971F
    jp      call_jump_table

; Jump Table from 2571F to 25722 (2 entries, indexed by unknown)
_DATA_2571F_:
.dw _LABEL_25723_ _LABEL_25761_

; 1st entry of Jump Table from 2571F (indexed by unknown)
_LABEL_25723_:
    inc     (ix+28)
    ld      hl, (_RAM_C30A_)
    ld      de, $E800
    add     hl, de
    ld      (ix+10), l
    ld      (ix+11), h
    ld      hl, (_RAM_C30C_)
    ld      (ix+12), l
    ld      (ix+13), h
    push    ix
    pop     hl
    ld      de, $000E
    add     hl, de
    ex      de, hl
    ld      hl, _RAM_C0B0_
    ld      a, (hl)
    inc     (hl)
    add     a, a
    add     a, a
    ld      hl, $976E
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ldi
    ldi
    ldi
    ldi
    ld      hl, $AD62
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 2571F (indexed by unknown)
_LABEL_25761_:
    call    _LABEL_254D4_
    jp      c, _LABEL_263F4_
    call    _LABEL_254E4_
    jp      c, _LABEL_263F4_
    ret

; Data from 2576E to 2577D (16 bytes)
.db $80 $FF $A0 $FE $A0 $FE $C0 $FF $A0 $FE $40 $00 $80 $FF $60 $01

; 78th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2577E_:
    call    _LABEL_26552_
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $978D
    jp      call_jump_table

; Jump Table from 2578D to 25792 (3 entries, indexed by unknown)
_DATA_2578D_:
.dw _LABEL_25793_ _LABEL_257AB_ _LABEL_257D2_

; 1st entry of Jump Table from 2578D (indexed by unknown)
_LABEL_25793_:
    inc     (ix+28)
    ld      (ix+23), $01
    call    _LABEL_26857_
    and     $3F
    ld      b, $10
    add     a, b
    ld      (ix+29), a
    ld      hl, $98E1
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 2578D (indexed by unknown)
_LABEL_257AB_:
    call    _LABEL_264C1_
    jr      nc, +
    ld      a, $A2
    ld      (Sound_MusicTrigger), a
+:
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $40
    inc     (ix+28)
    ld      (ix+23), $00
    call    _LABEL_2522F_
    ld      b, $4F
    call    z, _LABEL_25223_
    ld      hl, $98E5
    jp      _LABEL_26313_

; 3rd entry of Jump Table from 2578D (indexed by unknown)
_LABEL_257D2_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $00
    ret

; 79th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_257E1_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $97ED
    jp      call_jump_table

; Jump Table from 257ED to 257F0 (2 entries, indexed by unknown)
_DATA_257ED_:
.dw _LABEL_257F1_ _LABEL_25761_

; 1st entry of Jump Table from 257ED (indexed by unknown)
_LABEL_257F1_:
    inc     (ix+28)
    push    ix
    pop     hl
    ld      de, $FF4B
    add     hl, de
    ld      a, (hl)
    ld      (ix+11), a
    inc     hl
    inc     hl
    ld      a, (hl)
    ld      (ix+13), a
    call    _LABEL_31_
    ld      hl, $AD62
    jp      _LABEL_26313_

; 80th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2580E_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $981A
    jp      call_jump_table

; Jump Table from 2581A to 25821 (4 entries, indexed by unknown)
_DATA_2581A_:
.dw _LABEL_25822_ _LABEL_2582F_ _LABEL_25879_ _LABEL_258A8_

; 1st entry of Jump Table from 2581A (indexed by unknown)
_LABEL_25822_:
    inc     (ix+28)
    ld      (ix+30), $02
_LABEL_25829_:
    ld      hl, $AD6B
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 2581A (indexed by unknown)
_LABEL_2582F_:
    call    _LABEL_264C1_
    jr      c, _LABEL_25892_
    ld      a, (ix+17)
    or      a
    jr      nz, ++
    ld      hl, $FF00
    ld      a, (ix+13)
    cp      $80
    jr      nc, +
    ld      hl, $0101
+:
    ld      (ix+9), l
    ld      l, $00
    call    _LABEL_263CD_
++:
    ld      bc, $F0F8
    call    _LABEL_258C4_
    jp      nz, _LABEL_263CA_
    call    _LABEL_254D4_
    jp      c, _LABEL_26335_
    call    _LABEL_258D4_
    ret     nz
    inc     (ix+28)
    call    _LABEL_26398_
    ld      hl, $FE00
    or      a
    jr      z, +
    ld      h, $02
+:
    call    _LABEL_263CD_
    ld      hl, $AD6F
    jp      _LABEL_26313_

; 3rd entry of Jump Table from 2581A (indexed by unknown)
_LABEL_25879_:
    call    _LABEL_264C1_
    jr      c, _LABEL_25892_
    ld      bc, $ECF8
    call    _LABEL_258C4_
    jr      nz, +
    call    _LABEL_254D4_
    ret     nc
+:
    dec     (ix+28)
-:
    call    _LABEL_263CA_
    jr      _LABEL_25829_

_LABEL_25892_:
    dec     (ix+30)
    jp      z, _LABEL_26349_
    ld      a, $A2
    ld      (Sound_MusicTrigger), a
    ld      (ix+28), $03
    ld      (ix+29), $10
    jp      _LABEL_2633D_

; 4th entry of Jump Table from 2581A (indexed by unknown)
_LABEL_258A8_:
    ld      bc, $14F8
    bit     7, (ix+17)
    jr      z, +
    ld      b, $EC
+:
    call    _LABEL_266F8_
    cp      $06
    jr      nz, +
    dec     (ix+29)
    ret     nz
+:
    ld      (ix+28), $01
    jr      -

_LABEL_258C4_:
    ld      a, (ix+9)
    or      a
    jr      z, +
    ld      a, b
    neg
    ld      b, a
+:
    call    _LABEL_266F8_
    cp      $06
    ret

_LABEL_258D4_:
    ld      a, (_RAM_C20B_)
    and     $E0
    ld      b, a
    ld      a, (ix+11)
    and     $E0
    cp      b
    ret

; Data from 258E1 to 25908 (40 bytes)
.db $E9 $98 $F1 $98 $F9 $98 $01 $99 $02 $18 $F0 $F8 $88 $F0 $00 $8A
.db $02 $18 $F0 $F8 $D2 $F0 $00 $D0 $02 $18 $F0 $F8 $8C $F0 $00 $8E
.db $02 $18 $F0 $F8 $D6 $F0 $00 $D4

; 82nd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25909_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $991B
    jp      call_jump_table

; Jump Table from 2591B to 25920 (3 entries, indexed by unknown)
_DATA_2591B_:
.dw _LABEL_25921_ _LABEL_25932_ _LABEL_25952_

; 1st entry of Jump Table from 2591B (indexed by unknown)
_LABEL_25921_:
    inc     (ix+28)
    set     3, (ix+1)
    ld      a, $01
    ld      b, $03
    ld      hl, $ADAB
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 2591B (indexed by unknown)
_LABEL_25932_:
    call    _LABEL_2597A_
    ret     nc
    ld      b, $18
    call    _LABEL_263A7_
    ret     c
    inc     (ix+28)
    res     3, (ix+1)
    call    _LABEL_26398_
    ld      hl, $FF00
    call    _LABEL_254CA_
    ld      hl, $FD00
    jp      _LABEL_263C3_

; 3rd entry of Jump Table from 2591B (indexed by unknown)
_LABEL_25952_:
    call    _LABEL_254D4_
    call    c, _LABEL_26335_
    call    _LABEL_254E4_
    jr      c, +
    ld      de, $0030
    call    _LABEL_2635E_
    bit     7, h
    ret     nz
    ld      a, (_RAM_C222_)
    bit     0, a
    ret     nz
    call    _LABEL_2597A_
    ret     nc
+:
    ld      (ix+28), $00
    call    _LABEL_263C0_
    jp      _LABEL_263CA_

_LABEL_2597A_:
    ld      a, (_RAM_C20B_)
    add     a, $18
    sub     (ix+11)
    ret

; 89th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25983_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9995
    jp      call_jump_table

; Jump Table from 25995 to 2599A (3 entries, indexed by unknown)
_DATA_25995_:
.dw _LABEL_2599B_ _LABEL_259A8_ _LABEL_259B9_

; 1st entry of Jump Table from 25995 (indexed by unknown)
_LABEL_2599B_:
    inc     (ix+28)
    set     2, (ix+1)
    call    _LABEL_259F6_
    jp      _LABEL_25AE9_

; 2nd entry of Jump Table from 25995 (indexed by unknown)
_LABEL_259A8_:
    call    +
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $08
    jp      _LABEL_263CA_

; 3rd entry of Jump Table from 25995 (indexed by unknown)
_LABEL_259B9_:
    dec     (ix+29)
    ret     nz
    dec     (ix+28)
    call    _LABEL_259F6_
    ld      a, (ix+18)
    or      (ix+19)
    ret     nz
    call    _LABEL_2522F_
    ret     nz
    ld      b, $5A
    jp      _LABEL_25223_

+:
    ld      a, (ix+19)
    or      a
    jr      z, ++
    ld      hl, $0201
    rlca
    jr      c, +
    ld      hl, $FE00
+:
    ld      (ix+9), l
    jp      _LABEL_263CD_

++:
    ld      a, (ix+17)
    or      a
    ret     nz
    call    _LABEL_26398_
    ld      hl, $FE00
    jp      _LABEL_254CA_

_LABEL_259F6_:
    call    _LABEL_26857_
    and     $0F
    add     a, $10
    ld      (ix+29), a
    ret

; 90th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25A01_:
    call    _LABEL_26552_
    ld      a, (ix+19)
    or      a
    jp      nz, _LABEL_263F4_
    ld      a, (ix+28)
    ld      hl, $9A14
    jp      call_jump_table

; Jump Table from 25A14 to 25A17 (2 entries, indexed by unknown)
_DATA_25A14_:
.dw _LABEL_25A18_ _LABEL_25A4A_

; 1st entry of Jump Table from 25A14 (indexed by unknown)
_LABEL_25A18_:
    inc     (ix+28)
    push    ix
    pop     hl
    ld      de, $FF4B
    add     hl, de
    ld      a, (hl)
    ld      (ix+11), a
    inc     hl
    inc     hl
    ld      a, (hl)
    ld      (ix+13), a
    ld      hl, $0300
    call    _LABEL_263C3_
    set     2, (ix+1)
    ld      hl, $AE07
    ld      a, (StageNumber)
    cp      $02
    jr      z, +
    ld      hl, $B645
+:
    ld      a, $01
    ld      b, $03
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25A14 (indexed by unknown)
_LABEL_25A4A_:
    call    _LABEL_25513_
    cp      $02
    jp      z, _LABEL_263F4_
    call    _LABEL_254E4_
    jp      c, _LABEL_263F4_
    ret

; 91st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25A59_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9A70
    rst     $08    ; call_jump_table
    call    _LABEL_254D4_
    ret     nc
    jp      _LABEL_263CA_

; Jump Table from 25A70 to 25A77 (4 entries, indexed by unknown)
_DATA_25A70_:
.dw _LABEL_25A78_ _LABEL_25A82_ _LABEL_25A98_ _LABEL_25AD1_

; 1st entry of Jump Table from 25A70 (indexed by unknown)
_LABEL_25A78_:
    inc     (ix+28)
    set     2, (ix+1)
    jp      _LABEL_25AE9_

; 2nd entry of Jump Table from 25A70 (indexed by unknown)
_LABEL_25A82_:
    call    +
    ld      de, $FFD0
    call    _LABEL_263D4_
    bit     7, h
    ret     z
    ld      de, $0100
    add     hl, de
    ret     c
    ld      (ix+28), $02
    ret

; 3rd entry of Jump Table from 25A70 (indexed by unknown)
_LABEL_25A98_:
    call    +
    ld      de, $0030
    call    _LABEL_263D4_
    bit     7, h
    ret     nz
    ld      de, $0100
    or      a
    sbc     hl, de
    ret     c
    ld      (ix+28), $01
    ret

+:
    call    _LABEL_26398_
    ld      a, (ix+18)
    or      (ix+19)
    ret     nz
    ld      b, $30
    call    _LABEL_263A7_
    ret     nc
    pop     af
    ld      (ix+28), $03
    ld      hl, $0300
    call    _LABEL_263C3_
    ld      hl, $FF40
    jp      _LABEL_254CA_

; 4th entry of Jump Table from 25A70 (indexed by unknown)
_LABEL_25AD1_:
    ld      de, $FFD0
    call    _LABEL_263D4_
    bit     7, h
    ret     z
    ld      de, $0300
    add     hl, de
    ret     c
    ld      (ix+28), $02
    call    _LABEL_263C0_
    jp      _LABEL_263CA_

_LABEL_25AE9_:
    ld      a, (StageNumber)
    sub     $02
    ld      hl, $9AF9
    rst     $18    ; get_array_index
    ld      a, $01
    ld      b, $03
    jp      _LABEL_26315_

; Data from 25AF9 to 25AFC (4 bytes)
.db $D3 $AD $11 $B6

; 87th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25AFD_:
    ld      a, (_RAM_C313_)
    or      a
    jr      z, ++
    call    _LABEL_263C0_
    ld      hl, $0140
    bit     7, a
    jr      nz, +
    ld      hl, $FEC0
+:
    jp      _LABEL_263CD_

++:
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $9B1F
    jp      call_jump_table

; Jump Table from 25B1F to 25B2A (6 entries, indexed by unknown)
_DATA_25B1F_:
.dw _LABEL_25B2B_ _LABEL_25B38_ _LABEL_25B71_ _LABEL_25B40_ _LABEL_25BB8_ _LABEL_25BD9_

; 1st entry of Jump Table from 25B1F (indexed by unknown)
_LABEL_25B2B_:
    inc     (ix+28)
    ld      (ix+30), $04
    set     2, (ix+1)
    jr      _LABEL_25B87_

; 2nd entry of Jump Table from 25B1F (indexed by unknown)
_LABEL_25B38_:
    call    _LABEL_26552_
    call    _LABEL_264C1_
    jr      c, _LABEL_25BA1_
; 4th entry of Jump Table from 25B1F (indexed by unknown)
_LABEL_25B40_:
    call    _LABEL_254D4_
    call    c, _LABEL_26335_
    ld      hl, $B0AB
    bit     7, (ix+15)
    jr      nz, +
    ld      hl, $B06D
+:
    call    _LABEL_26313_
    ld      de, $0040
    call    _LABEL_254B5_
    ret     nz
    ld      (ix+28), $02
    call    _LABEL_263CA_
    ld      (ix+29), $08
    ld      (ix+31), $06
    ld      hl, $B0E9
    jp      _LABEL_26313_

; 3rd entry of Jump Table from 25B1F (indexed by unknown)
_LABEL_25B71_:
    call    _LABEL_26552_
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $08
    call    _LABEL_25BE0_
    dec     (ix+31)
    ret     nz
    ld      (ix+28), $01
_LABEL_25B87_:
    ld      hl, $FC00
    call    _LABEL_26857_
    rlca
    jr      c, +
    ld      h, $FD
+:
    call    _LABEL_263C3_
    ld      hl, $FEE0
    call    _LABEL_254CA_
    ld      hl, $B0AB
    jp      _LABEL_26313_

_LABEL_25BA1_:
    ld      hl, $0160
    call    _LABEL_254CA_
    dec     (ix+30)
    ld      a, $03
    jr      nz, +
    inc     a
+:
    ld      (ix+28), a
    ld      a, $A8
    ld      (Sound_MusicTrigger), a
    ret

; 5th entry of Jump Table from 25B1F (indexed by unknown)
_LABEL_25BB8_:
    call    _LABEL_254D4_
    call    c, _LABEL_26335_
    ld      de, $0040
    call    _LABEL_254B5_
    ret     nz
    inc     (ix+28)
    call    _LABEL_263CA_
    ld      (ix+29), $23
    ld      a, $01
    ld      b, $06
    ld      hl, $B11B
    jp      _LABEL_26315_

; 6th entry of Jump Table from 25B1F (indexed by unknown)
_LABEL_25BD9_:
    dec     (ix+29)
    ret     nz
    jp      _LABEL_26876_

_LABEL_25BE0_:
    ld      hl, _RAM_C320_
    ld      de, $0020
    ld      b, $08
-:
    ld      a, (hl)
    or      a
    jr      nz, +
    ld      (hl), $58
    ret

+:
    add     hl, de
    djnz    -
    ret

; 88th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25BF3_:
    call    _LABEL_26552_
    jp      c, _LABEL_263F4_
    ld      a, (ix+19)
    or      a
    jp      nz, _LABEL_263F4_
    ld      a, (ix+28)
    ld      hl, $9C09
    jp      call_jump_table

; Jump Table from 25C09 to 25C0C (2 entries, indexed by unknown)
_DATA_25C09_:
.dw _LABEL_25C0D_ _LABEL_25C41_

; 1st entry of Jump Table from 25C09 (indexed by unknown)
_LABEL_25C0D_:
    inc     (ix+28)
    ld      a, (_RAM_C30D_)
    ld      (ix+13), a
    ld      a, (_RAM_C30B_)
    add     a, $E0
    ld      (ix+11), a
    ld      hl, $FB00
    call    _LABEL_263C3_
    call    _LABEL_26857_
    and     $07
    ld      hl, $9C53
    rst     $18    ; get_array_index
    ld      (ix+16), l
    ld      (ix+17), h
    set     2, (ix+1)
    ld      a, $01
    ld      b, $02
    ld      hl, $B18B
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25C09 (indexed by unknown)
_LABEL_25C41_:
    ld      a, (_RAM_C31C_)
    cp      $04
    jp      nc, _LABEL_263F4_
    ld      de, $0060
    call    _LABEL_254B5_
    ret     nz
    jp      _LABEL_263F4_

; Data from 25C53 to 25C62 (16 bytes)
.db $C0 $FE $00 $FF $40 $FF $A0 $FF $40 $01 $00 $01 $C0 $00 $60 $00

; 96th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25C63_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $9C78
    jp      call_jump_table

; Jump Table from 25C78 to 25C83 (6 entries, indexed by unknown)
_DATA_25C78_:
.dw _LABEL_25C84_ _LABEL_25C91_ _LABEL_25CAC_ _LABEL_25CC5_ _LABEL_25CEB_ _LABEL_25D14_

; 1st entry of Jump Table from 25C78 (indexed by unknown)
_LABEL_25C84_:
    inc     (ix+28)
    set     3, (ix+1)
    ld      hl, $B657
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 25C78 (indexed by unknown)
_LABEL_25C91_:
    ld      b, $40
    call    _LABEL_263A7_
    ret     nc
    inc     (ix+28)
    res     3, (ix+1)
    ld      (ix+29), $17
    ld      a, $02
    ld      b, $08
    ld      hl, $B657
    jp      _LABEL_26315_

; 3rd entry of Jump Table from 25C78 (indexed by unknown)
_LABEL_25CAC_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
_LABEL_25CB3_:
    ld      hl, $FE80
    call    _LABEL_263C3_
    ld      hl, $FF00
    call    _LABEL_254CA_
    ld      hl, $B6AB
    jp      _LABEL_26313_

; 4th entry of Jump Table from 25C78 (indexed by unknown)
_LABEL_25CC5_:
    ld      de, $0020
    call    _LABEL_2635E_
    bit     7, h
    ret     nz
    call    _LABEL_254E4_
    jr      c, +
    ld      bc, $0000
    call    _LABEL_266F8_
    or      a
    ret     z
+:
    call    _LABEL_26374_
    inc     (ix+28)
    ld      (ix+29), $06
    ld      hl, $B6CB
    jp      _LABEL_26313_

; 5th entry of Jump Table from 25C78 (indexed by unknown)
_LABEL_25CEB_:
    call    +
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $03
    jr      _LABEL_25CB3_

+:
    call    _LABEL_2522F_
    ret     nz
    ld      b, $61
    call    _LABEL_25223_
    pop     af
    ld      (ix+28), $05
    ld      (ix+29), $0F
    ld      a, $02
    ld      b, $08
    ld      hl, $B6EB
    jp      _LABEL_26315_

; 6th entry of Jump Table from 25C78 (indexed by unknown)
_LABEL_25D14_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $00
    ret

; 97th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25D1D_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9D29
    jp      call_jump_table

; Jump Table from 25D29 to 25D2C (2 entries, indexed by unknown)
_DATA_25D29_:
.dw _LABEL_25D2D_ _LABEL_25D59_

; 1st entry of Jump Table from 25D29 (indexed by unknown)
_LABEL_25D2D_:
    inc     (ix+28)
    set     2, (ix+1)
    push    ix
    pop     hl
    ld      de, $FF49
    add     hl, de
    ld      a, (hl)
    ld      (ix+9), a
    inc     hl
    inc     hl
    ld      a, (hl)
    add     a, $F0
    ld      (ix+11), a
    inc     hl
    inc     hl
    ld      a, (hl)
    ld      (ix+13), a
    ld      hl, $FE00
    call    _LABEL_254CA_
    ld      hl, $B657
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 25D29 (indexed by unknown)
_LABEL_25D59_:
    ld      a, (ix+19)
    or      a
    jr      nz, +
    call    _LABEL_25505_
    or      a
    ret     z
+:
    jp      _LABEL_263F4_

; 98th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25D67_:
    call    _LABEL_264BB_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9D76
    jp      call_jump_table

; Jump Table from 25D76 to 25D7F (5 entries, indexed by unknown)
_DATA_25D76_:
.dw _LABEL_25D80_ _LABEL_25D9E_ _LABEL_25DDE_ _LABEL_25E06_ _LABEL_25E14_

; 1st entry of Jump Table from 25D76 (indexed by unknown)
_LABEL_25D80_:
    inc     (ix+28)
    ld      (ix+23), $01
_LABEL_25D87_:
    ld      (ix+29), $30
    call    _LABEL_26398_
    ld      hl, $FFA0
    call    _LABEL_254CA_
    ld      a, $01
    ld      b, $06
    ld      hl, $B73F
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25D76 (indexed by unknown)
_LABEL_25D9E_:
    call    _LABEL_258D4_
    jr      nz, +
    ld      b, $40
    call    _LABEL_263A7_
    jr      c, ++
+:
    ld      a, (ix+16)
    or      (ix+17)
    jr      z, _LABEL_25D87_
_LABEL_25DB2_:
    call    _LABEL_254B2_
    call    _LABEL_254F4_
    jr      nz, +
    call    _LABEL_25505_
    or      a
    jr      nz, +
    dec     (ix+29)
    ret     nz
+:
    ld      (ix+29), $20
    jp      _LABEL_26335_

++:
    inc     (ix+28)
    call    _LABEL_26398_
    ld      hl, $FE80
    call    _LABEL_254CA_
    ld      a, $01
    ld      b, $03
    jp      _LABEL_2631B_

; 3rd entry of Jump Table from 25D76 (indexed by unknown)
_LABEL_25DDE_:
    ld      b, $20
    call    _LABEL_263A7_
    jr      c, +
    call    _LABEL_25505_
    or      a
    jr      nz, +
    call    _LABEL_254F4_
    ret     z
+:
    inc     (ix+28)
    call    _LABEL_26398_
    call    _LABEL_263CA_
    ld      (ix+29), $2F
    ld      a, $01
    ld      b, $06
    ld      hl, $B77F
    jp      _LABEL_26315_

; 4th entry of Jump Table from 25D76 (indexed by unknown)
_LABEL_25E06_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+31), $20
    jp      _LABEL_25D87_

; 5th entry of Jump Table from 25D76 (indexed by unknown)
_LABEL_25E14_:
    call    _LABEL_264AB_
    jp      c, _LABEL_26349_
    dec     (ix+31)
    jr      nz, _LABEL_25DB2_
    ld      (ix+28), $01
    jp      _LABEL_263CA_

; 99th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25E26_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9E38
    jp      call_jump_table

; Jump Table from 25E38 to 25E3B (2 entries, indexed by unknown)
_DATA_25E38_:
.dw _LABEL_25E3C_ _LABEL_25E49_

; 1st entry of Jump Table from 25E38 (indexed by unknown)
_LABEL_25E3C_:
    inc     (ix+28)
    ld      a, $01
    ld      b, $06
    ld      hl, $B7CB
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25E38 (indexed by unknown)
_LABEL_25E49_:
    call    _LABEL_25505_
    or      a
    call    nz, _LABEL_26335_
    ld      de, $0040
    call    _LABEL_2635E_
    ld      bc, $0000
    call    _LABEL_266F8_
    or      a
    ret     z
    call    _LABEL_26398_
    ld      hl, $FE80
    call    _LABEL_254CA_
    ld      hl, $FE40
    call    _LABEL_263C3_
    ld      b, $51
    jp      _LABEL_25223_

; 100th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25E72_:
    call    _LABEL_264C1_
    jp      c, _LABEL_26349_
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9EA0
    rst     $08    ; call_jump_table
    call    _LABEL_25505_
    cp      $04
    call    nz, _LABEL_26335_
    call    _LABEL_254D4_
    call    c, _LABEL_26335_
    ld      a, (ix+17)
    or      (ix+16)
    ret     nz
    call    _LABEL_26398_
    ld      hl, $FF40
    jp      _LABEL_254CA_

; Jump Table from 25EA0 to 25EA5 (3 entries, indexed by unknown)
_DATA_25EA0_:
.dw _LABEL_25EA6_ _LABEL_25EB3_ _LABEL_25EC6_

; 1st entry of Jump Table from 25EA0 (indexed by unknown)
_LABEL_25EA6_:
    inc     (ix+28)
    ld      a, $01
    ld      b, $06
    ld      hl, $B7CB
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25EA0 (indexed by unknown)
_LABEL_25EB3_:
    ld      de, $FFD0
    call    _LABEL_263D4_
    bit     7, h
    ret     z
    ld      de, $0100
    add     hl, de
    ret     c
    ld      (ix+28), $02
    ret

; 3rd entry of Jump Table from 25EA0 (indexed by unknown)
_LABEL_25EC6_:
    ld      de, $0030
    call    _LABEL_263D4_
    bit     7, h
    ret     nz
    ld      de, $0100
    or      a
    sbc     hl, de
    ret     c
    ld      (ix+28), $01
    ret

; 101st entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25EDB_:
    ld      a, (ix+28)
    ld      hl, $9EE4
    jp      call_jump_table

; Jump Table from 25EE4 to 25EE7 (2 entries, indexed by unknown)
_DATA_25EE4_:
.dw _LABEL_25EE8_ _LABEL_25EF9_

; 1st entry of Jump Table from 25EE4 (indexed by unknown)
_LABEL_25EE8_:
    inc     (ix+28)
    ld      (ix+29), $30
    set     3, (ix+1)
    ld      hl, $B7CB
    jp      _LABEL_26313_

; 2nd entry of Jump Table from 25EE4 (indexed by unknown)
_LABEL_25EF9_:
    dec     (ix+29)
    ret     nz
    ld      (ix+29), $30
    ld      b, $12
    call    _LABEL_263A7_
    jr      nc, +
    ld      a, (_RAM_C20B_)
    add     a, $08
    and     $F0
    ld      b, a
    ld      a, (ix+11)
    sub     $08
    and     $F0
    cp      b
    ret     nz
+:
    call    _LABEL_2522F_
    ret     nz
    ld      b, $66
    jp      _LABEL_25223_

; 102nd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25F22_:
    call    _LABEL_26552_
    ld      a, (ix+19)
    or      (ix+18)
    jp      nz, _LABEL_263F4_
    ld      a, (ix+28)
    ld      hl, $9F37
    jp      call_jump_table

; Jump Table from 25F37 to 25F3E (4 entries, indexed by unknown)
_DATA_25F37_:
.dw _LABEL_25F3F_ _LABEL_25F6E_ _LABEL_25FA8_ _LABEL_25FDB_

; 1st entry of Jump Table from 25F37 (indexed by unknown)
_LABEL_25F3F_:
    inc     (ix+28)
    push    ix
    pop     hl
    ld      de, $000A
    add     hl, de
    ld      d, h
    ld      e, l
    ld      bc, $FF40
    add     hl, bc
    ldi
    ldi
    ldi
    ldi
    call    _LABEL_26398_
    ld      hl, $FEE0
    call    _LABEL_254CA_
    set     2, (ix+1)
    ld      a, $01
    ld      b, $03
    ld      hl, $B7F3
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 25F37 (indexed by unknown)
_LABEL_25F6E_:
    ld      bc, $0AF0
    bit     7, (ix+17)
    jr      z, +
    ld      b, $F6
+:
    call    _LABEL_266F8_
    or      a
    jr      nz, _LABEL_25FC6_
    ld      de, $0020
    call    _LABEL_2635E_
    ld      bc, $0000
    call    _LABEL_266F8_
    or      a
    ret     z
    cp      $02
    jp      z, _LABEL_26377_
    inc     (ix+28)
    cp      $06
    ld      hl, $00C0
    jr      z, +
    ld      hl, $FF40
+:
    call    _LABEL_263CD_
    ld      hl, $00C0
    jp      _LABEL_263C3_

; 3rd entry of Jump Table from 25F37 (indexed by unknown)
_LABEL_25FA8_:
    ld      bc, $0000
    call    _LABEL_266F8_
    cp      $02
    ret     nz
    dec     (ix+28)
    ld      hl, $FEE0
    bit     7, (ix+17)
    jr      nz, +
    ld      hl, $0120
+:
    call    _LABEL_263CD_
    jp      _LABEL_263C0_

_LABEL_25FC6_:
    ld      (ix+28), $03
    call    _LABEL_263CA_
    ld      (ix+29), $0B
    ld      a, $01
    ld      b, $06
    ld      hl, $B80B
    jp      _LABEL_26315_

; 4th entry of Jump Table from 25F37 (indexed by unknown)
_LABEL_25FDB_:
    dec     (ix+29)
    ret     nz
    jp      _LABEL_263F4_

; 103rd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_25FE2_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $9FEE
    jp      call_jump_table

; Jump Table from 25FEE to 25FF7 (5 entries, indexed by unknown)
_DATA_25FEE_:
.dw _LABEL_25FF8_ _LABEL_26002_ _LABEL_26022_ _LABEL_26036_ _LABEL_2604D_

; 1st entry of Jump Table from 25FEE (indexed by unknown)
_LABEL_25FF8_:
    inc     (ix+28)
    ld      a, (ix+11)
    ld      (ix+31), a
    ret

; 2nd entry of Jump Table from 25FEE (indexed by unknown)
_LABEL_26002_:
    inc     (ix+28)
    ld      a, (ix+31)
    ld      (ix+11), a
    ld      (ix+10), $00
    ld      hl, $0200
    ld      (ix+26), l
    ld      (ix+27), h
    ld      (ix+29), $20
    ld      hl, $B82B
    jp      _LABEL_26313_

; 3rd entry of Jump Table from 25FEE (indexed by unknown)
_LABEL_26022_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $07
    ld      l, (ix+26)
    ld      h, (ix+27)
    jp      _LABEL_263C3_

; 4th entry of Jump Table from 25FEE (indexed by unknown)
_LABEL_26036_:
    dec     (ix+29)
    ret     nz
    inc     (ix+28)
    ld      (ix+29), $07
    ld      l, (ix+26)
    ld      h, (ix+27)
    call    _LABEL_263ED_
    jp      _LABEL_263C3_

; 5th entry of Jump Table from 25FEE (indexed by unknown)
_LABEL_2604D_:
    dec     (ix+29)
    ret     nz
    ld      (ix+28), $01
    jp      _LABEL_263C0_

; 104th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_26058_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $A064
    jp      call_jump_table

; Jump Table from 26064 to 2606D (5 entries, indexed by unknown)
_DATA_26064_:
.dw _LABEL_25FF8_ _LABEL_2606E_ _LABEL_26022_ _LABEL_26036_ _LABEL_2604D_

; 2nd entry of Jump Table from 26064 (indexed by unknown)
_LABEL_2606E_:
    inc     (ix+28)
    ld      a, (ix+31)
    ld      (ix+11), a
    ld      (ix+10), $00
    ld      hl, $FE00
    ld      (ix+26), l
    ld      (ix+27), h
    ld      (ix+29), $20
    ld      hl, $B837
    jp      _LABEL_26313_

; 107th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2608E_:
    call    _LABEL_26552_
    call    _LABEL_26398_
    ld      a, (ix+28)
    ld      hl, $A0AC
    rst     $08    ; call_jump_table
    call    _LABEL_254D4_
    jr      c, +
    call    _LABEL_25505_
    or      a
    ret     z
+:
    ld      (ix+28), $01
    jp      _LABEL_263CA_

; Data from 260AC to 260FD (82 bytes)
.db $B4 $A0 $C1 $A0 $E0 $A0 $F3 $A0 $DD $34 $1C $3E $02 $06 $03 $21
.db $43 $B8 $C3 $15 $A3 $06 $40 $CD $A7 $A3 $D0 $DD $34 $1C $DD $36
.db $1D $10 $21 $00 $FE $CD $CA $94 $3E $FF $CB $7C $28 $02 $3E $01
.db $DD $77 $1A $C9 $DD $35 $1D $C0 $DD $34 $1C $DD $36 $1D $20 $DD
.db $66 $1A $2E $00 $C3 $CD $A3 $DD $35 $1D $C0 $DD $36 $1C $01 $C3
.db $CA $A3

; 29th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_260FE_:
    call    _LABEL_26552_
    call    _LABEL_264C1_
    jr      nc, +
    ld      a, $A2
    ld      (Sound_MusicTrigger), a
+:
    ld      a, (ix+28)
    ld      hl, $A114
    jp      call_jump_table

; Jump Table from 26114 to 26117 (2 entries, indexed by unknown)
_DATA_26114_:
.dw _LABEL_26118_ _LABEL_2613F_

; 1st entry of Jump Table from 26114 (indexed by unknown)
_LABEL_26118_:
    inc     (ix+28)
    ld      a, (ix+13)
    cp      $80
    ld      a, $01
    ld      hl, $0100
    jr      c, +
    ld      a, $00
    ld      hl, $FF00
+:
    ld      (ix+16), l
    ld      (ix+17), h
    ld      (ix+9), a
    ld      a, $02
    ld      b, $03
    ld      hl, $B843
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 26114 (indexed by unknown)
_LABEL_2613F_:
    ld      a, (ix+9)
    or      a
    ld      a, (ix+13)
    jr      z, +
    cp      $E8
    jp      nc, _LABEL_26335_
    ret

+:
    cp      $38
    jp      c, _LABEL_26335_
    ret

; 40th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_26154_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, $A160
    jp      call_jump_table

; Jump Table from 26160 to 26169 (5 entries, indexed by unknown)
_DATA_26160_:
.dw _LABEL_2616A_ _LABEL_253FD_ _LABEL_25406_ _LABEL_25430_ _LABEL_25457_

; 1st entry of Jump Table from 26160 (indexed by unknown)
_LABEL_2616A_:
    ld      a, (_RAM_C04E_)
    ld      hl, $B272
    cp      $03
    jr      c, +
    ld      hl, $B27E
+:
    jp      _LABEL_253E2_

; 43rd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2617A_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, $A186
    jp      call_jump_table

; Jump Table from 26186 to 2618F (5 entries, indexed by unknown)
_DATA_26186_:
.dw _LABEL_26190_ _LABEL_253FD_ _LABEL_25406_ _LABEL_25430_ _LABEL_25457_

; 1st entry of Jump Table from 26186 (indexed by unknown)
_LABEL_26190_:
    ld      hl, $B28A
    jp      _LABEL_253E2_

; 59th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_26196_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, $A1A2
    jp      call_jump_table

; Jump Table from 261A2 to 261AB (5 entries, indexed by unknown)
_DATA_261A2_:
.dw _LABEL_261AC_ _LABEL_253FD_ _LABEL_25406_ _LABEL_25430_ _LABEL_25457_

; 1st entry of Jump Table from 261A2 (indexed by unknown)
_LABEL_261AC_:
    ld      hl, $B296
    jp      _LABEL_253E2_

; 42nd entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_261B2_:
    call    _LABEL_26552_
    ld      a, (ix+28)
    ld      hl, $A1BE
    jp      call_jump_table

; Jump Table from 261BE to 261C3 (3 entries, indexed by unknown)
_DATA_261BE_:
.dw _LABEL_261C4_ _LABEL_261D3_ _LABEL_261DC_

; 1st entry of Jump Table from 261BE (indexed by unknown)
_LABEL_261C4_:
    ld      hl, $B2A2
    set     5, (ix+1)
    inc     (ix+28)
    xor     a
    ld      b, a
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 261BE (indexed by unknown)
_LABEL_261D3_:
    ld      b, $10
    call    _LABEL_263A7_
    ret     nc
    inc     (ix+28)
; 3rd entry of Jump Table from 261BE (indexed by unknown)
_LABEL_261DC_:
    ld      a, (ix+11)
    cp      $B8
    jp      nc, _LABEL_263F4_
    ld      de, $0020
    jp      _LABEL_2635E_

; 27th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_261EA_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, $A1F6
    jp      call_jump_table

; Jump Table from 261F6 to 261FB (3 entries, indexed by unknown)
_DATA_261F6_:
.dw _LABEL_261FC_ _LABEL_2620F_ _LABEL_2621D_

; 1st entry of Jump Table from 261F6 (indexed by unknown)
_LABEL_261FC_:
    set     2, (ix+1)
    inc     (ix+28)
    ld      (ix+14), $08
    ld      hl, $B2AE
    xor     a
    ld      b, a
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 261F6 (indexed by unknown)
_LABEL_2620F_:
    ld      a, (ix+11)
    cp      $B8
    ret     c
    inc     (ix+28)
    ld      (ix+15), $FF
    ret

; 3rd entry of Jump Table from 261F6 (indexed by unknown)
_LABEL_2621D_:
    ld      a, (ix+11)
    cp      $A2
    ret     nc
    dec     (ix+28)
    ld      (ix+15), $00
    ret

; 44th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_2622B_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, $A237
    jp      call_jump_table

; Jump Table from 26237 to 2623C (3 entries, indexed by unknown)
_DATA_26237_:
.dw _LABEL_2623D_ _LABEL_26250_ _LABEL_2625C_

; 1st entry of Jump Table from 26237 (indexed by unknown)
_LABEL_2623D_:
    ld      hl, $B2C6
    set     2, (ix+1)
    inc     (ix+28)
    ld      (ix+14), $80
    xor     a
    ld      b, a
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 26237 (indexed by unknown)
_LABEL_26250_:
    ld      a, (ix+11)
    cp      $B8
    ret     c
    inc     (ix+28)
    jp      _LABEL_26329_

; 3rd entry of Jump Table from 26237 (indexed by unknown)
_LABEL_2625C_:
    ld      a, (ix+11)
    cp      $A0
    ret     nc
    dec     (ix+28)
    jp      _LABEL_26329_

; 57th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_26268_:
    call    _LABEL_2659A_
    ld      a, (ix+28)
    ld      hl, $A274
    jp      call_jump_table

; Jump Table from 26274 to 2627B (4 entries, indexed by unknown)
_DATA_26274_:
.dw _LABEL_2627C_ _LABEL_2628D_ _LABEL_2629A_ _LABEL_262D9_

; 1st entry of Jump Table from 26274 (indexed by unknown)
_LABEL_2627C_:
    inc     (ix+28)
    ld      (ix+9), $01
    ld      hl, $B3EF
    ld      a, $01
    ld      b, $04
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 26274 (indexed by unknown)
_LABEL_2628D_:
    ld      a, (ix+23)
    or      a
    ret     z
    ld      (ix+17), $01
    inc     (ix+28)
    ret

; 3rd entry of Jump Table from 26274 (indexed by unknown)
_LABEL_2629A_:
    ld      a, (ix+17)
    or      a
    jr      nz, ++
    ld      a, (ix+9)
    or      a
    jr      nz, +
    dec     a
+:
    ld      (ix+17), a
++:
    ld      a, (ix+9)
    ld      bc, $0A04
    or      a
    jr      z, +
    ld      b, $F6
+:
    call    _LABEL_266F8_
    cp      $02
    jr      z, +
    cp      $07
    jr      z, +
    ld      (ix+28), $03
    ret

+:
    ld      a, (ix+9)
    ld      bc, $F5FC
    or      a
    jr      z, +
    ld      b, $0A
+:
    call    _LABEL_266F8_
    cp      $02
    jp      z, _LABEL_26335_
    ret

; 4th entry of Jump Table from 26274 (indexed by unknown)
_LABEL_262D9_:
    ld      a, (ix+11)
    cp      $B8
    jp      nc, _LABEL_263F4_
    ld      de, $0020
    jp      _LABEL_2635B_

; 58th entry of Jump Table from 2DD1 (indexed by unknown)
_LABEL_262E7_:
    call    _LABEL_26552_
    call    _LABEL_264C1_
    jr      nc, +
    ld      a, $A2
    ld      (Sound_MusicTrigger), a
+:
    ld      a, (ix+28)
    ld      hl, $A2FD
    jp      call_jump_table

; Jump Table from 262FD to 26300 (2 entries, indexed by unknown)
_DATA_262FD_:
.dw _LABEL_26301_ _LABEL_26312_

; 1st entry of Jump Table from 262FD (indexed by unknown)
_LABEL_26301_:
    ld      a, $01
    ld      (ix+23), a
    inc     (ix+28)
    ld      hl, $B40D
    inc     a
    ld      b, $04
    jp      _LABEL_26315_

; 2nd entry of Jump Table from 262FD (indexed by unknown)
_LABEL_26312_:
    ret

_LABEL_26313_:
    xor     a
    ld      b, a
_LABEL_26315_:
    ld      (ix+2), l
    ld      (ix+3), h
_LABEL_2631B_:
    ld      (ix+4), a
    ld      (ix+5), $00
    ld      (ix+6), b
    ld      (ix+7), b
    ret

_LABEL_26329_:
    ld      l, (ix+14)
    ld      h, (ix+15)
    call    _LABEL_263ED_
    jp      _LABEL_263C3_

_LABEL_26335_:
    ld      a, (ix+9)
    xor     $01
    ld      (ix+9), a
_LABEL_2633D_:
    ld      l, (ix+16)
    ld      h, (ix+17)
    call    _LABEL_263ED_
    jp      _LABEL_263CD_

_LABEL_26349_:
    ld      (ix+0), $01
    jr      +

_LABEL_2634F_:
    ld      (ix+0), $6C
+:
    xor     a
    ld      (ix+28), a
    ld      (ix+26), a
    ret

_LABEL_2635B_:
    call    _LABEL_263CA_
_LABEL_2635E_:
    ld      l, (ix+14)
    ld      h, (ix+15)
    add     hl, de
    ld      a, h
    bit     7, a
    jr      nz, +
    cp      $04
    jr      nz, +
    ld      hl, $03F0
+:
    jp      _LABEL_263C3_

_LABEL_26374_:
    call    _LABEL_263CA_
_LABEL_26377_:
    call    _LABEL_263C0_
    ld      h, (ix+11)
    ld      l, (ix+10)
    ld      de, (_RAM_C061_)
    call    _LABEL_263E6_
    or      a
    sbc     hl, de
    ld      a, h
    and     $F8
    ld      h, a
    ld      l, $00
    add     hl, de
    ld      (ix+11), h
    ld      (ix+10), l
    ret

_LABEL_26398_:
    ld      a, (_RAM_C20D_)
    sub     (ix+13)
    ld      a, $01
    jr      nc, +
    dec     a
+:
    ld      (ix+9), a
    ret

_LABEL_263A7_:
    ld      a, (_RAM_C20D_)
    sub     (ix+13)
    jr      nc, +
    neg
+:
    sub     b
    ret

; Data from 263B3 to 263BF (13 bytes)
.db $CD $98 $A3 $B7 $21 $80 $FF $28 $02 $26 $00 $18 $0D

_LABEL_263C0_:
    ld      hl, $0000
_LABEL_263C3_:
    ld      (ix+14), l
    ld      (ix+15), h
    ret

_LABEL_263CA_:
    ld      hl, $0000
_LABEL_263CD_:
    ld      (ix+16), l
    ld      (ix+17), h
    ret

_LABEL_263D4_:
    ld      l, (ix+14)
    ld      h, (ix+15)
    add     hl, de
    jr      _LABEL_263C3_

; Data from 263DD to 263E5 (9 bytes)
.db $DD $6E $10 $DD $66 $11 $19 $18 $E7

_LABEL_263E6_:
    xor     a
    sub     e
    ld      e, a
    sbc     a, a
    sub     d
    ld      d, a
    ret

_LABEL_263ED_:
    xor     a
    sub     l
    ld      l, a
    sbc     a, a
    sub     h
    ld      h, a
    ret

_LABEL_263F4_:
    push    ix
    pop     hl
    ld      bc, $001F
    jr      +

; Data from 263FC to 26411 (22 bytes)
.db $21 $40 $C2 $01 $BF $04 $18 $0E $21 $00 $C3 $01 $E0 $01 $18 $06
.db $21 $40 $DD $01 $0A $00

+:
    ld      e, l
    ld      d, h
    inc     de
    ld      (hl), $00
    ldir
    ret

_LABEL_2641A_:
    ex      af, af'
    ld      a, (_RAM_C0DD_)
    or      a
    ret     nz
    ex      af, af'
    ld      de, CurrentScore
    call    +
    ld      hl, _RAM_C0CE_
    call    c, ++
    jp      +++

+:
    ld      c, a
    ld      hl, $A899
    ld      b, $00
    add     hl, bc
    add     hl, bc
    add     hl, bc
    ld      a, (de)
    add     a, (hl)
    daa
    ld      (de), a
    inc     de
    inc     hl
    ld      a, (de)
    adc     a, (hl)
    daa
    ld      (de), a
    inc     de
    inc     hl
    ld      a, (de)
    adc     a, (hl)
    daa
    ld      (de), a
    ret

++:
    bit     0, (hl)
    ret     nz
    set     0, (hl)
    ld      c, $99
    inc     hl
    inc     hl
    ld      (hl), c
    inc     hl
    ld      (hl), c
    ret

+++:
    ld      hl, _RAM_C0D1_
    ld      de, _RAM_C0D5_
    ld      a, (de)
    sub     (hl)
    dec     hl
    dec     de
    jr      c, ++
    jr      z, +
    ret     nc
+:
    ld      a, (de)
    sub     (hl)
    ret     nc
++:
    ld      a, (hl)
    ld      (de), a
    inc     hl
    inc     de
    ld      a, (hl)
    ld      (de), a
    ret

_LABEL_26470_:
    ld      a, (ix+8)
    add     a, a
    add     a, a
    ld      hl, $A795
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (hl)
    inc     hl
    add     a, (ix+13)
    ret     nc
    cp      c
    ret     nc
    add     a, (hl)
    jr      c, +
    cp      b
    jr      c, +
    inc     hl
    ld      a, (hl)
    add     a, (ix+11)
    ret     nc
    cp      e
    ret     nc
    inc     hl
    add     a, (hl)
    jr      c, +
    cp      d
+:
    ccf
    ret

_LABEL_2649A_:
    ld      a, (_RAM_C212_)
    or      a
    ret     nz
    ld      a, (ix+19)
    or      (ix+18)
    ret     nz
    bit     3, (ix+1)
    ret

_LABEL_264AB_:
    call    _LABEL_2649A_
    ret     nz
    ld      a, (_RAM_C208_)
    bit     7, a
    ret     z
    call    _LABEL_2652B_
    jp      _LABEL_26470_

_LABEL_264BB_:
    call    _LABEL_2649A_
    ret     nz
    jr      +

_LABEL_264C1_:
    call    _LABEL_2649A_
    ret     nz
    ld      a, (_RAM_C208_)
    bit     7, a
    jr      z, +
    call    _LABEL_2652B_
    call    _LABEL_26470_
    ret     c
+:
    ld      a, (_RAM_C229_)
    bit     2, a
    jr      nz, +
    ld      a, (Alex_PowerupState)
    cp      $02
    jr      nz, _LABEL_26529_
+:
    ld      iy, _RAM_C240_
    ld      b, $05
_LABEL_264E7_:
    push    bc
    ld      a, (iy+0)
    cp      $02
    jr      z, +
    cp      $07
    jr      nz, ++
+:
    ld      a, (iy+13)
    sub     $06
    jr      nc, +
    xor     a
+:
    ld      b, a
    add     a, $0C
    jr      nc, +
    ld      a, $FF
+:
    ld      c, a
    ld      a, (iy+11)
    ld      e, a
    sub     $08
    jr      nc, +
    xor     a
+:
    ld      d, a
    call    _LABEL_26470_
    jr      nc, ++
    ld      b, $01
    ld      a, (ix+23)
    or      a
    jr      z, +
    inc     b
+:
    ld      (iy+26), b
    pop     bc
    scf
    ret

++:
    pop     bc
    ld      de, $0020
    add     iy, de
    djnz    _LABEL_264E7_
_LABEL_26529_:
    or      a
    ret

_LABEL_2652B_:
    ld      hl, $A640
    add     a, a
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C209_)
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C20D_)
    add     a, (hl)
    ld      b, a
    inc     hl
    add     a, (hl)
    ld      c, a
    inc     hl
    ld      a, (_RAM_C20B_)
    add     a, (hl)
    ld      d, a
    inc     hl
    add     a, (hl)
    ld      e, a
    ret

_LABEL_26552_:
    call    _LABEL_2649A_
    ret     nz
    ld      a, (_RAM_C227_)
    or      a
    ret     nz
    ld      a, (_RAM_C224_)
    or      a
    ret     nz
    ld      a, (_RAM_C222_)
    bit     1, a
    ret     nz
    ld      a, (_RAM_C229_)
    bit     2, a
    ret     nz
    ld      a, (_RAM_C208_)
    ld      hl, $A5E8
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C20D_)
    add     a, (hl)
    ld      b, a
    inc     hl
    add     a, (hl)
    ld      c, a
    inc     hl
    ld      a, (_RAM_C20B_)
    add     a, (hl)
    ld      d, a
    inc     hl
    add     a, (hl)
    ld      e, a
    call    _LABEL_26470_
    ret     nc
    ld      a, $01
    ld      (_RAM_C224_), a
    ld      a, (ix+13)
    ld      (_RAM_C0C4_), a
    ret

_LABEL_2659A_:
    ld      a, (_RAM_C212_)
    or      a
    ret     nz
    ld      a, (ix+19)
    or      (ix+18)
    ret     nz
    ld      a, (_RAM_C222_)
    bit     1, a
    ret     nz
    xor     a
    ld      hl, $A5E4
    add     a, a
    add     a, a
    add     a, l
    ld      l, a
    jr      nc, +
    inc     h
+:
    ld      a, (_RAM_C20D_)
    add     a, (hl)
    ld      b, a
    inc     hl
    add     a, (hl)
    ld      c, a
    inc     hl
    ld      a, (_RAM_C20B_)
    add     a, (hl)
    ld      d, a
    inc     hl
    add     a, (hl)
    ld      e, a
    call    _LABEL_26470_
    ld      (ix+23), $00
    ret     nc
    ld      a, $01
    bit     2, (ix+1)
    jr      nz, +
    inc     a
+:
    ld      (_RAM_C226_), a
    ld      (_RAM_C0C5_), ix
    ld      (ix+23), a
    ret

; Data from 265E4 to 266F7 (276 bytes)
.db $FA $0C $F8 $08 $F8 $10 $E4 $1C $F8 $10 $E4 $1C $F8 $10 $E4 $1C
.db $F8 $10 $E4 $1C $F8 $10 $FA $14 $F8 $10 $EC $14 $F8 $10 $EC $14
.db $F8 $10 $EC $14 $F8 $10 $00 $16 $F8 $10 $EA $16 $EC $14 $F2 $0C
.db $F0 $10 $F2 $0C $00 $10 $F2 $0C $00 $14 $F2 $0C $00 $00 $00 $00
.db $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00 $00
.db $00 $00 $00 $00 $00 $00 $00 $00 $F8 $10 $E4 $1C $E8 $20 $DC $0C
.db $F8 $20 $DC $0C $E8 $10 $DC $1C $08 $10 $DC $1C $E4 $24 $D4 $14
.db $F8 $24 $D4 $14 $E4 $24 $D4 $24 $F8 $24 $D4 $24 $E4 $18 $02 $14
.db $04 $18 $02 $14 $F2 $14 $F2 $14 $FA $14 $F2 $14 $EA $1C $F2 $14
.db $FA $1C $F2 $14 $E4 $28 $F0 $18 $F4 $28 $F0 $18 $F0 $20 $F6 $20
.db $F0 $20 $F6 $20 $F0 $20 $E6 $20 $F0 $20 $E6 $20 $E4 $20 $F0 $10
.db $E4 $20 $F0 $10 $EC $18 $F0 $10 $EC $18 $F0 $10 $FC $18 $F0 $10
.db $FC $18 $F0 $10 $FC $20 $F0 $10 $FC $20 $F0 $10 $F8 $10 $E8 $20
.db $F8 $10 $E8 $20 $F8 $10 $F0 $20 $F8 $10 $F0 $20 $F8 $10 $F0 $10
.db $F8 $10 $F0 $10 $F8 $10 $E0 $20 $F8 $10 $E0 $20 $E8 $20 $F0 $10
.db $F8 $20 $F0 $10 $F4 $18 $D6 $50 $F4 $18 $D6 $50 $D4 $58 $EC $18
.db $D4 $58 $EC $18 $E8 $10 $E0 $18 $08 $10 $E0 $18 $E8 $20 $F0 $18
.db $F8 $20 $F0 $18

_LABEL_266F8_:
    ld      a, (ix+19)
    or      (ix+18)
    jr      z, +
    xor     a
    ret

+:
    ld      h, (ix+11)
    ld      l, (ix+10)
    ld      d, c
    ld      e, $00
    add     hl, de
    call    _LABEL_26819_
    ex      de, hl
    ld      h, (ix+13)
    ld      l, (ix+12)
    ld      c, $00
    add     hl, bc
    ld      bc, (_RAM_C04F_)
    add     hl, bc
    ld      a, h
    ex      de, hl
    jp      _LABEL_26836_

; Data from 26723 to 26818 (246 bytes)
.db $DD $7E $04 $B7 $28 $3F $DD $7E $00 $B7 $20 $39 $DD $7E $05 $87
.db $87 $21 $6D $A7 $85 $6F $30 $01 $24 $7E $23 $DD $86 $03 $B9 $30
.db $24 $86 $B8 $38 $20 $23 $7E $DD $86 $02 $BB $30 $18 $23 $86 $BA
.db $38 $13 $DD $7E $02 $32 $1B $C2 $DD $7E $03 $32 $1A $C2 $DD $7E
.db $04 $32 $30 $C2 $C9 $AF $32 $30 $C2 $C9 $F0 $20 $F0 $20 $FD $06
.db $F0 $20 $FD $06 $D0 $60 $FD $06 $E8 $30 $FD $06 $FD $06 $E8 $30
.db $FE $04 $F0 $20 $FE $04 $F0 $20 $FB $0A $FA $0C $FA $0C $E0 $40
.db $F8 $10 $FA $0C $E4 $1C $F1 $1E $EE $04 $FC $08 $FC $08 $F4 $18
.db $E4 $18 $F8 $10 $D4 $2C $FC $08 $F2 $0E $FA $0C $FA $0C $FC $08
.db $FC $08 $F8 $10 $F8 $10 $F4 $18 $F4 $18 $F0 $1E $D0 $30 $FA $0C
.db $F0 $10 $F1 $1E $F6 $04 $FA $0C $F2 $0C $F2 $1C $00 $1E $F9 $0E
.db $EE $04 $EA $2C $EE $04 $FE $04 $FE $04 $FC $08 $FC $08 $F2 $1C
.db $F4 $0C $FA $0C $E0 $1E $FA $0C $E0 $20 $EC $28 $E8 $10 $FD $06
.db $F9 $06 $FA $0C $F4 $0C $F2 $1C $F2 $0C $F6 $14 $F4 $0C $F6 $14
.db $E0 $20 $FC $08 $F4 $0A $F6 $14 $EE $04 $FA $0C $E8 $16 $F2 $1C
.db $E8 $16 $FE $04 $E2 $1C

_LABEL_26819_:
    ld      de, (_RAM_C061_)
    add     hl, de
    ld      a, h
    jr      nc, +
    add     a, $20
+:
    cp      $E0
    jr      c, +
    sub     $E0
+:
    and     $F8
    ld      l, a
    ld      h, $00
    add     hl, hl
    add     hl, hl
    add     hl, hl
    ld      de, $D601
    add     hl, de
    ret

_LABEL_26836_:
    and     $F8
    rrca
    rrca
    add     a, l
    ld      l, a
    push    de
    ex      de, hl
    ld      hl, ScreenMapBuffer
    or      a
    sbc     hl, de
    jr      c, +
    ex      de, hl
    ld      hl, $DD00
    or      a
    sbc     hl, de
    ex      de, hl
+:
    ex      de, hl
    pop     de
    ld      a, (hl)
    rlca
    rlca
    rlca
    and     $07
    ret

_LABEL_26857_:
    push    hl
    ld      hl, (_RAM_C043_)
    ld      a, h
    rrca
    rrca
    xor     h
    rrca
    xor     l
    rrca
    rrca
    rrca
    rrca
    xor     l
    rrca
    adc     hl, hl
    jr      nz, +
    ld      hl, $733C
+:
    ld      a, r
    xor     l
    ld      (_RAM_C043_), hl
    pop     hl
    ret

_LABEL_26876_:
    ld      a, (_RAM_DD94_)
    or      a
    jr      nz, +
    ld      a, $01
    ld      (_RAM_DD37_), a
+:
    xor     a
    ld      (_RAM_DD94_), a
    jp      _LABEL_2634F_

; Data from 26888 to 268B9 (50 bytes)
.db $87 $87 $85 $6F $30 $01 $24 $7E $23 $56 $5F $23 $7E $23 $66 $6F
.db $C9 $00 $00 $00 $00 $01 $00 $00 $03 $00 $00 $05 $00 $00 $20 $00
.db $00 $40 $00 $00 $60 $00 $00 $80 $00 $00 $00 $01 $00 $20 $01 $00
.db $00 $02

_LABEL_268BA_:
    ld      bc, $0000
    call    _LABEL_266F8_
    cp      $02
    jr      z, +
    ld      de, $0030
    call    _LABEL_263D4_
    bit     7, h
    ret     nz
    ld      de, $03F0
    or      a
    sbc     hl, de
    ret     c
    ex      de, hl
    jp      _LABEL_263C3_

+:
    ld      (ix+28), $00
    jp      _LABEL_26374_

; Data from 268DF to 27FFF (5921 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_268df.inc"

.BANK 10
.ORG $0000

PAL_ScoreCardPalettes:                                                  ; $8000
PAL_ScoreCard_Stage1:
.incbin "palettes/scorecard_stage1.bin"

PAL_ScoreCard_Stage2:
.incbin "palettes/scorecard_stage2.bin"

.incbin "palettes/scorecard_stage_unknown.bin"

PAL_ScoreCard_Stage3:
.incbin "palettes/scorecard_stage3.bin"
PAL_ScoreCard_Stage4:
.incbin "palettes/scorecard_stage4.bin"

; Data from 280A0 to 280BF (32 bytes)
_DATA_280A0_:
.db $31 $3F $0B $00 $00 $29 $0F $06 $3C $08 $04 $03 $03 $03 $03 $03
.db $00 $0F $07 $00 $1B $05 $3F $38 $30 $20 $00 $2A $23 $12 $15 $00

; the score card fonts, character art, map & icons.
GFX_ScoreCard_Map_Tiles:                                        ; $A0C0
.incbin "graphics/scorecard_and_map_tiles.rle.bin"


; Data from 2A32C to 2A351 (38 bytes)
_DATA_2A32C_:
.db $88 $30 $20 $C0 $C0 $E0 $F0 $F8 $7C $00 $88 $0F $1F $BF $7F $3F
.db $1F $8F $C7 $00 $03 $00 $85 $80 $40 $A0 $50 $28 $00 $03 $7F $85
.db $3F $1F $0F $07 $83 $00


GFX_ScoreCard_Stage2_Icon:                                          ; $A352
.incbin "graphics/scorecard_stage2_icon.rle.bin"


; Data from 2A577 to 2A637 (193 bytes)
GFX_ScoreCard_Alex:                                                 ; $A577
.incbin "graphics/scorecard_alex.rle.bin"


; Data from 2A638 to 2A645 (14 bytes)
_DATA_2A638_:
.db $1D $00 $13 $00 $14 $00 $27 $00 $26 $00 $21 $00 $00 $00

; Data from 2A646 to 2A64D (8 bytes)
_DATA_2A646_:
.db $54 $00 $55 $00 $5C $00 $5D $00

; Data from 2A64E to 2A6F9 (172 bytes)
_DATA_2A64E_:
.db $44 $00 $45 $00 $52 $00 $53 $00 $60 $A6 $68 $A6 $70 $A6 $78 $A6
.db $80 $A6 $40 $08 $41 $08 $4E $08 $4F $08 $42 $08 $43 $08 $50 $08
.db $51 $08 $44 $08 $45 $08 $52 $08 $53 $08 $54 $08 $55 $08 $5C $08
.db $5D $08 $56 $08 $57 $08 $5E $08 $5F $08 

DATA_ScoreNumberMappings:                                           ; $A688
.dw DATA_ScoreNumberCharCodes
.dw DATA_ScoreNumberCharCodes + 2
.dw DATA_ScoreNumberCharCodes + 4
.dw DATA_ScoreNumberCharCodes + 6
.dw DATA_ScoreNumberCharCodes + 8
.dw DATA_ScoreNumberCharCodes + 10
.dw DATA_ScoreNumberCharCodes + 12
.dw DATA_ScoreNumberCharCodes + 14
.dw DATA_ScoreNumberCharCodes + 16
.dw DATA_ScoreNumberCharCodes + 18
.dw DATA_ScoreNumberCharCodes + 20

DATA_ScoreNumberCharCodes:
.db $2D $00 
.db $2E $00
.db $2F $00
.db $30 $00
.db $31 $00
.db $32 $00
.db $33 $00
.db $34 $00
.db $35 $00
.db $36 $00
.db $00 $00 


DATA_LifeNumberMappings:                                            ; $A6B4
.dw DATA_LifeNumberCharCodes
.dw DATA_LifeNumberCharCodes + 4
.dw DATA_LifeNumberCharCodes + 8
.dw DATA_LifeNumberCharCodes + 12
.dw DATA_LifeNumberCharCodes + 16
.dw DATA_LifeNumberCharCodes + 20
.dw DATA_LifeNumberCharCodes + 24
.dw DATA_LifeNumberCharCodes + 28
.dw DATA_LifeNumberCharCodes + 32
.dw DATA_LifeNumberCharCodes + 36
.dw DATA_LifeNumberCharCodes + 40

DATA_LifeNumberCharCodes                                            ; $A6CA
.db $68 $00 $74 $00
.db $69 $00 $75 $00
.db $6A $00 $76 $00
.db $6B $00 $6B $04
.db $6C $00 $77 $00
.db $6D $00 $78 $00
.db $6E $00 $71 $04
.db $6F $00 $79 $00
.db $70 $00 $70 $04
.db $71 $00 $6E $06
.db $00 $00 $00 $00

_DATA_2A6F6_:
.db $60 $00 $61 $00

; Data from 2A6FA to 2A845 (332 bytes)
_DATA_2A6FA_:
.db $63 $00 $00 $00 $00 $00 $00 $00 $00 $00 $63 $00 $1C $01 $71 $01
.db $72 $01 $73 $01 $74 $01 $75 $01 $76 $01 $77 $01 $78 $01 $79 $01
.db $7A $01 $7B $01 $7C $01 $7D $01 $7E $01 $7F $01 $1C $01 $1C $01
.db $1C $01 $1C $01 $1C $01 $1C $01 $1C $01 $1C $01 $1C $01 $1C $01
.db $1C $01 $1C $01 $1C $01 $1C $01 $1C $01 $1C $01 $1C $01 $80 $01
.db $81 $01 $12 $00 $1C $01 $82 $01 $5A $01 $5B $01 $83 $01 $84 $01
.db $85 $01 $86 $01 $87 $01 $88 $01 $89 $01 $8A $01 $1C $01 $1C $01
.db $55 $01 $12 $00 $1C $01 $1C $01 $5A $01 $5B $01 $1C $01 $1C $01
.db $62 $01 $12 $00 $65 $01 $60 $01 $61 $01 $12 $00 $8B $01 $8C $01
.db $8D $01 $8E $01 $8F $01 $90 $01 $91 $01 $92 $01 $93 $01 $94 $01
.db $95 $01 $96 $01 $97 $01 $98 $01 $99 $01 $9A $01 $8E $01 $8E $01
.db $8E $01 $8E $01 $8E $01 $8E $01 $8E $01 $8E $01 $9B $01 $8E $01
.db $8E $01 $8E $01 $9C $01 $9D $01 $9E $01 $9F $01 $98 $01 $99 $01
.db $9A $01 $1C $01 $9B $01 $9C $01 $9D $01 $9E $01 $9F $01 $A0 $01
.db $A1 $01 $A2 $01 $A3 $01 $A4 $01 $12 $00 $12 $00 $1C $01 $1C $01
.db $1C $01 $1C $01 $1C $01 $1C $01 $5F $01 $60 $01 $6C $01 $60 $01
.db $61 $01 $12 $00 $12 $00 $12 $00 $12 $00 $12 $00 $12 $00 $A5 $01
.db $A6 $01 $A7 $01 $A8 $01 $A9 $01 $AA $01 $AB $01 $AC $01 $AD $01
.db $AE $01 $AF $01 $B0 $01 $B1 $01 $B2 $01 $B3 $01 $12 $00 $12 $00
.db $12 $00 $12 $00 $12 $00 $12 $00 $12 $00 $12 $00 $2A $01 $2B $01
.db $2C $01 $2D $01 $1C $01 $1C $01 $1C $01 $1C $01


; This is the Alex Kidd sprite that is drawn on the stage title cards.
; It's drawn over the top of the default Shinobi Kid sprite. Not sure
; why they did this instead of editing the title card mappings directly.
SCR_TitleCardAlex:                                                  ; $A846
.dw $01B5 $01B6
.dw $01B7 $01B8
.dw $01B9 $01BA
.dw $01BB $01BC

; Data from 2A856 to 2A86F (26 bytes)
_DATA_2A856_:
.db $15 $00 $1E $00 $17 $00 $13 $00 $24 $00 $00 $00 $00 $00 $00 $00
.db $14 $00 $21 $00 $20 $00 $27 $00 $25 $00

; Data from 2A870 to 2A889 (26 bytes)
_DATA_2A870_:
.db $22 $00 $17 $00 $24 $00 $18 $00 $17 $00 $15 $00 $26 $00 $00 $00
.db $14 $00 $21 $00 $20 $00 $27 $00 $25 $00


_DATA_2A88A_:
.incbin "unknown_2A88A.bin"

SCR_TitleCard0:                                                     ; $A8A4
.incbin "screenmap/title_card_0.rle.map"

SCR_TitleCard1:                                                     ; $A98C
.incbin "screenmap/title_card_1.rle.map"

SCR_TitleCard_unknown:                                              ; $AA6C
.incbin "screenmap/title_card_unknown.rle.map"

SCR_TitleCard2:                                                     ; $AB51
.incbin "screenmap/title_card_2.rle.map"

SCR_TitleCard3:                                                     ; $AC38
.incbin "screenmap/title_card_3.rle.map"

; Data from 2AD1E to 2AFCD (688 bytes)
_DATA_2AD1E_:
.db $2A $C9 $8A $38 $39 $3A $3B $3C $3C $3D $3E $3F $3B $16 $C9 $8A
.db $46 $47 $48 $49 $4A $49 $4B $4C $4D $49 $2E $C9 $08 $1C $81 $1D
.db $05 $00 $81 $1E $03 $00 $84 $1E $00 $00 $1E $04 $00 $82 $1E $00
.db $04 $C9 $07 $1C $84 $1F $20 $00 $1E $06 $00 $03 $1E $03 $00 $81
.db $1E $04 $00 $04 $C9 $07 $1C $89 $21 $00 $00 $1E $00 $1E $00 $00
.db $1E $04 $00 $81 $1E $07 $00 $04 $C9 $07 $1C $85 $22 $00 $1E $00
.db $1E $06 $00 $81 $1E $08 $00 $81 $1E $04 $C9 $07 $1C $82 $23 $1E
.db $07 $00 $8C $24 $25 $26 $27 $28 $29 $2A $2B $2C $2D $2E $00 $04
.db $C9 $06 $1C $85 $2F $30 $00 $1E $1E $03 $00 $83 $31 $29 $32 $09
.db $1C $82 $33 $00 $04 $C9 $87 $1C $34 $35 $36 $37 $1C $38 $06 $00
.db $82 $1E $39 $09 $1C $84 $57 $58 $3C $1E $04 $C9 $88 $1C $3D $3D
.db $37 $3E $37 $3F $1E $03 $00 $84 $1E $00 $00 $40 $08 $1C $85 $41
.db $5D $5E $44 $00 $04 $C9 $04 $1C $84 $37 $45 $46 $47 $05 $48 $82
.db $49 $AF $07 $1C $02 $41 $84 $1C $4E $4F $00 $04 $C9 $06 $1C $82
.db $50 $51 $05 $00 $85 $52 $41 $41 $4C $4D $03 $1C $02 $41 $02 $1C
.db $83 $54 $00 $00 $04 $C9 $06 $1C $84 $55 $00 $00 $1E $03 $00 $85
.db $56 $1C $41 $4C $53 $03 $4B $81 $41 $03 $1C $83 $59 $00 $00 $04
.db $C9 $06 $1C $82 $5A $5B $04 $00 $82 $1E $5C $09 $1C $85 $5F $60
.db $61 $00 $00 $04 $C9 $06 $1C $81 $62 $06 $00 $81 $63 $09 $1C $85
.db $64 $00 $00 $1E $00 $04 $C9 $04 $1C $83 $65 $60 $61 $06 $00 $83
.db $66 $67 $32 $05 $1C $87 $5F $60 $61 $00 $00 $1E $00 $04 $C9 $04
.db $1C $81 $68 $05 $00 $81 $1E $04 $00 $87 $69 $6A $65 $6B $6C $60
.db $61 $03 $00 $83 $1E $00 $00 $04 $C9 $03 $1C $85 $6D $6E $00 $00
.db $1E $07 $00 $83 $1E $6F $24 $06 $00 $84 $1E $00 $1E $1E $04 $C9
.db $03 $1C $87 $70 $00 $00 $1E $00 $1E $1E $04 $00 $84 $1E $00 $1E
.db $1E $04 $00 $81 $1E $03 $00 $82 $1E $00 $7F $C9 $63 $C9 $00 $2A
.db $00 $0A $08 $16 $00 $0A $08 $2E $00 $09 $01 $05 $00 $81 $01 $03
.db $00 $84 $01 $00 $00 $01 $04 $00 $81 $01 $05 $00 $09 $01 $82 $00
.db $01 $06 $00 $03 $01 $03 $00 $81 $01 $08 $00 $08 $01 $02 $00 $86
.db $01 $00 $01 $00 $00 $01 $04 $00 $81 $01 $0B $00 $08 $01 $84 $00
.db $01 $00 $01 $06 $00 $81 $01 $08 $00 $81 $01 $04 $00 $09 $01 $07
.db $00 $0B $01 $05 $00 $08 $01 $83 $00 $01 $01 $03 $00 $0D $01 $05
.db $00 $07 $01 $06 $00 $0F $01 $04 $00 $02 $01 $82 $03 $07 $04 $01
.db $03 $00 $83 $01 $00 $00 $0D $01 $05 $00 $04 $01 $81 $07 $12 $01
.db $81 $07 $03 $01 $05 $00 $08 $01 $05 $00 $83 $01 $05 $03 $06 $01
.db $81 $07 $03 $01 $06 $00 $07 $01 $02 $00 $81 $01 $03 $00 $02 $01
.db $02 $05 $04 $01 $81 $07 $04 $01 $06 $00 $08 $01 $04 $00 $0E $01
.db $06 $00 $07 $01 $06 $00 $0B $01 $02 $00 $81 $01 $05 $00 $07 $01
.db $06 $00 $02 $01 $81 $05 $08 $01 $02 $00 $81 $01 $05 $00 $05 $01
.db $05 $00 $81 $01 $04 $00 $07 $01 $03 $00 $81 $01 $06 $00 $05 $01
.db $02 $00 $81 $01 $07 $00 $02 $01 $81 $07 $06 $00 $84 $01 $00 $01
.db $01 $04 $00 $04 $01 $02 $00 $84 $01 $00 $01 $01 $04 $00 $84 $01
.db $00 $01 $01 $04 $00 $81 $01 $03 $00 $81 $01 $7F $00 $64 $00 $00


SCR_GameOver:                                                       ; $AFCE
.incbin "screenmap/gameover.map"

GFX_Stage4_2:                                                       ; $B06F
.incbin "graphics/stage4_2.rle.bin"


.BANK 11
.ORG $0000

GFX_Stage4_Boss_Empty:                                              ; $8000
.incbin "graphics/stage4_boss_empty.rle.bin"

GFX_Stage4_Boss_DarkNinja:                                          ; $800C
.incbin "graphics/stage4_boss_dark_ninja.rle.bin"


GFX_Stage3_2_Enemies1:                                              ; $95EE
.incbin "graphics/stage3_2_enemies1.rle.bin"

GFX_Stage3_2_Enemies2:                                              ; $95FA
.incbin "graphics/stage3_2_enemies2.rle.bin"


.incbin "unknown_B11_9CC8.bin"

UNK_Stage3_2_mappings:                                              ; $9D34
.incbin "stage3_2_mappings.bin"

GFX_Stage4_1_Enemies1:                                              ; $9E4E
.incbin "graphics/stage4_1_enemies1.rle.bin"

GFX_Stage4_1_Enemies2:                                              ; $A477
.incbin "graphics/stage4_1_enemies2.rle.bin"

GFX_Stage4_1:                                                       ; $A686
.incbin "graphics/stage4_1.rle.bin"

UNK_Stage4_1_mappings:                                              ; $B855
.incbin "stage4_1_mappings.bin"


.incbin "unknown_B11_BA4D.bin"


.BANK 12
.ORG $0000

PAL_Stage1_Boss_BG:                                                 ; $8000
.incbin "palettes/stage1_boss_bg.bin"

PAL_Stage1_Boss_FG:                                                 ; $8010
.incbin "palettes/stage1_boss_fg.bin"

GFX_Stage1_Boss:                                                    ; $8020
.incbin "graphics/stage1_boss.rle.bin"

UNK_Stage1_Boss_mappings:                                           ; $8B1B
.incbin "stage1_boss_mappings.rle.bin"


_DATA_B12_8D41_:
.incbin "unknown_B12_8D41.bin"


GFX_Stage1_Boss_Kabuto:                                             ; $8D41
.incbin "graphics/stage1_boss_kabuto.rle.bin"

GFX_Stage1_Boss_Aux:                                                ; $95D5
.incbin "graphics/stage1_boss_aux.rle.bin"


PAL_Stage2_Boss_BG:                                                 ; $965B
.incbin "palettes/stage2_boss_bg.bin"

PAL_Stage2_Boss_FG:
.incbin "palettes/stage2_boss_fg.bin"                               ; $966B

GFX_Stage2_Boss_HeliCarrier:                                        ; $967B
.incbin "graphics/stage2_boss_helicarrier.rle.bin"

UNK_Stage2_Boss_mappings:                                           ; $97C3
.incbin "stage2_boss_mappings.rle.bin"

; probably mappings for the stage 2 boss background tiles, given
; proximity to the graphics tiles above and the fact that the
; mapping data is uncompressed
_DATA_B21_9C90_:                                                    ; $9C90
.incbin "unknown_B21_9C90.bin"

GFX_Stage2_Boss_SmallHeli:                                          ; $AC42
.incbin "graphics/stage2_boss_small_heli.rle.bin"


PAL_Stage3_Boss_BG:                                                 ; $ACF7
.incbin "palettes/stage3_boss_bg.bin"

PAL_Stage3_Boss_FG:                                                 ; $AD07
.incbin "palettes/stage3_boss_fg.bin"

GFX_Stage3_Boss:                                                    ; $AD17
.incbin "graphics/stage3_boss.rle.bin"

UNK_Stage3_Boss_mappings:                                           ; $B13C
.incbin "stage3_boss_mappings.rle.bin"

_DATA_B12_B246_:                                                    ; $B246
.incbin "unknown_B12_B246.bin"

GFX_Stage3_Boss_Robster:                                            ; $B2EB
.incbin "graphics/stage3_boss_robster.rle.bin"


PAL_Stage4_Boss_BG:                                                 ; $BA6B
.incbin "palettes/stage4_boss_bg.bin"

PAL_Stage4_Boss_FG:                                                 ; $BA7B
.incbin "palettes/stage4_boss_fg.bin"

GFX_Stage4_Boss:                                                    ; $BA8B
.incbin "graphics/stage4_boss.rle.bin"


; stamped over the boss graphic on the score card after
; the boss has been defeated.
GFX_Finish_Kanji:                                                   ; $BE9F
.incbin "graphics/kanji_finish.rle.bin"


.BANK 13
.ORG $0000

; Data from 34000 to 35F7A (8059 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_34000.inc"

_LABEL_35F7B_:
    ld      hl, _RAM_DD40_
    ld      b, $25
    call    _LABEL_3658E_
    ld      hl, _RAM_D631_
    exx
    ld      hl, _RAM_CB56_
    ld      de, $7830
    ld      bc, $9DCB
    ld      a, $4A
    jp      _LABEL_365C9_

; Data from 35F95 to 3658D (1529 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_35f95.inc"

_LABEL_3658E_:
    ld      a, (hl)
    or      a
    jr      z, +
    pop     hl
    jp      _LABEL_13_

+:
    inc     a
    ld      (hl), a
    ld      a, $AB
    ld      (Sound_MusicTrigger), a
    ld      a, b
    cp      $25
    jr      nz, +
    ld      a, (Alex_Health)
    cp      $06
    jr      c, +
    inc     b
+:
    ld      a, (_RAM_C480_)
    or      a
    ld      ix, _RAM_C480_
    jr      z, +
    ld      ix, _RAM_C4A0_
+:
    ld      a, b
    ld      (ix+0), a
    ld      a, (_RAM_C21B_)
    ld      (ix+11), a
    ld      a, (_RAM_C21A_)
    ld      (ix+13), a
    ret

_LABEL_365C9_:
    ld      (hl), a
    ld      (_RAM_DD0C_), de
    ld      (_RAM_DD0E_), bc
    exx
    ld      b, $01
    ld      (hl), b
    inc     l
    inc     l
    ld      (hl), b
    ld      a, $3E
    add     a, l
    ld      l, a
    ld      (hl), b
    inc     l
    inc     l
    ld      (hl), b
    jp      _LABEL_13_

; Data from 365E4 to 37FFF (6684 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_365e4.inc"

GFX_Stage3_1:                                                       ; $A771
.incbin "graphics/stage3_1.rle.bin"

UNK_Stage3_1_mappings:                                              ; $B51E
.incbin "stage3_1_mappings.bin"

UNK_Stage4_2_mappings:                                              ; $B670
.incbin "stage4_2_mappings.bin"


.BANK 14
.ORG $0000

; Data from 38000 to 380D7 (216 bytes)
DATA_StagePalettePointers:                                          ; $8000
.dw DATA_Stage1Palettes
.dw DATA_Stage2Palettes
.dw DATA_Stage3Palettes
.dw DATA_Stage4Palettes

DATA_Stage1Palettes:                                                ; $8008
.dw PAL_Stage1_0
.dw PAL_Stage1_0

DATA_Stage2Palettes:                                                ; $800C
.dw PAL_Stage2_0
.dw PAL_Stage2_0

DATA_Stage3Palettes:                                                ; $8010
.dw PAL_Stage3_0
.dw PAL_Stage3_1

DATA_Stage4Palettes:                                                ; $8014
.dw PAL_Stage4_0
.dw PAL_Stage4_1

PAL_Stage1_0:                                                       ; $8018
.incbin "palettes/stage1_0_bg.bin"
.incbin "palettes/stage1_0_fg.bin"

PAL_Stage2_0:
.incbin "palettes/stage2_0_bg.bin"
.incbin "palettes/stage2_0_fg.bin"

PAL_Stage3_0:
.incbin "palettes/stage3_0_bg.bin"
.incbin "palettes/stage3_0_fg.bin"

PAL_Stage3_1:
.incbin "palettes/stage3_1_bg.bin"
.incbin "palettes/stage3_1_fg.bin"

PAL_Stage4_0:
.incbin "palettes/stage4_0_bg.bin"
.incbin "palettes/stage4_0_fg.bin"

PAL_Stage4_1:
.incbin "palettes/stage4_1_bg.bin"
.incbin "palettes/stage4_1_fg.bin"


GFX_Powerup_and_HUD:                                                ; $80D8
.incbin "graphics/powerups_and_hud.rle.bin"

GFX_Stage1_Enemies1:                                               ; $8513
.incbin "graphics/stage1_enemies1.rle.bin"

GFX_Stage1_Enemies2:
.incbin "graphics/stage1_enemies2.rle.bin"

GFX_Stage2_Enemies1:                                                ; $8FB4
.incbin "graphics/stage2_enemies1.rle.bin"

GFX_Stage2_Enemies2:                                                ; $96B1
.incbin "graphics/stage2_enemies2.rle.bin"

_DATA_398FA_:
.incbin "unknown_398FA.bin"

_DATA_3991C_:
.incbin "graphics/font2.rle.bin"

_DATA_39A52_:
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_380d8.inc"

GFX_Stage2_1_Tiles:                                                 ; $9C91
.incbin "graphics/stage2_1.rle.bin"

UNK_Stage2_1_mappings:                                              ; $A814
.incbin "stage2_1_mappings.bin"

GFX_Stage2_2_Tiles:                                                 ; $A9AA
.incbin "graphics/stage2_2.rle.bin"

UNK_Stage2_2_mappings:                                              ; $B568
.incbin "stage2_2_mappings.bin"

GFX_Stage3_1_Enemies1:                                              ; $B665
.incbin "graphics/stage3_1_enemies1.rle.bin"

GFX_Stage3_1_Enemies2:                                              ; $BB29
.incbin "graphics/stage3_1_enemies2.rle.bin"

.BANK 15
.ORG $0000

; Data from 3C000 to 3F013 (12308 bytes)
.incbin "Alex_Kidd_in_Shinobi_World_(UE)_[!]_3c000.inc"

GFX_Stage1_1_Tiles:                                                 ; $A01D
.incbin "graphics/stage1_1.rle.bin"

UNK_Stage1_1_mappings:                                              ; $AD69
.incbin "unknown_B15_2D69.bin"


GFX_ScoreCard_HideMario1:                                           ; $B014
.incbin "graphics/scorecard_hide_mario1.rle.bin"

GFX_ScoreCard_HideMario2:                                           ; $B056
.incbin "graphics/scorecard_hide_mario2.rle.bin"

