@ECHO OFF

SETLOCAL

SET obj_file=shinobi_world.o
SET sym_file=shinobi_world.sym
SET sms_file=shinobi_world.sms

IF EXIST %obj_file% DEL %obj_file%
IF EXIST %sym_file% DEL %sym_file%
IF EXIST %sms_file% DEL %sms_file%

ECHO Assembling...
wla_dx_binaries_latest\wla-z80 -vo shinobi_world.asm %obj_file% > build_report.txt

IF %ERRORLEVEL% NEQ 0 GOTO assemble_fail
IF NOT EXIST %obj_file% GOTO assemble_fail

ECHO Linking...
wla_dx_binaries_latest\wlalink -rs link.txt %sms_file%
IF %ERRORLEVEL% NEQ 0 GOTO link_fail

ECHO ==========================
ECHO Build Success.
ECHO ==========================

REM Use fcomp to compare with original ROM
REM ECHO Comparing with original:
fc /b %sms_file% "Alex_Kidd_in_Shinobi_World_(UE)_[!].sms" > compare.txt
REM fcomp s2.sms "Sonic the Hedgehog 2 (UE) (V2.2).sms" > compare.txt

GOTO end

:assemble_fail
ECHO Error while assembling.
GOTO fail
:link_fail
ECHO Error while linking.
:fail

ECHO ==========================
ECHO Build failure.
ECHO ==========================

:end
